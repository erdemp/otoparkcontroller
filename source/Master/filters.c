#include "filters.h"

uint16_t timeDomStbCnt=0;

void timerFilter_CallBack(void const *arg);
osTimerDef(timerFilter, timerFilter_CallBack);
osTimerId timer_FilterID = NULL;
#define TIMER_FILTER_CALLBACK_PERIOD	10


/*******************************************************************************
* @brief				Bu fonksiyon median filte uygular
* @param[in]    medianPtr: median buffer'inin adresi
*								newVal:	buffer'a yeni gelen deger
* @return				median sonucu
*******************************************************************************/
uint32_t medianFilter_32bit(medianBuffer_32bit* medianPtr, uint32_t newVal){
	uint32_t i,k;
	uint8_t bit, enKucukBit;
	uint32_t medianBits=0x0000;
	uint32_t enKucuk=0;

	if(medianPtr->firstMedian){
		for(i=0;i<medianPtr->bufferLength;i++){
			medianPtr->buffer[i]=newVal;
		}
		medianPtr->firstMedian=FALSE;
		medianPtr->index=1;
		return newVal;
	}

	medianPtr->buffer[medianPtr->index]=newVal;
	medianPtr->index++;
	if(medianPtr->index >= medianPtr->bufferLength){
		medianPtr->index=0;		
	}
	
	for(i=0; i<((medianPtr->bufferLength+1)/2); i++){
		bit=0;
		while(medianBits&(0x01<<bit)){	// ilk de�er bul
			bit++;
		}
		enKucuk=medianPtr->buffer[bit];
		enKucukBit=bit;
		bit=0;
		for(k=0;k<medianPtr->bufferLength;k++){
			if(medianBits&(0x01<<bit)){	// daha �nce bak�lm�� m�?
				bit++;
			}else{
				if(medianPtr->buffer[bit]<enKucuk){
					enKucuk=medianPtr->buffer[bit];	// bu de�eri en k���k olarak kabul et.
					enKucukBit=bit;
				}
				bit++;
			}
		}
		medianBits|=(0x01<<enKucukBit);
	}
	return enKucuk;
}

/*******************************************************************************
* @brief				Bu fonksiyon ortalama filte uygular
* @param[in]    ortalamaPtr: average buffer'inin adresi
*								newVal:	buffer'a yeni gelen deger
* @return				ortalama sonucu
*******************************************************************************/
uint32_t averageFilter_32bit(averageBuffer_32bit* ortalamaPtr, uint32_t newVal){
	uint32_t total=0;
	uint32_t i,k;

	ortalamaPtr->buffer[ortalamaPtr->index]=newVal;

	ortalamaPtr->index++;
	if(ortalamaPtr->index >= ortalamaPtr->bufferLength){
		ortalamaPtr->index=0;
	}
	
	if(ortalamaPtr->counter < ortalamaPtr->bufferLength){
		ortalamaPtr->counter++;
	}	

	k=ortalamaPtr->index;
	if(k==0){
		k=ortalamaPtr->bufferLength;
	}
	k--;

	for(i=0; i<(ortalamaPtr->counter); i++){
		total+=ortalamaPtr->buffer[k];
		if(k==0){
			k=ortalamaPtr->bufferLength;
		}
		k--;
	}
	
	i=total/ortalamaPtr->counter;
	k=total%ortalamaPtr->counter;
	if((k*2)>=ortalamaPtr->counter){
		i++;
	}
	return i;
}




/*******************************************************************************
* @brief				Bu fonksiyon ortalama filte uygular
* @param[in]    ortalamaPtr: average buffer'inin adresi
*								newVal:	buffer'a yeni gelen deger
* @return				ortalama sonucu
*******************************************************************************/
double timeDomainStabilization(double newVal, uint16_t timeVal, bool equilibrium, bool initializeFilter){
	static double firstVal=0;
	
	
	if(initializeFilter){
		firstVal=newVal;
		timeDomStbCnt=timeVal;
		
		if(timer_FilterID==NULL){
			timer_FilterID= osTimerCreate(osTimer(timerFilter), osTimerPeriodic, NULL);
			osTimerStart(timer_FilterID, TIMER_FILTER_CALLBACK_PERIOD);
		}
		
		return firstVal;
	}
	
	if(equilibrium==false){
		timeDomStbCnt=timeVal;
	}
		
	if(timeDomStbCnt==0){
		firstVal=newVal;
		timeDomStbCnt=timeVal;
	}
	return firstVal;
}

/*******************************************************************************
* @brief				Bu fonksiyon ADS1232 isleri icin timer callback fonksiyonudur
* @param[in]    arg
* @return				None
*******************************************************************************/
void timerFilter_CallBack(void const *arg){
	if(timeDomStbCnt>=TIMER_FILTER_CALLBACK_PERIOD){
		timeDomStbCnt-=TIMER_FILTER_CALLBACK_PERIOD;
	}else{
		timeDomStbCnt=0;
	}
}


