
#ifndef DISPLAYDRIVER_H_
#define DISPLAYDRIVER_H_



#include "cmsis_os.h"
#include "chip.h"
#include "modbus.h"

#ifdef __cplusplus
extern "C"
{
#endif

		typedef enum{
			ZERO_CHAR_FULL=0x80,
			ZERO_CHAR_ARROW=0x43,
			ZERO_CHAR_CROSS=0x41
	} displayZeroChar_Type;

	typedef enum
	{
			OR_NORMAL=0x01,
			OR_INVERTED=0x00
	} dispayOrientation_Type;
	

	
	bool displayHabAyarDegistir(LPC_USART_T *port, uint8_t adres, haberlesmeAyarType data);
	bool displayAdresDegistir(LPC_USART_T *port, uint8_t adres, uint8_t yeniAdres);
	
	bool displayBaudrateDegistir(LPC_USART_T *port, uint8_t adres, uint32_t baudrate);
	bool displayReset(LPC_USART_T *port, uint8_t adres);
	bool displayKilitAc(LPC_USART_T *port, uint8_t adres, bool enable);
	bool displayComTimeoutGir(LPC_USART_T *port, uint8_t adres, uint16_t comTimeout);
	bool displayZeroCharDegistir(LPC_USART_T *port, uint8_t adres, displayZeroChar_Type zeroChar);
	bool displayYonDegistir(LPC_USART_T *port, uint8_t adres, dispayOrientation_Type orientation);
	bool displayAdresGoster(LPC_USART_T *port, uint8_t adres, bool enable);
	bool displayMaxValGir(LPC_USART_T *port, uint8_t adres, uint16_t maxVal);
	bool displaySayiGoster(LPC_USART_T *port, uint8_t adres, uint16_t val);
	bool displaySayiOku(LPC_USART_T *port, uint8_t adres, uint16_t *val);
	bool displayDigitSayisiGir(LPC_USART_T *port, uint8_t adres, uint16_t digitNumber);
	bool displayComTimeoutOku(LPC_USART_T *port, uint8_t adres, uint16_t *comTimeout);
	bool displayParlaklikDegistir(LPC_USART_T *port, uint8_t adres, uint16_t brigthnessVal);
	bool displayParlaklikOku(LPC_USART_T *port, uint8_t adres, uint16_t *brigthnessVal);
	bool displayComTimeoutOku(LPC_USART_T *port, uint8_t adres, uint16_t *comTimeout);
	bool displayZeroCharOku(LPC_USART_T *port, uint8_t adres, displayZeroChar_Type *zeroChar);
	bool displayYonOku(LPC_USART_T *port, uint8_t adres, dispayOrientation_Type *orientation);
	bool displayOkYonuDegistir(LPC_USART_T *port, uint8_t adres, uint8_t okYonu);
	bool displayOkYonuOku(LPC_USART_T *port, uint8_t adres, uint16_t *okYonu);
	


#ifdef __cplusplus
}
#endif

#endif /* DISPLAYDRIVER_H_ */
