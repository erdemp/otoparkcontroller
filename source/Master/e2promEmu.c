
#include "e2promEmu.h"


uint8_t e2PromData[E2PROM_USER_SIZE];


uint8_t *freeE2PromPtr;
uint8_t *lastE2PromPtr;

void getE2PromPointers(void);
Bool eraseE2Prom(void);
Bool writeE2PromWithOffset(uint32_t ofset);
Bool dataVerifyE2Prom(void);

/***********************************************************************************************************************
* Function Name: getE2PromPointers
* Description  : 
* Arguments    : 
* Return Value : 
***********************************************************************************************************************/
void getE2PromPointers(void){
	uint8_t *ptr;
	uint32_t i;
	
	ptr=(uint8_t *)(E2PROM_START_ADDRESS + E2PROM_PAGE_SIZE -1);

	for(i=0; i<E2PROM_PAGE_SIZE; i++){
		if((*ptr)!=0xFF){
			freeE2PromPtr=ptr;
			freeE2PromPtr++;
			if(freeE2PromPtr>=(uint8_t *)((uint32_t)E2PROM_START_ADDRESS + (uint32_t)E2PROM_PAGE_SIZE)){
				freeE2PromPtr=(uint8_t *)E2PROM_START_ADDRESS;
				lastE2PromPtr=(uint8_t *)((uint32_t)E2PROM_START_ADDRESS + (uint32_t)E2PROM_PAGE_SIZE - (uint32_t)E2PROM_USER_SIZE);
			}else{
				lastE2PromPtr=freeE2PromPtr;
				lastE2PromPtr-=E2PROM_USER_SIZE;
			}
			return;
		}else{
			ptr--;
		}
	}
	
	freeE2PromPtr=(uint8_t *)E2PROM_START_ADDRESS;
	lastE2PromPtr=freeE2PromPtr;
}

/***********************************************************************************************************************
* Function Name: e2PromEmptyCheck
* Description  : 
* Arguments    : 
* Return Value : 
***********************************************************************************************************************/
Bool e2PromEmptyCheck(void){
	getE2PromPointers();
	if((lastE2PromPtr == freeE2PromPtr)&&(lastE2PromPtr==((uint8_t *)E2PROM_START_ADDRESS))){
		return TRUE;
	}
	return FALSE;
}

 /***********************************************************************************************************************
* Function Name: getE2PromDataToRAM
* Description  : 
* Arguments    : 
* Return Value : 
***********************************************************************************************************************/
void getE2PromDataToRAM(void){
    uint32_t i;
    
    for(i=0; i<E2PROM_USER_SIZE; i++){
      e2PromData[i]=lastE2PromPtr[i];
    }
}

/***********************************************************************************************************************
* Function Name: eraseFlash
* Description  : 
  * Arguments    : 
* Return Value : None
***********************************************************************************************************************/
Bool eraseE2Prom(void){
	Bool result=FALSE;
	//uint32_t k;
	
	if (Chip_IAP_PreSectorForReadWrite(E2PROM_START_SECTOR_NUM, E2PROM_END_SECTOR_NUM) == IAP_CMD_SUCCESS){
			if( Chip_IAP_EraseSector(E2PROM_START_SECTOR_NUM, E2PROM_END_SECTOR_NUM)==IAP_CMD_SUCCESS){
			 result=TRUE;
			}
	 }
	 if(result==FALSE){
		 return FALSE;
	 }
	 
	 result=FALSE;
	 if( Chip_IAP_BlankCheckSector(E2PROM_START_SECTOR_NUM, E2PROM_END_SECTOR_NUM)==IAP_CMD_SUCCESS){
		 result=TRUE;
	 }
	
	 return result;
}

  /***********************************************************************************************************************
* Function Name: writeE2PromWithOffset
* Description  : 
 * Arguments    : 
* Return Value : None
***********************************************************************************************************************/
Bool writeE2PromWithOffset(uint32_t ofset){
	uint32_t k,m,i;
	uint32_t bolum;
	uint32_t kalan;
	uint8_t e2promDataArray[E2PROM_MIN_WRITE_BYTE];
	uint8_t *ptr8;
	uint16_t crc;
	uint16_t *ptr16;
	
// 	for(i=0; i<E2PROM_MIN_WRITE_BYTE; i++){
// 		e2promDataArray[i]=0xFF;
// 	}
	
	bolum=ofset/E2PROM_MIN_WRITE_BYTE; //okunacak bolum numarasi
	kalan=ofset%E2PROM_MIN_WRITE_BYTE; //
	
	ptr8 = (uint8_t *) (E2PROM_START_ADDRESS + (E2PROM_MIN_WRITE_BYTE * bolum));
	
	e2PromData[E2PROM_USER_SIZE-1]=0;
	
	crc = crc16(e2PromData, E2PROM_USER_SIZE-3 );
	ptr16=(uint16_t *)&e2PromData[E2PROM_USER_SIZE-3];
	*ptr16 = crc;
	
	k=0;
	for(i=0; i<E2PROM_MIN_WRITE_BYTE; i++){
		if((i>=kalan)&&(i<(kalan + E2PROM_USER_SIZE))){
			e2promDataArray[i]=e2PromData[k];
			k++;
		}else{
			e2promDataArray[i] = ptr8[i];
		}
	}

	k=E2PROM_START_ADDRESS + (E2PROM_MIN_WRITE_BYTE * bolum);
	m=(uint32_t)e2promDataArray;

	if (Chip_IAP_PreSectorForReadWrite(E2PROM_START_SECTOR_NUM, E2PROM_END_SECTOR_NUM) == IAP_CMD_SUCCESS){
		if (Chip_IAP_CopyRamToFlash(k,m,E2PROM_MIN_WRITE_BYTE) != IAP_CMD_SUCCESS)
		{
			return FALSE;
		}
	}else{
			return FALSE;
	}
	
	return TRUE;
	
}

/***********************************************************************************************************************
* Function Name: writeE2Prom
* Description  : 
 * Arguments    : 
* Return Value : None
***********************************************************************************************************************/
Bool writeE2Prom(void){
	static uint32_t ofset;
	
	ofset = (uint32_t )(freeE2PromPtr - E2PROM_START_ADDRESS);
	if(ofset==0){
		if(eraseE2Prom()==FALSE){
			return FALSE;
		}
	}
	if(writeE2PromWithOffset(ofset)){
		lastE2PromPtr=freeE2PromPtr;
		freeE2PromPtr+=E2PROM_USER_SIZE;
		if(freeE2PromPtr>=(uint8_t *)((uint32_t)E2PROM_START_ADDRESS + (uint32_t)E2PROM_PAGE_SIZE)){
			freeE2PromPtr=(uint8_t *)E2PROM_START_ADDRESS;
		}
		if(dataVerifyE2Prom()==FALSE){
			return FALSE;
		}
		return TRUE;
	}
	return FALSE;
}

 /***********************************************************************************************************************
* Function Name: dataVerifyE2Prom
* Description  : 
 * Arguments    : 
* Return Value : None
***********************************************************************************************************************/
Bool dataVerifyE2Prom(void){
	uint16_t i;
	
	for(i=0; i<E2PROM_USER_SIZE; i++){
		if(lastE2PromPtr[i]!=e2PromData[i]){
			return FALSE;
		}
	}
	return TRUE;
}

//****************************************************************************************************
//FonksiyonAdy:		SaveE2PromRAM
//A�yklama:   bu fonksiyon ile girilen data RAM'e yazilir. yazilan yer flash image'inin tutuldugu yerdir.	
//
void SaveE2PromRAM(uint8_t *data, uint32_t index, uint32_t length){
	uint32_t i;
	for(i=index; i<(index+length); i++){
		e2PromData[i]=*data;
		data++;
	}
}

//****************************************************************************************************
//FonksiyonAdy:		GetE2PromDataPtr
//A�yklama:   RAM'den index'i verilen yere iliskin adresi alir	
//
uint8_t *GetE2PromDataPtr(uint32_t index){

  return &e2PromData[index];

}




