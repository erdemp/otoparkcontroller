

#ifndef MASTER_H_
#define MASTER_H_

#include "cmsis_os.h"
#include "filters.h"
#include <math.h>
#include "convertions.h"
//#include "keyboard.h"


#include <string.h>
#include <stdlib.h>
//#include "RS485.h"
//#include "serialFlash.h"
//#include "pc_output.h"
//#include "modbus.h"
//#include "Ethernet.h"
//#include "device.h"
#include "Ethernet.h"
#include "device.h"
#include "internalFlash.h"

#ifdef __cplusplus
extern "C"
{
#endif
	
	#define KEY_READY					0x0004

	#define UZUN	1
	#define KISA	0
	
	#define STARTDEVICESEARCH_SIGNAL				0x0001
	#define	STOPDEVICESEARCH_SIGNAL					0x0002
	#define CANCELDEVICESEARCH_SIGNAL				0x0004
	#define SENDSTATUS_SIGNAL								0x0008
	#define	STARTDEVICEPROGRAMMING_SIGNAL		0x0010


	
	extern osThreadId Master_TaskID;
	void Master_Task(void const *argument);
	bool extract_between (const char *str, const char *p1, const char *p2, char * returndata, uint16_t *fark);
	void checkAnySignalFromEthernetThread (void);
	//void flashDiagnostic(void);
//	extern sensorType *sensor_data;
	


#ifdef __cplusplus
}
#endif

#endif /* INDICATOR_H_ */


