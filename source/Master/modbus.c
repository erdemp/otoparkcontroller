#include "modbus.h"

/****************************************************************
* modbus komutlari
***************************************************************/
#define WRITE_SINGLE_REG		0x06
#define READ_HOLDING_REGS		0x03

/***********************************************************************
 * @brief 	adresi verilen holding registera datayi yazar
 * @param		portNo: haberlesilecek UART numarasi
 *					devAdres: haberlesilecek sensorun modbus adresi
 *					regAdres: yazilacak holding reg adresi
 * 					data: yazilacak data
 * @return		true, false 
 ***********************************************************************/
bool modbusWriteHoldingReg(LPC_USART_T *port, uint8_t devAdres, uint16_t regAdres, uint16_t data){
	uint8_t modBusPackage[10];
	uint8_t returnPackage[10];
	uint16_t i16;
	
	modBusPackage[0] = devAdres;
	modBusPackage[1] = WRITE_SINGLE_REG; //write single register
	i16 = (regAdres-1);
	modBusPackage[2] = (uint8_t)(i16>>8); //register high byte
	i16 = (regAdres-1);
	modBusPackage[3] = (uint8_t) (i16); // register low byte
	
	modBusPackage[4] = (uint8_t)(data>>8);
	modBusPackage[5] = (uint8_t)(data);
	
	i16 = crc16( modBusPackage, 6 );
	modBusPackage[6] = (uint8_t)i16;
	modBusPackage[7] = (uint8_t)(i16>>8);
	
	if(rs485SendData (port , modBusPackage, 8, returnPackage, 8, 2000)){
		i16 = returnPackage[7];
		i16<<=8;
		i16|=returnPackage[6];
		if(crc16( returnPackage, 6 )!=i16){
			return false;
		}
	}else{
		return false;
	}
	return true;
}

/***********************************************************************
 * @brief 	sensoru iki clock modundaki deltaV degeri
 * @param		portNo: haberlesilecek UART numarasi
 *					devAdres: haberlesilecek sensorun modbus adresi
 *					regAdres: yazilacak holding reg adresi
 * 					data: yazilacak data
 * @return		true, false 
 ***********************************************************************/
bool modbusReadHoldingReg(LPC_USART_T *port, uint8_t devAdres, uint16_t regAdres, uint16_t *data, uint16_t waittimeout){
	uint8_t modBusPackage[10];
	uint8_t returnPackage[10];
	uint16_t i16;
	
	modBusPackage[0] = devAdres;
	modBusPackage[1] = READ_HOLDING_REGS; //read single register
	i16 = (regAdres-1);
	modBusPackage[2] = (uint8_t)(i16>>8); // register high byte
	i16 = (regAdres-1);
	modBusPackage[3] = (uint8_t) (i16); // register low byte
	modBusPackage[4] = 0; //
	modBusPackage[5] = 1; //	
	
	i16 = crc16( modBusPackage, 6 );
	modBusPackage[6] = (uint8_t)i16;
	modBusPackage[7] = (uint8_t)(i16>>8);
	
	// timeout 20 iken calisiyor 10 iken calismiyor.
	if(rs485SendData (port , modBusPackage, 8, returnPackage, 7, waittimeout )){
		i16 = returnPackage[6];
		i16<<=8;
		i16|=returnPackage[5];
		if(crc16( returnPackage, 5 )!=i16){
			return false;
		}
		
		i16 = returnPackage[3];
		i16<<=8;
		i16|=returnPackage[4];
		*data = i16;
	}else{
		return false;
	}
	return true;
	
}

