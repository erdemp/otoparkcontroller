
#ifndef INTERNALFLASH_H_
#define INTERNALFLASH_H_

#include "crc.h"
#include "iap.h"
#include "cmsis_os.h"
#include "chip.h"

#ifdef __cplusplus
extern "C"
{
#endif




//----------BIRINCIL FLASH ALANI
//settings
#define FLASH_START_ADDRESS	0x00028000
#define FLASH_PAGE_SIZE		32768
#define FLASH_START_SECTOR_NUM	19 //
#define FLASH_END_SECTOR_NUM	19	
#define FLASH_USER_SIZE		(8*1024)	// 32K flash alani  //(32*1024)	// 32K flash alani 
//flash yerleşimi 
//#define FLASH_AYARLAR	 0	  // 256 bytex4 =1024byte ayar belleği bırakılmıştır.
#define FLASH_POINTER	 0
#define FLASH_BELLEK	 FLASH_POINTER+4				// flash bellek


//----------IKINCIL FLASH ALANI
//mixer feeder
#define FLASH1_START_ADDRESS	0x00030000
#define FLASH1_PAGE_SIZE		32768
#define FLASH1_START_SECTOR_NUM	20 //
#define FLASH1_END_SECTOR_NUM	20	
#define FLASH1_USER_SIZE		(8*1024)	// 32K flash alani//(32*1024)	// 32K flash alani
//flash yerleşimi 
//#define FLASH1_AYARLAR	 0	  // 256 bytex4 =1024byte ayar belleği bırakılmıştır.
#define FLASH1_POINTER	 0
#define FLASH1_BELLEK	 FLASH1_POINTER+4				// flash bellek

//----------UCUNCUL FLASH ALANI
//backup
#define FLASH2_START_ADDRESS	0x00038000
#define FLASH2_PAGE_SIZE		32768
#define FLASH2_START_SECTOR_NUM	21 
#define FLASH2_END_SECTOR_NUM	21	
#define FLASH2_USER_SIZE		(8*1024)	// 32K flash alani //(32*1024)	// 32K flash alani
//flash yerleşimi 
//#define FLASH2_AYARLAR	 0	  // 256 bytex4 =1024byte ayar belleği bırakılmıştır.
#define FLASH2_POINTER	 0
#define FLASH2_BELLEK	 FLASH2_POINTER+4				// flash bellek


#define RAM_BUFFER_SIZE		(8*1024) //(10*1024)	//FLASH1_USER_SIZE	//ramde ayrilacak alani en buyuk flash alani belirler. Kullanici dikkatli olmalidir.
#define MAX_WRITABLE_SIZE	(4*1024)


#define BACKUP_FLASH_NO	2		//ucuncul flash alani backup alandir kullanilmamalidir
#define FLASH_PARAMETER_AREA		0
#define FLASH_BACKUP_AREA				2

//Flash Tipleri
typedef enum{
	FlashSettings							=0,	//ayarlarin tutuldugu flash alani numarasi
	FlashMixerFeeder					=1,	//mixer feederda rasyon,dagitim ve malzeme isimlerinin tutuldugu alan ve reçetelerin tutuldugu alandir
	FlashBackup								=2,
}Flash_Type;

void flashInit(uint8_t flashNo);
Bool flashErase(uint8_t flashNo);

Bool flashWrite(void);

Bool SaveRAM(uint8_t *data, uint32_t lenght);

Bool DelRAM_Flash(void);
uint8_t GetData(uint32_t index);
uint8_t *GetDataPtr(uint32_t index);
Bool compareFlash(uint32_t address, uint32_t data, uint8_t length);
Bool checkFlashCRC(void);
Bool restoreFlashBackup(void);
uint32_t  getFlashPointers(void);
uint32_t  getLastRecordStartAdress(void);
bool SaveToRAMbuff (uint8_t *data, uint32_t address, uint32_t length);
void eraseFlashRamArea (void);
void getDataFromFlashToRAM (uint32_t adress) ;
void selectFlashArea (uint8_t area);
void eraseDeviceRegsInRAMArea (uint32_t index, uint8_t data);
bool getEmptyAdressForCommandFromRAMbuff (uint32_t *emptyAdress,uint32_t *emptySize, uint32_t address, uint8_t emptyChar);
void eraseCommandsRamArea (uint32_t index, uint8_t data);
uint8_t * getparametersRAMStartAdress (void);
#ifdef __cplusplus
}
#endif

#endif /* INTERNALFLASH_H_ */

