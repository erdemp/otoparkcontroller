#include "displayDriver.h"

/****************************************************************
* display holding register haritasi
***************************************************************/
#define DISPLAY_MODBUS_ADRES_HR				2001
#define DISPLAY_MODBUS_HAB_AYARI_HR		2002
#define DISPLAY_MODBUS_BAUDRATE_HR		3003 
#define DISPLAY_MODBUS_RESET_HR				2004
#define DISPLAY_MODBUS_FIRMWARE_UPDATE_HR		2005
#define DISPLAY_MODBUS_LOCK_HR				2006
#define DISPLAY_MODBUS_COM_TIMEOUT_HR	2007
#define DISPLAY_MODBUS_BRIGHTNESS_HR				2008
#define DISPLAY_MODBUS_ZERO_CHAR_HR				2009
#define DISPLAY_MODBUS_ORIENTATION_HR				2010
#define DISPLAY_MODBUS_ADRES_GOSTERME_HR				2011
#define DISPLAY_MODBUS_MAX_VAL_HR				2012
#define DISPLAY_MODBUS_VAL_HR				2013
#define DISPLAY_MODBUS_DIGIT_NUMBER_HR				2014
#define DISPLAY_MODBUS_OKYONU_HR				2016


/***********************************************************************
 * @brief 	display adresini degistirir.
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek display modbus adresi
*					yeniAdres: yeni adres girilir
 * @return		true, false (display ile haberlesme sikintisi var ise false)
 ***********************************************************************/
bool displayAdresDegistir(LPC_USART_T *port, uint8_t adres, uint8_t yeniAdres){
	
	return modbusWriteHoldingReg(port, adres, DISPLAY_MODBUS_ADRES_HR, yeniAdres);
}

/***********************************************************************
 * @brief 	display haberlesme ayarlarini degistirir.
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek sensorun modbus adresi
*					data: yeni haberlesme ayari
 * @return		true, false (display ile haberlesme sikintisi var ise false)
 ***********************************************************************/
bool displayHabAyarDegistir(LPC_USART_T *port, uint8_t adres, haberlesmeAyarType data){
	uint16_t reg;
	
	if(data==haberlesmeAyar_8N2){
		reg=0;
	}else if(data==haberlesmeAyar_8E1){
		reg=1;
	}else{
		reg=2;
	}
	return modbusWriteHoldingReg(port, adres, DISPLAY_MODBUS_HAB_AYARI_HR, reg);
}

/***********************************************************************
 * @brief 	sensorun haberlesme hizini degistirir
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek sensorun modbus adresi
*					baudrate: yeni baudrate
 * @return		true, false (display ile haberlesme sikintisi var ise false)
 ***********************************************************************/
bool displayBaudrateDegistir(LPC_USART_T *port, uint8_t adres, uint32_t baudrate){
	uint16_t reg;
	if(baudrate==9600){
		reg=0;
	}else if(baudrate==19200){
		reg=1;
	}else if(baudrate==38400){
		reg=2;
	}else if(baudrate==57600){
		reg=3;
	}else{//115200
		reg=4;
	}
	return modbusWriteHoldingReg(port, adres, DISPLAY_MODBUS_BAUDRATE_HR, reg);
}

/***********************************************************************
 * @brief 	displayi yeniden baslatir
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek displayin modbus adresi
 * @return		true, false (display ile haberlesme sikintisi var ise false)
 ***********************************************************************/
bool displayReset(LPC_USART_T *port, uint8_t adres){
	return modbusWriteHoldingReg(port, adres, DISPLAY_MODBUS_RESET_HR, 1);
}


/***********************************************************************
 * @brief 	displayin kilitli registerlara yazmak i�in kilit acar
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek display modbus adresi
*						enable: true ise kilit acar false ise kapatir
 * @return		true, false (display ile haberlesme sikintisi var ise false)
 ***********************************************************************/
bool displayKilitAc(LPC_USART_T *port, uint8_t adres, bool enable){
	if(enable){
		return modbusWriteHoldingReg(port, adres, DISPLAY_MODBUS_LOCK_HR, 1071);
	}
	return modbusWriteHoldingReg(port, adres, DISPLAY_MODBUS_LOCK_HR, 0);
}

/***********************************************************************
 * @brief 	display haberlesme timeout'unu degistirir.
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek display modbus adresi
*					comTimeout: haberlesme timeoutu girilmelidir
 * @return		true, false (display ile haberlesme sikintisi var ise false)
 ***********************************************************************/
bool displayComTimeoutGir(LPC_USART_T *port, uint8_t adres, uint16_t comTimeout){
	
	return modbusWriteHoldingReg(port, adres, DISPLAY_MODBUS_COM_TIMEOUT_HR, comTimeout);
}

/***********************************************************************
 * @brief 	display haberlesme timeout'unu okur.
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek display modbus adresi
*					comTimeout: haberlesme timeoutu girilmelidir
 * @return		true, false (display ile haberlesme sikintisi var ise false)
 ***********************************************************************/
bool displayComTimeoutOku(LPC_USART_T *port, uint8_t adres, uint16_t *comTimeout){
	
	return modbusReadHoldingReg(port, adres, DISPLAY_MODBUS_COM_TIMEOUT_HR, comTimeout,100);
}
/***********************************************************************
 * @brief 	display zeroChari degistirir.
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek display modbus adresi
*					zeroChar: zarochar turu
 * @return		true, false (display ile haberlesme sikintisi var ise false)
 ***********************************************************************/
bool displayZeroCharDegistir(LPC_USART_T *port, uint8_t adres, displayZeroChar_Type zeroChar){
	uint16_t i;
	if(zeroChar==ZERO_CHAR_FULL){
		i=0x80;
	}else if(zeroChar==ZERO_CHAR_ARROW){
		i=0x43;
	}else{
		i=0x41;
	}
	return modbusWriteHoldingReg(port, adres, DISPLAY_MODBUS_ZERO_CHAR_HR, i);
}/***********************************************************************
 * @brief 	display zeroChari okur.
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek display modbus adresi
*					zeroChar: zarochar turu
 * @return		true, false (display ile haberlesme sikintisi var ise false)
 ***********************************************************************/
bool displayZeroCharOku(LPC_USART_T *port, uint8_t adres, displayZeroChar_Type *zeroChar){
	uint16_t i;
//	if(zeroChar==ZERO_CHAR_FULL){
//		i=0x80;
//	}else if(zeroChar==ZERO_CHAR_ARROW){
//		i=0x43;
//	}else{
//		i=0x41;
//	}
	if ( modbusReadHoldingReg(port, adres, DISPLAY_MODBUS_ZERO_CHAR_HR, &i,100) == false ) {
		return false;
	}else {
		if ( i== 0x80 ) {
			*zeroChar=ZERO_CHAR_FULL;
		}else if ( i == 0x43 ) {
			*zeroChar=ZERO_CHAR_ARROW;
		}else {
			*zeroChar=ZERO_CHAR_CROSS;
		}
	}
	return true;
	
}
/***********************************************************************
 * @brief 	display parlaklik degistirir.
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek display modbus adresi
*					parlaklik: 0-255
 * @return		true, false (display ile haberlesme sikintisi var ise false)
 ***********************************************************************/

bool displayParlaklikDegistir(LPC_USART_T *port, uint8_t adres, uint16_t brigthnessVal) {
	
	return modbusWriteHoldingReg(port, adres, DISPLAY_MODBUS_BRIGHTNESS_HR, brigthnessVal);
	
}

/***********************************************************************
 * @brief 	display parlaklik oku.
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek display modbus adresi
*					parlaklik: 0-255
 * @return		true, false (display ile haberlesme sikintisi var ise false)
 ***********************************************************************/

bool displayParlaklikOku(LPC_USART_T *port, uint8_t adres, uint16_t *brigthnessVal) {
	
	return modbusReadHoldingReg(port, adres, DISPLAY_MODBUS_BRIGHTNESS_HR, brigthnessVal,100);
	
}
/***********************************************************************
 * @brief 	display yon degistirir.
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek display modbus adresi
*					orientation: yon bilgisi
 * @return		true, false (display ile haberlesme sikintisi var ise false)
 ***********************************************************************/
bool displayYonDegistir(LPC_USART_T *port, uint8_t adres, dispayOrientation_Type orientation){
	uint16_t i;
	if(orientation==OR_NORMAL){
		i=0x01;
	}else{
		i=0x00;
	}
	return modbusWriteHoldingReg(port, adres, DISPLAY_MODBUS_ORIENTATION_HR, i);
}

/***********************************************************************
 * @brief 	display yon degistirir.
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek display modbus adresi
*					orientation: yon bilgisi
 * @return		true, false (display ile haberlesme sikintisi var ise false)
 ***********************************************************************/
bool displayYonOku(LPC_USART_T *port, uint8_t adres, dispayOrientation_Type *orientation){
	uint16_t i;
//	if(orientation==OR_NORMAL){
//		i=0x01;
//	}else{
//		i=0x00;
//	}
	if ( modbusReadHoldingReg(port, adres, DISPLAY_MODBUS_ORIENTATION_HR, &i, 100) == false ) {
		return false;
	}else {
		if ( i== 0x01 ) {
			*orientation = OR_NORMAL;
		}else {
			*orientation = OR_INVERTED;
			
		}
	}
	return true;
}

/***********************************************************************
 * @brief 	display ok younunu degistirir.
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek display modbus adresi
*					ok yonu: 0 - sol , 1 yukari
 * @return		true, false (display ile haberlesme sikintisi var ise false)
 ***********************************************************************/

bool displayOkYonuDegistir(LPC_USART_T *port, uint8_t adres, uint8_t okYonu) {
	
	return modbusWriteHoldingReg(port, adres, DISPLAY_MODBUS_OKYONU_HR, okYonu);
	
}

/***********************************************************************
 * @brief 	display ok yonu oku.
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek display modbus adresi
*					ok yonu: 0-sol, 1-yukari
 * @return		true, false (display ile haberlesme sikintisi var ise false)
 ***********************************************************************/

bool displayOkYonuOku(LPC_USART_T *port, uint8_t adres, uint16_t *okYonu) {
	
	return modbusReadHoldingReg(port, adres, DISPLAY_MODBUS_OKYONU_HR, okYonu,100);
	
}
/***********************************************************************
 * @brief 	displayde adres gostermek icin kullanilir
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek display modbus adresi
*						enable: true ise adres gosterilir, false ise deger gosterilir
 * @return		true, false (display ile haberlesme sikintisi var ise false)
 ***********************************************************************/
bool displayAdresGoster(LPC_USART_T *port, uint8_t adres, bool enable){
	if(enable){
		return modbusWriteHoldingReg(port, adres, DISPLAY_MODBUS_ADRES_GOSTERME_HR, 1);
	}
	return modbusWriteHoldingReg(port, adres, DISPLAY_MODBUS_ADRES_GOSTERME_HR, 0);
}

/***********************************************************************
 * @brief 	display maksimum sayi
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek display modbus adresi
*					maxVal: maksimum sayi
 * @return		true, false (display ile haberlesme sikintisi var ise false)
 ***********************************************************************/
bool displayMaxValGir(LPC_USART_T *port, uint8_t adres, uint16_t maxVal){
	
	return modbusWriteHoldingReg(port, adres, DISPLAY_MODBUS_MAX_VAL_HR, maxVal);
}

/***********************************************************************
 * @brief 	displayde girilen degeri gostermek icin kullanilir
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek display modbus adresi
*					val: gosterilecek sayi
 * @return		true, false (display ile haberlesme sikintisi var ise false)
 ***********************************************************************/
bool displaySayiGoster(LPC_USART_T *port, uint8_t adres, uint16_t val){
	
	return modbusWriteHoldingReg(port, adres, DISPLAY_MODBUS_VAL_HR, val);
}

/***********************************************************************
 * @brief 	sensorun olctugu mesafeyi okur
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek sensorun modbus adresi
 *					val: displayde gosterilen sayi
 * @return		true, false (sensor ile haberlesme sikintisi var ise false)
 ***********************************************************************/
bool displaySayiOku(LPC_USART_T *port, uint8_t adres, uint16_t *val){
	return modbusReadHoldingReg(port, adres, DISPLAY_MODBUS_VAL_HR, val, 100);
}

/***********************************************************************
 * @brief 	displayin digit sayisini girmek icin kullanilir
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek display modbus adresi
*					digitNumber: displayin digit sayisi girilir
 * @return		true, false (display ile haberlesme sikintisi var ise false)
 ***********************************************************************/
bool displayDigitSayisiGir(LPC_USART_T *port, uint8_t adres, uint16_t digitNumber){
	
	return modbusWriteHoldingReg(port, adres, DISPLAY_MODBUS_DIGIT_NUMBER_HR, digitNumber);
}



