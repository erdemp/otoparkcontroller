#ifndef CONVERTIONS_H_
#define CONVERTIONS_H_

//#include "lpc_types.h"
#include "chip.h"
#include <string.h>

#ifdef __cplusplus
extern "C"
{
#endif

int32_t parse_doubleToint32(double d);	
void itoa32(int32_t n, char *ret);
uint32_t atoi32(char *str);
void stringToIntDotPos(char *str, uint32_t *num, uint8_t *dotPos);
void intDotPosToStr(int32_t num, uint8_t dotPos, char *str);
void intDotPosToStrWithStartDigit(int32_t num, uint8_t dotPos, char *str, uint8_t DigitCnt);
char *intToHexString(uint16_t num, char *str);

#ifdef __cplusplus
}
#endif

#endif /* CONVERTIONS_H_ */

