


#ifndef CRC_H_
#define CRC_H_


#include "chip.h"
#include "cmsis_os.h"

#ifdef __cplusplus
extern "C"
{
#endif

	uint16_t crc16( uint8_t * pucFrame, uint32_t usLen );

	

#ifdef __cplusplus
}
#endif

#endif /* CRC_H_ */


