#ifndef DEVICE_H_
#define DEVICE_H_



#include "cmsis_os.h"
#include "chip.h"
#include "displayDriver.h"
#include "sensorDriver.h"
#include "bsp.h"
#include "master.h"

#ifdef __cplusplus
extern "C"
{
#endif
	#define MAX_PORT_DEVICE_CNT	128
	#define SENSORGRUPSAYISI  				5	 
	#define DISPLAYCOMMANDREGSAYISI		5
	#define DEVICEDATAMAXSIZE					30

	#define MASTERSTAT_EMPTY								0
	#define MASTERSTAT_DEVICESEARCH					1
	#define MASTERSTAT_SENSORREAD						2
	#define MASTERSTAT_SENSORLEDUPDATE			3		
	#define MASTERSTAT_SENSORGRUPREGUPDATE	4
	#define MASTERSTAT_DISPUPDATE						5
	#define MASTERSTAT_CHECKDEVICE					6
	#define MASTERSTAT_PROGRAMDEVICE				7
	
	#define DEVICE_SENSOR_STR				'2'
	#define DEVICE_DISPLAY_STR			'1'
	#define DEVCIE_NONE							'0'
	
	#define DISPLAY_FIRSTCHAR_CROSS			65
	#define DISPLAY_FIRSTCHAR_REDARROW	67
	
	#define DISPLAY_OK_YUKARI				1
	#define DISPLAY_OK_ASAGI				2
	#define DISPLAY_OK_YAN_IC				3
	#define DISPLAY_OK_YAN_DIS			4
	
	#define MAXVAL_SENSORLIMIT				400
	
	
	
	typedef enum{
		devSENSOR = 0x02,
		devDISPLAY = 0x01,
		devNONE = 0
	}devType;

	typedef struct{
		uint8_t deviceAddress;	//device'in adresi
		devType device;					// cihaz turu: sensor veya display
		uint8_t idiotBuff[DEVICEDATAMAXSIZE];
	}deviceType;
	
	typedef struct{
		uint8_t empty;
		uint8_t deviceAddress;	//sensor'in adresi
		devType device;					//cihaz turu: sensor 
		uint8_t GrupNo;
		uint8_t LEDGrubu;
		Bool LEDbagli;
		uint8_t	workingMode;		// sensorun calisma modu: sensor mod ise 0, merkez mod ise 1
		uint16_t	LimitVal;
		Bool comError;					// cihazla haberlesilmek istendiginde haberlesme yapilamazsa true, yapilirsa false
		uint8_t empty1;					// flash  yazilan bolge. 12 byte i tamamlamak i�in ve
		uint8_t empty2;					// ileride bir seyler eklenir diye bos yer birakildi.
		uint8_t empty3;
		//uint8_t empty4;
		//uint8_t empty5;
		
		uint32_t comErrorCnt;		// cihazla haberlesilmek istendiginde eger haberlesme yapilamazsa arttirilir
		Bool		Dolu;					// sensor altinda arac varsa 1, yoksa 0
		Bool		checked;				// ????????????????
		//uint8_t registerNo;			// sensorun dolu bos hesaplamasi icin kullanilan InternalRegister indexi
	}sensorType;
	
	typedef struct{
		uint8_t empty;
		uint8_t deviceAddress;	//display'in adresi
		devType device;					//cihaz turu: display
		uint8_t firstChar;
		uint32_t HaberlesmeGecikmesi;
		Bool	inverted;
		//uint32_t HaberlesmeGecikmesi;
		uint8_t	Parlaklik;
		uint8_t OkYonu;					// flash  yazilan bolge. 12 byte i tamamlamak i�in ve
		Bool comError;					// cihazla haberlesilmek istendiginde haberlesme yapilamazsa true, yapilirsa false
		uint8_t empty2;					// ileride bir seyler eklenir diye bos yer birakildi.
		//uint8_t empty3;
		
		uint32_t comErrorCnt;		// cihazla haberlesilmek istendiginde eger haberlesme yapilamazsa arttirilir
		uint16_t val;
		
	}displayType;
	
	
//	typedef struct {		
//		sensorType	sensorData;
//		uint8_t		port;				
//	}mpoolSensorType;
	
	typedef struct {
	  sensorType sensorData;
		uint8_t port;
		uint8_t OK;
	}mpoolSensorType;
	
	typedef struct {
	  displayType displayData;
		uint8_t port;
		uint8_t OK;
	}mpoolDisplayType;	
	
	
	typedef struct {
	  uint16_t ToplamSensor;
		uint16_t ToplamBosYer;
	}sensGrupReg;
	
	
	typedef struct {
		uint8_t adres;
		uint16_t val;
	}dispCommandReg;
	
	typedef struct {
		uint8_t status;
		uint8_t searchedDeviceRatio;
		uint16_t ReadCount;
		uint8_t programmedDeviceRatio;
	}MasterGeneralReg;
	
	extern deviceType port1Devices[MAX_PORT_DEVICE_CNT];
	extern deviceType port2Devices[MAX_PORT_DEVICE_CNT];
	extern deviceType port3Devices[MAX_PORT_DEVICE_CNT];
	extern deviceType port4Devices[MAX_PORT_DEVICE_CNT];
	extern sensGrupReg sensorGrupRegPort1[SENSORGRUPSAYISI];
	extern sensGrupReg sensorGrupRegPort2[SENSORGRUPSAYISI];
	extern sensGrupReg sensorGrupRegPort3[SENSORGRUPSAYISI];
	extern sensGrupReg sensorGrupRegPort4[SENSORGRUPSAYISI];
	extern dispCommandReg displayCommandRegPort1[DISPLAYCOMMANDREGSAYISI];
	extern dispCommandReg displayCommandRegPort2[DISPLAYCOMMANDREGSAYISI];
	extern dispCommandReg displayCommandRegPort3[DISPLAYCOMMANDREGSAYISI];
	extern dispCommandReg displayCommandRegPort4[DISPLAYCOMMANDREGSAYISI];
	extern MasterGeneralReg	Master;
	extern uint8_t port1DeviceCount;
	extern uint8_t port2DeviceCount;
	extern uint8_t port3DeviceCount;
	extern uint8_t port4DeviceCount;
		
	 
	void EthernetControl(void);
	void sensorDurumlariniOku(void);
	void sensorGrupRegUpdate(void);
	void sensorLedGuncelle(void);
	void displayUpdate (void);
	Bool SetSensorGroup (LPC_USART_T *port,uint8_t adres, uint8_t group );
	Bool SetDisplayCommandReg (LPC_USART_T *port,uint8_t adres, uint8_t regNo );
	void ClearSensorGrupRegs(void);
	void ClearDisplayCommandRegs (void);
	void clearDeviceRegs (void);
	Bool getDisplayParameters (LPC_USART_T *port, uint8_t adres, displayType *disp);
	void cihazTaramasi(void);
//	void CheckIncomingData (uint8_t *buff, uint16_t datasize) ;
	Bool CheckSensorParametersSuitable (sensorType *sensorPtr );
	Bool CheckSensorParametersChanged ( sensorType *sensptr, deviceType *portDevPtr, uint8_t *portDeviceCnt )  ;
	Bool CheckDisplayParametersSuitable (displayType *displayPtr );
	Bool CheckDisplayParametersChanged ( displayType *displayPtr, deviceType *portDevPtr, uint8_t *portDeviceCnt );
	Bool  SetSensorParameterAndCheck (uint8_t portNo, sensorType *ptr);
	Bool  SetDisplayParameterAndCheck (uint8_t portNo, displayType *ptr) ;
	void CheckDeviceParameters (void) ;
	void ProgramDevices (void) ;
	void sensorBilgisiAl(LPC_USART_T *port, uint8_t adres);
	Bool readSensorParameter( uint8_t port, uint8_t adres, sensorType *Sptr );
	bool getCommand (uint8_t *readPtr, uint8_t *retBuff, uint16_t *retSize, uint16_t maxCmdSize);

//	//osPoolDef(mpool_sensor, 1, sensorType);                    // Define memory pool
//	extern osPoolId  mpool_sensor_id;
//	extern sensorType *sensor_data;
//	osMessageQDef(MsgBox_sensor, 16, &sensorType);              // Define message queue
//	osMessageQId  MsgBox;

#ifdef __cplusplus
}
#endif

#endif /* DEVICE_H_ */


