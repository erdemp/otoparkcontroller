

#ifndef DELAYS_H_
#define DELAYS_H_

//#include "LPC17xx.h"
//#include "lpc_types.h"
//#include "lpc17xx_timer.h"
#include "cmsis_os.h"

#ifdef __cplusplus
extern "C"
{
#endif

	extern osMutexId delay_mutex_id; 
	void initDelays(void);
	void delayUs(uint32_t usecDelay);
//void delayUs2(uint32_t usecDelay);
	void delayUs_TurnOff(void);

#ifdef __cplusplus
}
#endif

#endif /* DELAYS_H_ */

