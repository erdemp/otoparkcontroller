
#ifndef ETHERNET_H_
#define ETHERNET_H_


#include "chip.h"
#include "cmsis_os.h"
#include <stdio.h>
#include <string.h>
#include "crc.h"
#include "bsp.h"
#include "socket.h"
#include "fifo8.h"
#include "master.h"
#include "device.h"
#include "settings.h"

#ifdef __cplusplus
extern "C"
{
#endif

	
	#define SENSORGRUPSAYISI  				5	 
	#define DISPLAYCOMMANDREGSAYISI		5
	
	#define SOCK_TCP_PC				0
	#define SOCK_TCP_1				1
	#define SOCK_TCP_2				2
	#define SOCK_TCP_3				3
	#define SOCK_TCP_4				4
	#define SOCK_TCP_5				5
	#define SOCK_TCP_6				6
	
	
	#define ETHERNET_TCP_PCPORT_BUF_SIZE	1024
	#define ETHERNET_TCP_MASTERPORTS_BUF_SIZE_RX	512
	#define ETHERNET_TCP_MASTERPORTS_BUF_SIZE_TX	128
	
	
//	#define SOCK_TCPS_INCOMMING_PC     			1
//	#define SOCK_TCPC_OUTGOING         			0
//	#define SOCK_TCPS_INCOMING_CONTROLLER		2
	#define PORT_PC										5000
	#define PORT_SISTEM								5001
	#define PORT_PORT1							  5001
	#define PORT_PORT2							  5001
	#define PORT_PORT3							  5001
	#define PORT_PORT4							  5001
	#define PORT_PORT5							  5001
	#define PORT_PORT6							  5001
	
	#define CONNECTPORTSOCKET_CNT			5
	
//	#define CONNECT_REQUEST				1
//	#define CONNECT_CONNECTING		2
//	#define CONNECT_ASKDATA			3
//	#define CONNECT_DATAWAIT			4
	
	#define SOCKET_OPENED					1
	#define SOCKET_CONNECTED			2
	#define SOCKET_DATAWAIT				4
	#define SOCKET_DARARECEIVED		5
	#define SOCKET_CLOSED					6
	
	#define STX		0x02
	#define ETX   0x03
	
	#define ETHERNET_SIGNAL_DEVICEUPDATE				0x0001
	#define ETHERNET_SIGNAL_DEVICEREAD					0x0002
	//#define ETHERNET_SENDRECEIVEDATA	0x0002

//	#define ETHERNET_PORTLISTEN_PC_BUF_SIZE		3000
//	#define ETHERNET_PORTLISTEN_PC_TX_BUF_SIZE		100
//	#define ETHERNET_PORTLISTEN_CONTROLLER_BUF_SIZE		3000
//	#define ETHERNET_PORTCONNECT_BUF_SIZE		100
	
	#define STARTOFTEXT	0x02
	#define ENDOFTEXT		0x03
	
	#define MAXPACKAGESIZE		1400// 1096
	#define MAXMASTERCOUNTIN_A_COMMAND	50
	
	#define TIMER_ETH_CALLBACK_PERIOD	10
	#define PORTLISTENYARIMPAKETTIMEOUT		100
	#define TIMER_ETH_MASTERCONNECTTIMEOUT		8000
	#define TIMER_ETH_CONNECTSOKETTIMEOUT			5000
	
		
	#define	CMD_STARTDEVICESEARCH						"<STARTDEVICESEARCH>"
	#define CMD_STOPDEVICESEARCH						"<STOPDEVICESEARCH>"
	#define CMD_CANCELDEVICESEARCH					"<CANCELDEVCIESEARCH>"
	#define CMD_SENDSTATUS									"<READSTATUS>"
	#define CMD_SENDSPECIFICDEVICEPARAMETER	"<READDEVICEPARAM>"
	#define CMD_SENDDEVICEPARAMETERS				"<READDEVICEPARAMETERS>"
	#define CMD_SENDPORT1DEVICEPARAMETERS		"<READPORT1DEVICEPARAMETERS>"	
	#define CMD_SENDPORT2DEVICEPARAMETERS		"<READPORT2DEVICEPARAMETERS>"
	#define CMD_SENDPORT3DEVICEPARAMETERS		"<READPORT3DEVICEPARAMETERS>"
	#define CMD_SENDPORT4DEVICEPARAMETERS		"<READPORT4DEVICEPARAMETERS>"
//	#define CMD_READPORT1SPECIFICDEVICE			"<READPORT1SPECIFICDEVICE>"	
//	#define CMD_READPORT2SPECIFICDEVICE			"<READPORT2SPECIFICDEVICE>"	
//	#define CMD_READPORT3SPECIFICDEVICE			"<READPORT3SPECIFICDEVICE>"	
//	#define CMD_READPORT4SPECIFICDEVICE			"<READPORT4SPECIFICDEVICE>"	
	#define CMD_SETDEVICEPARAMETERS					"<SETDEVICEPARAMETERS>"
	#define CMD_PROGRAMMASTER								"<PROGRAMMASTER>"
	#define CMD_PROGRAMDEVICES							"<PROGRAMDEVICES>"
	#define CMD_PROGRAMDEVICESSTATUS				"<PROGRAMDEVICESSTATUS>"
//	#define CMD_PROGRAMSENSORS							"<PROGRAMSENSORS>"
//	#define CMD_PROGRAMDISPLAYS							"<PROGRAMDISPLAYS>"
	#define CMD_READSENSORGRUPREGS					"<READSENSGRPREGS>"
	#define CMD_READDISPREGS								"<READDISPREGS>"
	#define CMD_READSENSDISPREGS						"<READSENSDISPREGS>"
	#define CMD_SETDISPLAYCOMMANDREGS				"<SETDISPCMDREGS>"
	#define CMD_SETNETWORKPARAMETERS				"<SETNETPARAMETERS>"
	#define CMD_READNETPARAMETERS						"<READNETPARAMETERS>"
	#define CMD_RESETMASTER									"<RESETMASTER>"
	#define CMD_RESETALLSENSORS							"<RESETALLSENSORS>"
	#define CMD_RESETALLDISPLAYS						"<RESETALLDISPLAYS>"
	#define CMD_RESETSENSORS								"<RESETSENSORS>"
	#define CMD_RESETDISPLAYS								"<RESETDISPLAYS>"
	
	// Controller commands 
	#define CMD_CONTROLLERCOMMANDS					"IP["
	#define CMD_RECORDCOMMANDS							"<RECORDCOMMANDS>"
	#define CMD_DELETECOMMANDS							"<DELETECOMMANDS>"
	#define CMD_RESETCONTROLLER							"<RESETCONTROLLER>"
	
	#define ANSWER_DEVICESEARCHSTARTED							"<STARTED>"
	#define ANSWER_DEVICESEARCHSTOPED	  						"<STOPPED>"
	#define	ANSWER_MASTERSTATUS_EMPTY								"<STATUS=EMPTY>"
	#define ANSWER_MASTERSTATUS_DEVICESEARCH				"<STATUS=DEVICESEARCH>"
	#define ANSWER_MASTERSTATUS_SENSORREAD					"<STATUS=SENSORREAD>"			
	#define ANSWER_MASTERSTATUS_SENSORLEDUPDATE			"<STATUS=SENSORLEDUPDATE>"
	#define ANSWER_MASTERSTATUS_SENSORGRUPREGUPDATE	"<STATUS=SENSORGRUPREGUPDATE>"
	#define ANSWER_MASTERSTATUS_DISPUPDATE					"<STATUS=DISPUPDATE>"
	#define ASNWER_MASTERSTATUS_DEVICECHECK					"<STATUS=DEVICECHECK>"
	#define ASNWER_MASTERSTATUS_PROGRAMDEVICE				"<STATUS=PROGRAMDEVICE>"
	#define ANSWER_MASTERNETPARAMETERS							"<MASTERNETPARAMETERS>"
	
	#define ANSWER_DEVICEREGSALL										"<DEVICEREGSALL>"
	#define ANSWER_DEVICEREGSPORT1									"<DEVICEREGSPORT1>"
	#define ANSWER_DEVICEREGSPORT2									"<DEVICEREGSPORT2>"
	#define ANSWER_DEVICEREGSPORT3									"<DEVICEREGSPORT3>"
	#define ANSWER_DEVICEREGSPORT4									"<DEVICEREGSPORT4>"
	#define	ANSWER_PORT1SPECIFICDEVICEPARAMETER			"<DEVICEREGSPORT1_X>"
	#define	ANSWER_PORT2SPECIFICDEVICEPARAMETER			"<DEVICEREGSPORT2_X>"
	#define	ANSWER_PORT3SPECIFICDEVICEPARAMETER			"<DEVICEREGSPORT3_X>"
	#define	ANSWER_PORT4SPECIFICDEVICEPARAMETER			"<DEVICEREGSPORT4_X>"
	#define ANSWER_DEVICEREGSEMPTY									"<DEVICEREGSEMPTY>"
	#define ANSWER_DEVICEREGSPORT1EMPTY							"<DEVICEREGSPORT1EMPTY>"
	#define ANSWER_DEVICEREGSPORT2EMPTY							"<DEVICEREGSPORT2EMPTY>"
	#define ANSWER_DEVICEREGSPORT3EMPTY							"<DEVICEREGSPORT3EMPTY>"
	#define ANSWER_DEVICEREGSPORT4EMPTY							"<DEVICEREGSPORT4EMPTY>"
	#define ANSWER_SENSORGRUPREG										"<SENSGRPREGS>"
	#define ANSWER_DISPCOMMANDREGS									"<DISPCMDREGS>"				
	#define ANSWER_PORT1														"<PORT1>"
	#define ANSWER_PORT2														"<PORT2>"
	#define ANSWER_PORT3														"<PORT3>"
	#define ANSWER_PORT4														"<PORT4>"
	#define ANSWER_PORTERROR												"<PORTERROR>"
	#define ANSWER_DATAOK														"<DATAOK>"
	#define ANSWER_DATANOK													"<DATANOK>"
	#define ANSWER_PORTNOERROR											"<PORTNOERROR>"
	#define ANSWER_DEVICEPARAMETERNOCHANGE					"<DEVICEPARAMETERNOCHANGE>"
	#define ANSWER_DEVICEPARAMETERSETINGOK					"<DEVICEPARAMETERSETOK>"
	#define	ANSWER_MASTERPROGRAMOK									"<MASTERPROGRAMOK>"
	#define ANSWER_MASTERPROGRAMERROR								"<MASTERPROGRAMERROR>"
	#define ANSWER_SENSORPROGRAMDONE								"<SENSORPROGRAMDONE>"
	#define ANSWER_DISPLAYPROGRAMDONE								"<DISPLAYPROGRAMDONE>"
	
	
	
	#define DEVICEALL											10
	#define	DEVICERS485PORT1							1
	#define	DEVICERS485PORT2							2
	#define	DEVICERS485PORT3							3
	#define	DEVICERS485PORT4							4
	
	typedef struct 
	{
		uint8_t		  IP[4];		// Connect yaparken baglanti kurulan karsi cihazin IP si
		uint32_t		PortNo;
		int8_t			Port_ErrorCode;
		fifo8_t 		Eth_Port_tx_fifo;
		fifo8_t 		Eth_Port_rx_fifo;
		uint8_t 		Eth_Port_tx_buf[ETHERNET_TCP_MASTERPORTS_BUF_SIZE_TX];
		uint8_t 		Eth_Port_rx_buf[ETHERNET_TCP_MASTERPORTS_BUF_SIZE_RX];
		uint8_t			Port_txBuff[ETHERNET_TCP_MASTERPORTS_BUF_SIZE_TX];
	}ETH_SocketBuff_Typedef;
	
	
		typedef struct
	{
		uint8_t     GatewayIP[4];
		uint8_t			SubnetMaskAdress[4];
		uint8_t		  LocalIP[4];
		uint8_t 		commandDisable;
//		uint8_t		  RemoteIPListen_PC[4];		// Listen yaparken baglanti kurulan karsi cihazin IP si
//		uint8_t		  RemoteIPListen_Controller[4];		// Listen yaparken baglanti kurulan karsi cihazin IP si
//		uint8_t		  RemoteIPConnect[4];		// Connect yaparken baglanti kurulan karsi cihazin IP si
//		uint32_t		PortListen_PC;
//		uint32_t		PortListen_Controller;
//		uint32_t		PortConnect;
//		int8_t			ListenPortErrorCode;
//		int8_t			ConnectPortErrorCode;
		
		uint8_t		  PortPC_RemoteIPConnect[4];		// Connect yaparken baglanti kurulan karsi cihazin IP si
		uint32_t		Port_PCPortNo;
		uint32_t		Port_SistemPortNo;
		int8_t			PortPC_ErrorCode;
		fifo8_t 		Eth_PortPC_tx_fifo;
		fifo8_t 		Eth_PortPC_rx_fifo;
		uint8_t 		Eth_PortPC_tx_buf[ETHERNET_TCP_PCPORT_BUF_SIZE];
		uint8_t 		Eth_PortPC_rx_buf[ETHERNET_TCP_PCPORT_BUF_SIZE];
		uint8_t			PortPC_txBuff[ETHERNET_TCP_PCPORT_BUF_SIZE];		
//		
//		uint8_t		  Port1_RemoteIPConnect[4];		// Connect yaparken baglanti kurulan karsi cihazin IP si
//		uint32_t		Port1_PortNo;
//		int8_t			Port1_ErrorCode;
//		fifo8_t 		Eth_Port1_tx_fifo;
//		fifo8_t 		Eth_Port1_rx_fifo;
//		uint8_t 		Eth_Port1_tx_buf[ETHERNET_TCP_MASTERPORTS_BUF_SIZE];
//		uint8_t 		Eth_Port1_rx_buf[ETHERNET_TCP_MASTERPORTS_BUF_SIZE];
//		uint8_t			Port1_txBuff[ETHERNET_TCP_MASTERPORTS_BUF_SIZE];
//		

//		uint8_t		  Port2_RemoteIPConnect[4];		// Connect yaparken baglanti kurulan karsi cihazin IP si
//		uint32_t		Port2_PortNo;
//		int8_t			Port2_ErrorCode;
//		fifo8_t 		Eth_Port2_tx_fifo;
//		fifo8_t 		Eth_Port2_rx_fifo;
//		uint8_t 		Eth_Port2_tx_buf[ETHERNET_TCP_MASTERPORTS_BUF_SIZE];
//		uint8_t 		Eth_Port2_rx_buf[ETHERNET_TCP_MASTERPORTS_BUF_SIZE];
//		uint8_t			Port2_txBuff[ETHERNET_TCP_MASTERPORTS_BUF_SIZE];
//		
//		uint8_t		  Port3_RemoteIPConnect[4];		// Connect yaparken baglanti kurulan karsi cihazin IP si
//		uint32_t		Port3_PortNo;
//		int8_t			Port3_ErrorCode;
//		fifo8_t 		Eth_Port3_tx_fifo;
//		fifo8_t 		Eth_Port3_rx_fifo;
//		uint8_t 		Eth_Port3_tx_buf[ETHERNET_TCP_MASTERPORTS_BUF_SIZE];
//		uint8_t 		Eth_Port3_rx_buf[ETHERNET_TCP_MASTERPORTS_BUF_SIZE];
//		uint8_t			Port3_txBuff[ETHERNET_TCP_MASTERPORTS_BUF_SIZE];

//		uint8_t		  Port4_RemoteIPConnect[4];		// Connect yaparken baglanti kurulan karsi cihazin IP si
//		uint32_t		Port4_PortNo;
//		int8_t			Port4_ErrorCode;
//		fifo8_t 		Eth_Port4_tx_fifo;
//		fifo8_t 		Eth_Port4_rx_fifo;
//		uint8_t 		Eth_Port4_tx_buf[ETHERNET_TCP_MASTERPORTS_BUF_SIZE];
//		uint8_t 		Eth_Port4_rx_buf[ETHERNET_TCP_MASTERPORTS_BUF_SIZE];
//		uint8_t			Port4_txBuff[ETHERNET_TCP_MASTERPORTS_BUF_SIZE];
//		
//		uint8_t		  Port5_RemoteIPConnect[4];		// Connect yaparken baglanti kurulan karsi cihazin IP si
//		uint32_t		Port5_PortNo;
//		int8_t			Port5_ErrorCode;
//		fifo8_t 		Eth_Port5_tx_fifo;
//		fifo8_t 		Eth_Port5_rx_fifo;
//		uint8_t 		Eth_Port5_tx_buf[ETHERNET_TCP_MASTERPORTS_BUF_SIZE];
//		uint8_t 		Eth_Port5_rx_buf[ETHERNET_TCP_MASTERPORTS_BUF_SIZE];
//		uint8_t			Port5_txBuff[ETHERNET_TCP_MASTERPORTS_BUF_SIZE];

//		uint8_t		  Port6_RemoteIPConnect[4];		// Connect yaparken baglanti kurulan karsi cihazin IP si
//		uint32_t		Port6_PortNo;
//		int8_t			Port6_ErrorCode;
//		fifo8_t 		Eth_Port6_tx_fifo;
//		fifo8_t 		Eth_Port6_rx_fifo;
//		uint8_t 		Eth_Port6_tx_buf[ETHERNET_TCP_MASTERPORTS_BUF_SIZE];
//		uint8_t 		Eth_Port6_rx_buf[ETHERNET_TCP_MASTERPORTS_BUF_SIZE];
//		uint8_t			Port6_txBuff[ETHERNET_TCP_MASTERPORTS_BUF_SIZE];
//		
		

		
	}ETHERNET_HandleTypeDef;
	
	
	
	typedef struct 
	{
		uint8_t SensGrupRegs [SENSORGRUPSAYISI];
		uint8_t DispRegs [DISPLAYCOMMANDREGSAYISI];
	}portRegs_HandeTypeDef;
	
		typedef struct
	{
		uint8_t		  masterIP[4];	
		uint8_t		  RS485Port;		// Connect yaparken baglanti kurulan karsi cihazin IP si
		uint8_t		  DispReg;		
	}masterDispReg_HandleTypeDef;
	
	typedef struct
	{
		uint8_t		  IP[4];	
		portRegs_HandeTypeDef	ports[4];
		uint8_t 		status;
	}masterReadRegs_HandleTypeDef;
	
	typedef struct 
	{
		uint8_t 		IP[4];
		uint8_t 		status;
		uint32_t    timeOut;
	}socket_HandleTypeDef;
	
	extern osThreadId Ethernet_TaskID ;
//	extern uint16_t	timerETH_ListenPortYarimPaketTimeOut;
	extern uint16_t  timerETH_globalTimeOut ;
	extern uint16_t	timerETH_SocketBusyTimeOut ;
	extern ETHERNET_HandleTypeDef	ETH;	

	
	int32_t  ETH_SendData_Orginal (uint8_t sn, uint8_t* buf, uint8_t* destip, uint16_t destport);
//  bool  ETH_CheckConnectPortClosed(uint16_t timeout);
//	bool ETH_GetDataFromRemoteIP(uint8_t *RemoteIP, uint16_t RemotePort, uint8_t *senddata, uint8_t *returndata, uint8_t *retsize, uint16_t timeout);
	bool ETH_WaitDataFromConnectPort(uint8_t *databuff,  uint8_t *retsize, int8_t *errcode);
	bool ETH_SendDataConnectPort (uint8_t* destip, uint16_t destport, uint8_t *senddata, uint8_t size);
	void timerETH_CallBack(void const *arg);
  bool ETH_SendDataFromListenPortPC (uint8_t *data, uint16_t size );
//	bool ETH_getListenPackageFromFifo (uint8_t *data, uint16_t *datasize);
	bool ETH_getListenPackageFromFifo (uint32_t portNo , uint8_t *data, uint16_t *datasize);
	int32_t  ETH_SendData (uint8_t sn, uint8_t* buf, uint8_t* destip, uint16_t destport) ;
	uint32_t  wizETH_SetClientSocket(uint8_t sn, uint16_t portno);
	void Ethernet_Task(void const *argument);
	bool  ETH_CheckIncomingDataOnListenPortPc(uint8_t sn, uint16_t portno, int8_t *errcode);
	bool  ETH_CheckIncomingDataOnListenPortController(uint8_t sn, uint16_t portno, int8_t *errcode);
	uint32_t  wizETH_SetServerSocket(uint8_t sn, uint16_t portno);
	void  wizETH_Init(void);
  void  wizchip_Init(void);
	void  wizchip_reset(void);
	void  wizchip_noReset(void);
	void  wizchip_select(void);
	void  wizchip_deselect(void);
	void  wizchip_write(uint8_t wb);
	uint8_t wizchip_read(void);
	void ETH_GetDefaultParameters ( ETHERNET_HandleTypeDef *ptrETH);
	void ETH_PackageControl (uint32_t portNo) ;
	void ETH_PackageControl_PC ( uint32_t portNo);
	void ETH_SendStatus (uint8_t status,  uint32_t portNo);
	void ETH_SetNetworkParameters ( void);
	void ETH_SendDataOK (  uint32_t portNo);
	void ETH_SendDataNOK (  uint32_t portNo);
	void ETH_GetNewNetworkParameters (uint8_t * databuff, uint32_t datasize );
	void ETH_SendNetworkParameters (uint32_t portNo);
	void ETH_SaveNetworkParameters (void);
	void ETH_SendDeviceParameters (uint32_t portNo, uint8_t param);
	void ETH_SendSensGrupRegs (uint32_t portNo);
	void ETH_UpdateDisplayCommandRegs (uint8_t * databuff, uint32_t datasize , uint32_t portNo) ;
	//void ETH_UpdateDeviceParameters (uint8_t * databuff, uint32_t datasize, uint8_t param ) ;
	void ETH_UpdateDeviceParameters (uint8_t * databuff, uint32_t datasize, uint8_t param, uint32_t portNo );
	void ETH_ReadSpecificDevice (uint8_t * databuff, uint32_t datasize, uint32_t portNo );			
  void ETH_CheckDataOnListenPort(uint8_t sn, uint16_t portno);
	int32_t sendDataAndRetyIfBusy(uint8_t sn, uint8_t * buf, uint16_t len ,uint16_t timeout, uint16_t BusyTimeOut);
	bool  ETH_GetAnyDataOnListenPort(uint8_t sn, uint16_t portno , int8_t *errcode);
	bool  ETH_CheckPackage(uint8_t sn);
	void ETH_SendString (  uint32_t portNo,uint8_t *data, uint16_t size );
	void ETH_ProgramMaster (uint8_t * databuff, uint32_t datasize, uint32_t portNo ) ;
	void ETH_ProgramDevices (uint32_t portNo );
	void ETH_SendProgDeviceStatus ( uint32_t portNo ) ;
	bool ETH_SendDataFromListenPortController (uint8_t *data, uint16_t size ) ;
	void ETH_SendSTX(  uint32_t portNo);
	void ETH_SendETX(  uint32_t portNo);
	void ETH_RecordCommands (void );
	void ETH_DeleteCommands (void );
	void ETH_ProgramController (uint8_t * databuff, uint32_t datasize, uint32_t portNo ) ;
	void ETH_MasterFunc (void);
	void ETH_ReadMasterUpdateDisp (masterDispReg_HandleTypeDef	*guncellenecekDispRegPtr, masterReadRegs_HandleTypeDef *okunacakMasterlarPtr );
	bool ETH_getNextMasterAddress ( masterReadRegs_HandleTypeDef *masterbuff, uint8_t *adresPtr);
	int32_t  ETH_socketCommReceive (uint8_t sn, fifo8_t* buf, uint16_t sentsize, uint8_t* destip, uint16_t destport, uint8_t *socketStatus);
	bool ETH_connectToMaster ( masterReadRegs_HandleTypeDef *masterPtr, uint8_t socketNo );
	bool ETH_checkEmptySocket (uint8_t *sockNoPtr);
	bool  ETH_checkSocketClosed (uint8_t sn);	
	bool ETH_commInProgress (void);
	void ETH_readMasters (masterReadRegs_HandleTypeDef *okunacakMasterlarPtr, uint16_t *freeLot);
	bool ETH_GetDataFromRemoteIP(uint8_t *RemoteIP, uint16_t RemotePort, uint8_t *senddata, uint8_t *returndata, uint8_t *retsize, uint16_t timeout);
	bool  ETH_CheckConnectPortClosed(uint16_t timeout, uint16_t sn);
	void ETH_closeAllSocket (void) ;
	int32_t  ETH_socketComm (uint8_t sn, uint8_t* buff, uint16_t sentsize, uint8_t* destip, uint16_t destport, uint8_t *socketStatus) ;
	void ETH_addFreeLot ( masterReadRegs_HandleTypeDef *okunacakMasterlarPtr, ETH_SocketBuff_Typedef * buffPtr, uint16_t *freeLot );
  bool ETH_getPackageFromFifo (fifo8_t *fifoPtr, uint8_t *data, uint16_t *datasize) ;
	bool ETH_setDisplayReg (masterDispReg_HandleTypeDef	*guncellenecekDispRegPtr , uint16_t freeLot);
	
	extern osPoolId	mpoolSensor;
	extern osMessageQId  MsgBoxSensor;
	
	extern osPoolId	mpoolDisplay;
	extern osMessageQId  MsgBoxDisplay;	
	
	extern osPoolId	mpoolReadSensor;
	extern osMessageQId  MsgBoxReadSensor;
	
	extern osPoolId	mpoolReadDisplay;
	extern osMessageQId  MsgBoxReadDisplay;		
	
	
#ifdef __cplusplus
}
#endif

#endif /* ETHERNET_H_ */

