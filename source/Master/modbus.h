#ifndef MODBUS_H_
#define MODBUS_H_


#include "cmsis_os.h"
#include "chip.h"
#include "crc.h"
#include "RS485.h"



#ifdef __cplusplus
extern "C"
{
#endif

	#define MAX_MODBUS_ADDRESS	247
	
	typedef enum {
		haberlesmeAyar_8N2 = 0,
		haberlesmeAyar_8E1 = 1,
		haberlesmeAyar_8O1 = 2
	}haberlesmeAyarType;
	
	bool modbusWriteHoldingReg(LPC_USART_T *port, uint8_t devAdres, uint16_t regAdres, uint16_t data);
	bool modbusReadHoldingReg(LPC_USART_T *port, uint8_t devAdres, uint16_t regAdres, uint16_t *data, uint16_t waittimeout);

#ifdef __cplusplus
}
#endif

#endif /* MODBUS_H_ */
