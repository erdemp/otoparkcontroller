
#include "convertions.h"

/*******************************************************************************
* @brief				Bu fonksiyon double bir degiskeni int32_t degiskene yuvarlatma
*								islemi yaparak atar
* @param[in]    none
* @return				none
*******************************************************************************/
int32_t parse_doubleToint32(double d){
	int32_t ret;
	
	ret = (int32_t)d;
	if(d>=0){
		if((d-(double)ret)>=0.5){
			ret++;
		}
	}else{
		if((d-(double)ret)<=-0.5){
			ret--;
		}
	}
	return ret;
}

//-----------------------------------------------------------------------
// 	  itoa
// integer sayiyi ascii char dizisine atar.
//-----------------------------------------------------------------------
void itoa32(int32_t n, char *ret) {
    uint16_t numChars = 0;
    // Determine if integer is negative
    uint8_t isNegative = 0;
	  uint32_t temp;
	  int i;
	
    if (n < 0) {
        n = -n;
        isNegative = 1;
        //numChars++;
    }
    // Count how much space we will need for the string
    temp = n;
    do {
        numChars++;
        temp /= 10;
    } while ( temp );
    //Allocate space for the string (1 for negative sign, 1 for each digit, and 1 for null terminator)
    //ret = new char[ numChars + 1 ];
		if (isNegative){
			numChars++;
			ret[0] = '-';
		}
		ret[numChars]=0;
    // Copy digits to string in reverse order
    i = numChars - 1;
    do {
        ret[i--] = n%10 + '0';
        n /= 10;
    } while (n);
}
//-----------------------------------------------------------------------
// 	  itoa
// integer sayiyi ascii char dizisine atar.
//-----------------------------------------------------------------------
uint32_t atoi32(char *str) {
	uint8_t basamak=0;
	uint32_t i;
	uint32_t k;
	uint32_t ret=0;
	while(str[basamak]!=0){
		if((str[basamak]<'0')&&(str[basamak]>'9')){
			return 0;
		}
		basamak++;
	}
	i=1;
	while(basamak>0){
		k=str[basamak-1]-'0';
		k*=i;
		ret+=k;
		i*=10;
		basamak--;
	}
	return ret;
}

/*******************************************************************************
* @brief				Bu fonk. double turden bir stringin nokta yerini ve int karsiligini
*								girilen degiskenlere atar
* @param[in]    str: string
* @return				num: stringin noktasiz int karsiligi
*								dotPos: noktanin yeri
*******************************************************************************/
void stringToIntDotPos(char *str, uint32_t *num, uint8_t *dotPos){
	char buf[20];
	uint8_t i=0;
	uint8_t k=0;
	while(*str!=0){
		if(*str=='.'){
			str++;
			k=i;
		}else if((*str<'0')&&(*str>'9')){
			*num=0;
			*dotPos=0;
			return;
		}else{
			buf[i]=*str;
			i++;
			if(i>=20-1){
				*num=0;
				*dotPos=0;
				return;
			}
			str++;
		}
	}
	if(k==0){
		*dotPos=0;
	}else{
		*dotPos=i-k;
	}
	buf[i]=0;
	*num=atoi32(buf);
}

/*******************************************************************************
* @brief				Bu fonk. integer sayiyi ve bu sayida konulmak istenen nokta yerini
*								stringe atar
* @param[in]    num: stringin noktasiz int karsiligi
*								dotPos: noktanin yeri
* @return				str: string
*								
*******************************************************************************/
void intDotPosToStr(int32_t num, uint8_t dotPos, char *str){
	char buf[20];
	uint8_t basamakSayisi=0;
	uint8_t i=0;
	int8_t k;
	int8_t m;
	bool negative=false;
	
	if(num<0){
		negative=true;
		num=~num;
		num++;
	}
	itoa32((int32_t)num, buf);
	
	while(buf[i]>0){
		i++;
		basamakSayisi++;
	}
	
	
	
	if(dotPos==0){
		i=0;
		k=0;
		if(negative){
			str[k]='-';
			k++;
		}
		while(basamakSayisi>0){
			str[k]=buf[i];
			i++;
			k++;
			basamakSayisi--;
		}
		str[k]=0;
	}else if((int8_t)(dotPos + 1 - basamakSayisi)<=0){
		k=1;
		i=0;
		if(negative){
			m=basamakSayisi+1;
		}else{
			m=basamakSayisi;
		}
		str[m+1]=0;
		do{
			if(negative){
				if(m==0){
					str[m]='-';
				}else{
					str[m]=buf[m-1-k];
				}
			}else{
				str[m] = buf[m-k];
			}
			m--;
			i++;
			if(i==dotPos){
				str[m]='.';
				k=0;
				m--;
			}
		}while(m>=0);
	}else{
		m=basamakSayisi-1;
		i=0;
		k=dotPos + 1;
		if(negative){
			k++;
			str[k+1]=0;
		}else{
			str[k+1]=0;
		}
		do{
			if(m>=0){
				str[k]=buf[m];
				m--;
			}else{
				str[k]='0';
			}
			k--;
			i++;
			if(i==dotPos){
				str[k]='.';
				k--;
			}
			if(k==0){
				if(negative){
					str[k]='-';
					k--;
				}
			}
		}while(k>=0);
	}

	
}

/*******************************************************************************
* @brief				Bu fonk. integer sayiyi ve bu sayida konulmak istenen nokta yerini
*								stringe atar
* @param[in]    num: stringin noktasiz int karsiligi
*								dotPos: noktanin yeri
* @return				str: string
*								
*******************************************************************************/
void intDotPosToStrWithStartDigit(int32_t num, uint8_t dotPos, char *str, uint8_t DigitCnt){
	char buf[20];
	uint8_t basamakSayisi=0;
	uint8_t i=0;
	int8_t k;
	int8_t m;
	bool negative=false;
	
	if(num<0){
		negative=true;
		num=~num;
		num++;
	}
	itoa32((int32_t)num, buf);
	
	while(buf[i]>0){
		i++;
		basamakSayisi++;
	}
	
	
	
	if(dotPos==0){
		i=0;
		k=0;
		if(negative){
			str[k]='-';
			k++;
		}
		while (DigitCnt-basamakSayisi>0) {
			str[k]=' ';
			k++;
			DigitCnt--;
		}
		while(basamakSayisi>0){
			str[k]=buf[i];
			i++;
			k++;
			basamakSayisi--;
		}
		str[k]=0;
	}else if((int8_t)(dotPos + 1 - basamakSayisi)<=0){
		k=1;
		i=0;
		if(negative){
			m=basamakSayisi+1;
		}else{
			m=basamakSayisi;
		}
		str[m+1]=0;
		do{
			if(negative){
				if(m==0){
					str[m]='-';
				}else{
					str[m]=buf[m-1-k];
				}
			}else{
				str[m] = buf[m-k];
			}
			m--;
			i++;
			if(i==dotPos){
				str[m]='.';
				k=0;
				m--;
			}
		}while(m>=0);
	}else{
		m=basamakSayisi-1;
		i=0;
		k=dotPos + 1;
		if(negative){
			k++;
			str[k+1]=0;
		}else{
			str[k+1]=0;
		}
		do{
			if(m>=0){
				str[k]=buf[m];
				m--;
			}else{
				str[k]='0';
			}
			k--;
			i++;
			if(i==dotPos){
				str[k]='.';
				k--;
			}
			if(k==0){
				if(negative){
					str[k]='-';
					k--;
				}
			}
		}while(k>=0);
	}

	
}
/*******************************************************************************
* @brief				Bu fonk. integer sayiyi ve hex notasyonunda stringe cevirir
* @param[in]    num: integer sayi
* @return				str: string
*								
*******************************************************************************/
char *intToHexString(uint16_t num, char *str){
	uint8_t buf[11];
	uint8_t bufStr[11];
	uint8_t i=0;
	uint8_t k=0;
	uint8_t kalan;
	uint16_t bolum;
	
	for(i=0;i<11;i++){
		buf[i]=0;
		bufStr[i]=0;
	}
	
	i=0;
	do{
		kalan=num%16;
		bolum=num/16;
		buf[i]=kalan;
		i++;
		num=bolum;
	}while(bolum>0);
	
	for(i=11;i>0;i--){
		if(buf[i-1]!=0){
			//i=i-1;
			break;
		}
	}
	
	while(i>0){
		if(buf[i-1]<=9){
			bufStr[k]=buf[i-1]+'0';
		}else if((buf[i-1]>=10)&&(buf[i-1]<=15)){
			bufStr[k]=buf[i-1]-10+'A';
		}
		i--;
		k++;
	}
	
	strcpy(str,(const char *)bufStr);
	return str;
	
}


