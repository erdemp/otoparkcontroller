#include "device.h"

//#define MAX_PORT_DEVICE_CNT	128

 deviceType port1Devices[MAX_PORT_DEVICE_CNT];
 uint8_t port1DeviceCount=0;
 
 deviceType port2Devices[MAX_PORT_DEVICE_CNT];
 uint8_t port2DeviceCount=0;
 
 deviceType port3Devices[MAX_PORT_DEVICE_CNT];
 uint8_t port3DeviceCount=0;
 
 deviceType port4Devices[MAX_PORT_DEVICE_CNT];
 uint8_t port4DeviceCount=0;
 
// #define MAX_INT_REGS_CNT 32
// uint16_t sensorInternalRegisters[MAX_INT_REGS_CNT];
 
 sensGrupReg sensorGrupRegPort1[SENSORGRUPSAYISI];
 sensGrupReg sensorGrupRegPort2[SENSORGRUPSAYISI];
 sensGrupReg sensorGrupRegPort3[SENSORGRUPSAYISI];
 sensGrupReg sensorGrupRegPort4[SENSORGRUPSAYISI];
 dispCommandReg displayCommandRegPort1[DISPLAYCOMMANDREGSAYISI];
 dispCommandReg displayCommandRegPort2[DISPLAYCOMMANDREGSAYISI];
 dispCommandReg displayCommandRegPort3[DISPLAYCOMMANDREGSAYISI];
 dispCommandReg displayCommandRegPort4[DISPLAYCOMMANDREGSAYISI];
 
 MasterGeneralReg	Master;

 void sensorControl(uint8_t adres);
// Bool sensorBilgisiAl(LPC_USART_T *port, uint8_t adres, sensorType *Sptr);
 void displayBilgisiAl(LPC_USART_T *port, uint8_t adres);
 void displayControl(uint8_t adres);
 void sensorCheckClear(void);
 Bool sensorGrupKontrol(uint8_t ledPort, uint8_t ledAdres);
 

/***********************************************************************
 * @brief 	sistemdeki cihazlarin taramasini yapar
 * @param		
 * @return		-
 ***********************************************************************/
 void cihazTaramasi(void){
uint8_t adres=1;
osEvent evt;	 
	 
	 clearDeviceRegs();	
	 
   Master.searchedDeviceRatio=0;
	 Master.status = MASTERSTAT_DEVICESEARCH;
	
	 
	// SaveAllParameterToFlash();
	 
	 
	 port1DeviceCount=0;
	 port2DeviceCount=0;
	 port3DeviceCount=0;
	 port4DeviceCount=0;
	 
	 for(adres=1; adres<=MAX_MODBUS_ADDRESS; adres++){
		 evt = osSignalWait(STOPDEVICESEARCH_SIGNAL, 0);
		 if (evt.status == osEventSignal)  {
			if(evt.value.signals&STOPDEVICESEARCH_SIGNAL){
					break;
			}
		 }
		 sensorControl(adres);
		 displayControl(adres);
		 Master.searchedDeviceRatio = adres*100 / MAX_MODBUS_ADDRESS;
		 //osDelay(5);
	 }
	// display 5 no da idi. arada bir sensor bulamadi. display i 4 no da buldu. ama 5 no yine duruyor. taramadan once silinsin mi hersey
	//---------------------	
//	SaveDeviceRegsToRAM ();
//  SaveDisplayCommandRegsToRAM ();
//	__disable_irq();	
//	while( flashWrite()==0);
//	__enable_irq();
//	//---------------------

	 SaveAllParameterToFlash();
	 
	 Master.status = MASTERSTAT_EMPTY;
	 
 }
  /***********************************************************************
 * @brief 	adresi girilen sensorun sistemde olup olmadigina bakar
 * @param		adres: kontrol edilecek adres
 * @return		-
 ***********************************************************************/
 
 /***********************************************************************
 * @brief 	adresi girilen sensorun sistemde olup olmadigina bakar
 * @param		adres: kontrol edilecek adres
 * @return		-
 ***********************************************************************/
 void sensorControl(uint8_t adres){
	 uint16_t i16;
	 uint8_t var=0;
	 
	 while ( var < 2)  {
		 if(sensorMesafeOku(RS485_Port_1, adres, &i16)){
				 sensorBilgisiAl(RS485_Port_1, adres);
				 var=2;
		 }
		 var++;
//		 osDelay(15);
		}
	 
		var=0;
		while ( var < 2)  {
			if(sensorMesafeOku(RS485_Port_2, adres, &i16)){
				sensorBilgisiAl(RS485_Port_2, adres);
				var=2;
			}
			var++;
			//osDelay(15);
		}
		
		var=0;
		while ( var < 2)  {
			if(sensorMesafeOku(RS485_Port_3, adres, &i16)){
				sensorBilgisiAl(RS485_Port_3, adres);
				var=2;
			}
			var++;
			//osDelay(15);
		}
		
		var=0;
		while ( var < 2 ) {
			if(sensorMesafeOku(RS485_Port_4, adres, &i16)){
				sensorBilgisiAl(RS485_Port_4, adres);
			}
			var++;
			//osDelay(15);
		}
		
 }
 
  /***********************************************************************
 * @brief 	adresi girilen ve portu girilen sensore iliskin ilgili bilgileri
 *					alir ve cihaz listesine kaydeder
 * @param		port: cihazin bulundugu port
 *					adres: kontrol edilecek cihaz adres
 * @return		-
 ***********************************************************************/
void sensorBilgisiAl(LPC_USART_T *port, uint8_t adres ){
sensorType *sensor;
uint16_t limitVal=0;
sensorModType sensMode;
//Bool retVal=FALSE;
	 
	 if(port == RS485_Port_1){
		if(port1DeviceCount<MAX_PORT_DEVICE_CNT){
			port1Devices[port1DeviceCount].device = devSENSOR;
			port1Devices[port1DeviceCount].deviceAddress = adres;
	    sensor = (sensorType *)(&port1Devices[port1DeviceCount].device);
			sensor->deviceAddress = adres;
			sensor->device = devSENSOR;
			if ( sensorModuOku (RS485_Port_1,adres, &sensMode) == TRUE ) {
				sensor->workingMode = sensMode;
			}else {
				sensor->workingMode = 0xFF;
			}
			osDelay(20);
			if ( sensorLimitDegerOku (RS485_Port_1,adres, &limitVal) == TRUE ) {
				sensor->LimitVal = limitVal;
			}else {
				sensor->LimitVal = 0xFF;
			}
			port1DeviceCount++;
		}
	 }else if(port == RS485_Port_2){
		 if(port2DeviceCount<MAX_PORT_DEVICE_CNT){
			port2Devices[port2DeviceCount].device = devSENSOR;
			port2Devices[port2DeviceCount].deviceAddress = adres;
			sensor = (sensorType *)(&port2Devices[port2DeviceCount].device);
			sensor->deviceAddress = adres;
			sensor->device = devSENSOR;
			if ( sensorModuOku (RS485_Port_2,adres, &sensMode) == TRUE ) {
				sensor->workingMode = sensMode;
			}else {
				sensor->workingMode = 0xFF;
			}
			osDelay(20);
			if ( sensorLimitDegerOku (RS485_Port_2,adres, &limitVal) == TRUE ) {
				sensor->LimitVal = limitVal;
			}else {
				sensor->LimitVal = 0xFF;
			}
			port2DeviceCount++;
		 }
	 }else if(port == RS485_Port_3){
		 if(port3DeviceCount<MAX_PORT_DEVICE_CNT){
			port3Devices[port3DeviceCount].device = devSENSOR;
			port3Devices[port3DeviceCount].deviceAddress = adres;
			sensor = (sensorType *)(&port3Devices[port3DeviceCount].device);
			sensor->deviceAddress = adres;
			sensor->device = devSENSOR;
			if ( sensorModuOku (RS485_Port_3,adres, &sensMode) == TRUE ) {
				sensor->workingMode = sensMode;
			}else {
				sensor->workingMode = 0xFF;
			}
			osDelay(20);
			if ( sensorLimitDegerOku (RS485_Port_3,adres, &limitVal) == TRUE ) {
				sensor->LimitVal = limitVal;
			}else {
				sensor->LimitVal = 0xFF;
			}
			port3DeviceCount++;
		 }
	 }else if(port == RS485_Port_4){
		 if(port4DeviceCount<MAX_PORT_DEVICE_CNT){
			port4Devices[port4DeviceCount].device = devSENSOR;
			port4Devices[port4DeviceCount].deviceAddress = adres;
			sensor = (sensorType *)(&port4Devices[port4DeviceCount].device);
			sensor->deviceAddress = adres;
			sensor->device = devSENSOR;
			if ( sensorModuOku (RS485_Port_4,adres, &sensMode) == TRUE ) {
				sensor->workingMode = sensMode;
			}else {
				sensor->workingMode = 0xFF;
			}
			osDelay(20);
			if ( sensorLimitDegerOku (RS485_Port_4,adres, &limitVal) == TRUE ) {
				sensor->LimitVal = limitVal;
			}else {
				sensor->LimitVal = 0xFF;
			}			 
			port4DeviceCount++;
		 }
	 }
	 
 }
 
  /***********************************************************************
 * @brief 	adresi girilen ve portu girilen sensore iliskin ilgili bilgileri
 *					alir ve cihaz listesine kaydeder
 * @param		port: cihazin bulundugu port
 *					adres: kontrol edilecek cihaz adres
 * @return		-
 ***********************************************************************/
Bool readSensorParameter( uint8_t port, uint8_t adres, sensorType *Sptr ){
//sensorType *sensor;
uint16_t limitVal=0;
sensorModType sensMode;
LPC_USART_T *portNo;	
uint8_t durum=0;
Bool retVal=FALSE;
	 	 
	
		if(port == 1){
			portNo = RS485_Port_1;			
		}else if  (port == 2) {
			portNo = RS485_Port_2;			
		}else if (port == 3) {
			portNo = RS485_Port_3;			
		}else if (port == 4) {
			portNo = RS485_Port_4;			
		}
	 
		if ( sensorModuOku (portNo,adres, &sensMode) == TRUE ) {
			Sptr->workingMode = sensMode;
			osDelay(40);
			if ( sensorLimitDegerOku (portNo,adres, &limitVal) == TRUE ) {
				Sptr->LimitVal = limitVal;
				osDelay(40);
				if ( sensorDurum( portNo,adres, &durum) == TRUE ) {
					Sptr->Dolu=durum;
					retVal = TRUE;
				}
			}else {
				Sptr->LimitVal = 0xFF;
			}			
						
		}else {
			Sptr->workingMode = 0xFF;
		}
		
	 return retVal;	 
 
}  
  /***********************************************************************
 * @brief 	adresi girilen displayin sistemde olup olmadigina bakar
 * @param		adres: kontrol edilecek adres
 * @return		-
 ***********************************************************************/
 void displayControl(uint8_t adres){
	 uint16_t i16;
	 uint8_t var=0;
	 
	 var=0;
	 while (var < 2) { 
		 if(displaySayiOku(RS485_Port_1, adres, &i16)){
				 displayBilgisiAl(RS485_Port_1, adres);
			 var=2;
		 }
		 var++;
		 osDelay(5);
		}
	 
		var=0;
		while (var < 2) { 
			if(displaySayiOku(RS485_Port_2, adres, &i16)){
				displayBilgisiAl(RS485_Port_2, adres);
				var=2;
			}
			var++;
			osDelay(5);
		}
		
		var=0;
		while (var < 2) { 
			if(displaySayiOku(RS485_Port_3, adres, &i16)){
			 displayBilgisiAl(RS485_Port_3, adres);
				var=2;
			}
			var++;
			osDelay(5);
		}
		
		var=0;
		while (var < 2) { 
			if(displaySayiOku(RS485_Port_4, adres, &i16)){
				displayBilgisiAl(RS485_Port_4, adres);
				var=2;
			}
			var++;
			osDelay(5);
		}
	 
 }
 
  /***********************************************************************
 * @brief 	adresi girilen ve portu girilen display'e iliskin ilgili bilgileri
 *					alir ve cihaz listesine kaydeder
 * @param		port: cihazin bulundugu port
 *					adres: kontrol edilecek cihaz adres
 * @return		-
 ***********************************************************************/
 void displayBilgisiAl(LPC_USART_T *port, uint8_t adres){
displayType *display;
	 
	 if(port == RS485_Port_1){
		 if(port1DeviceCount<MAX_PORT_DEVICE_CNT){
			port1Devices[port1DeviceCount].device = devDISPLAY;
			port1Devices[port1DeviceCount].deviceAddress = adres;
			display = (displayType *)(&port1Devices[port1DeviceCount].device);
			display->deviceAddress = adres;
			display->device = devDISPLAY;;
			(void)getDisplayParameters (RS485_Port_1,adres,display);
			port1DeviceCount++;
			
		 }
	 }else if(port == RS485_Port_2){
		 if(port2DeviceCount<MAX_PORT_DEVICE_CNT){
			port2Devices[port2DeviceCount].device = devDISPLAY;
			port2Devices[port2DeviceCount].deviceAddress = adres;
			display = (displayType *)(&port2Devices[port2DeviceCount].device);
			display->deviceAddress = adres;
			display->device = devDISPLAY;;
			(void) getDisplayParameters (RS485_Port_2,adres,display);
			port2DeviceCount++;
		 }
	 }else if(port == RS485_Port_3){
		 if(port3DeviceCount<MAX_PORT_DEVICE_CNT){
			port3Devices[port3DeviceCount].device = devDISPLAY;
			port3Devices[port3DeviceCount].deviceAddress = adres;
			display = (displayType *)(&port3Devices[port3DeviceCount].device);
			display->deviceAddress = adres;
			display->device = devDISPLAY;;
			(void)getDisplayParameters (RS485_Port_2,adres,display);
			port3DeviceCount++;
		 }
	 }else if(port == RS485_Port_4){
		 if(port4DeviceCount<MAX_PORT_DEVICE_CNT){
			port4Devices[port4DeviceCount].device = devDISPLAY;
			port4Devices[port4DeviceCount].deviceAddress = adres;
			display = (displayType *)(&port4Devices[port4DeviceCount].device);
			display->deviceAddress = adres;
			display->device = devDISPLAY;;
			(void)getDisplayParameters (RS485_Port_2,adres,display);
			port4DeviceCount++;
		 }
	 }
 }
  /***********************************************************************
 * @brief 	displaye ait onemli parametreleri okur.  parlaklik, zerochar, vs..
 * 					
 * @param		-
 *				
 * @return		-
 ***********************************************************************/
 Bool getDisplayParameters (LPC_USART_T *port, uint8_t adres , displayType *disp) {
displayZeroChar_Type zeroChar;
dispayOrientation_Type orientation;
uint16_t temp;	
Bool retVal = FALSE;	 

	 
	 if ( displayZeroCharOku (port,adres, &zeroChar ) == FALSE ) {
		 disp->firstChar = 0xff;
	 }else {
		 disp->firstChar = zeroChar;
		 	 if ( displayYonOku (port,adres, &orientation ) == FALSE ) {
				 disp->inverted = 0xff;
			 }else {
				 disp->inverted = zeroChar;
					if ( displayComTimeoutOku (port,adres, &temp) == FALSE ) {
						disp->HaberlesmeGecikmesi = 0xff;
					}else {
						disp->HaberlesmeGecikmesi = temp;
  					if ( displayParlaklikOku (port,adres, &temp) == FALSE ) {
							disp->Parlaklik = 0xff;
						}else {
							disp->Parlaklik = temp;
							if ( displayOkYonuOku (port,adres, &temp) == FALSE ) {
								disp->OkYonu = 0xff;
							}else {
								disp->OkYonu = temp;
								retVal = TRUE;
							}
						}
					}
			 }
	 }
	 
	return retVal;
	 
 }
 /***********************************************************************
 * @brief 	display device registerlarini tek tek kontrol eder. her display adresini 
 * 					sensor command registarlarinda arar. eger bulur ise sensor command registeri i�indeki
 *          value yu sensor device register a yazar ve rs485 �zerinden de display e gonderir.
 * @param		-
 *				
 * @return		-
 ***********************************************************************/
 void displayUpdate (void) {
	 
uint8_t i,k,m;
uint8_t devCnt;
deviceType *portDev;
displayType *display;
LPC_USART_T *port485;
//uint8_t ledGrup;
//uint8_t ledDurum;
//uint8_t ledAdres;
dispCommandReg   *dispcommdReg;	 
	 
	 
	 //Bool grupDurum;
	 Master.status = MASTERSTAT_DISPUPDATE;
	 
	 displayCommandRegPort1[0].val = sensorGrupRegPort1[0].ToplamBosYer;
	 for(k=1; k<=4; k++){
		 if(k == 1){
			 portDev=port1Devices;
			 devCnt=port1DeviceCount;
			 port485=RS485_Port_1;
			 dispcommdReg = displayCommandRegPort1;
		 }else if(k == 2){
			 portDev=port2Devices;
			 devCnt=port2DeviceCount;
			 port485=RS485_Port_2;
			 dispcommdReg = displayCommandRegPort2;
		 }else if(k == 3){
			 portDev=port3Devices;
			 devCnt=port3DeviceCount;
			 port485=RS485_Port_3;
			 dispcommdReg = displayCommandRegPort3;
		 }else if(k == 4){
			 portDev=port4Devices;
			 devCnt=port4DeviceCount;
			 port485=RS485_Port_4;
			 dispcommdReg = displayCommandRegPort4;
		 }
		 for(i=0; i<devCnt; i++){
			 
			
			 if(portDev[i].device != devDISPLAY){
					continue;							// cihaz display degil ise dongu devam
			 }
			 
			 display = (displayType *)(&portDev[i].device);
			 for (m=0;m<DISPLAYCOMMANDREGSAYISI;m++) {
				 if (display->deviceAddress == dispcommdReg[m].adres ) {
					 display->val = dispcommdReg[m].val;
					 displaySayiGoster(port485,display->deviceAddress,display->val);
					 displaySayiGoster(port485,display->deviceAddress,display->val);
					 break;
				 }
			 }
			 
		 }
		 
	 }
	 
	 Master.status = MASTERSTAT_EMPTY;
	 
 }
 
  /***********************************************************************
 * @brief 	Display komut registeri guncelle
 * @param		-
 *				
 * @return		-
 ***********************************************************************/ 
 Bool SetDisplayCommandReg (LPC_USART_T *port,uint8_t adres, uint8_t regNo ) {
		
	  if ( adres > 255 ) {
			return FALSE;
		}
		
		if ( regNo > DISPLAYCOMMANDREGSAYISI ) {
			return FALSE;
		}
		if(port == RS485_Port_1){
			displayCommandRegPort1[regNo].adres = adres;
		} else if(port == RS485_Port_2){
			displayCommandRegPort2[regNo].adres = adres;
		} else if(port == RS485_Port_3){
			displayCommandRegPort3[regNo].adres = adres;
		} else if(port == RS485_Port_4){ 
			displayCommandRegPort4[regNo].adres = adres;
		}
		return TRUE;
}
   /***********************************************************************
 * @brief 	Display komut registeri temizler
 * @param		-
 *				
 * @return		-
 ***********************************************************************/ 
void ClearDisplayCommandRegs (void) {
uint16_t i;
	
	for (i=0;i<DISPLAYCOMMANDREGSAYISI;i++) {
		displayCommandRegPort1[i].adres = 0;
		displayCommandRegPort1[i].val = 0;
	}

	for (i=0;i<DISPLAYCOMMANDREGSAYISI;i++) {
		displayCommandRegPort2[i].adres = 0;
		displayCommandRegPort2[i].val = 0;
	}
	
	for (i=0;i<DISPLAYCOMMANDREGSAYISI;i++) {
		displayCommandRegPort3[i].adres = 0;
		displayCommandRegPort3[i].val = 0;
	}
	
	for (i=0;i<DISPLAYCOMMANDREGSAYISI;i++) {
		displayCommandRegPort4[i].adres = 0;
		displayCommandRegPort4[i].val = 0;
	}
	
}
 /***********************************************************************
 * @brief 	onceden cihaz listesindeki sensorlere iliskin durum verileri
 * 					okunarak guncellenir
 * @param		-
 *				
 * @return		-
 ***********************************************************************/
 void sensorDurumlariniOku(void){
	 uint8_t i,k;
	 uint8_t devCnt;
	 deviceType *portDev;
	 uint8_t sensor_Dolu=0;
	 sensorType *sensor;
	 LPC_USART_T *port;
	 
	 Master.status = MASTERSTAT_SENSORREAD;
	 
	 for(k=1; k<=4; k++){
		 if(k == 1){
			 port=RS485_Port_1;
			 portDev=port1Devices;
			 devCnt=port1DeviceCount;
		 }else if(k == 2){
			 port=RS485_Port_2;
			 portDev=port2Devices;
			 devCnt=port2DeviceCount;
		 }else if(k == 3){
			 port=RS485_Port_3;
			 portDev=port3Devices;
			 devCnt=port3DeviceCount;
		 }else if(k == 4){
			 port=RS485_Port_4;
			 portDev=port4Devices;
			 devCnt=port4DeviceCount;
		 }
		 for(i=0; i<devCnt; i++){
			 if(portDev[i].device == devSENSOR){
				 sensor = (sensorType *)(&portDev[i].device);
				 if(sensorDurum(port, sensor->deviceAddress, &sensor_Dolu)){
						// okuma hatasi yoksa durumu degerlendir
					 sensor->comError=FALSE;	
					 if (sensor_Dolu == TRUE ) {
						 	sensor->Dolu=TRUE;						 
					 }else {
						 	sensor->Dolu=FALSE;
					 }
				 }else {
					 	sensor->comError=TRUE;
						sensor->comErrorCnt++;
				 }
				 osDelay(5);
			 }
		 }
	 }
	 Master.status = MASTERSTAT_EMPTY;
 }
 
  /***********************************************************************
 * @brief 	cihaz listelerindeki sensorlere iliskin mod verileri okunur
 *					eger merkez mod ise iliskili sensor ledi kontrol edilerek
 *					ilgili led yesil ya da kirmizi yakilir
 * @param		-
 *				
 * @return		-
 ***********************************************************************/
 void sensorLedGuncelle(void){
	 uint8_t i,k,m;
	 uint8_t devCnt;
	 deviceType *portDev;
	 sensorType *sensor,*tempsensor;
	 LPC_USART_T *port485;
 	 uint8_t ledGrup;
   uint8_t ledDurum;
	 uint8_t ledAdres;
	 
	 Master.status = MASTERSTAT_SENSORLEDUPDATE;
//	 Bool grupDurum;
	 
	 for(k=1; k<=4; k++){
		 if(k == 1){
			 portDev=port1Devices;
			 devCnt=port1DeviceCount;
			 port485=RS485_Port_1;
		 }else if(k == 2){
			 portDev=port2Devices;
			 devCnt=port2DeviceCount;
			 port485=RS485_Port_2;
		 }else if(k == 3){
			 portDev=port3Devices;
			 devCnt=port3DeviceCount;
			 port485=RS485_Port_3;
		 }else if(k == 4){
			 portDev=port4Devices;
			 devCnt=port4DeviceCount;
			 port485=RS485_Port_4;
		 }
		 
		 for(i=0; i<devCnt; i++){
			 
			
			 if(portDev[i].device != devSENSOR){
					continue;							// cihaz sens�r degil ise dongu devam
			 }
			 
			 sensor = (sensorType *)(&portDev[i].device);
			 if(sensor->workingMode==0){
				 sensor->checked=TRUE;				//merkez kontroll� mod degil ise dongu devam.
				 continue;
			 }
			 
			 if(sensor->checked){
				 continue;										// daha once kontrol edildi ise dongu devam. 
			 }
			 
			 
			 ledGrup = sensor->LEDGrubu;
			 sensor->checked=TRUE;
			 if (sensor->Dolu == TRUE ) {
				 ledDurum = 1;
			 }else {
				 ledDurum = 0;
			 }
			 if (sensor->LEDbagli == TRUE ) {
				 ledAdres = sensor->deviceAddress;
			 }
			 
			 for (m=i;m<devCnt;m++) {
				 tempsensor = (sensorType *)(&portDev[m].device);
				if (tempsensor->LEDGrubu == ledGrup) {
						tempsensor->checked = TRUE;
						if (tempsensor->LEDbagli == TRUE ) {
							ledAdres = tempsensor->deviceAddress;
						}
					  if ( tempsensor->Dolu == TRUE ) {
							ledDurum &= 1;
						}else {
							ledDurum &= 0;
						}

				}					
				 
			 }
			 
			 if (ledDurum == 1 ) {
				 sensorLed_G_Yak(port485, ledAdres, 0);
				 sensorLed_R_Yak(port485, ledAdres, 1);
			 }else {
				 sensorLed_R_Yak(port485, ledAdres, 0);
				 sensorLed_G_Yak(port485, ledAdres, 1);
			 }
			 
			 
//			 grupDurum=sensorGrupKontrol(sensor[i].ledPort, sensor[i].ledAdres);
//			 
//			 if(sensor[i].ledPort==1){
//				 port=RS485_Port_1;
//			 }else if(sensor[i].ledPort==2){
//				 port=RS485_Port_2;
//			 }else if(sensor[i].ledPort==3){
//				 port=RS485_Port_3;
//			 }else{
//				 port=RS485_Port_4;
//			 }
//			 if(grupDurum){
//				 sensorLed_G_Yak(port, sensor[i].ledAdres, 0);
//				 sensorLed_R_Yak(port, sensor[i].ledAdres, 1);
//			 }else{
//				 sensorLed_R_Yak(port, sensor[i].ledAdres, 0);
//				 sensorLed_G_Yak(port, sensor[i].ledAdres, 1);
//			 }

		 }
	 }
	 sensorCheckClear();
	 
	 Master.status = MASTERSTAT_EMPTY;
	 
 }
 
    /***********************************************************************
 * @brief 	Ilgili RS485 portu i�in master kendi i�indeki sens�r registerlarini okuyarak
 *					sens�r grup registerlarini update eder.  Ilgili rup taki sens�rlerin dolu bos
 *				  bilgilerini toplayarak grup registerina yazar.
 * @param		rs485Port: 
 *					
 *				
 * @return		-
 ***********************************************************************/
 void sensorGrupRegUpdate(void){
	deviceType *portDev; 
	uint8_t k;
	uint16_t i;
	uint16_t grupNO, devCnt; 
	sensorType *sensor;
	sensGrupReg *sensorGrupPort;
	sensGrupReg  tempGrupData[SENSORGRUPSAYISI];
	 
	Master.status = MASTERSTAT_SENSORGRUPREGUPDATE;
	 
	 for(k=1; k<=4; k++){
		 
		 for (i=0;i<SENSORGRUPSAYISI;i++) {
			tempGrupData[i].ToplamBosYer=0;
			tempGrupData[i].ToplamSensor=0;
		 }
			 
		 if(k == 1){
			 portDev=port1Devices;
			 devCnt=port1DeviceCount;
			 sensorGrupPort = sensorGrupRegPort1;
		 }else if(k == 2){
			 portDev=port2Devices;
			 devCnt=port2DeviceCount;
			 sensorGrupPort = sensorGrupRegPort2;
		 }else if(k == 3){
			 portDev=port3Devices;
			 devCnt=port3DeviceCount;
			 sensorGrupPort = sensorGrupRegPort3;
		 }else if(k == 4){
			 portDev=port4Devices;
			 devCnt=port4DeviceCount;
			 sensorGrupPort = sensorGrupRegPort4;
		 }
		 for(i=0; i<devCnt; i++){
			 
			 	if(portDev[i].device != devSENSOR){
				continue;
				}
				sensor = (sensorType *)(&portDev[i].device);
			  //if(sensor[i].checked){
				// continue;
			  //}
				if (sensor->GrupNo <= SENSORGRUPSAYISI ) {
					grupNO = sensor->GrupNo;
					tempGrupData[grupNO-1].ToplamSensor ++;
						if (sensor->Dolu == FALSE) {
							tempGrupData[grupNO-1].ToplamBosYer ++;
						}
				}					
		 }
		 
		 for (i=0;i<SENSORGRUPSAYISI;i++) {
				sensorGrupPort[i].ToplamBosYer = tempGrupData[i].ToplamBosYer;
				sensorGrupPort[i].ToplamSensor = tempGrupData[i].ToplamSensor;
		 }
		 
	 }
	 
	 Master.status = MASTERSTAT_EMPTY;
 }
 

    /***********************************************************************
 * @brief 	sensor grup registerlarinin i�erigini tamamen temizler
 *					
 * @param		rs485Port: 
 *					
 *				
 * @return		-
 ***********************************************************************/
 void ClearSensorGrupRegs(void){
//	deviceType *portDev; 
//	uint8_t k;
	uint16_t i;
//	uint16_t grupNO, devCnt; 
//	sensorType *sensor;
	//sensGrupReg *sensorGrupPort;
	//sensGrupReg  tempGrupData[SENSORGRUPSAYISI];
	 

	 for (i=0;i<SENSORGRUPSAYISI;i++) {
		sensorGrupRegPort1[i].ToplamBosYer=0;
		sensorGrupRegPort1[i].ToplamSensor=0;
	 }
	 
	for (i=0;i<SENSORGRUPSAYISI;i++) {
		sensorGrupRegPort2[i].ToplamBosYer=0;
		sensorGrupRegPort2[i].ToplamSensor=0;
	 }

	 for (i=0;i<SENSORGRUPSAYISI;i++) {
		sensorGrupRegPort3[i].ToplamBosYer=0;
		sensorGrupRegPort3[i].ToplamSensor=0;
	 }	 
	 
	 for (i=0;i<SENSORGRUPSAYISI;i++) {
		sensorGrupRegPort4[i].ToplamBosYer=0;
		sensorGrupRegPort4[i].ToplamSensor=0;
	 }
	 
 }
 
 /***********************************************************************
 * @brief 	SetSengorGroup : ilgili sensor�n hangi grup ta olacagi bilgisi sensor datasinin i�ine yazilir.
 *
 * @param		port : sensorun bulundugu rs485 portu 
 *					adres : sensor adres
 *					grup no : sensorun ait oldugu grup
 *				
 * @return		-
 ***********************************************************************/
 Bool SetSensorGroup (LPC_USART_T *port,uint8_t adres, uint8_t group ) {
 deviceType *portDev; 	
 uint16_t i, devCnt;  
sensorType *sensor;

	 if(port == RS485_Port_1){
			portDev=port1Devices;
			devCnt=port1DeviceCount;
	 }else if(port == RS485_Port_2){
		 	portDev=port2Devices;
			devCnt=port2DeviceCount;
	 }else if(port == RS485_Port_3) {
		 	portDev=port3Devices;
			devCnt=port3DeviceCount;
	 }else if(port == RS485_Port_4){
			portDev=port4Devices;
			devCnt=port4DeviceCount;
	 }else {
		 return FALSE;
	 }
	 
	 for(i=0; i<devCnt; i++){
	 
		if(portDev[i].device != devSENSOR){
		continue;
		}
		sensor = (sensorType *)(&portDev[i].device);

		if (sensor->deviceAddress == adres ) {
			sensor->GrupNo= group;
			
		}
			
 }
		return TRUE;
 }
   /***********************************************************************
 * @brief 	led portu ve adresi verilen sensor gruplarinin durumlarini
 *					kontrol eder ve grupun dolu veya bos oldugunu belirler
 * @param		ledPort: aranacak led portu
 *					ledAdres: aranacak led adresi
 *				
 * @return		-
 ***********************************************************************/
 Bool sensorGrupKontrol(uint8_t ledPort, uint8_t ledAdres){
//	 uint8_t i,k;
//	 uint8_t devCnt;
//	 deviceType *portDev;
//	 sensorType *sensor;

	 Bool grupDurum=TRUE;
//	 
//	 for(k=1; k<=4; k++){
//		 if(k == 1){
//			 portDev=port1Devices;
//			 devCnt=port1DeviceCount;
//		 }else if(k == 2){
//			 portDev=port2Devices;
//			 devCnt=port2DeviceCount;
//		 }else if(k == 3){
//			 portDev=port3Devices;
//			 devCnt=port3DeviceCount;
//		 }else if(k == 4){
//			 portDev=port4Devices;
//			 devCnt=port4DeviceCount;
//		 }
//		 for(i=0; i<devCnt; i++){
//			 if(portDev[i].device != devSENSOR){
//					continue;
//			 }
//			 sensor = (sensorType *)(&portDev[i]);
//			 if(sensor[i].checked){
//				 continue;
//			 }
//			 if(sensor[i].workingMode==0){
//				 sensor[i].checked=TRUE;
//				 continue;
//			 }
//			 if(sensor[i].ledAdres!=ledAdres){
//				 continue;
//			 }
//			 if(sensor[i].ledPort!=ledPort){
//				 continue;
//			 }
//			 
//			 grupDurum&=sensor[i].durum;
//			 sensor[i].checked=TRUE;
//		 }
//	 }
	 return grupDurum;
	 
 }
 
    /***********************************************************************
 * @brief 	sensor islem check'ini clear yapar
 * @param		-
 *				
 * @return		-
 ***********************************************************************/
 void sensorCheckClear(void){
	 uint8_t i,k;
	 uint8_t devCnt;
	 deviceType *portDev;
	 sensorType *sensor;


	 
	 for(k=1; k<=4; k++){
		 if(k == 1){
			 portDev=port1Devices;
			 devCnt=port1DeviceCount;
		 }else if(k == 2){
			 portDev=port2Devices;
			 devCnt=port2DeviceCount;
		 }else if(k == 3){
			 portDev=port3Devices;
			 devCnt=port3DeviceCount;
		 }else if(k == 4){
			 portDev=port4Devices;
			 devCnt=port4DeviceCount;
		 }
		 for(i=0; i<devCnt; i++){
			 if(portDev[i].device != devSENSOR){
					continue;
			 }
			 sensor = (sensorType *)(&portDev[i].device);
			 sensor->checked=FALSE;
		 }
	 }
 }
 /***********************************************************************
 * @brief 	clear Sensor Grup Regs
 * @param		-
 *				
 * @return		-
 ***********************************************************************/ 
 void clearDeviceRegs (void) {
//uint32_t i,k;	 
//	 
//	 for (i=0;i<DEVICEREGPARAMETERS_SIZE;i++) {
//		 eraseDeviceRegsInRAMArea(DEVICEREGPARAMETERS+i,0);
//	 }
//	 
//	 for (i=0;i<MAX_PORT_DEVICE_CNT;i++) {
//		 port1Devices[i].deviceAddress = 0;
//		 port1Devices[i].device = devNONE;
//		 for (k=0;k<DEVICEDATAMAXSIZE;k++) {
//			 port1Devices[i].idiotBuff[k]=0;
//		 }
//	 }
//	 
//	 
//	 for (i=0;i<MAX_PORT_DEVICE_CNT;i++) {
//		 port2Devices[i].deviceAddress = 0;
//		 port2Devices[i].device = devNONE;
//		 for (k=0;k<DEVICEDATAMAXSIZE;k++) {
//			 port2Devices[i].idiotBuff[k]=0;
//		 }
//	 }
//	 
//	 for (i=0;i<MAX_PORT_DEVICE_CNT;i++) {
//		 port3Devices[i].deviceAddress = 0;
//		 port3Devices[i].device = devNONE;
//		 for (k=0;k<DEVICEDATAMAXSIZE;k++) {
//			 port3Devices[i].idiotBuff[k]=0;
//		 }
//	 }
//	 
//	 
//	 for (i=0;i<MAX_PORT_DEVICE_CNT;i++) {
//		 port4Devices[i].deviceAddress = 0;
//		 port4Devices[i].device = devNONE;
//		 for (k=0;k<DEVICEDATAMAXSIZE;k++) {
//			 port4Devices[i].idiotBuff[k]=0;
//		 }
//	 }
//	 
// 	 port1DeviceCount=0;
//	 port2DeviceCount=0;
//	 port3DeviceCount=0;
//	 port4DeviceCount=0;
//	 
	 
 }
    /***********************************************************************
 * @brief 	butun sensor internal registerlerini iliskili sensorlere bakarak 
 *					update eder. Eger sensor bossa registeri 1 arttirir. Boylece ayni
 *					register grubundaki sensorler icin kac adet bos yer bilgisi internal 
 *					registera yazilmis olur.
 * @param		-
 *				
 * @return		-
 ***********************************************************************/
// void sensorInternalRegisterUpdate(void){
//	 uint8_t i,k;
//	 uint8_t devCnt;
//	 deviceType *portDev;
//	 sensorType *sensor;
//	 uint16_t reg=0;

//	 for(k=0; k<MAX_INT_REGS_CNT; k++){
//		 sensorInternalRegisters[k]=0;
//	 }
//	 
//	 for(k=1; k<=4; k++){
//		 if(k == 1){
//			 portDev=port1Devices;
//			 devCnt=port1DeviceCount;
//		 }else if(k == 2){
//			 portDev=port2Devices;
//			 devCnt=port2DeviceCount;
//		 }else if(k == 3){
//			 portDev=port3Devices;
//			 devCnt=port3DeviceCount;
//		 }else if(k == 4){
//			 portDev=port4Devices;
//			 devCnt=port4DeviceCount;
//		 }
//		 for(i=0; i<devCnt; i++){
//			 if(portDev[i].device != devSENSOR){
//					continue;
//			 }
//			 sensor = (sensorType *)(&portDev[i]);
//			 if(sensor[i].registerNo<MAX_INT_REGS_CNT){
//				 sensorInternalRegisters[sensor[i].registerNo]++;
//			 }
//		 }
//	 }
// }
 
 /***********************************************************************
 * @brief 	Ethernet Init
 * @param		-
 *				
 * @return		-
 ***********************************************************************/
  void EthernetInit(void){
		
	}
     /***********************************************************************
 * @brief 	EthernetGetDataFromOtherDevice
 * @param		-
 *				
 * @return		-
 ***********************************************************************/
Bool EthernetGetDataFromOtherDevice(uint8_t *RemoteIP, uint8_t RemotePort, uint8_t *senddata, uint16_t timeout){
//	
//		if ( ETH_GetDataFromRemoteIP(RemoteIP, RemotePort, senddata, returndata, retsize,  timeout) == TRUE ) {
//			
//		}
	 
	 return FALSE;
 }	 
 /***********************************************************************
 * @brief 	Ethernet control
 * @param		-
 *				
 * @return		-
 ***********************************************************************/
 void EthernetControl(void){
//osEvent evt;
//uint8_t buff[2000];
//uint16_t datasize;
//static uint8_t ReadData	=0;
//uint8_t returndata[100];
//uint8_t retsize;
//uint8_t RemoteIP[4];
//uint16_t RemotePort;
//uint8_t senddata[50];	 
//	 
//	 
//		evt = osSignalWait(ETHERNET_DATAONLISTENPORT_PC, 0);
//		if (evt.status == osEventSignal)  {
//			if(evt.value.signals&ETHERNET_DATAONLISTENPORT_PC){
//				if ( ETH_getListenPackageFromFifo ( buff, &datasize ) == TRUE ) {
//					CheckIncomingData(buff, datasize);
//					if (ETH_SendDataFromListenPort( buff,datasize) == TRUE ) {
//						__nop();
////						temp = 1;
//					}
//					
//				}
//			}
//		}
//		

//		

//		
//		if (ReadData==1) {
//		RemoteIP[0]=192;
//		RemoteIP[1]=168;
//		RemoteIP[2]=1;
//		RemoteIP[3]=3;
//		RemotePort = 5001;
//		senddata[0]=0x02;
//		senddata[1]='C';
//		senddata[2]='L';
//		senddata[3]='I';
//		senddata[4]='E';
//		senddata[5]='N';
//		senddata[6]='T';
//		senddata[7]=0x03;			
//			if ( ETH_GetDataFromRemoteIP(RemoteIP, RemotePort, senddata, returndata, &retsize,  2000) == TRUE ) {
//				__nop();
//			
//			}
//			
//		}
//		
//		
	}
 
/***********************************************************************
 * @brief 	CheckSensorParametersSuitable 
 *					

 * @param		-
 *				
 * @return		-
 ***********************************************************************/
Bool CheckSensorParametersSuitable (sensorType *sensorPtr ) {
	
	if ( sensorPtr->GrupNo > SENSORGRUPSAYISI) {
		return FALSE;
	}

	if ( sensorPtr->LEDGrubu > MAX_PORT_DEVICE_CNT) {
		return FALSE;
	}
	
	if ( (uint8_t)sensorPtr->LEDbagli > 1) {
		return FALSE;
	}
	
	if ( (uint8_t)sensorPtr->workingMode > 1) {
		return FALSE;
	}
	
	if ( sensorPtr->LimitVal > MAXVAL_SENSORLIMIT) {
		return FALSE;
	}
	
	
	if ( sensorPtr->Dolu > 1) {
		return FALSE;
	}	
	
	return TRUE;
}


 
/***********************************************************************
 * @brief 	CheckDisplayParametersSuitable 
 *					

 * @param		-
 *				
 * @return		-
 ***********************************************************************/
Bool CheckDisplayParametersSuitable (displayType *displayPtr ) {
	
	
	if ( displayPtr->firstChar != DISPLAY_FIRSTCHAR_CROSS && displayPtr->firstChar != DISPLAY_FIRSTCHAR_REDARROW ) {
		return FALSE;
	}
	
	
	if ( (uint8_t)displayPtr->OkYonu != DISPLAY_OK_YUKARI &&
   		 (uint8_t)displayPtr->OkYonu != DISPLAY_OK_ASAGI && 
	     (uint8_t)displayPtr->OkYonu != DISPLAY_OK_YAN_IC && 
	     (uint8_t)displayPtr->OkYonu != DISPLAY_OK_YAN_DIS  ) {
		return FALSE;
	}
	
	return TRUE;

}

    /***********************************************************************
 * @brief 	CheckSensorParametersChanged 
 *					

 * @param		-
 *				
 * @return		-
 ***********************************************************************/
Bool CheckSensorParametersChanged ( sensorType *sensptr, deviceType *portDevPtr, uint8_t *portDeviceCnt ) {
uint16_t i=0;
sensorType *MevcutKayit;
//uint16_t NewDevice = 0;
	
	for (i=0;i<MAX_PORT_DEVICE_CNT;i++) {
		
		if ( (sensptr->deviceAddress == portDevPtr[i].deviceAddress) && (sensptr->device == portDevPtr[i].device)  ) {
			
				MevcutKayit = (sensorType *)(&portDevPtr[i].device);
				
				if ( 	sensptr->GrupNo == MevcutKayit->GrupNo &&
							sensptr->LEDGrubu == MevcutKayit->LEDGrubu &&	
							sensptr->LEDbagli == MevcutKayit->LEDbagli &&	
							sensptr->workingMode == MevcutKayit->workingMode &&
							portDevPtr[i].device == devSENSOR &&
							sensptr->LimitVal == MevcutKayit->LimitVal  )  {
								
							return FALSE;
								
				}
							portDevPtr[i].device = devSENSOR;
							MevcutKayit->deviceAddress = sensptr->deviceAddress;
							MevcutKayit->GrupNo = sensptr->GrupNo;
							MevcutKayit->LEDGrubu = sensptr->LEDGrubu;
							MevcutKayit->LEDbagli = sensptr->LEDbagli;
							MevcutKayit->workingMode = sensptr->workingMode;
							MevcutKayit->LimitVal  = sensptr->LimitVal;
				
							return TRUE;
		}
		
		
	}
	
	for (i=0;i<MAX_PORT_DEVICE_CNT;i++) {
		
		if ( portDevPtr[i].deviceAddress == 0 &&  portDevPtr[i].device == 0 ) {
			
			portDevPtr[i].deviceAddress = sensptr->deviceAddress;
			portDevPtr[i].device = devSENSOR;
			MevcutKayit = (sensorType *)(&portDevPtr[i].device);
			MevcutKayit->deviceAddress = sensptr->deviceAddress;
			MevcutKayit->GrupNo = sensptr->GrupNo;
			MevcutKayit->LEDGrubu = sensptr->LEDGrubu;
			MevcutKayit->LEDbagli = sensptr->LEDbagli;
			MevcutKayit->workingMode = sensptr->workingMode;
			MevcutKayit->LimitVal  = sensptr->LimitVal;
	    
			(*portDeviceCnt)++;
			return TRUE;
		}
		
	}
	
	return TRUE;


}	



    /***********************************************************************
 * @brief 	CheckSensorParametersChanged 
 *					

 * @param		-
 *				
 * @return		-
 ***********************************************************************/
Bool CheckDisplayParametersChanged ( displayType *displayPtr, deviceType *portDevPtr, uint8_t *portDeviceCnt ) {
uint16_t i=0;
displayType *MevcutKayit;
//uint16_t NewDevice = 0;

	for (i=0;i<MAX_PORT_DEVICE_CNT;i++) {
		
		if ( (displayPtr->deviceAddress == portDevPtr[i].deviceAddress) && (displayPtr->device == portDevPtr[i].device)  ) {
			
				MevcutKayit = (displayType *)(&portDevPtr[i].device);
				
				if ( 	displayPtr->firstChar == MevcutKayit->firstChar &&
							displayPtr->inverted == MevcutKayit->inverted &&	
							displayPtr->HaberlesmeGecikmesi == MevcutKayit->HaberlesmeGecikmesi &&	
							displayPtr->Parlaklik == MevcutKayit->Parlaklik &&
		      		portDevPtr[i].device == devDISPLAY &&
							displayPtr->OkYonu == MevcutKayit->OkYonu  )
							//displayPtr->val == MevcutKayit->val   
				{
								
							return FALSE;
								
				}
							portDevPtr[i].device = devDISPLAY;
							MevcutKayit->deviceAddress = displayPtr->deviceAddress;
							MevcutKayit->firstChar = displayPtr->firstChar;
							MevcutKayit->inverted = displayPtr->inverted;
							MevcutKayit->HaberlesmeGecikmesi = displayPtr->HaberlesmeGecikmesi;
							MevcutKayit->Parlaklik = displayPtr->Parlaklik;
							MevcutKayit->OkYonu  = displayPtr->OkYonu;
				
							return TRUE;
		}
		
		
	}
	
	for (i=0;i<MAX_PORT_DEVICE_CNT;i++) {
		
		if ( portDevPtr[i].deviceAddress == 0 &&  portDevPtr[i].device == 0 ) {
			
			portDevPtr[i].deviceAddress = displayPtr->deviceAddress;
			portDevPtr[i].device = devDISPLAY;
			MevcutKayit = (displayType *)(&portDevPtr[i].device);
			MevcutKayit->deviceAddress = displayPtr->deviceAddress;
			MevcutKayit->firstChar = displayPtr->firstChar;
			MevcutKayit->inverted = displayPtr->inverted;
			MevcutKayit->HaberlesmeGecikmesi = displayPtr->HaberlesmeGecikmesi;
			MevcutKayit->Parlaklik = displayPtr->Parlaklik;
			MevcutKayit->OkYonu  = displayPtr->OkYonu;
	    
			(*portDeviceCnt)++;
			return TRUE;
		}
		
	}
	
	return TRUE;


}	
 /***********************************************************************
 * @brief 	SetSensorParameterAndCheck
 *					

 * @param		-
 *				
 * @return		-
 ***********************************************************************/
Bool  SetSensorParameterAndCheck (uint8_t portNo, sensorType *ptr) {
uint16_t readlimitVal=0;
sensorModType readworkingMode;
LPC_USART_T *port;	
//uint32_t	i=0;	
//uint16_t limitVal;
	

			
	    if ( portNo == 1 ) {
				port = RS485_Port_1;
			}else if ( portNo == 2 ) {
				port = RS485_Port_2;
			}else if ( portNo == 3 ) {
				port = RS485_Port_3;
			}else if (portNo == 4 ) {
				port = RS485_Port_4;				
			}else {
				return FALSE;
			}
			
			if ( sensorKilitAc ( port, ptr->deviceAddress, TRUE) == FALSE ) {
				return FALSE;
			}
			osDelay(40);
//			if ( sensorKilitAc ( port, ptr->deviceAddress, TRUE) == FALSE ) {
//				return FALSE;
//			}
//			osDelay(40);
//			if ( sensorKilitAc ( port, ptr->deviceAddress, TRUE) == FALSE ) {
//				return FALSE;
//			}
			osDelay(40);
			if ( sensorLimitDegerDegistir ( port, ptr->deviceAddress, ptr->LimitVal) == FALSE ) {
				return FALSE;
			}
			osDelay(40);
			if ( sensorModuDegistir ( port, ptr->deviceAddress, ptr->workingMode) == FALSE ) {
				return FALSE;
			}
			osDelay(40);
			if ( sensorKilitAc ( port, ptr->deviceAddress, FALSE) == FALSE ) {
				return FALSE;
			}
			osDelay(40);
			if ( sensorLimitDegerOku(port,ptr->deviceAddress, &readlimitVal) == FALSE ) {
				return FALSE;
			}
			osDelay(40);			
			if ( sensorModuOku(port,ptr->deviceAddress,&readworkingMode) == FALSE ) {
				return FALSE;
			}
			
			if (  ptr->LimitVal ==  readlimitVal && ptr->workingMode ==  readworkingMode ) {									
					return TRUE;
					
			} else {
					return FALSE;
			}

			return FALSE;
}
/***********************************************************************
 * @brief 	SetSensorParameterAndCheck
 *					

 * @param		-
 *				
 * @return		-
 ***********************************************************************/
Bool  SetDisplayParameterAndCheck (uint8_t portNo, displayType *ptr) {
//uint16_t readlimitVal=0;
//sensorModType readworkingMode;
LPC_USART_T *port;	
//uint32_t	i=0;	
//uint16_t limitVal;
displayZeroChar_Type  readfirstChar;
dispayOrientation_Type	readinverted;
uint16_t readHaberlesmeGecikmesi;
uint16_t	readParlaklik;
uint16_t readOkYonu;			

			
	    if ( portNo == 1 ) {
				port = RS485_Port_1;
			}else if ( portNo == 2 ) {
				port = RS485_Port_2;
			}else if ( portNo == 3 ) {
				port = RS485_Port_3;
			}else if (portNo == 4 ) {
				port = RS485_Port_4;				
			}else {
				return FALSE;
			}
			//------YAZ-----------------
			if ( displayKilitAc ( port, ptr->deviceAddress, TRUE) == FALSE ) {
				return FALSE;
			}
			osDelay(40);
			if ( displayKilitAc ( port, ptr->deviceAddress, TRUE) == FALSE ) {
				return FALSE;
			}
			osDelay(40);
			if ( displayZeroCharDegistir(port, ptr->deviceAddress,ptr->firstChar) == FALSE ) {
				return FALSE;
			}
			osDelay(40);
			if ( displayYonDegistir(port,ptr->deviceAddress,ptr->inverted) == FALSE ) {
				return FALSE;
			}
			osDelay(40);
			if ( displayComTimeoutGir(port,ptr->deviceAddress,ptr->HaberlesmeGecikmesi) == FALSE ) {
				return FALSE;
			}
			osDelay(40);
			if ( displayParlaklikDegistir(port,ptr->deviceAddress,ptr->Parlaklik) == FALSE ) {
				return FALSE;
			}			
			osDelay(40);
			if ( displayOkYonuDegistir(port,ptr->deviceAddress,ptr->OkYonu) == FALSE ) {
				return FALSE;
			}	
			osDelay(40);
			if ( displayKilitAc ( port, ptr->deviceAddress, FALSE) == FALSE ) {
				return FALSE;
			}
			osDelay(40);
	
			//----OKU----------------
			if ( displayZeroCharOku(port,  ptr->deviceAddress,&readfirstChar) == FALSE ) {
				return FALSE;
			}
			osDelay(40);
			if ( displayYonOku(port,ptr->deviceAddress,&readinverted) == FALSE ) {
				return FALSE;
			}
			osDelay(40);
			if ( displayComTimeoutOku(port,ptr->deviceAddress,&readHaberlesmeGecikmesi) == FALSE ) {
				return FALSE;
			}
			osDelay(40);
			if ( displayParlaklikOku(port,ptr->deviceAddress,&readParlaklik) == FALSE ) {
				return FALSE;
			}
			osDelay(40);
			if ( displayOkYonuOku(port,ptr->deviceAddress,&readOkYonu) == FALSE ) {
				return FALSE;
			}
			osDelay(40);

			if ( ptr->firstChar == readfirstChar &&
					 ptr->inverted	 == readinverted	&&
					 ptr->HaberlesmeGecikmesi == readHaberlesmeGecikmesi &&
					 ptr->Parlaklik == readParlaklik &&
					 ptr->OkYonu == readOkYonu ) 
			{
					return TRUE;	
			}else {
				return FALSE;
			}
			


			return FALSE;

}

    /***********************************************************************
 * @brief 	ProgramDevices
 *					Master hafizasindaki parmetreleri cihazlara y�kler. Y�klenmeyen cihazin deviceparameter reg indeki sensor->comError yada display->commError sifirdan farkli olur. 

 * @param		-
 *				
 * @return		-
 ***********************************************************************/
void ProgramDevices (void) {
uint8_t i,k,ratio;
uint8_t devCnt;
deviceType *portDev;
//uint8_t sensor_Dolu=0;
sensorType *sensor;
displayType *display;
//LPC_USART_T *port;
//uint16_t readlimitVal=0;
//sensorModType readworkingMode;
uint8_t commError=0;
//displayZeroChar_Type  readfirstChar;
//dispayOrientation_Type	readinverted;
//uint16_t readHaberlesmeGecikmesi;
//uint16_t	readParlaklik;
//uint16_t readOkYonu;
	

	ratio = 1;
	Master.programmedDeviceRatio = ratio*100 / (port1DeviceCount + port2DeviceCount + port3DeviceCount + port4DeviceCount);
	
	Master.status = MASTERSTAT_PROGRAMDEVICE;

	for(k=1; k<=4; k++){
	 if(k == 1){
		 //port=RS485_Port_1;
		 portDev=port1Devices;
		 devCnt=port1DeviceCount;
	 }else if(k == 2){
		 //port=RS485_Port_2;
		 portDev=port2Devices;
		 devCnt=port2DeviceCount;
	 }else if(k == 3){
		 //port=RS485_Port_3;
		 portDev=port3Devices;
		 devCnt=port3DeviceCount;
	 }else if(k == 4){
		 //port=RS485_Port_4;
		 portDev=port4Devices;
		 devCnt=port4DeviceCount;
	 }
	 for(i=0; i<devCnt; i++){
		 if(portDev[i].device == devSENSOR){			  
			 
				sensor = (sensorType *)(&portDev[i].device);					
				if ( SetSensorParameterAndCheck( k, sensor )) {
					commError = 0;
				}else {
					commError = 1;
				}		
				sensor->comError = commError;
				ratio++;
				
		 }else if(portDev[i].device == devDISPLAY){
				display = (displayType *)(&portDev[i].device);
				osDelay(40);
				commError = 0 ;	
				if ( SetDisplayParameterAndCheck( k, display )) {
						commError = 0;
				}else {
						commError = 1;
				}				 

				display->comError = commError;
				ratio++;
		 }
		 Master.programmedDeviceRatio = ratio*100 / (port1DeviceCount + port2DeviceCount + port3DeviceCount + port4DeviceCount);
	 }
	}
	Master.status = MASTERSTAT_EMPTY;
	
	
}
    /***********************************************************************
 * @brief 	CheckDeviceParameters
 *					master hafizasindaki cihazlari tek tek okur. cihaz parametreleri masterdakindan farkli ise cihazi gunceller.

 * @param		-
 *				
 * @return		-
 ***********************************************************************/
void CheckDeviceParameters (void) {
uint8_t i,k;
uint8_t devCnt;
deviceType *portDev;
//uint8_t sensor_Dolu=0;
sensorType *sensor;
displayType *display;
LPC_USART_T *port;
uint16_t readlimitVal=0;
sensorModType readworkingMode;
uint8_t commError=0;
displayZeroChar_Type  readfirstChar;
dispayOrientation_Type	readinverted;
uint16_t readHaberlesmeGecikmesi;
uint16_t	readParlaklik;
uint16_t readOkYonu;


	Master.status = MASTERSTAT_CHECKDEVICE;

	for(k=1; k<=4; k++){
	 if(k == 1){
		 port=RS485_Port_1;
		 portDev=port1Devices;
		 devCnt=port1DeviceCount;
	 }else if(k == 2){
		 port=RS485_Port_2;
		 portDev=port2Devices;
		 devCnt=port2DeviceCount;
	 }else if(k == 3){
		 port=RS485_Port_3;
		 portDev=port3Devices;
		 devCnt=port3DeviceCount;
	 }else if(k == 4){
		 port=RS485_Port_4;
		 portDev=port4Devices;
		 devCnt=port4DeviceCount;
	 }
	 for(i=0; i<devCnt; i++){
		 if(portDev[i].device == devSENSOR){
				sensor = (sensorType *)(&portDev[i].device);
				osDelay(40);
				commError = 0 ;
				if ( sensorLimitDegerOku(port,sensor->deviceAddress, &readlimitVal) == FALSE ) {
			  	commError = 1;
					sensor->comError=TRUE;
				}else {
					commError = 0;
				}
				
				osDelay(40);			
				if ( sensorModuOku(port,sensor->deviceAddress,&readworkingMode) == FALSE ) {
				  commError = 1;
					sensor->comError=TRUE;
				}else {
					commError = 0;
				}
				
				if (commError == 0 && ( sensor->LimitVal != readlimitVal || sensor->workingMode != readworkingMode) ) {
					if ( SetSensorParameterAndCheck( k, sensor )) {
						commError = 0;
					}else {
						commError = 1;
					}
				}
				sensor->comError = commError;
				
		 }else if(portDev[i].device == devDISPLAY){
				display = (displayType *)(&portDev[i].device);
				osDelay(40);
				commError = 0 ;	
				if ( displayKilitAc ( port, display->deviceAddress, TRUE) == FALSE ) {
					commError = 1 ;	
				}	
				
				if ( displayZeroCharOku(port,  display->deviceAddress,&readfirstChar) == TRUE && display->firstChar != readfirstChar ) {
					osDelay(40);
					if ( displayZeroCharDegistir(port, display->deviceAddress,display->firstChar) == TRUE ) {
						if ( displayZeroCharOku(port,  display->deviceAddress,&readfirstChar)== TRUE &&  display->firstChar == readfirstChar){
							commError = 0;
						}else {
							commError = 1 ;	
						}
					}else {
						commError = 1 ;	
					}
				}else {
					commError = 1 ;	
				}
				
				if ( displayYonOku(port,display->deviceAddress,&readinverted) == TRUE && display->inverted != readinverted ) {
					osDelay(40);
					if ( displayYonDegistir(port, display->deviceAddress,display->inverted) == TRUE ) {
						if ( displayYonOku(port,display->deviceAddress,&readinverted)== TRUE &&  display->inverted == readinverted){
							commError = 0;
						}else {
							commError = 1 ;	
						}
					}else {
						commError = 1 ;	
					}
				}else {
					commError = 1 ;	
				}
				
				if (  displayComTimeoutOku(port,display->deviceAddress,&readHaberlesmeGecikmesi) == TRUE && display->HaberlesmeGecikmesi != readHaberlesmeGecikmesi ) {
					osDelay(40);
					if ( displayParlaklikDegistir(port,display->deviceAddress,display->Parlaklik) == TRUE ) {
						if ( displayComTimeoutOku(port,display->deviceAddress,&readHaberlesmeGecikmesi) == TRUE &&  display->HaberlesmeGecikmesi == readHaberlesmeGecikmesi){
							commError = 0;
						}else {
							commError = 1 ;	
						}
					}else {
						commError = 1 ;	
					}
				}else {
					commError = 1 ;	
				}
				
				if ( displayParlaklikOku(port,display->deviceAddress,&readParlaklik) == TRUE && display->Parlaklik != readParlaklik ) {
					osDelay(40);
					if ( displayParlaklikDegistir(port,display->deviceAddress,display->Parlaklik) == TRUE ) {
						if ( displayParlaklikOku(port,display->deviceAddress,&readParlaklik)== TRUE &&  display->Parlaklik == readParlaklik){
							commError = 0;
						}else {
							commError = 1 ;	
						}
					}else {
						commError = 1 ;	
					}
				}else {
					commError = 1 ;	
				}

				if ( displayOkYonuOku(port,display->deviceAddress,&readOkYonu) == TRUE && display->OkYonu != readOkYonu ) {
					osDelay(40);
					if ( displayOkYonuDegistir(port,display->deviceAddress,display->OkYonu) == TRUE ) {
						if ( displayOkYonuOku(port,display->deviceAddress,&readOkYonu)== TRUE &&  display->OkYonu == readOkYonu){
							commError = 0;
						}else {
							commError = 1 ;	
						}
					}else {
						commError = 1 ;	
					}
				}else {
					commError = 1 ;	
				}
				display->comError = commError;
				
		 }
	 }
	}
	Master.status = MASTERSTAT_EMPTY;
	
	
}
    /***********************************************************************
 * @brief 	RAM deki komut buffer inda komut varsa true doner.
 *					

 * @param		-
 *				
 * @return		-
 ***********************************************************************/
bool getCommand (uint8_t *readPtr, uint8_t *retBuff, uint16_t *retSize, uint16_t maxCmdSize) {
uint16_t i=0;
uint8_t *endPtr;
uint8_t cmdStart=0;
	
	endPtr =getCmdBuffEndAdress(); 
	
	if (readPtr >= endPtr ) {
		return false;
	}
	
	while ( readPtr < endPtr ){
		if (cmdStart == 0 ) {
			if (*readPtr == STX ) {
				cmdStart = 1;
				retBuff[i]=*readPtr;
				i++;
			}
			readPtr++;
		}else if ( *readPtr == ETX ) {
			retBuff[i] = ETX;
			readPtr++;
			*retSize = i+1;
			return true;
		}else {
			retBuff[i]=*readPtr;
			i++;
			readPtr++;
		}
		
	}
	*retSize = 0;
	return false;
}


