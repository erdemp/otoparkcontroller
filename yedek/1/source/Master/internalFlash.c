#include "internalFlash.h"


//#include "lpc17xx_IAP.h"
//#include "lpc17xx.h"                    /* LPC11xx definitions                */


static uint8_t flashArea[RAM_BUFFER_SIZE];



static uint32_t *flashPointer;

static uint8_t selectedFlashNo=0;

Bool flashWriteBackup(void);

//****************************************************************************************************
//FonksiyonAd�:		flashInit
//A��klama:   kullan�c�n�n tan�mlad��� RAM'deki alan� temizler	
//
void flashInit(uint8_t flashNo){
 uint32_t i;
 uint8_t *ptr;
 uint32_t freeflashadress;
 uint8_t *flashPtr;
	
 freeflashadress = getFlashPointers();
	
	
 switch(flashNo){
		case 1:
		 ptr=(uint8_t *)FLASH1_START_ADDRESS;
		 for(i=0;i<FLASH1_USER_SIZE;i++){
			flashArea[i]=ptr[i];
		 }

		 flashPointer=(uint32_t *)&flashArea[FLASH1_POINTER];

		 if(*flashPointer>=FLASH1_USER_SIZE){
			*flashPointer=FLASH1_POINTER+4;
		 }
		 selectedFlashNo=1;
		break;
		 
		case 2:
		 ptr=(uint8_t *)FLASH2_START_ADDRESS;
		 for(i=0;i<FLASH2_USER_SIZE;i++){
			flashArea[i]=ptr[i];
		 }

		 flashPointer=(uint32_t *)&flashArea[FLASH2_POINTER];

		 if(*flashPointer>=FLASH2_USER_SIZE){
			*flashPointer=FLASH2_POINTER+4;
		 }
		 selectedFlashNo=2;
		break;
		 
		case 0:
		default:
			
		 if ( freeflashadress == FLASH_START_ADDRESS ) {
			 flashPtr = (uint8_t *)FLASH_START_ADDRESS;
		 }else if ( freeflashadress == ( FLASH_START_ADDRESS + 1*FLASH_USER_SIZE)) {
			 flashPtr = (uint8_t *)FLASH_START_ADDRESS;
		 }else if ( freeflashadress == (FLASH_START_ADDRESS + 2*FLASH_USER_SIZE) ) {
			 flashPtr = (uint8_t *)( FLASH_START_ADDRESS + 1*FLASH_USER_SIZE);
		 }else if ( freeflashadress == ( FLASH_START_ADDRESS + 3*FLASH_USER_SIZE) ) {
			 flashPtr = (uint8_t *)( FLASH_START_ADDRESS + 2*FLASH_USER_SIZE); 
		 }
		 
		 //ptr=(uint8_t *)FLASH_START_ADDRESS;
		 
		 
		 for(i=0;i<FLASH_USER_SIZE;i++){
			flashArea[i]=flashPtr[i];
		 }

		 flashPointer=(uint32_t *)&flashArea[FLASH_POINTER];

		 if(*flashPointer>=FLASH_USER_SIZE){
			*flashPointer=FLASH_POINTER+4;
		 }
		 selectedFlashNo=0;
		break;
 }

}
 
//****************************************************************************************************
//FonksiyonAd�:		flashErase
//A��klama:   ka� adet page tan�mland�ysa FLASH_START_ADRESS'ten itibaren bunlar� siler	
//
Bool flashErase(uint8_t flashNo){
Bool result=FALSE;
//uint32_t k;

	
	 switch(flashNo){
		 
		 case 1:
			 if (Chip_IAP_PreSectorForReadWrite(FLASH1_START_SECTOR_NUM, FLASH1_END_SECTOR_NUM) == IAP_CMD_SUCCESS){
					if( Chip_IAP_EraseSector(FLASH1_START_SECTOR_NUM, FLASH1_END_SECTOR_NUM)==IAP_CMD_SUCCESS){
					 result=TRUE;
					}
			 }else{
				 return FALSE;
			 }
			 
			 if( Chip_IAP_BlankCheckSector(FLASH1_START_SECTOR_NUM, FLASH1_END_SECTOR_NUM)==IAP_CMD_SUCCESS){
				 result=TRUE;
			 }else{
					return FALSE;
			 }
		 break;
		
		case 2:
			 if (Chip_IAP_PreSectorForReadWrite(FLASH2_START_SECTOR_NUM, FLASH2_END_SECTOR_NUM) == IAP_CMD_SUCCESS){
					if( Chip_IAP_EraseSector(FLASH2_START_SECTOR_NUM, FLASH2_END_SECTOR_NUM)==IAP_CMD_SUCCESS){
					 result=TRUE;
				}
			 }
			 if( Chip_IAP_BlankCheckSector(FLASH2_START_SECTOR_NUM, FLASH2_END_SECTOR_NUM)==IAP_CMD_SUCCESS){
				 result=TRUE;
			 }else{
					return FALSE;
			 }
		break;
		
		case 0:
			 if (Chip_IAP_PreSectorForReadWrite(FLASH_START_SECTOR_NUM, FLASH_END_SECTOR_NUM) == IAP_CMD_SUCCESS){
					if( Chip_IAP_EraseSector(FLASH_START_SECTOR_NUM, FLASH_END_SECTOR_NUM)==IAP_CMD_SUCCESS){
					 result=TRUE;
					}
			 }else{
				 return FALSE;
			 }
			 
			 if( Chip_IAP_BlankCheckSector(FLASH_START_SECTOR_NUM, FLASH_END_SECTOR_NUM)==IAP_CMD_SUCCESS){
				 result=TRUE;
			 }else{
					return FALSE;
			 }
			 
		 break;
		 
	}
   
   return result;
}

//****************************************************************************************************
//FonksiyonAd�:		flashWrite
//A��klama:   ram'ideki 32K veriyi flash'a yazar
//			  flasha yazar
//
Bool flashWrite(void){
	Bool result=FALSE;
	uint32_t k,i,m,freeflashadress;

	uint8_t *ptr;
	uint16_t crc;
	uint16_t *ptr16;
	
	
	
	if(!flashWriteBackup()){
		return FALSE;
	}


	
	
  freeflashadress = getFlashPointers();

	flashArea[FLASH_USER_SIZE-4] = 0;
	flashArea[FLASH_USER_SIZE-3] = 0;
	flashArea[FLASH_USER_SIZE-2] = 0;
	flashArea[FLASH_USER_SIZE-1] = 0;
	crc = crc16(flashArea, FLASH_USER_SIZE-6 );
	ptr16=(uint16_t *)&flashArea[FLASH_USER_SIZE-6];
	*ptr16 = crc;
	
//	burada yazmadan once flashin sektorunu siliyor. parca parca yazmali 4-5 seferde bir silmeli.
//	yazilacak data hazirlanacak ram de tutulacak, sonuna iki byte crc eklenecek. crc den sonra iki byte sifir
//	eklenecek yada page katsayina erisecek kadar sifir eklenecek. (min yazma ne kadar. 256 byte mi. oyle ise page in 
//	ortasina kalamamali. sonra ki yazmayi yapacaz.
//	yazmadan once de asagidan yukariya dogru gidecek sifir lari gordugu yerden sonra yazacak.
	
//	if(flashErase(selectedFlashNo)){
			switch(selectedFlashNo){
							
				case 1:
					k= freeflashadress; //FLASH1_START_ADDRESS;
					i=0;
					for(i=0;i<FLASH1_USER_SIZE;i+=MAX_WRITABLE_SIZE){
						m=(uint32_t)(flashArea+i);
						if (Chip_IAP_PreSectorForReadWrite(FLASH1_START_SECTOR_NUM, FLASH1_END_SECTOR_NUM) == IAP_CMD_SUCCESS){
							if (Chip_IAP_CopyRamToFlash(k,m,MAX_WRITABLE_SIZE) != IAP_CMD_SUCCESS)
							{
								return FALSE;
							}
						}else{
								return FALSE;

						}
						k+=MAX_WRITABLE_SIZE;
					}
					
					ptr=(uint8_t *)freeflashadress;
					 for(i=0;i<FLASH1_USER_SIZE;i++){
						 if(ptr[i]!=flashArea[i]){
							 return FALSE;
						 }
					}
					
					result=TRUE;
				break;
					
				case 2:
					k=freeflashadress;
					i=0;
					for(i=0;i<FLASH2_USER_SIZE;i+=MAX_WRITABLE_SIZE){
						m=(uint32_t)(flashArea+i);
						if (Chip_IAP_PreSectorForReadWrite(FLASH2_START_SECTOR_NUM, FLASH2_END_SECTOR_NUM) == IAP_CMD_SUCCESS){
							if (Chip_IAP_CopyRamToFlash(k,m,MAX_WRITABLE_SIZE) != IAP_CMD_SUCCESS)
							{
								return FALSE;
							}
						}else{
								return FALSE;

						}
						k+=MAX_WRITABLE_SIZE;
					}
					ptr=(uint8_t *)freeflashadress;
					 for(i=0;i<FLASH2_USER_SIZE;i++){
						 if(ptr[i]!=flashArea[i]){
							 return FALSE;
						 }
					}
					result=TRUE;
				break;
					
				case 0:
					k=freeflashadress;
					i=0;
					for(i=0;i<FLASH_USER_SIZE;i+=MAX_WRITABLE_SIZE){
						m=(uint32_t)(flashArea+i);
						if (Chip_IAP_PreSectorForReadWrite(FLASH_START_SECTOR_NUM, FLASH_END_SECTOR_NUM) == IAP_CMD_SUCCESS){
							if (Chip_IAP_CopyRamToFlash(k,m,MAX_WRITABLE_SIZE) != IAP_CMD_SUCCESS)
							{
								return FALSE;
							}
						}else{
								return FALSE;

						}
						k+=MAX_WRITABLE_SIZE;
					}
					
					ptr=(uint8_t *)freeflashadress;
					 for(i=0;i<FLASH_USER_SIZE;i++){
						 if(ptr[i]!=flashArea[i]){
							 return FALSE;
						 }
					}
					result=TRUE;
				break;
					
			}
		
//	}
	
	return result;
}

//****************************************************************************************************
//FonksiyonAdi:		flashWriteBackup
//Aciklama:   ram'ideki 32K veriyi backup flash'a yazar
//			  
//
Bool flashWriteBackup(void){
	uint32_t k, i, m;
	uint8_t *ptr;
	uint16_t crc;
	uint16_t *ptr16;
	
	flashArea[FLASH_USER_SIZE-4] = 0;
	flashArea[FLASH_USER_SIZE-3] = 0;
	flashArea[FLASH_USER_SIZE-2] = 0;
	flashArea[FLASH_USER_SIZE-1] = 0;
	crc = crc16(flashArea, FLASH_USER_SIZE-6 );
	ptr16=(uint16_t *)&flashArea[FLASH_USER_SIZE-6];
	*ptr16 = crc;
	
	
//	crc = crc16(flashArea, FLASH_USER_SIZE-2 );
//	ptr16=(uint16_t *)&flashArea[FLASH_USER_SIZE-2];
//	*ptr16 = crc;
	
	if(!flashErase(2)){
		return FALSE;
	}
	 
	 k=FLASH2_START_ADDRESS;
	 i=0;
	 for(i=0;i<FLASH2_USER_SIZE;i+=MAX_WRITABLE_SIZE){
		m=(uint32_t)(flashArea+i);
		if (Chip_IAP_PreSectorForReadWrite(FLASH2_START_SECTOR_NUM, FLASH2_END_SECTOR_NUM) == IAP_CMD_SUCCESS){
			if (Chip_IAP_CopyRamToFlash(k,m,MAX_WRITABLE_SIZE) != IAP_CMD_SUCCESS)
			{
				return FALSE;
			}
		}else{
				return FALSE;

		}
		k+=MAX_WRITABLE_SIZE;
	 }
	 
	 
	 
	 ptr=(uint8_t *)FLASH2_START_ADDRESS;
	 for(i=0;i<FLASH2_USER_SIZE;i++){
			 if(ptr[i]!=flashArea[i]){
				 return FALSE;
			 }
	 }
	
	 return TRUE;
}

 /***********************************************************************************************************************
* Function Name: getFlashPointers
* Description  : 
* Arguments    : 
* Return Value : 
***********************************************************************************************************************/
  uint32_t  getLastRecordStartAdress(void){
	static uint8_t *ptr;
	uint32_t i; 
	uint32_t flashBolge = 0;
	uint32_t	LastRecordStartAdress=0;
	uint32_t SelectedFlashStartAdress =0 ;
		
		switch(selectedFlashNo){

			case 0:
				SelectedFlashStartAdress = FLASH_START_ADDRESS;
				break;
			
			case 1:
				SelectedFlashStartAdress = FLASH1_START_ADDRESS;
				break;
			
			case 2:
				SelectedFlashStartAdress = FLASH2_START_ADDRESS;
				break;

		}		
			
		ptr = (uint8_t *)SelectedFlashStartAdress;
		
		for (i= (FLASH_PAGE_SIZE -1) ;i>0; i--) {
			if ( ptr[i] != 0xff && ptr[i]==0) {
				if (ptr[i-1] != 0xff && ptr[i-1]==0) {
					if (ptr[i-2] != 0xff && ptr[i-2]==0) {
						if (ptr[i-3] != 0xff && ptr[i-3]==0) {
					flashBolge = i / FLASH_USER_SIZE; 
					//flashBolge ++;
//					if  (flashBolge * FLASH_USER_SIZE >= FLASH_PAGE_SIZE ) {
//						if(flashErase(selectedFlashNo)){
//							freeflashptr = SelectedFlashStartAdress;
//							return freeflashptr;
//						}
//					}else {
						LastRecordStartAdress = SelectedFlashStartAdress + (flashBolge * FLASH_USER_SIZE);
						return LastRecordStartAdress;
					//}
						}
					}
				}
			}
		}  

		LastRecordStartAdress = SelectedFlashStartAdress;
		return LastRecordStartAdress;
	
	}

	/***********************************************************************************************************************
* Function Name: getDataFromFlashToRAM
* Description  :belirtilen adresten itibaren flashsize kadar datayi alip RAM buffer a yazar.
* Arguments    : 
* Return Value : 
***********************************************************************************************************************/
void getDataFromFlashToRAM(uint32_t adress) {
 uint32_t i;
// uint8_t *ptr;
// uint32_t freeflashadress;
 uint8_t *flashPtr;
			 
//	
//			 if ( freeflashadress == FLASH_START_ADDRESS ) {
//			 flashPtr = (uint8_t *)FLASH_START_ADDRESS;
//		 }else if ( freeflashadress == ( FLASH_START_ADDRESS + 1*FLASH_USER_SIZE)) {
//			 flashPtr = (uint8_t *)FLASH_START_ADDRESS;
//		 }else if ( freeflashadress == (FLASH_START_ADDRESS + 2*FLASH_USER_SIZE) ) {
//			 flashPtr = (uint8_t *)( FLASH_START_ADDRESS + 1*FLASH_USER_SIZE);
//		 }else if ( freeflashadress == ( FLASH_START_ADDRESS + 3*FLASH_USER_SIZE) ) {
//			 flashPtr = (uint8_t *)( FLASH_START_ADDRESS + 2*FLASH_USER_SIZE); 
//		 }
		 
		 flashPtr = (uint8_t *)adress ;
		 
	
		 for(i=0;i<FLASH_USER_SIZE;i++){
			flashArea[i]=flashPtr[i];
		 }

//		 flashPointer=(uint32_t *)&flashArea[FLASH_POINTER];

//		 if(*flashPointer>=FLASH_USER_SIZE){
//			*flashPointer=FLASH_POINTER+4;
//		 }
		 
		
}

 /***********************************************************************************************************************
* Function Name: getFlashPointers
* Description  : 
* Arguments    : 
* Return Value : 
***********************************************************************************************************************/
  uint32_t  getFlashPointers(void){
	static uint8_t *ptr;
	uint32_t i; 
	uint32_t flashBolge = 0;
	uint32_t	freeflashptr=0;
	uint32_t SelectedFlashStartAdress =0 ;

		
	switch(selectedFlashNo){

		case 0:
			SelectedFlashStartAdress = FLASH_START_ADDRESS;
			break;
		
		case 1:
			SelectedFlashStartAdress = FLASH1_START_ADDRESS;
			break;
		
		case 2:
			SelectedFlashStartAdress = FLASH2_START_ADDRESS;
			break;

	}		
		
	ptr = (uint8_t *)SelectedFlashStartAdress;
		
	for (i= (FLASH_PAGE_SIZE -1) ;i>0; i--) {
		if ( ptr[i] != 0xff && ptr[i]==0) {
			if (ptr[i-1] != 0xff && ptr[i-1]==0) {
				if (ptr[i-2] != 0xff && ptr[i-2]==0) {
					if (ptr[i-3] != 0xff && ptr[i-3]==0) {
				flashBolge = i / FLASH_USER_SIZE; 
				flashBolge ++;
				if  (flashBolge * FLASH_USER_SIZE >= FLASH_PAGE_SIZE ) {
					if(flashErase(selectedFlashNo)){
						freeflashptr = SelectedFlashStartAdress;
						return freeflashptr;
					}
				}else {
					freeflashptr = SelectedFlashStartAdress + (flashBolge * FLASH_USER_SIZE);
					return freeflashptr;
				}
					}
				}
			}
		}
	}  

	freeflashptr = SelectedFlashStartAdress;
	return freeflashptr;	
		
	

  }

	
//****************************************************************************************************
//FonksiyonAd�:		SaveRAM
//A��klama:   bu fonksiyon ile girilen data RAM'e yaz�l�r. Yaz�lan yer flash image'inin tutuldu�u yerdir.	
//
bool getEmptyAdressForCommandFromRAMbuff (uint32_t *emptyAdress,uint32_t *emptySize, uint32_t address, uint8_t emptyChar) {
uint32_t i;
	
	i= address;
	if ( address == RAM_BUFFER_SIZE - 1 ) {
		return false;
	}
		
	
	for ( i= address ; i< (RAM_BUFFER_SIZE - 1) ; i++ ) {
		if ( flashArea[i] == emptyChar ) {
			*emptyAdress= i;
			*emptySize = RAM_BUFFER_SIZE - i;
			return true;
		}
		
	}
	 
	return false;
	
//	if((address+length)>=RAM_BUFFER_SIZE){
//		return FALSE;
//	}
//	
//	for(i=0;i<length;i++){
//		flashArea[address+i]=*data;
//		data++;
//	}

//	return TRUE;
	
}	
//****************************************************************************************************
//FonksiyonAd�:		SaveRAM
//A��klama:   bu fonksiyon ile girilen data RAM'e yaz�l�r. Yaz�lan yer flash image'inin tutuldu�u yerdir.	
//
bool SaveToRAMbuff (uint8_t *data, uint32_t address, uint32_t length) {
uint32_t i;
	
	
	if((address+length)>=RAM_BUFFER_SIZE){
		return FALSE;
	}
	
	for(i=0;i<length;i++){
		flashArea[address+i]=*data;
		data++;
	}

	return TRUE;
	
}

//****************************************************************************************************
//FonksiyonAd�:		eraseFlashRamArea
//A��klama:   
//
uint8_t * getparametersRAMStartAdress (void) {
uint32_t i;	
	
	return 	flashArea;

	
}


//****************************************************************************************************
//FonksiyonAd�:		eraseFlashRamArea
//A��klama:   
//
void eraseFlashRamArea (void) {
uint32_t i;	
	

		for(i=0;i<RAM_BUFFER_SIZE;i++){
	  	flashArea[i]=0;
		
	}
	
}


//****************************************************************************************************
//FonksiyonAd�:		eraseFlashRamArea
//A��klama:   
//
void eraseCommandsRamArea (uint32_t index, uint8_t data) {
uint32_t i;	
	

		for(i=0;i<RAM_BUFFER_SIZE;i++){
	  	flashArea[i]=0;
		
	}
	
}
//****************************************************************************************************
//FonksiyonAd�:		eraseDeviceRegsInRAMArea
//A��klama:   
//
void eraseDeviceRegsInRAMArea (uint32_t index, uint8_t data) {
uint32_t i;	

	  	flashArea[index]=data;

	
}
//****************************************************************************************************
//FonksiyonAd�:		SaveRAM
//A��klama:   bu fonksiyon ile girilen data RAM'e yaz�l�r. Yaz�lan yer flash image'inin tutuldu�u yerdir.	
//
Bool SaveRAM(uint8_t *data, uint32_t lenght){
	uint32_t i;
	//uint8_t *ptr;

	if((*flashPointer+lenght)>=RAM_BUFFER_SIZE){
		return FALSE;
	}
	//ptr=data;

	for(i=*flashPointer;i<(*flashPointer+lenght);i++){
// 		flashArea[i]=*ptr;
// 		ptr++;
		flashArea[i]=*data;
		data++;
	}
	*flashPointer+=lenght;

	return TRUE;
}

//****************************************************************************************************
//FonksiyonAd�:		DelRAM_Flash
//A��klama:   RAM'i ve Flash'� siler	
//
Bool DelRAM_Flash(void){
	if(flashErase(selectedFlashNo)){
	 	flashInit(selectedFlashNo);
		return TRUE;
	}
	return FALSE;
}

//****************************************************************************************************
//FonksiyonAd�:		GetData
//A��klama:   RAM'den index'i verilen yere ili�kin datay� al�r	
//
uint8_t GetData(uint32_t index){
	
	switch(selectedFlashNo){
		case 1:
			return flashArea[index];//+FLASH1_BELLEK];
		//break;
		case 2:
			return flashArea[index];//+FLASH2_BELLEK];
		//break;
		case 0:
		default:
			return flashArea[index];//+FLASH_BELLEK];
		//break;
	}

}


//****************************************************************************************************
//FonksiyonAd�:		GetDataPtr
//A��klama:   RAM'den index'i verilen yere ili�kin adresi al�r	
//
uint8_t *GetDataPtr(uint32_t index){
	switch(selectedFlashNo){
		case 1:
			return &flashArea[index];//+FLASH1_BELLEK];
		//break;
		case 2:
			return &flashArea[index];//+FLASH2_BELLEK];
		//break;
		case 0:
		default:
			return &flashArea[index];//+FLASH_BELLEK];
		//break;
	}
	

}


//****************************************************************************************************
//FonksiyonAdi:		compareFlash
//Aciklama:   adresi girilen flash alani ile data, length kadar karsilastirilir
//			  
//
Bool compareFlash(uint32_t address, uint32_t data, uint8_t length){
	uint8_t *ptr;
	uint8_t *dataPtr;
	uint8_t i;
	
	switch(selectedFlashNo){
		case 0:
			ptr=(uint8_t *)(address + FLASH_START_ADDRESS);// + FLASH_BELLEK);
		break;
		case 1:
			ptr=(uint8_t *)(address + FLASH1_START_ADDRESS);// + FLASH1_BELLEK);
		break;
		case 2:
			ptr=(uint8_t *)(address + FLASH2_START_ADDRESS);/// + FLASH2_BELLEK);
		break;
	}
	
	dataPtr=(uint8_t *)&data;
	
	for(i=0; i<length; i++){
		if(ptr[i]!=dataPtr[i]){
			return FALSE;
		}
	}
	return TRUE;
}

//****************************************************************************************************
//FonksiyonAdi:		checkFlashCRC
//Aciklama:   secili flash'in CRC'si dogru mu kontrol eder
//			  
//
Bool checkFlashCRC(void){
	uint16_t crc;
	uint16_t *ptr16;
	uint32_t LastRecordStartAdress=0;
//	uint32_t SeciliFlashStartAdress=0;
	
	LastRecordStartAdress = getLastRecordStartAdress();
	
	switch(selectedFlashNo){
		
		case 0:
			ptr16=(uint16_t *)(LastRecordStartAdress + FLASH_USER_SIZE -6);
			crc = crc16((uint8_t *)LastRecordStartAdress, FLASH_USER_SIZE-6 );
			break;
		
		case 1:
			ptr16=(uint16_t *)(LastRecordStartAdress + FLASH1_USER_SIZE -6);
			crc = crc16((uint8_t *)LastRecordStartAdress, FLASH1_USER_SIZE-6 );
			break;
		
		case 2:
			ptr16=(uint16_t *)(LastRecordStartAdress + FLASH2_USER_SIZE -6);
			crc = crc16((uint8_t *)LastRecordStartAdress, FLASH2_USER_SIZE-6 );
			break;
		
	}
	
	
	
	
	
//	switch(selectedFlashNo){
//		case 0:
//			ptr16=(uint16_t *)(FLASH_START_ADDRESS + FLASH_USER_SIZE -2);
//			crc = crc16((uint8_t *)FLASH_START_ADDRESS, FLASH_USER_SIZE-2 );
//		break;
//		case 1:
//			ptr16=(uint16_t *)(FLASH1_START_ADDRESS + FLASH1_USER_SIZE -2);
//			crc = crc16((uint8_t *)FLASH1_START_ADDRESS, FLASH1_USER_SIZE-2 );
//		break;
//		case 2:
//			ptr16=(uint16_t *)(FLASH2_START_ADDRESS + FLASH2_USER_SIZE -2);
//			crc = crc16((uint8_t *)FLASH2_START_ADDRESS, FLASH2_USER_SIZE-2 );
//		break;
//	}
//	
	if(*ptr16!=crc){
		return FALSE;
	}
	
	return TRUE;
	
	
}

//****************************************************************************************************
//FonksiyonAdi:		restoreFlashBackup
//Aciklama:   secili flash'i backup ile degistirir
//			  
//
Bool restoreFlashBackup(void){
	
	uint32_t k,i,m;

	uint8_t *ptr;
	
	if(flashErase(selectedFlashNo)==FALSE){
		return FALSE;
	}
	switch(selectedFlashNo){
		case 0:
			selectFlashArea(FLASH_BACKUP_AREA);
			//flashInit(2);
		  getDataFromFlashToRAM(FLASH2_START_ADDRESS);
			k=FLASH_START_ADDRESS;
			i=0;
			for(i=0;i<FLASH_USER_SIZE;i+=MAX_WRITABLE_SIZE){
				m=(uint32_t)(flashArea+i);
				if (Chip_IAP_PreSectorForReadWrite(FLASH_START_SECTOR_NUM, FLASH_END_SECTOR_NUM) == IAP_CMD_SUCCESS){
					if (Chip_IAP_CopyRamToFlash(k,m,MAX_WRITABLE_SIZE) != IAP_CMD_SUCCESS)
					{
						return FALSE;
					}
				}else{
						return FALSE;

				}
				k+=MAX_WRITABLE_SIZE;
			}
			
			ptr=(uint8_t *)FLASH_START_ADDRESS;
			for(i=0;i<FLASH_USER_SIZE;i++){
				 if(ptr[i]!=flashArea[i]){
					 return FALSE;
				 }
			}
			selectFlashArea(FLASH_PARAMETER_AREA);
			getDataFromFlashToRAM( getLastRecordStartAdress() );
			//flashInit(0);
			return TRUE;
		//break;
//		case 1:
//			selectFlashArea(FLASH_BACKUP_AREA);
//			flashInit(2);
//			k=FLASH1_START_ADDRESS;
//			i=0;
//			for(i=0;i<FLASH1_USER_SIZE;i+=MAX_WRITABLE_SIZE){
//				m=(uint32_t)(flashArea+i);
//				if (Chip_IAP_PreSectorForReadWrite(FLASH1_START_SECTOR_NUM, FLASH1_END_SECTOR_NUM) == IAP_CMD_SUCCESS){
//					if (Chip_IAP_CopyRamToFlash(k,m,MAX_WRITABLE_SIZE) != IAP_CMD_SUCCESS)
//					{
//						return FALSE;
//					}
//				}else{
//						return FALSE;

//				}
//				k+=MAX_WRITABLE_SIZE;
//			}
//			
//			ptr=(uint8_t *)FLASH1_START_ADDRESS;
//			for(i=0;i<FLASH1_USER_SIZE;i++){
//				 if(ptr[i]!=flashArea[i]){
//					 return FALSE;
//				 }
//			}
//			flashInit(1);
//			return TRUE;
		//break;
	}
	
	return FALSE;
}

//****************************************************************************************************
//FonksiyonAd�:		selectFlashArea
//A��klama:   
//

void selectFlashArea (uint8_t area) {
	
	
	if ( area != FLASH_PARAMETER_AREA ) {
		
		selectedFlashNo = 2;
	}else {
		selectedFlashNo = 0;
	}
	
}




