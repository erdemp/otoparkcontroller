#include "Ethernet.h"



uint8_t memsize[2][8] = {{2,2,2,2,2,2,2,2},{2,2,2,2,2,2,2,2}};
ETHERNET_HandleTypeDef	ETH;	
ETH_SocketBuff_Typedef  socketBuff[CONNECTPORTSOCKET_CNT];
socket_HandleTypeDef connectSockets[CONNECTPORTSOCKET_CNT];

///////////////////////////////////
// Default Network Configuration //
///////////////////////////////////
//wiz_NetInfo gWIZNETINFO = { .mac = {0x00, 0x08, 0xdc,0x00, 0xab, 0xcd},
//                            .ip = {192, 168, 1, 123},
//                            .sn = {255,255,255,0},
//                            .gw = {192, 168, 1, 1},
//                            .dns = {0,0,0,0},
//                            .dhcp = NETINFO_STATIC };


//uint16_t	timerETH_ListenPortYarimPaketTimeOut = 0;
uint16_t  timerETH_globalTimeOut = 0;
uint16_t  timerETH_connectPortTimeOut = 0;
uint16_t	timerETH_SocketBusyTimeOut =0;
uint16_t  timerETH_masterConnectTimeOut = 0;
	
osThreadId Ethernet_TaskID = NULL;
osTimerDef(timerETH, timerETH_CallBack);
osTimerId timerETH_ID = NULL;
	
uint8_t localIPDefault[4] = {192,168,1,201};
uint8_t gatewayIPDefault[4] = {192,168,1,1};
uint8_t SubnetMaskAdressDefault[4] = {255,255,255,0};


/*******************************************************************************
* @brief				Bu fonksiyon Ethernet ana isleri icin hazirlanmis RTOS taskidir.
								Basalngi�ta   wiznet mod�l� kosullar. Bir adet dinleme yapan port a�ar. Bir adette gonderme i�in 
* @param[in]    None
* @return				None
*******************************************************************************/
void Ethernet_Task(void const *argument){
uint8_t gw[7],i;
	
	osDelay(250);

	
	if(timerETH_ID==NULL){
			timerETH_ID= osTimerCreate(osTimer(timerETH), osTimerPeriodic, NULL);
			osTimerStart(timerETH_ID, TIMER_ETH_CALLBACK_PERIOD);
	}
	
	ETH.Eth_PortPC_tx_fifo.buf_ptr = ETH.Eth_PortPC_tx_buf;
	init_fifo8(&ETH.Eth_PortPC_tx_fifo, ETHERNET_TCP_PCPORT_BUF_SIZE);	
	ETH.Eth_PortPC_rx_fifo.buf_ptr = ETH.Eth_PortPC_rx_buf;
	init_fifo8(&ETH.Eth_PortPC_rx_fifo, ETHERNET_TCP_PCPORT_BUF_SIZE);
	
	for (i=0; i<CONNECTPORTSOCKET_CNT; i++) {
		
		socketBuff[i].Eth_Port_tx_fifo.buf_ptr = socketBuff[i].Eth_Port_tx_buf;
		init_fifo8(&socketBuff[i].Eth_Port_tx_fifo, ETHERNET_TCP_MASTERPORTS_BUF_SIZE_TX);	
		socketBuff[i].Eth_Port_rx_fifo.buf_ptr = socketBuff[i].Eth_Port_rx_buf;
		init_fifo8(&socketBuff[i].Eth_Port_rx_fifo, ETHERNET_TCP_MASTERPORTS_BUF_SIZE_RX);		
		
	}

	
//	ETH.Eth_Port1_tx_fifo.buf_ptr = ETH.Eth_Port1_tx_buf;
//	init_fifo8(&ETH.Eth_Port1_tx_fifo, ETHERNET_TCP_MASTERPORTS_BUF_SIZE);	
//	ETH.Eth_Port1_rx_fifo.buf_ptr = ETH.Eth_Port1_rx_buf;
//	init_fifo8(&ETH.Eth_Port1_rx_fifo, ETHERNET_TCP_MASTERPORTS_BUF_SIZE);
//	
//	ETH.Eth_Port2_tx_fifo.buf_ptr = ETH.Eth_Port2_tx_buf;
//	init_fifo8(&ETH.Eth_Port2_tx_fifo, ETHERNET_TCP_MASTERPORTS_BUF_SIZE);	
//	ETH.Eth_Port2_rx_fifo.buf_ptr = ETH.Eth_Port2_rx_buf;
//	init_fifo8(&ETH.Eth_Port1_rx_fifo, ETHERNET_TCP_MASTERPORTS_BUF_SIZE);
//	
//	ETH.Eth_Port3_tx_fifo.buf_ptr = ETH.Eth_Port3_tx_buf;
//	init_fifo8(&ETH.Eth_Port3_tx_fifo, ETHERNET_TCP_MASTERPORTS_BUF_SIZE);	
//	ETH.Eth_Port3_rx_fifo.buf_ptr = ETH.Eth_Port3_rx_buf;
//	init_fifo8(&ETH.Eth_Port3_rx_fifo, ETHERNET_TCP_MASTERPORTS_BUF_SIZE);

//	ETH.Eth_Port4_tx_fifo.buf_ptr = ETH.Eth_Port4_tx_buf;
//	init_fifo8(&ETH.Eth_Port4_tx_fifo, ETHERNET_TCP_MASTERPORTS_BUF_SIZE);	
//	ETH.Eth_Port4_rx_fifo.buf_ptr = ETH.Eth_Port4_rx_buf;
//	init_fifo8(&ETH.Eth_Port4_rx_fifo, ETHERNET_TCP_MASTERPORTS_BUF_SIZE);

//	ETH.Eth_Port5_tx_fifo.buf_ptr = ETH.Eth_Port5_tx_buf;
//	init_fifo8(&ETH.Eth_Port5_tx_fifo, ETHERNET_TCP_MASTERPORTS_BUF_SIZE);	
//	ETH.Eth_Port5_rx_fifo.buf_ptr = ETH.Eth_Port5_rx_buf;
//	init_fifo8(&ETH.Eth_Port5_rx_fifo, ETHERNET_TCP_MASTERPORTS_BUF_SIZE);
//	
//	ETH.Eth_Port6_tx_fifo.buf_ptr = ETH.Eth_Port6_tx_buf;
//	init_fifo8(&ETH.Eth_Port6_tx_fifo, ETHERNET_TCP_MASTERPORTS_BUF_SIZE);	
//	ETH.Eth_Port6_rx_fifo.buf_ptr = ETH.Eth_Port6_rx_buf;
//	init_fifo8(&ETH.Eth_Port6_rx_fifo, ETHERNET_TCP_MASTERPORTS_BUF_SIZE);
	
	
	wizETH_Init();
	ETH_SetNetworkParameters();
	wizETH_SetServerSocket(SOCK_TCP_PC,PORT_PC);
	
	getGAR(gw);	
	getSIPR(ETH.LocalIP);


	
	for(;;){


		if ( ETH_CheckIncomingDataOnListenPortPc(SOCK_TCP_PC,PORT_PC,&ETH.PortPC_ErrorCode) == true ) {
//					ETH_PackageControl(PORT_LISTEN_PC);
		}
		ETH_PackageControl_PC(PORT_PC);
		
		if (ETH.commandDisable==0) {
			ETH_MasterFunc();
		}
		
//		if ( ETH_CheckIncomingDataOnListenPortController(SOCK_TCPS_INCOMING_CONTROLLER,PORT_LISTEN_CONTROLLER,&ETH.ListenPortErrorCode) == true ) {
////					ETH_PackageControl(PORT_LISTEN_PC);
//		}		
//		
//		ETH_PackageControl(PORT_LISTEN_CONTROLLER);
		
		__nop();

		osDelay(10);

	}
	
}
 /***********************************************************************
 * @brief 	buffer icerisindeki data kontrol edilir. cihazda tanimli bir paket geldi ise  
 *					istenen data geri gonderilir yada islem yapilir.

 * @param		-
 *				
 * @return		-
 ***********************************************************************/
void ETH_PackageControl_PC ( uint32_t portNo) {
uint8_t buff[MAXPACKAGESIZE];
uint16_t datasize;	
	
	memset (buff,0,MAXPACKAGESIZE);
	
	if ( ETH_getListenPackageFromFifo (portNo, buff, &datasize ) == false ) {
		return;
	}
	
	
	if ((strncmp((char const*)buff,CMD_RECORDCOMMANDS,sizeof(CMD_RECORDCOMMANDS)-1) == 0) ) { 
		
		ETH_RecordCommands();			

	}else if ((strncmp((char const*)buff,CMD_DELETECOMMANDS,sizeof(CMD_DELETECOMMANDS)-1) == 0) ) { 
		
		ETH_DeleteCommands();			
			
	}else if ((strncmp((char const*)buff,CMD_CONTROLLERCOMMANDS,sizeof(CMD_CONTROLLERCOMMANDS)-1) == 0) ) { 
		
		ETH_ProgramController(buff,datasize+2,PORT_PC);	
		
	}	else if ((strncmp((char const*)buff,CMD_SENDSTATUS,sizeof(CMD_SENDSTATUS)) == 0) ) { 
			ETH_SendStatus (Master.status,portNo);
		
	}	else if ((strncmp((char const*)buff,CMD_RESETCONTROLLER,sizeof(CMD_RESETCONTROLLER)) == 0) ) { 
			ETH_SendDataOK (portNo);
		  NVIC_SystemReset();
		
	}	else if ((strncmp((char const*)buff,CMD_SETNETWORKPARAMETERS,sizeof(CMD_SETNETWORKPARAMETERS)-1) == 0) ) { 
			ETH_SendDataOK (portNo);
			ETH_GetNewNetworkParameters(buff+sizeof(CMD_SETNETWORKPARAMETERS),datasize-sizeof(CMD_SETNETWORKPARAMETERS));
			ETH_SaveNetworkParameters();
		
	}	else if ((strncmp((char const*)buff,CMD_READNETPARAMETERS,sizeof(CMD_READNETPARAMETERS)) == 0) ) { 
	
			ETH_SendNetworkParameters(portNo);
		
	}
	


}


 /***********************************************************************
 * @brief 	masterlar ile haberlesilen ana fonksiyon.  burada komutlar ��z�mlenir masterlara g�nderilir.
 *					

 * @param		-
 *				
 * @return		-
 ***********************************************************************/
void ETH_MasterFunc (void) {
masterDispReg_HandleTypeDef	guncellenecekDispReg;	
masterReadRegs_HandleTypeDef okunacakMasterlar[MAXMASTERCOUNTIN_A_COMMAND];
uint8_t cmdBuff[1024];
char Data[50];
uint8_t *startPtr;
uint16_t retsize=0;
uint16_t i,farksize;
uint8_t mastercnt=0;
uint8_t portCnt=0;
uint8_t dispregCnt=0;
uint8_t sensregCnt=0;
uint8_t temp=0;
uint8_t k,m;
	
	memset (&guncellenecekDispReg, 0, sizeof(guncellenecekDispReg));
	for (i=0; i<MAXMASTERCOUNTIN_A_COMMAND; i++ ) {
		memset (&okunacakMasterlar,0,sizeof (okunacakMasterlar));
	}
	memset (cmdBuff,0,1024);
	memset (Data,0,50);
	
	startPtr = getCmdBuffStartAdress();
	while ( getCommand(startPtr,cmdBuff,&retsize,1024) == true ) {  // buffer dan paket varsa al.
  																																
		memset (Data,0,50);
		i=0;	
		if ( extract_between ((const char *) cmdBuff, "IP[","]",Data,&farksize) == true ) {
			sscanf(Data,"%d.%d.%d.%d",(int *)&guncellenecekDispReg.masterIP[0],(int *)&guncellenecekDispReg.masterIP[1],(int *)&guncellenecekDispReg.masterIP[2],(int *)&guncellenecekDispReg.masterIP[3]);
		}
		if (guncellenecekDispReg.masterIP[0] == 0 && 
				guncellenecekDispReg.masterIP[1] == 0 &&
				guncellenecekDispReg.masterIP[2] == 0 &&
				guncellenecekDispReg.masterIP[3] == 0  ) {
					memset (cmdBuff,0,1024);										// IP no su 0.0.0.0 ise devam etme. yeni paket var mi diye bak.
					memset (Data,0,50);
					startPtr += retsize;
					continue;
				}
		i += farksize;
		i += strlen(Data);
		i += 4; // IP[ ] i�in
		
		memset (Data,0,50);
		if ( extract_between ((const char *) (cmdBuff+i), "P[","]",Data,&farksize) == true ) {
			sscanf(Data,"%d",(int *)&guncellenecekDispReg.RS485Port);
		}
		if ( guncellenecekDispReg.RS485Port == 0 || guncellenecekDispReg.RS485Port > 4 ) {
			memset (cmdBuff,0,1024);														// Port no su hatali ise devam etme. yeni paket var mi diye bak.
			memset (Data,0,50);
			startPtr += retsize;
			continue;
		}
		
		i += farksize;
		i += strlen(Data);
		i += 3; // P[ ] i�in
		
		memset (Data,0,50);
		if ( extract_between ((const char *) (cmdBuff+i), "DR[","]",Data,&farksize) == true ) {
			sscanf(Data,"%d",(int *)&guncellenecekDispReg.DispReg);
		}
		if ( guncellenecekDispReg.DispReg == 0 || guncellenecekDispReg.DispReg > DISPLAYCOMMANDREGSAYISI ) {
			memset (cmdBuff,0,1024);
			memset (Data,0,50);																	// Display grup no su hatali ise devam etme. yeni paket var mi diye bak.
			startPtr += retsize;
			continue;
		}
		i += farksize;
		i += strlen(Data);
		i += 4; // DR[ ] i�in

		if ( cmdBuff[i] != '=' || cmdBuff[i] == 0 || cmdBuff[i] == STX) {
			memset (cmdBuff,0,1024);							// paket hatali ise devam etme.
			memset (Data,0,50);
			startPtr += retsize;
			continue;
		}
		
		mastercnt = 0;
		while ( cmdBuff[i] != ETX && cmdBuff[i] != 0 ) {
																														// paket var. STX g�rene kadar paket i�erigini analiz et.
			//{02}IP[192.168.1.3]P[3]DR[1]=IP[192.168.1.5]P[1]SG[1]SG[2]SG[3]P[2]SG[1]DR[1]+IP[192.168.1.4]P[1]SG[1]SG[2]SG[3]P[2]SG[1]DR[1]{03}
			// = den sonra + ya kadar olan kisim i�in analiz yap.
			// IP[192.168.1.5]P[1]SG[1]SG[2]SG[3]P[2]SG[1]DR[1]  gibi.
			// bunlari okunacakMasterlar dizisine at. Bu dizinin bir elemani i�inde okunacak master in IP si, o IP deki portlar, her bir porttaki sens grup ve disp regleri vardir.
			// rutinin sonunda okunacakMasterlar dizisinin 5 tane elemani varsa, gidip bu bes IP yi okur ve ilgili registerlarini toplar.
			memset (Data,0,50);
			if ( extract_between ((const char *) (cmdBuff+i), "IP[","]",Data,&farksize) == true ) {  // okunacak master in IP sini kaydet.
				sscanf(Data,"%d.%d.%d.%d",(int *)&(okunacakMasterlar[mastercnt].IP[0]),(int *)&(okunacakMasterlar[mastercnt].IP[1]),(int *)&(okunacakMasterlar[mastercnt].IP[2]),(int *)&(okunacakMasterlar[mastercnt].IP[3]));
			}else {
				__nop();
				break; // hatali data;
			}
			i += farksize;
			i += strlen(Data);
			i += 4; // IP[ ] i�in
			
			
			
			while ( cmdBuff[i] != ETX && cmdBuff[i] != 0  && cmdBuff[i] != '+') {  
			
			 
				  portCnt=0;
					if (cmdBuff[i] == 'P' && cmdBuff[i+1]== '[' && cmdBuff[i+3] == ']' ) {
							
						portCnt =  (uint8_t) atoi((const char *)cmdBuff+i+2);
						
						i += 4; // P[x] i�in
						if ( portCnt > 4 || portCnt == 0) {
							__nop();
							break;
						}
						portCnt--;
					
						dispregCnt=0;
						sensregCnt=0;
						while ( cmdBuff[i] != ETX && cmdBuff[i] != 0  && cmdBuff[i] != '+' && cmdBuff[i]!= 'P') {
							// paket analizleri.
							//P[1]SG[1]SG[2]SG[3]
							//P[2]SG[1]DR[1]
							
							if ( cmdBuff[i] == 'S'  && cmdBuff[i+1] == 'G' && cmdBuff[i+2] == '[' && cmdBuff[i+4] == ']'  ) {
								
									temp =  (uint8_t) atoi((const char *)cmdBuff+i+3);
									i+=5;
									if (temp == 0 || temp > SENSORGRUPSAYISI  ) {
										break;
									}
									
									if ( (sensregCnt + 1) >  ( SENSORGRUPSAYISI -1 ) ) {
										okunacakMasterlar[mastercnt].ports[portCnt].SensGrupRegs[sensregCnt-1] = 0;
									}else {
										okunacakMasterlar[mastercnt].ports[portCnt].SensGrupRegs[sensregCnt] = temp;
										sensregCnt++;
									}
	
								
							}else if ( cmdBuff[i] == 'D' && cmdBuff[i+1] == 'R' && cmdBuff[i+2] == '[' && cmdBuff[i+4] == ']' ) {
							
									temp =  (uint8_t) atoi((const char *)cmdBuff+i+3);
									i+=5;
									if (temp == 0 || temp > DISPLAYCOMMANDREGSAYISI  ) {
										break;
									}
									if ( (dispregCnt + 1) >  ( DISPLAYCOMMANDREGSAYISI -1 ) ) {
										okunacakMasterlar[mastercnt].ports[portCnt].DispRegs[dispregCnt-1] = 0;
									}else {
										okunacakMasterlar[mastercnt].ports[portCnt].DispRegs[dispregCnt] = temp;
										sensregCnt++;
									}
							}else {
								__nop();
								break; // hatali data var.
							}
						}
						__nop();
					}else {
						__nop();
						break; // hatali data var.
					}
					__nop();
			}
			//bir paketin i�inde ayni adresli masterdan okuma varsa. onu engelle. cunku baglanti sorunlari oluyor.
			//{02}IP[192.168.1.3]P[3]DR[1]=IP[192.168.1.5]P[1]SG[1]SG[2]SG[3]P[2]SG[1]DR[1]+IP[192.168.1.5]P[1]SG[1]SG[2]SG[3]P[2]SG[1]DR[1]{03}
			// gibi
			for (k=0;k<mastercnt;k++) {
				if ( okunacakMasterlar[k].IP[0] == okunacakMasterlar[mastercnt].IP[0] &&
						 okunacakMasterlar[k].IP[1] == okunacakMasterlar[mastercnt].IP[1] &&					
				     okunacakMasterlar[k].IP[2] == okunacakMasterlar[mastercnt].IP[2] &&
						 okunacakMasterlar[k].IP[3] == okunacakMasterlar[mastercnt].IP[3] ) {
							
							 okunacakMasterlar[mastercnt].IP[0] = 0;
							 okunacakMasterlar[mastercnt].IP[1] = 0;
							 okunacakMasterlar[mastercnt].IP[2] = 0;
							 okunacakMasterlar[mastercnt].IP[3] = 0;
							 okunacakMasterlar[mastercnt].status = 0;
							 for (m=0;m<DISPLAYCOMMANDREGSAYISI;m++) {
								 okunacakMasterlar[mastercnt].ports[0].DispRegs[m] = 0;
								 okunacakMasterlar[mastercnt].ports[1].DispRegs[m] = 0;
								 okunacakMasterlar[mastercnt].ports[2].DispRegs[m] = 0;
								 okunacakMasterlar[mastercnt].ports[3].DispRegs[m] = 0;
								}		
							 for (m=0;m<SENSORGRUPSAYISI;m++) {
								 okunacakMasterlar[mastercnt].ports[0].SensGrupRegs[m] = 0;
								 okunacakMasterlar[mastercnt].ports[1].SensGrupRegs[m] = 0;
								 okunacakMasterlar[mastercnt].ports[2].SensGrupRegs[m] = 0;
								 okunacakMasterlar[mastercnt].ports[3].SensGrupRegs[m] = 0;
 						 }	
						 mastercnt--;							 
				}
			}
			mastercnt++;
			__nop();
			
		}
		//{02}IP[192.168.1.3]P[3]DR[1]=IP[192.168.1.5]P[1]SG[1]SG[2]SG[3]P[2]SG[1]DR[1]+IP[192.168.1.4]P[1]SG[1]SG[2]SG[3]P[2]SG[1]DR[1]{03}
		// 192.168.1.3 i�in hangi masterlarin okunmasi gerektigi analizi bitti. hangi masterlerin okunmasi gerektigi 	okunacakMasterlar
		// dizisine atildi.
		startPtr += retsize;
		// simdi guncellenecekDispReg = IP[192.168.1.3]P[3]DR[1]
		// okunacak masterlar ise 
		// IP[192.168.1.5]P[1]SG[1]SG[2]SG[3]P[2]SG[1]DR[1]  ve 
		// IP[192.168.1.4]P[1]SG[1]SG[2]SG[3]P[2]SG[1]DR[1] 
		ETH_ReadMasterUpdateDisp ( &guncellenecekDispReg, &okunacakMasterlar[0]);
		// istenen display register i guncellendi yada guncellenemedi. 
		// baska komut var mi diye dongu basine cik bak.
	}
	
	
	
}

 /***********************************************************************
 * @brief 	okunacak masterler bilgilerine bakarak, gider okur ve guncellenecek mastera yazar.
 *					

 * @param		-
 *				
 * @return		-
 ***********************************************************************/
void ETH_ReadMasterUpdateDisp (masterDispReg_HandleTypeDef	*guncellenecekDispRegPtr, masterReadRegs_HandleTypeDef *okunacakMasterlarPtr ) {
uint16_t i;
uint8_t MasterPtr=0;
uint8_t emptySocket;
uint16_t freeLot=0;
uint8_t socketNo;
			
	ETH_closeAllSocket();
	
	for (i=0; i<MAXMASTERCOUNTIN_A_COMMAND; i++) {
		okunacakMasterlarPtr[i].status=0;
	}
	for (i=0; i<CONNECTPORTSOCKET_CNT; i++) {
		connectSockets[i].status=0;
	}
	
	i=0;
	
	timerETH_masterConnectTimeOut = TIMER_ETH_MASTERCONNECTTIMEOUT;
	
	while ( ETH_getNextMasterAddress( okunacakMasterlarPtr, &MasterPtr ) == true || ETH_commInProgress()==true ) {
		// okunacak master yada hali hazirda haberlesilen bir master var ise ,
		// okunacak masterlar bitene yada son haberlesme bitene yada time out olana kadar d�n
		if ( ETH_getNextMasterAddress( okunacakMasterlarPtr, &MasterPtr ) == true && ETH_checkEmptySocket(&emptySocket) == true ) {
			// okunacak master ve bos bir TCP soketi var ise, bu soketi okunacak master i�in kullan. 
			// su an 5 adet TCP soketi var. okunacak bir master var ise bostaki sokete ilgili bilgileri yaziliyor.
			//  soket IP adresini ve status bilgisi tutuyor. status 0 ise soket bos demek. 
			// ayni anda 5 adet TCP soketi masterlar ile haberlesmek i�in kullaniliyor. 
			// yani 7 adet master okunmasi gerekiyor ise, 5 adet paralel olarak okunuyor. soket bosalinca 6. ve 7. de okunuyor.
				if ( ETH_connectToMaster ( & (okunacakMasterlarPtr[MasterPtr]), emptySocket ) == true ) {
					// master a baglan. ama bekleme yapma.
					timerETH_masterConnectTimeOut = TIMER_ETH_MASTERCONNECTTIMEOUT;
					if ( MasterPtr +1 < MAXMASTERCOUNTIN_A_COMMAND ) {
							MasterPtr ++;
					}else {
						break;  // bu kadara master olmamali. Var ise �ik. 
					}
					
				}
		}

		for (i=0; i<CONNECTPORTSOCKET_CNT; i++) {
			if ( connectSockets[i].timeOut  == 0 && connectSockets[i].status != 0 ) {			// herhangi bir sokette timeout oldu mu
				if ( i == 0 ) {															// 
					socketNo = SOCK_TCP_1;
				}else if ( i == 1 ) {
					socketNo = SOCK_TCP_2;
				}else if ( i == 2 ) {
					socketNo = SOCK_TCP_3;
				}else if ( i == 3) {
					socketNo = SOCK_TCP_4;
				}else if ( i == 4 ) {
					socketNo = SOCK_TCP_5;
				}else if ( i == 5 ) {
					socketNo = SOCK_TCP_6;
				}else {
					ETH_closeAllSocket();												// soket numarasi tanimli olmayan bir numara olarak g�z�k�yor.
					return;																			// b�t�n soketleri kapat, temizle ve �ik. 
				}
				(void)disconnect(socketNo,SOCKETIMEOUT);
				close(socketNo ,SOCKETIMEOUT); // socket close			// o soketi kapat. ve i�indeki bilgileri temizle.	
				(void) ETH_CheckConnectPortClosed(1000,socketNo);
				connectSockets[i].status = 0;
				connectSockets[i].IP[0]=0;
				connectSockets[i].IP[1]=0;
				connectSockets[i].IP[2]=0;
				connectSockets[i].IP[3]=0;

			}
		}
		
		ETH_readMasters(okunacakMasterlarPtr, & freeLot);
		
		if ( timerETH_masterConnectTimeOut == 0 && ETH_getNextMasterAddress( okunacakMasterlarPtr, &MasterPtr ) == false ) {
			ETH_closeAllSocket();
			//masterlari okumada timeout olustu. belki 5 tanenin 3 tanesini okunustu.
			// o yuzden guncellenecekDispReg a bosyer sayisini genede gonder. 
			(void)ETH_setDisplayReg(guncellenecekDispRegPtr, freeLot); // == false ) {
				ETH_closeAllSocket();
		//	}
			return;
		}else {
			timerETH_masterConnectTimeOut = TIMER_ETH_MASTERCONNECTTIMEOUT;
		}
		
		
		
	}
	
	ETH_setDisplayReg(guncellenecekDispRegPtr, freeLot);
	ETH_closeAllSocket();
	
}
/*********************************************************************//**
 * @brief 	ETH_setDisplayReg 	
 * @param		None
 * @return		None
 ***********************************************************************/
bool ETH_setDisplayReg (masterDispReg_HandleTypeDef	*guncellenecekDispRegPtr , uint16_t freeLot) {
uint8_t i ;
uint8_t socketNo;
uint8_t databuff[50];
uint16_t datasize;
char strbuff[20];
	
	i=0;
	
	timerETH_masterConnectTimeOut = TIMER_ETH_MASTERCONNECTTIMEOUT;
	
	for (;;) {
		
		if ( connectSockets[i].status == 0 ) {
			
			connectSockets[i].timeOut = TIMER_ETH_CONNECTSOKETTIMEOUT;	
			if ( i == 0 ) {
				socketNo = SOCK_TCP_1;	
			}else if ( i == 1 ) {
				socketNo = SOCK_TCP_2;
			}else if ( i == 2 ) {
				socketNo = SOCK_TCP_3;
			}else if ( i == 3) {
				socketNo = SOCK_TCP_4;
			}else if ( i == 4 ) {
				socketNo = SOCK_TCP_5;
			}else {
				break; 
			}
	
			connectSockets[i].IP[0]=guncellenecekDispRegPtr->masterIP[0];
			connectSockets[i].IP[1]=guncellenecekDispRegPtr->masterIP[1];
			connectSockets[i].IP[2]=guncellenecekDispRegPtr->masterIP[2];
			connectSockets[i].IP[3]=guncellenecekDispRegPtr->masterIP[3];
			
			for(;;) { 
				
				if ( timerETH_masterConnectTimeOut == 0 ) {
					return false;
				}

				if ( connectSockets[i].timeOut  == 0 ) {
					(void)disconnect(socketNo,SOCKETIMEOUT);
					close(socketNo ,SOCKETIMEOUT); // socket close		
					(void) ETH_CheckConnectPortClosed(1000,socketNo);
					connectSockets[i].status = 0;
					connectSockets[i].IP[0]=0;
					connectSockets[i].IP[1]=0;
					connectSockets[i].IP[2]=0;
					connectSockets[i].IP[3]=0;
					return false;

				}
				if ( connectSockets[i].status == SOCKET_CONNECTED ) {
					
					memset(databuff,0,50);
					databuff[0] = STX;
					memcpy(databuff+1,CMD_SETDISPLAYCOMMANDREGS,sizeof(CMD_SETDISPLAYCOMMANDREGS)-1);
					if ( guncellenecekDispRegPtr->RS485Port == 1 ) {
						strcat((char *)databuff,ANSWER_PORT1);
					}else if ( guncellenecekDispRegPtr->RS485Port == 2 ) {
					  strcat((char *)databuff,ANSWER_PORT2);
					}else if ( guncellenecekDispRegPtr->RS485Port == 3 ) {
						strcat((char *)databuff,ANSWER_PORT3);
					}else if ( guncellenecekDispRegPtr->RS485Port == 4 ) {
						strcat((char *)databuff,ANSWER_PORT3);
					}else {
						close(socketNo ,SOCKETIMEOUT); // socket close		
						if ( ETH_CheckConnectPortClosed(1000,socketNo) == true ){
							connectSockets[i].status = 0;
							break;
						}
					}
					memset (strbuff,0,20);
					sprintf(strbuff, "[%d,%d]", guncellenecekDispRegPtr->DispReg, freeLot ); 
					strcat ((char *)databuff,strbuff);
					databuff[strlen((char *)databuff)] = ETX;
					
					if ( ETH_socketComm(socketNo, databuff, strlen((char *)databuff),connectSockets[i].IP, ETH.Port_SistemPortNo, &(connectSockets[i].status)) != SOCK_OK ) {
				//		break;
					}	
					
				}else if ( connectSockets[i].status == SOCKET_DATAWAIT  )  {
					osDelay(200);
					if ( ETH_socketCommReceive(socketNo, &(socketBuff[i].Eth_Port_rx_fifo),1, connectSockets[i].IP, ETH.Port_SistemPortNo, &(connectSockets[i].status)) == SOCK_OK ) {
						if ( ETH_getPackageFromFifo ( &socketBuff[i].Eth_Port_rx_fifo, databuff, &datasize ) == true ) {
							if ((strncmp((const char*)databuff,"<DATAOK>",sizeof("<DATAOK>")-1) == 0) ) {
								(void)disconnect(socketNo,SOCKETIMEOUT);
								close(socketNo ,SOCKETIMEOUT); // socket close		
								if ( ETH_CheckConnectPortClosed(1000,socketNo) == true ){
									connectSockets[i].status = 0;
									return true;
								}
								__nop();	
							}
						}			

						return false;
					}
					

//					close(socketNo ,SOCKETIMEOUT); // socket close		
//					if ( ETH_CheckConnectPortClosed(1000,socketNo) == true ){
//						connectSockets[i].status = 0;
//						break;
//					}
//					__nop();		
					
				}else  {
					memset(databuff,0,50);
					if ( ETH_socketComm(socketNo, databuff,10, connectSockets[i].IP, ETH.Port_SistemPortNo, &(connectSockets[i].status)) != SOCK_OK ) {
					//	break;
					}
					
				}
			}
		}else {
			i++;
		}
			
	}
	return false;
}
/*********************************************************************//**
 * @brief 	Ethernet get Data 	
 * @param		None
 * @return		None
 ***********************************************************************/
bool ETH_GetDataFromRemoteIP(uint8_t *RemoteIP, uint16_t RemotePort, uint8_t *senddata, uint8_t *returndata, uint8_t *retsize, uint16_t timeout){
#define BUFFSIZE	100
uint8_t buff[BUFFSIZE];
uint8_t i;
//int32_t ret=0;
int8_t err=0;
//uint8_t DataOK=0;	
//uint8_t gw[4];
uint8_t DataSendOK = 0;
uint8_t connectedIP[4];
	


	
		if (senddata[0] != STARTOFTEXT ) {
			return false;
		}
		
		i=0;
		while (1) {
			buff[i] = senddata[i];
			if (buff[i] == ENDOFTEXT ) {
				break;
			}			
			i++;
			if (i >= BUFFSIZE){
				return false;
			}

		}
		i++;
		timerETH_connectPortTimeOut = timeout;		
		while ( timerETH_connectPortTimeOut != 0 ) {
//				if ( ETH_SendData (1,senddata,RemoteIP,5000) == true ) {
//					
//				}
				if (  ETH_SendDataConnectPort (RemoteIP,RemotePort,buff,i) == true ) {
					DataSendOK=1;
					break;					
				}
		}
		
		if ( DataSendOK == 0 ) {
				timerETH_connectPortTimeOut = 0;
				close(1 ,SOCKETIMEOUT); // socket close	
				return false;
		}
		
		for (i=0;i<BUFFSIZE;i++) {
			buff[i]=0;
		}
		
		timerETH_connectPortTimeOut = timeout;
		while ( timerETH_connectPortTimeOut != 0 ) {
				if ( ETH_WaitDataFromConnectPort (buff, retsize, &err) == true ) {
						timerETH_connectPortTimeOut = 0;
						getSn_DIPR(1,connectedIP);		
						if ( RemoteIP[0] == connectedIP[0] && RemoteIP[1] == connectedIP[1] && RemoteIP[2] == connectedIP[2] && RemoteIP[3] == connectedIP[3] ) {
							// dogru. baglandimiz yerden cevap geldi. 
						  for (i=0;i<*retsize;i++) {
								returndata[i] = buff[i];
							}
							close(1 ,SOCKETIMEOUT); // socket close		
							if ( ETH_CheckConnectPortClosed(1000,1) == true ){
								return true;	
							}
							return true;	
						}					
						break;
				}else {
			    if (err < 0 ) {
						//error occured. �ik git
						
						break;
					}
				}
			}

			close(1 ,SOCKETIMEOUT); // socket close	
			if ( ETH_CheckConnectPortClosed(1000,1) == true ){
				__nop();
				//	return true;	
			}			

			
			return false;

}

/*********************************************************************//**
 * @brief 	Ethernet mod�l Driver Init	
 * @param		None
 * @return		None
 ***********************************************************************/
bool  ETH_WaitDataFromConnectPort(uint8_t *databuff,  uint8_t *retsize, int8_t *errcode){
int32_t ret;//,i,k,m;
uint16_t size = 0;// sentsize=0;
#define DATA_BUF_SIZE 200
#define STRLEN	10
//static uint8_t buf[DATA_BUF_SIZE];
//static bool soketestablished = 0;
//static bool comm_started=true;
//char str[STRLEN];
//uint8_t len;
	
//	 if ( timerETH_ListenPortYarimPaketTimeOut == 0 ) {
//		 soketestablished = false;  // ger�ek anlamda soketi kapatmaz. bir sekilde yarim kalan paketin devamini algilamak i�in kullanilan birsey.
//																// 
//	 }
	 
	 switch(getSn_SR(1))
   {
      case SOCK_ESTABLISHED :			
			
         if(getSn_IR(1) & Sn_IR_CON)
         {
            setSn_IR(1,Sn_IR_CON);
         }
         if((size = getSn_RX_RSR(1)) > 0)
         {
            if(size > DATA_BUF_SIZE) size = DATA_BUF_SIZE;
            ret = recv(1,databuff,size,SOCKETIMEOUT);
            if(ret <= 0) {
							*errcode = ret;
							return false;
						}
//						if ( soketestablished == 0 ) {
//							timerETH_ListenPortYarimPaketTimeOut = PORTLISTENYARIMPAKETTIMEOUT;  // data geldikten sonra 100ms ge�ip tekrar data gelirse onu yeni paket gidi algilamak i�in
//							soketestablished = true;
							if (databuff[0] == STARTOFTEXT && databuff[size-1] == ENDOFTEXT) {
									*retsize = size;
									*errcode = 0;
									return true;
////								comm_started = true;
////								getSn_DIPR(SOCK_TCPC_OUTGOING,ETH.RemoteIPListen);
////								
////								addElement_fifo8 (&(ETH.Eth_PortListen_rx_fifo),buf[0]);
////								
////								for (k=0;k<4;k++) {
////									for (m=0;m<STRLEN;m++) {
////										str[m] = 0;
////									}
////									itoa32(ETH.RemoteIPListen[k],str);
////									len=strlen(str);
////									for (i=0;i<len;i++) {
////										addElement_fifo8 (&(ETH.Eth_PortListen_rx_fifo),str[i]);
////									}
////									if ( k < 3) {
////										addElement_fifo8 (&(ETH.Eth_PortListen_rx_fifo),'.');									
////									}
////								}
													
//								addElement_fifo8 (&(ETH.Eth_PortListen_rx_fifo),':');
//								for (i=1;i<size;i++) {
//									addElement_fifo8 (&(ETH.Eth_PortListen_rx_fifo),databuff[i]);
//								}
//								if (databuff[size-1] == ENDOFTEXT) {
//									*retsize = i;
//									*errcode = 0;
//									return true;
//								}
							}else {
								clear_fifo8( &ETH.Eth_PortPC_rx_fifo);
								*errcode=0;
								return false;
							}
//						}else {
//							if ( comm_started == true ) {
//								for (i=0;i<size;i++) {
//									addElement_fifo8 (&(ETH.Eth_PortListen_rx_fifo),buf[i]);
//								}
//								if (buf[size-1] == ENDOFTEXT) {
//									*errcode = SOCK_OK;
//									return true;
//								}
//								
//							}
//							
//						}

         }
         break;
//				 
      case SOCK_CLOSE_WAIT :
//				 comm_started = false;
//				 soketestablished = false;
			   *errcode = ret;
			   return false;
//				break;
//			
      case SOCK_INIT :
//				 soketestablished = false;
//				 comm_started = false;
			   *errcode = ret;
			   return false;
//         break;
//			
      case SOCK_CLOSED:
//				 soketestablished = false;
//				 comm_started = false;
			   *errcode = ret;
			   return false;
//         break;
				 
				 
      default:
         break;
		}
	  *errcode=0;
		return false;
}
/*********************************************************************//**
 * @brief 	ETH_CheckConnectPortClosed	
 * @param		None
 * @return		None
 ***********************************************************************/
bool  ETH_CheckConnectPortClosed(uint16_t timeout, uint16_t sn){

	 timerETH_connectPortTimeOut = timeout;
	
	 while ( timerETH_connectPortTimeOut != 0 ) {
	 
		 switch(getSn_SR(sn))
		 {
				
				case SOCK_CLOSED:
					 return true;
//					 break;
					 
					 
				default:
					 break;
			}
		}
	 
		return false;
}
/*********************************************************************//**
 * @brief 	Ethernet ETH_SendData	
 * @param		None
 * @return		None
 ***********************************************************************/
bool  ETH_SendDataConnectPort (uint8_t* destip, uint16_t destport, uint8_t *senddata, uint8_t size ) {
//#define DATA_BUF_SIZE 200
int32_t ret; // return value for SOCK_ERRORs
//uint16_t  sentsize=0;

   uint16_t any_port = 	50000;
   // Socket Status Transitions
   // Check the W5500 Socket n status register (Sn_SR, The 'Sn_SR' controlled by Sn_CR command or Packet send/recv status)
   switch(getSn_SR(1))
   {
      case SOCK_ESTABLISHED :
					if(getSn_IR(1) & Sn_IR_CON)	// Socket n interrupt register mask; TCP CON interrupt = connection with peer is successful
					{
						setSn_IR(1, Sn_IR_CON);  // this interrupt should be write the bit cleared to '1'
					}
					ret = sendDataAndRetyIfBusy(1, senddata, size ,SOCKETIMEOUT,SOCKETBUSYTIMEOUT); // Data send process (User's buffer -> Destination through H/W Tx socket buffer)
	//				ret = send(SOCK_TCPC_OUTGOING, senddata, size ,SOCKETIMEOUT); // Data send process (User's buffer -> Destination through H/W Tx socket buffer)
					if(ret < 0) // Send Error occurred (sent data length < 0)
					{					
						return false;
					}else {
						return true;
					}
//         break;

      case SOCK_CLOSE_WAIT :
         if((ret=disconnect(1,SOCKETIMEOUT)) != SOCK_OK) return false;
         break;

      case SOCK_INIT :
    	 if( (ret = connect(1, destip, destport,SOCKETIMEOUT)) != SOCK_OK) return false;	//	Try to TCP connect to the TCP server (destination)
         break;						

      case SOCK_CLOSED:
    	  close(1,SOCKETIMEOUT);
    	  if((ret=socket(1, Sn_MR_TCP, any_port++, 0x00,SOCKETIMEOUT)) != 1) return false; // TCP socket open with 'any_port' port number
         break;			
			
      default:
         break;
			
   }

   return false;
}

/*********************************************************************//**
 * @brief 	ETH_masterConnections
 * @param		None
 * @return		None
 ***********************************************************************/
void ETH_readMasters (masterReadRegs_HandleTypeDef *okunacakMasterlarPtr, uint16_t *freeLot) {
uint8_t i;
uint8_t socketNo;
uint8_t databuff[50];
	
	// b�t�n TCP soketleri i�in kontrol et. bir tanesi soket open yapmaya �alirken digeri data bekliyor olabilir.
	// o yuzden bir master i�in once SOKETOPEN sonra SOKETCONNECT sonra SOKETDATAWAIT yok. 
	// bir master i�in SOKETOPEN yaptiginda gidip diger masterleri kontrol ediyor. belki SOKETOPEN s�resi boyunca baska bir master ile haberlesebilir.
	// bir master i�in bu fonkiyona girip SOKETCONNECT yapip �ikabilir. sonra geldiginde soketten data gelip gelmedigini kontrol edebilir.
	for (i=0; i<CONNECTPORTSOCKET_CNT; i++) {
		if ( connectSockets[i].status != 0 ) {
			if ( i == 0 ) {
				socketNo = SOCK_TCP_1;
			}else if ( i == 1 ) {
				socketNo = SOCK_TCP_2;
			}else if ( i == 2 ) {
				socketNo = SOCK_TCP_3;
			}else if ( i == 3) {
				socketNo = SOCK_TCP_4;
			}else if ( i == 4 ) {
				socketNo = SOCK_TCP_5;
			}else {
				break;
			}
			
			if ( connectSockets[i].status == SOCKET_CONNECTED ) {

				if ( ETH_socketComm(socketNo, (uint8_t *)CMD_READSENSORGRUPREGS, sizeof(CMD_READSENSORGRUPREGS),connectSockets[i].IP, ETH.Port_SistemPortNo, &(connectSockets[i].status)) != SOCK_OK ) {
					close(socketNo ,SOCKETIMEOUT); // socket close		
					if ( ETH_CheckConnectPortClosed(1000,socketNo) == true ){
						connectSockets[i].status = 0;
						break;
					}
					__nop();		
					break;
				}	
				
			}else if ( connectSockets[i].status == SOCKET_DATAWAIT  )  {

				if ( ETH_socketCommReceive(socketNo, &(socketBuff[i].Eth_Port_rx_fifo),1, connectSockets[i].IP, ETH.Port_SistemPortNo, &(connectSockets[i].status)) != SOCK_OK ) {
//					close(socketNo ,SOCKETIMEOUT); // socket close		
//					if ( ETH_CheckConnectPortClosed(1000,socketNo) == true ){
//						connectSockets[i].status = 0;
//						break;
//					}
//					__nop();	
					break;
				}
				if ( connectSockets[i].status ==  SOCKET_DARARECEIVED ) {
						socketBuff[i].IP[0] = connectSockets[i].IP[0];
						socketBuff[i].IP[1] = connectSockets[i].IP[1];
						socketBuff[i].IP[2] = connectSockets[i].IP[2];
						socketBuff[i].IP[3] = connectSockets[i].IP[3];
						ETH_addFreeLot(okunacakMasterlarPtr, &socketBuff[i], freeLot );  // data gelmis
				}
				close(socketNo ,SOCKETIMEOUT); // socket close		
				if ( ETH_CheckConnectPortClosed(1000,socketNo) == true ){
					connectSockets[i].status = 0;
					break;
				}
				__nop();		
				
			}else  {
        memset(databuff,0,50);
				if ( ETH_socketComm(socketNo, databuff,10, connectSockets[i].IP, ETH.Port_SistemPortNo, &(connectSockets[i].status)) != SOCK_OK ) {
					close(socketNo ,SOCKETIMEOUT); // socket close		
					if ( ETH_CheckConnectPortClosed(1000,socketNo) == true ){
						connectSockets[i].status = 0;
						connectSockets[i].IP[0] = 0;
						connectSockets[i].IP[1] = 0;
						connectSockets[i].IP[2] = 0;
						connectSockets[i].IP[3] = 0;
						break;
					}
					__nop();	
					break;
				}
				
		  }
	}
	
	
	}
}
/*********************************************************************//**
 * @brief 	ETH_addFreeLot
 * @param		None
 * @return		None
 ***********************************************************************/
void ETH_addFreeLot ( masterReadRegs_HandleTypeDef *okunacakMasterlarPtr, ETH_SocketBuff_Typedef * buffPtr, uint16_t *freeLot ) {
uint16_t i;//,k;	
uint8_t databuff[600];
char tempbuff[150];
uint16_t datasize,farksize;	
//masterReadRegs_HandleTypeDef	tempOkunacak;
uint8_t portCnt=0;
uint8_t temp1,temp2;
uint8_t *ptr;
	
	for (i=0; i<MAXMASTERCOUNTIN_A_COMMAND; i++) {
		if ( okunacakMasterlarPtr[i].IP[0]== buffPtr->IP[0] &&
				 okunacakMasterlarPtr[i].IP[1]== buffPtr->IP[1] &&
		     okunacakMasterlarPtr[i].IP[2]== buffPtr->IP[2] &&
		     okunacakMasterlarPtr[i].IP[3]== buffPtr->IP[3] ) {
					 
			

				if ( ETH_getPackageFromFifo ( &buffPtr->Eth_Port_rx_fifo, databuff, &datasize ) == false ) {
					return;
				}
				
				//ptr = databuff
//				memset(tempbuff,0,150);
//				memcpy(tempbuff,databuff,sizeof("<SENSGRPREGS>")-1);
				
				if ((strncmp((const char*)databuff,"<SENSGRPREGS>",sizeof("<SENSGRPREGS>")-1) == 0) ) {
					
					memset(tempbuff,0,150);
					portCnt = 0;
					ptr = ( databuff + sizeof("<SENSGRPREGS>") -1 );				
					
					for (portCnt=0;portCnt<4;portCnt++) {

						if ( portCnt == 0 ) {
							if ((strncmp((const char*)ptr,"<PORT1>",sizeof("<PORT1>")-1) != 0) ) {
								continue;
							}
							ptr += sizeof("<PORT1>")-1;
							
						}else if ( portCnt == 1 ) {
							if ((strncmp((const char*)ptr,"<PORT2>",sizeof("<PORT2>")-1) != 0) ) {
								continue;
							}
							ptr += sizeof("<PORT2>")-1;
							
						}else if ( portCnt == 2 ) {
							if ((strncmp((const char*)ptr,"<PORT3>",sizeof("<PORT3>")-1) != 0) ) {
								continue;
							}
							ptr += sizeof("<PORT3>")-1;
							
						}else if ( portCnt == 3 ) {
							if ((strncmp((const char*)ptr,"<PORT4>",sizeof("<PORT4>")-1) != 0) ) {
								continue;
							}
							ptr += sizeof("<PORT4>")-1;
						}else {
							break;
						}
						
						for (i=0;i<SENSORGRUPSAYISI;i++) {
							if ( extract_between ((const char *) ptr, "[","]",tempbuff,&farksize) == true ) {
								sscanf(tempbuff,"%hhd,%hhd",&temp1,&temp2);
								if (okunacakMasterlarPtr->ports[portCnt].SensGrupRegs[i] != 0 ) {
									*freeLot += temp2;
								}
							}
							ptr += farksize;
							ptr += strlen(tempbuff);	 					
							ptr += 2  	;		// [ ve ] karakterleri i�in.	
						}	
				  }

					
					if ((strncmp((const char*)ptr,"<DISPREGS>",sizeof("<DISPREGS>")-1) == 0) ) {
						portCnt = 0;
						ptr = ( ptr + sizeof("<DISPREGS>")-1);				

						for (portCnt=0;portCnt<4;portCnt++) {

						if ( portCnt == 0 ) {
						if ((strncmp((const char*)ptr,"<PORT1>",sizeof("<PORT1>")-1) != 0) ) {
						continue;
						}
						ptr += sizeof("<PORT1>")-1;

						}else if ( portCnt == 1 ) {
						if ((strncmp((const char*)ptr,"<PORT2>",sizeof("<PORT2>")-1) != 0) ) {
						continue;
						}
						ptr += sizeof("<PORT2>")-1;

						}else if ( portCnt == 2 ) {
						if ((strncmp((const char*)ptr,"<PORT3>",sizeof("<PORT3>")-1) != 0) ) {
						continue;
						}
						ptr += sizeof("<PORT3>")-1;

						}else if ( portCnt == 3 ) {
						if ((strncmp((const char*)ptr,"<PORT4>",sizeof("<PORT4>")-1) != 0) ) {
						continue;
						}
						ptr += sizeof("<PORT4>")-1;
						}else {
						break;
						}

						for (i=0;i<DISPLAYCOMMANDREGSAYISI;i++) {
						if ( extract_between ((const char *) ptr, "[","]",tempbuff,&farksize) == true ) {
						sscanf(tempbuff,"%hhd,%hhd",&temp1,&temp2);
						if (okunacakMasterlarPtr->ports[portCnt].DispRegs[i] != 0 ) {
						*freeLot += temp2;
						}
						}
						ptr += farksize;
						ptr += strlen(tempbuff);	 					
						ptr += 2  	;		// [ ve ] karakterleri i�in.	
						}	
						}


					}	
				
				}				
					
			
		}					 
			

	}
	
}


/*********************************************************************//**
 * @brief 	ETH_getPackageFromFifo	
 * @param		None
 * @return		None
 ***********************************************************************/
bool ETH_getPackageFromFifo (fifo8_t *fifoPtr, uint8_t *data, uint16_t *datasize) {
uint8_t temp;
//uint8_t ETXdetect = 0;
uint16_t i;
//uint8_t buff[200];	
	

			if ( isEmpty_fifo8 (fifoPtr) ) {
				return false;
			}
			
			while (1) {
				temp = getElement_fifo8(fifoPtr);
				if (temp == STARTOFTEXT) {	
					i=0;
	//				data[0] = temp;
	//				i++;
					while ( isEmpty_fifo8 (fifoPtr) == false ) {
						if (i > MAXPACKAGESIZE ) {
							*datasize =MAXPACKAGESIZE;
							return false;
						}else {
							data[i] = getElement_fifo8(fifoPtr);
							if ( data[i] == ENDOFTEXT ) {
								data[i]=0;
								*datasize = i;
								//*datasize = i+1;
								return true;
							}
						}
						
						i++;
					}
					
					
				}
				if ( isEmpty_fifo8 (fifoPtr) ) {
					return false;
				}
			}
			


	return false;
}
/*********************************************************************//**
 * @brief 	ETH_commInProgress
 * @param		None
 * @return		None
 ***********************************************************************/
bool ETH_commInProgress (void) {
uint8_t i;
	
	for (i=0; i<CONNECTPORTSOCKET_CNT; i++) {
		if ( connectSockets[i].status != 0 ) {
			return true;
		}
	}
	return false;
}


/*********************************************************************//**
 * @brief 	ETH_closeAllSocket
 * @param		None
 * @return		None
 ***********************************************************************/
void ETH_closeAllSocket (void) {
uint8_t i;
uint8_t socketNo;
	
	for (i=0; i<CONNECTPORTSOCKET_CNT; i++) {

			if ( i == 0 ) {
				socketNo = SOCK_TCP_1;
			}else if ( i == 1 ) {
				socketNo = SOCK_TCP_2;
			}else if ( i == 2 ) {
				socketNo = SOCK_TCP_3;
			}else if ( i == 3) {
				socketNo = SOCK_TCP_4;
			}else if ( i == 4 ) {
				socketNo = SOCK_TCP_5;
			}

			(void)disconnect(socketNo,SOCKETIMEOUT);
			close(socketNo,SOCKETIMEOUT);
		
	}
	
	for (i=0; i<CONNECTPORTSOCKET_CNT; i++) {
		connectSockets[i].status=0;
		connectSockets[i].IP[0]=0;
		connectSockets[i].IP[1]=0;
		connectSockets[i].IP[2]=0;
		connectSockets[i].IP[3]=0;
	}
		

}
/*********************************************************************//**
 * @brief 	ETH_checkEmptySocket
 * @param		None
 * @return		None
 ***********************************************************************/
bool ETH_checkEmptySocket (uint8_t *sockNoPtr) {
uint8_t i;
uint8_t socketNo;
	
	for (i=0; i<CONNECTPORTSOCKET_CNT; i++) {
		if ( connectSockets[i].status == 0 ) {
			
			if ( i == 0 ) {
				socketNo = SOCK_TCP_1;
			}else if ( i == 1 ) {
				socketNo = SOCK_TCP_2;
			}else if ( i == 2 ) {
				socketNo = SOCK_TCP_3;
			}else if ( i == 3) {
				socketNo = SOCK_TCP_4;
			}else if ( i == 4 ) {
				socketNo = SOCK_TCP_5;
			}else {
				return false;
			}
			if ( ETH_checkSocketClosed (socketNo) ) {
				*sockNoPtr = i;
				return true;
			}else {
//				(void)disconnect(sn,SOCKETIMEOUT);
//    	  close(sn,SOCKETIMEOUT);
				return false;
			}
			
		}
		
	}
	return false;
}
/*********************************************************************//**
 * @brief 	ETH_connectToMaster
 * @param		None
 * @return		None
 ***********************************************************************/
bool ETH_connectToMaster ( masterReadRegs_HandleTypeDef *masterPtr, uint8_t socketNo ) {
uint8_t temp=0;
uint8_t soc;

	
	if ( socketNo == 0 ) {
		soc = SOCK_TCP_1;
	}else if ( socketNo == 1 ) {
		soc = SOCK_TCP_2;
	}else if ( socketNo == 2 ) {
		soc = SOCK_TCP_3;
	}else if ( socketNo == 3) {
		soc = SOCK_TCP_4;
	}else if ( socketNo == 4 ) {
		soc = SOCK_TCP_5;
	}else if ( socketNo == 5 ) {
		soc = SOCK_TCP_6;
	}else {
		return false;
	}	
	
	if ( ETH_socketComm(soc, &temp,1, masterPtr->IP, ETH.Port_SistemPortNo, &(connectSockets[socketNo].status)) != SOCK_OK ) {
		return false;
	}
	masterPtr->status = SOCKET_OPENED;
	connectSockets[socketNo].status = SOCKET_OPENED;
	connectSockets[socketNo].IP[0] = masterPtr->IP[0];
	connectSockets[socketNo].IP[1] = masterPtr->IP[1];
	connectSockets[socketNo].IP[2] = masterPtr->IP[2];
	connectSockets[socketNo].IP[3] = masterPtr->IP[3];
	connectSockets[socketNo].timeOut = TIMER_ETH_CONNECTSOKETTIMEOUT;
	
//	for (i=0; i<CONNECTPORTSOCKET_CNT; i++) {
//		if ( connectSockets[i].status == 0 ) {
//			connectSockets[i].status = CONNECT_CONNECTING;
//			connectSockets[i].IP[0] = masterPtr->IP[0];
//			connectSockets[i].IP[1] = masterPtr->IP[1];
//			connectSockets[i].IP[2] = masterPtr->IP[2];
//			connectSockets[i].IP[3] = masterPtr->IP[3];
//		}
//		
//	}
	
	return true;

}	
/*********************************************************************//**
 * @brief 	Ethernet ETH_SendData	
 * @param		None
 * @return		None
 ***********************************************************************/
bool  ETH_checkSocketClosed (uint8_t sn) {
//   int32_t ret; // return value for SOCK_ERRORs
   // Destination (TCP Server) IP info (will be connected)
   // >> loopback_tcpc() function parameter
   // >> Ex)
   //	uint8_t destip[4] = 	{192, 168, 0, 214};
   //	uint16_t destport = 	5000;

   // Port number for TCP client (will be increased)
//   uint16_t any_port = 	50000;

   // Socket Status Transitions
   // Check the W5500 Socket n status register (Sn_SR, The 'Sn_SR' controlled by Sn_CR command or Packet send/recv status)
   switch(getSn_SR(sn))
   {

      case SOCK_CLOSED:
				return true;
//				break;
			
      default:
				return false;
 //        break;
			
   }
   return false;
}
/*********************************************************************//**
 * @brief 	Ethernet ETH_SendData	
 * @param		None
 * @return		None
 ***********************************************************************/
int32_t  ETH_socketComm (uint8_t sn, uint8_t* buff, uint16_t sentsize, uint8_t* destip, uint16_t destport, uint8_t *socketStatus) {
int32_t ret; // return value for SOCK_ERRORs
//uint16_t size = 0;//, sentsize=0;
//uint8_t i;
//bool STXDONE = false;	
#define SOCKETDATA_BUF_SIZE 500
//static uint8_t buf[SOCKETDATA_BUF_SIZE];
	
   uint16_t any_port = 	50000;

   // Socket Status Transitions
   // Check the W5500 Socket n status register (Sn_SR, The 'Sn_SR' controlled by Sn_CR command or Packet send/recv status)
   switch(getSn_SR(sn))
   {
      case SOCK_ESTABLISHED :
         if(getSn_IR(sn) & Sn_IR_CON)	// Socket n interrupt register mask; TCP CON interrupt = connection with peer is successful
         {
						setSn_IR(sn, Sn_IR_CON);  // this interrupt should be write the bit cleared to '1'
         }
				 ret = sendDataAndRetyIfBusy(sn, buff, sentsize ,SOCKETIMEOUT,SOCKETBUSYTIMEOUT); // Data send process (User's buffer -> Destination through H/W Tx socket buffer)
				 if(ret < 0) // Send Error occurred (sent data length < 0)
				 {
						return 0;
				 }
				 *socketStatus = SOCKET_DATAWAIT;
         break;

      case SOCK_CLOSE_WAIT :
         if((ret=disconnect(sn,SOCKETIMEOUT)) != SOCK_OK) {
					 return ret;
				 }
				 *socketStatus = SOCKET_CLOSED;	
         break;

      case SOCK_INIT :
    	 if( (ret = connect(sn, destip, destport,SOCKETIMEOUT)) != SOCK_OK) {
				 return ret;	//	Try to TCP connect to the TCP server (destination)
			 }
			 *socketStatus = SOCKET_CONNECTED;	
				 break;

      case SOCK_CLOSED:
    	  close(sn,SOCKETIMEOUT);
    	  if((ret=socket(sn, Sn_MR_TCP, any_port++, 0x00,SOCKETIMEOUT)) != sn) {
					return ret; // TCP socket open with 'any_port' port number
				}
				*socketStatus = SOCKET_OPENED;
					break;
      default:
         break;
   }
   return 1;
}
/*********************************************************************//**
 * @brief 	Ethernet ETH_SendData	
 * @param		None
 * @return		None
 ***********************************************************************/
int32_t  ETH_socketCommReceive (uint8_t sn, fifo8_t* fifobuff, uint16_t sentsize, uint8_t* destip, uint16_t destport, uint8_t *socketStatus) {
int32_t ret; // return value for SOCK_ERRORs
uint16_t size = 0;//, sentsize=0;
uint16_t i;
bool STXDONE = false;	
#define SOCKETDATA_BUF_SIZE 500
static uint8_t buf[SOCKETDATA_BUF_SIZE];
	
   uint16_t any_port = 	50000;

   // Socket Status Transitions
   // Check the W5500 Socket n status register (Sn_SR, The 'Sn_SR' controlled by Sn_CR command or Packet send/recv status)
   switch(getSn_SR(sn))
   {
      case SOCK_ESTABLISHED :
         if(getSn_IR(sn) & Sn_IR_CON)	// Socket n interrupt register mask; TCP CON interrupt = connection with peer is successful
         {
						setSn_IR(sn, Sn_IR_CON);  // this interrupt should be write the bit cleared to '1'
         }
				 if( *socketStatus == SOCKET_DATAWAIT) { // Sn_RX_RSR: Socket n Received Size Register, Receiving data length
				 
					 if ( (size = getSn_RX_RSR(sn)) > 0  ) {
						if(size > DATA_BUF_SIZE) size = SOCKETDATA_BUF_SIZE; // DATA_BUF_SIZE means user defined buffer size (array)
						ret = recv(sn, buf, size ,SOCKETIMEOUT); // Data Receive process (H/W Rx socket buffer -> User's buffer)

						if(ret <= 0) return ret; // If the received data length <= 0, receive failed and process end

						 	for (i=0;i<size;i++) {
								if (STXDONE == false ) {
									if (buf[i]==STARTOFTEXT) {
										STXDONE = true;
										//clear_fifo8( &ETH.Eth_PortListen_Pc_rx_fifo);
										
										addElement_fifo8 (fifobuff,buf[i]);
									}
								}else {
									if (buf[i]==ENDOFTEXT) {
										
										addElement_fifo8 (fifobuff,buf[i]);
										
										STXDONE = false;
									}else if (buf[i]==STARTOFTEXT) {
										STXDONE = true;
										//clear_fifo8( &ETH.Eth_PortListen_Pc_rx_fifo);
										addElement_fifo8 (fifobuff,buf[i]);		
									}else {
										addElement_fifo8 (fifobuff,buf[i]);								
									}
								
								}

//
																			
							}
						 
						 
					 *socketStatus = SOCKET_DARARECEIVED;
					  return 1;
					 }else {
						 return 0;
					 }
//buralara bak.
					}else {
//						while (sentsize > WIZCHIP_READ_WRITE_SIZE-3 ) {
							//ret = send(SOCK_TCPS_INCOMMING_PC, data, WIZCHIP_READ_WRITE_SIZE-3,SOCKETIMEOUT); // Data send process (User's buffer -> Destination through H/W Tx socket buffer)
							ret = sendDataAndRetyIfBusy(sn, buf, sentsize ,SOCKETIMEOUT,SOCKETBUSYTIMEOUT); // Data send process (User's buffer -> Destination through H/W Tx socket buffer)
							if(ret < 0) // Send Error occurred (sent data length < 0)
							{
								return 0;
							}
//							buf += WIZCHIP_READ_WRITE_SIZE-3;
//							size -= WIZCHIP_READ_WRITE_SIZE-3;
//							
//							if ( size < WIZCHIP_READ_WRITE_SIZE-3 ) {
//								break;
//							}
//						}
						*socketStatus = SOCKET_DATAWAIT;
					}
		 
         break;

      case SOCK_CLOSE_WAIT :
         if((ret=disconnect(sn,SOCKETIMEOUT)) != SOCK_OK) {
					 return ret;
				 }
				 *socketStatus = SOCKET_CLOSED;	
         break;

      case SOCK_INIT :
    	 if( (ret = connect(sn, destip, destport,SOCKETIMEOUT)) != SOCK_OK) {
				 return ret;	//	Try to TCP connect to the TCP server (destination)
			 }
			 *socketStatus = SOCKET_CONNECTED;	
				 break;

      case SOCK_CLOSED:
    	  close(sn,SOCKETIMEOUT);
    	  if((ret=socket(sn, Sn_MR_TCP, any_port++, 0x00,SOCKETIMEOUT)) != sn) {
					return ret; // TCP socket open with 'any_port' port number
				}
				*socketStatus = SOCKET_OPENED;
					break;
      default:
         break;
   }
   return 1;
}
/*********************************************************************//**
 * @brief 	ETH_SetNetworkParameters	
 * @param		None
 * @return		None
 ***********************************************************************/
bool ETH_getNextMasterAddress ( masterReadRegs_HandleTypeDef *masterbuff, uint8_t *adresPtr) {
//uint8_t var=0;
uint8_t i,k;
	
	for (i=0; i<MAXMASTERCOUNTIN_A_COMMAND; i++) {
		if ( masterbuff[i].IP[0] != 0 && 
			   masterbuff[i].IP[1] != 0 && 
		     masterbuff[i].IP[2] != 0 &&  
		     masterbuff[i].IP[3] != 0 &&  
		     masterbuff[i].status==0 ) {
					 
					 for (k=0;k<CONNECTPORTSOCKET_CNT;k++) {
							if ( masterbuff[i].IP[0] == connectSockets[k].IP[0] && 
									 masterbuff[i].IP[1] == connectSockets[k].IP[1] &&
									 masterbuff[i].IP[2] == connectSockets[k].IP[2] && 							
									 masterbuff[i].IP[3] == connectSockets[k].IP[3]  ) {
									return false;
							}										 
					 }
				*adresPtr = i;
		    return true;
		}
	}
	
	return false;
	
}
/*********************************************************************//**
 * @brief 	ETH_SetNetworkParameters	
 * @param		None
 * @return		None
 ***********************************************************************/
void ETH_SetNetworkParameters ( void) {
		
	setGAR(ETH.GatewayIP);	
	setSUBR(ETH.SubnetMaskAdress);
	setSIPR(ETH.LocalIP);
	
}
/*********************************************************************//**
 * @brief 	ETH_SetDefaultParameters	
 * @param		None
 * @return		None
 ***********************************************************************/
void ETH_GetDefaultParameters ( ETHERNET_HandleTypeDef *ptrETH) {
	
	ptrETH->GatewayIP[0] = gatewayIPDefault[0];
	ptrETH->GatewayIP[1] = gatewayIPDefault[1];
	ptrETH->GatewayIP[2] = gatewayIPDefault[2];
	ptrETH->GatewayIP[3] = gatewayIPDefault[3];
	
	ptrETH->LocalIP[0] = localIPDefault[0];
	ptrETH->LocalIP[1] = localIPDefault[1];
	ptrETH->LocalIP[2] = localIPDefault[2];
	ptrETH->LocalIP[3] = localIPDefault[3];

	ptrETH->SubnetMaskAdress[0] = SubnetMaskAdressDefault[0];
	ptrETH->SubnetMaskAdress[1] = SubnetMaskAdressDefault[1];
	ptrETH->SubnetMaskAdress[2] = SubnetMaskAdressDefault[2];
	ptrETH->SubnetMaskAdress[3] = SubnetMaskAdressDefault[3];
	


	ptrETH->Port_PCPortNo = PORT_PC;
	socketBuff[0].PortNo =PORT_PORT1;
	socketBuff[1].PortNo =PORT_PORT2;
	socketBuff[2].PortNo =PORT_PORT3;
	socketBuff[3].PortNo =PORT_PORT4;
	socketBuff[4].PortNo =PORT_PORT5;
	

	ptrETH->Port_SistemPortNo = PORT_SISTEM;

}


/*********************************************************************//**
 * @brief 	Ethernet mod�l Driver Init	
 * @param		None
 * @return		None
 ***********************************************************************/
bool  ETH_CheckIncomingDataOnListenPortPc(uint8_t sn, uint16_t portno, int8_t *errcode){
int32_t ret,i;//,k,m;
uint16_t size = 0;// sentsize=0;
#define DATA_BUF_SIZE_LISTEN 1500
#define STRLEN	10
uint8_t buf[DATA_BUF_SIZE_LISTEN];
bool STXDONE = false;	

	 switch(getSn_SR(sn))
   {
      case SOCK_ESTABLISHED :		
         if(getSn_IR(sn) & Sn_IR_CON)
         {
            setSn_IR(sn,Sn_IR_CON);
         }								 
				 if((size = getSn_RX_RSR(sn)) > 0)
         {  
					 memset (buf,0,DATA_BUF_SIZE_LISTEN);
						if(size > DATA_BUF_SIZE_LISTEN) size = DATA_BUF_SIZE_LISTEN;
							ret = recv(sn,buf,size,SOCKETIMEOUT);
						if(ret <= 0) {
							*errcode = ret;
							return false;
						}
						//BURDA DATAYI EKSIK ALIYOR.
						for (i=0;i<size;i++) {
							if (STXDONE == false ) {
								if (buf[i]==STARTOFTEXT) {
									STXDONE = true;
									//clear_fifo8( &ETH.Eth_PortListen_Pc_rx_fifo);
									
									addElement_fifo8 (&(ETH.Eth_PortPC_rx_fifo),buf[i]);
								}
							}else {
								if (buf[i]==ENDOFTEXT) {
									
									addElement_fifo8 (&(ETH.Eth_PortPC_rx_fifo),buf[i]);
									
									STXDONE = false;
								}else if (buf[i]==STARTOFTEXT) {
									STXDONE = true;
									//clear_fifo8( &ETH.Eth_PortListen_Pc_rx_fifo);
									addElement_fifo8 (&(ETH.Eth_PortPC_rx_fifo),buf[i]);		
								}else {
									addElement_fifo8 (&(ETH.Eth_PortPC_rx_fifo),buf[i]);								
								}
							
							}

//
																			
						}
				break;
			
      case SOCK_CLOSE_WAIT :
				 memset (buf,0,DATA_BUF_SIZE_LISTEN);
         if((ret=disconnect(sn,SOCKETIMEOUT)) != SOCK_OK) {
					 clear_fifo8( &ETH.Eth_PortPC_rx_fifo);
					 *errcode = ret;
					 return false;
				 }				
				break;
			
      case SOCK_INIT :
				 STXDONE = false;				
			   memset (buf,0,DATA_BUF_SIZE_LISTEN);
         if( (ret = listen(sn,SOCKETIMEOUT)) != SOCK_OK){
					 clear_fifo8( &ETH.Eth_PortPC_rx_fifo);
					 *errcode = ret;
					 return false;
				 }
         break;
			
      case SOCK_CLOSED:
				 STXDONE = false;				
			   memset (buf,0,DATA_BUF_SIZE_LISTEN);
			   clear_fifo8( &ETH.Eth_PortPC_rx_fifo);
         if((ret=socket(sn,Sn_MR_TCP,portno,0x00,SOCKETIMEOUT)) != sn) {
					 *errcode = ret;
					 return false;
				 }

         break;
				 
			case SOCK_LISTEN:
				__nop();
				break;


			   default:
         break;
		}
	
	}
	 
	return false;

}


/*********************************************************************//**
 * @brief 	Ethernet get Listen Data	
 * @param		None
 * @return		None
 ***********************************************************************/
bool ETH_getListenPackageFromFifo (uint32_t portNo , uint8_t *data, uint16_t *datasize) {
uint8_t temp;
//uint8_t ETXdetect = 0;
uint16_t i;
//uint8_t buff[200];	
	
	if ( portNo == PORT_PC ) {
			if ( isEmpty_fifo8 (&(ETH.Eth_PortPC_rx_fifo)) ) {
				return false;
			}
			
			while (1) {
				temp = getElement_fifo8(&(ETH.Eth_PortPC_rx_fifo));
				if (temp == STARTOFTEXT) {	
					i=0;
	//				data[0] = temp;
	//				i++;
					while ( isEmpty_fifo8 (&(ETH.Eth_PortPC_rx_fifo)) == false ) {
						if (i > MAXPACKAGESIZE ) {
							*datasize =MAXPACKAGESIZE;
							return false;
						}else {
							data[i] = getElement_fifo8(&(ETH.Eth_PortPC_rx_fifo));
							if ( data[i] == ENDOFTEXT ) {
								data[i]=0;
								*datasize = i;
								//*datasize = i+1;
								return true;
							}
						}
						
						i++;
					}
					
					
				}
				if ( isEmpty_fifo8 (&(ETH.Eth_PortPC_rx_fifo)) ) {
					return false;
				}
			}
	}		
//	}else if ( portNo == PORT_LISTEN_CONTROLLER) {
//			if ( isEmpty_fifo8 (&(ETH.Eth_PortListen_Controller_rx_fifo)) ) {
//				return false;
//			}
//			
//			while (1) {
//				temp = getElement_fifo8(&(ETH.Eth_PortListen_Controller_rx_fifo));
//				if (temp == STARTOFTEXT) {	
//					i=0;
//	//				data[0] = temp;
//	//				i++;
//					while ( isEmpty_fifo8 (&(ETH.Eth_PortListen_Controller_rx_fifo)) == false ) {
//						if (i > MAXPACKAGESIZE ) {
//							*datasize =MAXPACKAGESIZE;
//							return false;
//						}else {
//							data[i] = getElement_fifo8(&(ETH.Eth_PortListen_Controller_rx_fifo));
//							if ( data[i] == ENDOFTEXT ) {
//								data[i]=0;
//								*datasize = i;
//								//*datasize = i+1;
//								return true;
//							}
//						}
//						
//						i++;
//					}
//					
//					
//				}
//				if ( isEmpty_fifo8 (&(ETH.Eth_PortListen_Pc_rx_fifo)) ) {
//					return false;
//				}
//			}		
//		
//		
//	}

	return false;
}



/*********************************************************************//**
 * @brief 	Ethernet send Listen Data	
 * @param		None
 * @return		None
 ***********************************************************************/
bool ETH_SendDataFromListenPortPC (uint8_t *data, uint16_t size ) {	
	
int32_t ret; // return value for SOCK_ERRORs
//uint8_t STXptr = STARTOFTEXT;
//uint8_t ETXptr = ENDOFTEXT;


   // Socket Status Transitions
   // Check the W5500 Socket n status register (Sn_SR, The 'Sn_SR' controlled by Sn_CR command or Packet send/recv status)
   switch(getSn_SR(SOCK_TCP_PC))
   {
      case SOCK_ESTABLISHED :
         if(getSn_IR(SOCK_TCP_PC) & Sn_IR_CON)	// Socket n interrupt register mask; TCP CON interrupt = connection with peer is successful
         {
					setSn_IR(SOCK_TCP_PC, Sn_IR_CON);  // this interrupt should be write the bit cleared to '1'
         }

         //////////////////////////////////////////////////////////////////////////////////////////////
         // Data Transaction Parts; Handle the [data receive and send] process
         //////////////////////////////////////////////////////////////////////////////////////////////

				// Data sentsize control
				//  ret = sendDataAndRetyIfBusy(SOCK_TCPS_INCOMMING_PC, &STXptr, 1,SOCKETIMEOUT,SOCKETBUSYTIMEOUT); // Data send process (User's buffer -> Destination through H/W Tx socket buffer)
					if(ret < 0) // Send Error occurred (sent data length < 0)
					{
						return false;
					}
				  
					while (size > WIZCHIP_READ_WRITE_SIZE-3 ) {
						//ret = send(SOCK_TCPS_INCOMMING_PC, data, WIZCHIP_READ_WRITE_SIZE-3,SOCKETIMEOUT); // Data send process (User's buffer -> Destination through H/W Tx socket buffer)
						ret = sendDataAndRetyIfBusy(SOCK_TCP_PC, data, WIZCHIP_READ_WRITE_SIZE-3,SOCKETIMEOUT,SOCKETBUSYTIMEOUT); // Data send process (User's buffer -> Destination through H/W Tx socket buffer)
						if(ret < 0) // Send Error occurred (sent data length < 0)
						{
							return false;
						}
						data += WIZCHIP_READ_WRITE_SIZE-3;
						size -= WIZCHIP_READ_WRITE_SIZE-3;
						
						if ( size < WIZCHIP_READ_WRITE_SIZE-3 ) {
							break;
						}
					}
					if (size > 0 ) {
						//ret = send(SOCK_TCPS_INCOMMING_PC, data, size,SOCKETIMEOUT); // Data send process (User's buffer -> Destination through H/W Tx socket buffer)
						
						ret = sendDataAndRetyIfBusy(SOCK_TCP_PC, data, size,SOCKETIMEOUT,SOCKETBUSYTIMEOUT); // Data send process (User's buffer -> Destination through H/W Tx socket buffer)
						if(ret < 0) // Send Error occurred (sent data length < 0)
						{
						return false;
						}
					}
				
					//ret = sendDataAndRetyIfBusy(SOCK_TCPS_INCOMMING_PC, &ETXptr, 1,SOCKETIMEOUT,SOCKETBUSYTIMEOUT); // Data send process (User's buffer -> Destination through H/W Tx socket buffer)				 
					
					if(ret < 0) // Send Error occurred (sent data length < 0)
					{
						return false;
					}
					else {
						return true;
					}


 //        break;

      case SOCK_CLOSE_WAIT :

         return false;
//         break;

      case SOCK_INIT :

         return false;
//         break;

      case SOCK_CLOSED:
         return false;
       //  break;
			
      default:
         break;
   }
	 
   return false;	
	
}

///*********************************************************************//**
// * @brief 	Ethernet get Data 	
// * @param		None
// * @return		None
// ***********************************************************************/
//bool ETH_GetDataFromRemoteIP(uint8_t *RemoteIP, uint16_t RemotePort, uint8_t *senddata, uint8_t *returndata, uint8_t *retsize, uint16_t timeout){
//#define BUFFSIZE	100
//uint8_t buff[BUFFSIZE];
//uint8_t i;
////int32_t ret=0;
//int8_t err=0;
////uint8_t DataOK=0;	
////uint8_t gw[4];
//uint8_t DataSendOK = 0;
//	


//	
//		if (senddata[0] != STARTOFTEXT ) {
//			return false;
//		}
//		
//		i=0;
//		while (1) {
//			buff[i] = senddata[i];
//			if (buff[i] == ENDOFTEXT ) {
//				break;
//			}			
//			i++;
//			if (i >= BUFFSIZE){
//				return false;
//			}

//		}
//		i++;
//		timerETH_connectPortTimeOut = timeout;		
//		while ( timerETH_connectPortTimeOut != 0 ) {
////				if ( ETH_SendData (1,senddata,RemoteIP,5000) == true ) {
////					
////				}
//				if (  ETH_SendDataConnectPort (RemoteIP,RemotePort,buff,i) == true ) {
//					DataSendOK=1;
//					break;					
//				}
//		}
//		
//		if ( DataSendOK == 0 ) {
//				timerETH_connectPortTimeOut = 0;
//				close(SOCK_TCPC_OUTGOING ,SOCKETIMEOUT); // socket close	
//				return false;
//		}
//		
//		for (i=0;i<BUFFSIZE;i++) {
//			buff[i]=0;
//		}
//		
//		timerETH_connectPortTimeOut = timeout;
//		while ( timerETH_connectPortTimeOut != 0 ) {
//				if ( ETH_WaitDataFromConnectPort (buff, retsize, &err) == true ) {
//						timerETH_connectPortTimeOut = 0;
//						getSn_DIPR(SOCK_TCPC_OUTGOING,ETH.RemoteIPConnect);		
//						if ( RemoteIP[0] == ETH.RemoteIPConnect[0] && RemoteIP[1] == ETH.RemoteIPConnect[1] && RemoteIP[2] == ETH.RemoteIPConnect[2] && RemoteIP[3] == ETH.RemoteIPConnect[3] ) {
//							// dogru. baglandimiz yerden cevap geldi. 
//						  for (i=0;i<*retsize;i++) {
//								returndata[i] = buff[i];
//							}
//							close(SOCK_TCPC_OUTGOING ,SOCKETIMEOUT); // socket close		
//							if ( ETH_CheckConnectPortClosed(1000) == true ){
//								return true;	
//							}
//							return true;	
//						}					
//						break;
//				}else {
//			    if (err < 0 ) {
//						//error occured. �ik git
//						
//						break;
//					}
//				}
//			}

//			close(SOCK_TCPC_OUTGOING ,SOCKETIMEOUT); // socket close	
//			if ( ETH_CheckConnectPortClosed(1000) == true ){
//				//	return true;	
//			}			

//			
//			return false;

//}


 /***********************************************************************
 * @brief 	ETH_ProgramMaster 
 *					

 * @param		-
 *				
 * @return		-
 ***********************************************************************/
void ETH_RecordCommands (void )  {
		

	SaveAllParameterToFlash();
	getDataFromFlash();

	ETH_SendDataOK (PORT_PC);
	ETH.commandDisable=0;
	
}
 /***********************************************************************
 * @brief 	ETH_ProgramMaster 
 *					

 * @param		-
 *				
 * @return		-
 ***********************************************************************/
void ETH_DeleteCommands (void )  {
uint32_t i;
	
	for (i=0;i<CONTROLLERCOMMANDS_SIZE;i++) {
		 eraseCommandsRamArea(CONTROLLERCOMMANDS+i,0);
	 }

	SaveAllParameterToFlash();
	getDataFromFlash();

	ETH_SendDataOK (PORT_PC);
	ETH.commandDisable=1;
	
}
 /***********************************************************************
 * @brief 	ETH_ProgramMaster 
 *					

 * @param		-
 *				
 * @return		-
 ***********************************************************************/
void ETH_ProgramController (uint8_t * databuff, uint32_t datasize, uint32_t portNo )  {
uint8_t temp1;

	temp1 = STX;
	if ( addControllerCommandsToRAM ( &temp1, 1 ) == true ) {
		if ( addControllerCommandsToRAM ( databuff, datasize) == true ) {
			temp1 = ETX ;
			if ( addControllerCommandsToRAM ( &temp1, 1 ) == true ) {
				ETH_SendDataOK (portNo);
				return;
			}
		}
	}
	
	ETH_SendDataNOK (portNo);
	
}

	 /***********************************************************************
 * @brief 	ETH_SaveNetworkParameters 
 *					

 * @param		-
 *				
 * @return		-
 ***********************************************************************/
void ETH_SaveNetworkParameters (void) {
	
  flashInit(FlashSettings);
	SaveEthernetDataToRAM();	
	//flashInit(FlashSettings);
	__disable_irq();	
	while( flashWrite()==0);
	__enable_irq();
	//flashInit(FlashSettings);
	getLastRecordFromFlashToRAM();
	
}
    /***********************************************************************
 * @brief 	ETH_GetNewNetworkParameters 
 *					PC yada controller dan gelen yeni network parametrelerini gelen datanin i�inden alir.
 *  				RAM deki ETH parametrelerine yazar. 
 * @param		-
 *				
 * @return		-
 ***********************************************************************/
void ETH_SendNetworkParameters (uint32_t portNo){
char Data [150];

		sprintf(Data, "<MASTERNETPARAMETERS>[IP:%d.%d.%d.%d][GTW:%d.%d.%d.%d][SUB:%d.%d.%d.%d][PORTSISTEM:%d][PORTPC:%d]",
						ETH.LocalIP[0],ETH.LocalIP[1],ETH.LocalIP[2],ETH.LocalIP[3],
						ETH.GatewayIP[0],ETH.GatewayIP[1],ETH.GatewayIP[2],ETH.GatewayIP[3],
						ETH.SubnetMaskAdress[0],ETH.LocalIP[1],ETH.LocalIP[2],ETH.LocalIP[3],
						ETH.Port_SistemPortNo,
						ETH.Port_PCPortNo
 						);
			
		ETH_SendSTX(portNo);
		if ( portNo == PORT_PC ) {
				if (ETH_SendDataFromListenPortPC( (uint8_t*)Data, strlen(Data)) == true ) {__nop();}
		}
//			if ( portNo == PORT_LISTEN_CONTROLLER ) {
//				if (ETH_SendDataFromListenPortController( (uint8_t*)Data, strlen(Data)) == true ) {__nop();}
//		}
	  ETH_SendETX(portNo);
	
}
    /***********************************************************************
 * @brief 	ETH_GetNewNetworkParameters 
 *					

 * @param		-
 *				
 * @return		-
 ***********************************************************************/
void ETH_GetNewNetworkParameters (uint8_t * databuff, uint32_t datasize ) {
char tempArray[50];
uint16_t farksize=0;
	
	memset(tempArray, 0, 50);
	if ( extract_between ((const char *) databuff, "[IP:","]",tempArray,&farksize) == true ) {
		sscanf(tempArray,"%d.%d.%d.%d",(int *)&ETH.LocalIP[0],(int *)&ETH.LocalIP[1],(int *)&ETH.LocalIP[2],(int *)&ETH.LocalIP[3]);
	}
	
	memset(tempArray, 0, 50);
	if ( extract_between ((const char *) databuff, "[GTW:","]",tempArray,&farksize) == true ) {
		sscanf(tempArray,"%d.%d.%d.%d",(int *)&ETH.GatewayIP[0],(int *)&ETH.GatewayIP[1],(int *)&ETH.GatewayIP[2],(int *)&ETH.GatewayIP[3]);
	}
	
	memset(tempArray, 0, 50);
	if ( extract_between ((const char *) databuff, "[SUB:","]",tempArray,&farksize) == true ) {
		sscanf(tempArray,"%d.%d.%d.%d",(int *)&ETH.SubnetMaskAdress[0],(int *)&ETH.SubnetMaskAdress[1],(int *)&ETH.SubnetMaskAdress[2],(int *)&ETH.SubnetMaskAdress[3]);
	}
	
	memset(tempArray, 0, 50);
	if ( extract_between ((const char *) databuff, "[PORTSISTEM:","]",tempArray,&farksize) == true ) {
		sscanf(tempArray,"%d",(int *)&ETH.Port_SistemPortNo );
	}
	
	memset(tempArray, 0, 50);
	if ( extract_between ((const char *) databuff, "[PORTPC:","]",tempArray,&farksize) == true ) {
		sscanf(tempArray,"%d",(int *)&ETH.Port_PCPortNo );
	}
	

}


    /***********************************************************************
 * @brief 	bazi paketler geldiginde o paketin dogru alindigina dair karsi tgonerilen onay paketi
 *					

 * @param		-
 *				
 * @return		-
 ***********************************************************************/
void ETH_SendString (  uint32_t portNo,uint8_t *data, uint16_t size ) {
	
		if ( portNo == PORT_PC ) {
				if (ETH_SendDataFromListenPortPC( (uint8_t*)data, size) == true ) {__nop();}
		}
//			if ( portNo == PORT_LISTEN_CONTROLLER ) {
//				if (ETH_SendDataFromListenPortController ( (uint8_t*)data, size) == true ) {__nop();}
//		}
	
}
    /***********************************************************************
 * @brief 	bazi paketler geldiginde o paketin dogru alindigina dair karsi tgonerilen onay paketi
 *					

 * @param		-
 *				
 * @return		-
 ***********************************************************************/
void ETH_SendDataOK (  uint32_t portNo) {
	
		ETH_SendSTX(portNo);
		if ( portNo == PORT_PC ) {
				if (ETH_SendDataFromListenPortPC( (uint8_t*)ANSWER_DATAOK, sizeof(ANSWER_DATAOK)-1) == true ) {__nop();}
		}
//			if ( portNo == PORT_LISTEN_CONTROLLER ) {
//				if (ETH_SendDataFromListenPortController( (uint8_t*)ANSWER_DATAOK, sizeof(ANSWER_DATAOK)-1) == true ) {__nop();}
//		}
		ETH_SendETX(portNo);
}

    /***********************************************************************
 * @brief 	bazi paketler geldiginde o paketin dogru alindigina dair karsi tgonerilen onay paketi
 *					

 * @param		-
 *				
 * @return		-
 ***********************************************************************/
void ETH_SendDataNOK (  uint32_t portNo) {
	
		ETH_SendSTX(portNo);
		if ( portNo == PORT_PC ) {
				if (ETH_SendDataFromListenPortPC( (uint8_t*)ANSWER_DATANOK, sizeof(ANSWER_DATANOK)-1) == true ) {__nop();}
		}
//			if ( portNo == PORT_LISTEN_CONTROLLER ) {
//				if (ETH_SendDataFromListenPortController( (uint8_t*)ANSWER_DATAOK, sizeof(ANSWER_DATAOK)-1) == true ) {__nop();}
//		}
		ETH_SendETX(portNo);
}
    /***********************************************************************
 * @brief 	bazi paketler geldiginde o paketin dogru alindigina dair karsi tgonerilen onay paketi
 *					

 * @param		-
 *				
 * @return		-
 ***********************************************************************/
void ETH_SendSTX(  uint32_t portNo) {
uint8_t stxptr = STX;
	
		if ( portNo == PORT_PC ) {
				if (ETH_SendDataFromListenPortPC( &stxptr,1 ) == true ) {__nop();}
		}
//			if ( portNo == PORT_LISTEN_CONTROLLER ) {
//				if (ETH_SendDataFromListenPortController( &stxptr,1 ) == true ) {__nop();}
//		}
	
}
    /***********************************************************************
 * @brief 	bazi paketler geldiginde o paketin dogru alindigina dair karsi tgonerilen onay paketi
 *					

 * @param		-
 *				
 * @return		-
 ***********************************************************************/
void ETH_SendETX(  uint32_t portNo) {
uint8_t stxptr = ETX;
	
		if ( portNo == PORT_PC ) {
				if (ETH_SendDataFromListenPortPC( &stxptr,1 ) == true ) {__nop();}
		}
//			if ( portNo == PORT_LISTEN_CONTROLLER ) {
//				if (ETH_SendDataFromListenPortController( &stxptr,1 ) == true ) {__nop();}
//		}
	
}

    /***********************************************************************
 * @brief 	Cihazin o anki durumu ne ise bunu ethernet uzerinden gonderir.  
 *					

 * @param		-
 *				
 * @return		-
 ***********************************************************************/
void ETH_SendStatus (uint8_t status,  uint32_t portNo) {
char Data[50];
uint8_t *ptr;
	  
		ETH_SendSTX(portNo);
	
	  Master.ReadCount++;
	  memset(Data, 0, 50);
	
		switch (status) {
			
			case	MASTERSTAT_SENSORREAD:

				if ( portNo == PORT_PC ) {
					if (ETH_SendDataFromListenPortPC( (uint8_t*)ANSWER_MASTERSTATUS_SENSORREAD, sizeof(ANSWER_MASTERSTATUS_SENSORREAD)-1) == true ) {
						__nop();
					}
				}
//				if ( portNo == PORT_LISTEN_CONTROLLER ) {
//					if (ETH_SendDataFromListenPortController( (uint8_t*)ANSWER_MASTERSTATUS_SENSORREAD, sizeof(ANSWER_MASTERSTATUS_SENSORREAD)-1) == true ) {
//						__nop();
//					}
//				}
			break;
				
			case	MASTERSTAT_SENSORLEDUPDATE:

				if ( portNo == PORT_PC ) {
					if (ETH_SendDataFromListenPortPC( (uint8_t*)ANSWER_MASTERSTATUS_SENSORLEDUPDATE, sizeof(ANSWER_MASTERSTATUS_SENSORLEDUPDATE)-1) == true ) {
						__nop();
					}
				}
//				if ( portNo == PORT_LISTEN_CONTROLLER ) {
//					if (ETH_SendDataFromListenPortController( (uint8_t*)ANSWER_MASTERSTATUS_SENSORLEDUPDATE, sizeof(ANSWER_MASTERSTATUS_SENSORLEDUPDATE)-1) == true ) {
//						__nop();
//					}
//				}
			break;
				
			case	MASTERSTAT_SENSORGRUPREGUPDATE:

				if ( portNo == PORT_PC ) {
					if (ETH_SendDataFromListenPortPC( (uint8_t*)ANSWER_MASTERSTATUS_SENSORGRUPREGUPDATE, sizeof(ANSWER_MASTERSTATUS_SENSORGRUPREGUPDATE)-1) == true ) {
						__nop();
					}
				}
//				if ( portNo == PORT_LISTEN_CONTROLLER ) {
//					if (ETH_SendDataFromListenPortController( (uint8_t*)ANSWER_MASTERSTATUS_SENSORGRUPREGUPDATE, sizeof(ANSWER_MASTERSTATUS_SENSORGRUPREGUPDATE)-1) == true ) {
//						__nop();
//					}
//				}
			break;
				
			case	MASTERSTAT_DISPUPDATE:

				if ( portNo == PORT_PC ) {
					if (ETH_SendDataFromListenPortPC( (uint8_t*)ANSWER_MASTERSTATUS_DISPUPDATE, sizeof(ANSWER_MASTERSTATUS_DISPUPDATE)-1) == true ) {
						__nop();
					}
				}
//				if ( portNo == PORT_LISTEN_CONTROLLER ) {
//					if (ETH_SendDataFromListenPortController( (uint8_t*)ANSWER_MASTERSTATUS_DISPUPDATE, sizeof(ANSWER_MASTERSTATUS_DISPUPDATE)-1) == true ) {
//						__nop();
//					}
//				}
			break;
				
				
			
			case	MASTERSTAT_DEVICESEARCH:
						ptr = (uint8_t * )Data;
						strcat((char *)Data,ANSWER_MASTERSTATUS_DEVICESEARCH);
						ptr += (sizeof(ANSWER_MASTERSTATUS_DEVICESEARCH)-1);
						sprintf((char*)ptr, "[%%%d]", Master.searchedDeviceRatio);
					
		
						if ( portNo == PORT_PC ) {
								if (ETH_SendDataFromListenPortPC( (uint8_t*)Data, strlen(Data)) == true ) {__nop();}
						}
//						if ( portNo == PORT_LISTEN_CONTROLLER ) {					
//								if (ETH_SendDataFromListenPortController( (uint8_t*)Data, strlen(Data)) == true ) {__nop();}
//						}

				break;
						
			case	MASTERSTAT_CHECKDEVICE:
				if ( portNo == PORT_PC ) {
					if (ETH_SendDataFromListenPortPC( (uint8_t*)ASNWER_MASTERSTATUS_DEVICECHECK, sizeof(ASNWER_MASTERSTATUS_DEVICECHECK)-1) == true ) {
						__nop();
					}
				}
//				if ( portNo == PORT_LISTEN_CONTROLLER ) {
//					if (ETH_SendDataFromListenPortController( (uint8_t*)ASNWER_MASTERSTATUS_DEVICECHECK, sizeof(ASNWER_MASTERSTATUS_DEVICECHECK)-1) == true ) {
//						__nop();
//					}
//				}							
				break;
			
			case	MASTERSTAT_PROGRAMDEVICE:
						ptr = (uint8_t * )Data;
						strcat((char *)Data,ASNWER_MASTERSTATUS_PROGRAMDEVICE);
						ptr += (sizeof(ASNWER_MASTERSTATUS_PROGRAMDEVICE)-1);
						sprintf((char*)ptr, "[%%%d]", Master.programmedDeviceRatio);
						
						if ( portNo == PORT_PC ) {
								if (ETH_SendDataFromListenPortPC( (uint8_t*)Data, strlen(Data)) == true ) {__nop();}
						}
//						if ( portNo == PORT_LISTEN_CONTROLLER ) {					
//								if (ETH_SendDataFromListenPortController( (uint8_t*)Data, strlen(Data)) == true ) {__nop();}
//						}				
				
			
				break;			
			
						
				default:

				if ( portNo == PORT_PC ) {
					if (ETH_SendDataFromListenPortPC( (uint8_t*)ANSWER_MASTERSTATUS_EMPTY, sizeof(ANSWER_MASTERSTATUS_EMPTY)-1) == true ) {
						__nop();
					}
				}
//				if ( portNo == PORT_LISTEN_CONTROLLER ) {
//					if (ETH_SendDataFromListenPortController( (uint8_t*)ANSWER_MASTERSTATUS_EMPTY, sizeof(ANSWER_MASTERSTATUS_EMPTY)-1) == true ) {
//						__nop();
//					}
//				}
			break;
			
		}
		
		ETH_SendETX(portNo);
}


    /***********************************************************************
 * @brief 	Ethernet ten datayi gonderir soket busy ise timeout olana kadar tekrar gonderir   
 *					

 * @param		-
 *				
 * @return		-
 ***********************************************************************/
int32_t sendDataAndRetyIfBusy(uint8_t sn, uint8_t * buf, uint16_t len ,uint16_t timeout, uint16_t BusyTimeOut)
{
uint8_t retVal;
//uint16_t temp;


	retVal = SOCK_BUSY;
	timerETH_SocketBusyTimeOut = BusyTimeOut;
		
	while (retVal == SOCK_BUSY ) {
			retVal = send (sn,buf,len,timeout);
		  
			if ( timerETH_SocketBusyTimeOut == 0 ) {
				return SOCKERR_TIMEOUT;
			}
			
//			temp = timerETH_SocketBusyTimeOut;
//			while ( temp == timerETH_SocketBusyTimeOut ) {
//				__nop();
//				__nop();
//			}
	
	}
	return retVal;
}


/*********************************************************************//**
 * @brief 	Dinleme yapmak i�in soket a�iyor.	
 * @param		None
 * @return		None
 ***********************************************************************/
uint32_t  wizETH_SetServerSocket(uint8_t sn, uint16_t portno)
{
int32_t ret=0;
//uint16_t size = 0, sentsize=0;


	switch(getSn_SR(sn))
   {

		     case SOCK_CLOSED:
         
         if((ret=socket(sn,Sn_MR_TCP,portno,0x00,SOCKETIMEOUT)) != sn)
            return ret;
         break;
				 
				 default:
         break;
	 }
	 return ret;
}	
/*********************************************************************//**
 * @brief 	Client yapmak i�in soket a�iyor.	
 * @param		None
 * @return		None
 ***********************************************************************/
uint32_t  wizETH_SetClientSocket(uint8_t sn, uint16_t portno)
{
int32_t ret=0;
//uint16_t size = 0, sentsize=0;


	switch(getSn_SR(sn))
   {

		     case SOCK_CLOSED:
         
         if((ret=socket(sn,Sn_MR_TCP,portno,0x00,SOCKETIMEOUT)) != sn)
            return ret;
         break;
				 
				 default:
         break;
	 }
	 
	 return ret;
}
/*********************************************************************//**
 * @brief 	Ethernet mod�l Driver Init	
 * @param		None
 * @return		None
 ***********************************************************************/
void  wizETH_Init(void)
{
uint8_t tmp;
//int32_t ret = 0;	
	
	
	wizchip_Init();
	
	/* Chip selection call back */
#if   _WIZCHIP_IO_MODE_ == _WIZCHIP_IO_MODE_SPI_VDM_
    reg_wizchip_cs_cbfunc(wizchip_select, wizchip_deselect);
#elif _WIZCHIP_IO_MODE_ == _WIZCHIP_IO_MODE_SPI_FDM_
    reg_wizchip_cs_cbfunc(wizchip_select, wizchip_select);  // CS must be tried with LOW.
#else
   #if (_WIZCHIP_IO_MODE_ & _WIZCHIP_IO_MODE_SIP_) != _WIZCHIP_IO_MODE_SIP_
      #error "Unknown _WIZCHIP_IO_MODE_"
   #else
      reg_wizchip_cs_cbfunc(wizchip_select, wizchip_deselect);
   #endif
#endif
    /* SPI Read & Write callback function */
    reg_wizchip_spi_cbfunc(wizchip_read, wizchip_write);
    
		
		//ne ise yariyor tam anlamadim.-------------
    /* WIZCHIP SOCKET Buffer initialize */
    if(ctlwizchip(CW_INIT_WIZCHIP,(void*)memsize) == -1)
    {
       printf("WIZCHIP Initialized fail.\r\n");
       while(1);
    }
		//-----------------------------------------------
    /* PHY link status check */
    do
    {
       if(ctlwizchip(CW_GET_PHYLINK, (void*)&tmp) == -1)
          printf("Unknown PHY Link stauts.\r\n");
    }while(tmp == PHY_LINK_OFF);

		//wiz550io has default network setup, so dont need - demis.
		//network_init();
	
}
/*********************************************************************//**
 * @brief 	Wiznet mod�l pin ve spi kosullamalari	
 * @param		None
 * @return		None
 ***********************************************************************/
void  wizchip_Init(void)
{
	Chip_IOCON_PinMuxSet(LPC_IOCON, eNet_RST_Port, eNet_RST_Pin, eNet_RST_IOCON );
	Chip_IOCON_PinMuxSet(LPC_IOCON, eNet_INT_Port, eNet_INT_Pin, eNet_INT_IOCON );
	Chip_IOCON_PinMuxSet(LPC_IOCON, eNet_RDY_Port, eNet_RDY_Pin, eNet_RDY_IOCON );
	
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, eNet_RST_Port, eNet_RST_Pin);		
	
	wizchip_reset();
	osDelay(20);
	wizchip_noReset();
	
	osDelay(200);
	
	Chip_IOCON_PinMuxSet(LPC_IOCON, eNet_CS_Port, eNet_CS_Pin, eNet_CS_IOCON );
	wizchip_deselect();
	
	Chip_IOCON_PinMuxSet(LPC_IOCON, eNet_SCK_Port, eNet_SCK_Pin, eNet_SCK_IOCON );
	Chip_IOCON_PinMuxSet(LPC_IOCON, eNet_MISO_Port, eNet_MISO_Pin, eNet_MISO_IOCON );
	Chip_IOCON_PinMuxSet(LPC_IOCON, eNet_MOSI_Port, eNet_MOSI_Pin, eNet_MOSI_IOCON );
	
	/* SSP initialization */
	//Chip_SSP_Init(eNet_SSP);
	Chip_Clock_EnablePeriphClock(eNet_SSP_CLK);
	
	Chip_SSP_Set_Mode(eNet_SSP, SSP_MODE_MASTER);
	//Chip_SSP_SetFormat(eNet_SSP, SSP_BITS_8, SSP_FRAMEFORMAT_SPI, SSP_CLOCK_CPHA0_CPOL0);
	Chip_SSP_SetFormat(eNet_SSP, SSP_BITS_8, SSP_FRAMEFORMAT_SPI, SSP_CLOCK_CPHA1_CPOL1);
	Chip_SSP_SetBitRate(eNet_SSP, 1000000); //1000000);
	
	
	Chip_SSP_Enable(eNet_SSP);
}
/***********************************************************************
 * @brief 	SPI Callback function for accessing WIZCHIP  - CHIP SELECT
 * @param		(Call back function for W5500 SPI)
 * @return		
 ***********************************************************************/
void  wizchip_reset(void)
{
  
	Chip_GPIO_SetPinState(LPC_GPIO, eNet_RST_Port , eNet_RST_Pin	, false);
}
/***********************************************************************
 * @brief 	SPI Callback function for accessing WIZCHIP  - CHIP SELECT
 * @param		(Call back function for W5500 SPI)
 * @return		
 ***********************************************************************/
void  wizchip_noReset(void)
{
  
	Chip_GPIO_SetPinState(LPC_GPIO, eNet_RST_Port , eNet_RST_Pin	, true);
}
/***********************************************************************
 * @brief 	SPI Callback function for accessing WIZCHIP  - CHIP SELECT
 * @param		(Call back function for W5500 SPI)
 * @return		
 ***********************************************************************/
void  wizchip_select(void)
{
  
	Chip_GPIO_SetPinState(LPC_GPIO, eNet_CS_Port , eNet_CS_Pin	, false);
}
/***********************************************************************
 * @brief 	SPI Callback function for accessing WIZCHIP  -CHIP DESELECT
 * @param		(Call back function for W5500 SPI)
 * @return		
 ***********************************************************************/
void  wizchip_deselect(void)
{
   Chip_GPIO_SetPinState(LPC_GPIO, eNet_CS_Port , eNet_CS_Pin	, true);
}

/***********************************************************************
 * @brief 	SPI Callback function for accessing WIZCHIP - SEND DATA
 * @param		(Call back function for W5500 SPI)
 * @return		
 ***********************************************************************/
void  wizchip_write(uint8_t wb)
{
//uint8_t sendBuffer[1];
//uint8_t result[1];
//	
//	SSPdata.tx_data = &wb;
//	SSPdata.rx_data = result;
//	SSPdata.length = 1;
//	SSPdata.tx_cnt=0;
//	SSPdata.rx_cnt=0;
//	Chip_SSP_RWFrames_Blocking(eNet_SSP, &SSPdata);
	Chip_SSP_WriteFrames_Blocking(eNet_SSP,&wb,1);
	
  //xSPISingleDataReadWrite(WIZCHIP_SPI_BASE,wb);

}

/***********************************************************************
 * @brief 	SPI Callback function for accessing WIZCHIP - READ DATA
 * @param		(Call back function for W5500 SPI)
 * @return		
 ***********************************************************************/
uint8_t wizchip_read(void)
{
//	uint8_t sendBuffer[2];
	uint8_t result[2];
	
//	sendBuffer[0]=CMD_READ_STATUS_REG;
//	
//	xferConfig.tx_data = sendBuffer;
//	xferConfig.rx_data = result;
//	xferConfig.length = sizeof(sendBuffer);
//	xferConfig.tx_cnt=0;
//	xferConfig.rx_cnt=0;
//	Chip_SSP_RWFrames_Blocking(eNet_SSP, &xferConfig);
	
	Chip_SSP_ReadFrames_Blocking(eNet_SSP,result,1);
	//eNet_SSP,result,1);
	return result[1];
	
   //return xSPISingleDataReadWrite(WIZCHIP_SPI_BASE,0xFF);
}
/***********************************************************************
 * @brief 	SPI Callback function for accessing WIZCHIP - READ DATA
 * @param		
 * @return		
 ***********************************************************************/
void network_init(void)
{
//   uint8_t tmpstr[6];
//	ctlnetwork(CN_SET_NETINFO, (void*)&gWIZNETINFO);
//	ctlnetwork(CN_GET_NETINFO, (void*)&gWIZNETINFO);

//	// Display Network Information
//	ctlwizchip(CW_GET_ID,(void*)tmpstr);
//	printf("\r\n=== %s NET CONF ===\r\n",(char*)tmpstr);
//	printf("MAC: %02X:%02X:%02X:%02X:%02X:%02X\r\n",gWIZNETINFO.mac[0],gWIZNETINFO.mac[1],gWIZNETINFO.mac[2],
//		  gWIZNETINFO.mac[3],gWIZNETINFO.mac[4],gWIZNETINFO.mac[5]);
//	printf("SIP: %d.%d.%d.%d\r\n", gWIZNETINFO.ip[0],gWIZNETINFO.ip[1],gWIZNETINFO.ip[2],gWIZNETINFO.ip[3]);
//	printf("GAR: %d.%d.%d.%d\r\n", gWIZNETINFO.gw[0],gWIZNETINFO.gw[1],gWIZNETINFO.gw[2],gWIZNETINFO.gw[3]);
//	printf("SUB: %d.%d.%d.%d\r\n", gWIZNETINFO.sn[0],gWIZNETINFO.sn[1],gWIZNETINFO.sn[2],gWIZNETINFO.sn[3]);
//	printf("DNS: %d.%d.%d.%d\r\n", gWIZNETINFO.dns[0],gWIZNETINFO.dns[1],gWIZNETINFO.dns[2],gWIZNETINFO.dns[3]);
//	printf("======================\r\n");
}
/*******************************************************************************
* @brief				Bu fonksiyon ETHERNET driver isleri icin timer callback fonksiyonudur
* @param[in]    arg
* @return				None
*******************************************************************************/
void timerETH_CallBack(void const *arg){
uint8_t i;
	
	if(timerETH_globalTimeOut>=TIMER_ETH_CALLBACK_PERIOD){
		timerETH_globalTimeOut-=TIMER_ETH_CALLBACK_PERIOD;
	}else{
		timerETH_globalTimeOut=0;
	}
	
	if(timerETH_SocketBusyTimeOut>=TIMER_ETH_CALLBACK_PERIOD){
		timerETH_SocketBusyTimeOut-=TIMER_ETH_CALLBACK_PERIOD;
	}else{
		timerETH_SocketBusyTimeOut=0;
	}	
	
	
	if(timerETH_connectPortTimeOut>=TIMER_ETH_CALLBACK_PERIOD){
		timerETH_connectPortTimeOut-=TIMER_ETH_CALLBACK_PERIOD;
	}else{
		timerETH_connectPortTimeOut=0;
	}	
	
	if ( timerETH_masterConnectTimeOut >= TIMER_ETH_CALLBACK_PERIOD) {
		timerETH_masterConnectTimeOut -= TIMER_ETH_CALLBACK_PERIOD;
	}else {
		timerETH_masterConnectTimeOut =0;
	}
	
	for (i=0;i<CONNECTPORTSOCKET_CNT;i++) {
		if ( connectSockets[i].timeOut  >= TIMER_ETH_CALLBACK_PERIOD) {
			connectSockets[i].timeOut -= TIMER_ETH_CALLBACK_PERIOD;
		}else {
			connectSockets[i].timeOut = 0;
		}
	}
	
}

///*********************************************************************//**
// * @brief 	Ethernet mod�l Driver Init	
// * @param		None
// * @return		None
// ***********************************************************************/
//bool  ETH_CommunicateAnyDataOnListenPort(uint8_t sn, uint16_t portno , int8_t *errcode){
//int32_t ret,i;//,k,m;
//uint16_t size = 0;// sentsize=0;
//#define DATA_BUF_SIZE_LISTEN 1500
//#define STRLEN	10
//static uint8_t buf[DATA_BUF_SIZE_LISTEN];
////static bool soketestablished = 0;
////static bool comm_started=true;
//bool STXDONE = false;
////bool ETXDONE = false;			

//	 switch(getSn_SR(sn))
//   {
//      case SOCK_ESTABLISHED :		
//         if(getSn_IR(sn) & Sn_IR_CON)
//         {
//            setSn_IR(sn,Sn_IR_CON);
//         }								 
//				 if((size = getSn_RX_RSR(sn)) > 0)
//         {  
//					 memset (buf,0,DATA_BUF_SIZE_LISTEN);
//						if(size > DATA_BUF_SIZE_LISTEN) size = DATA_BUF_SIZE_LISTEN;
//							ret = recv(sn,buf,size,SOCKETIMEOUT);
//						if(ret <= 0) {
//							*errcode = ret;
//							return false;
//						}
//						
//						for (i=0;i<size;i++) {
//							addElement_fifo8 (&(ETH.Eth_PortListen_Pc_rx_fifo),buf[i]);							
//						}
//						

//				break;
//			
//      case SOCK_CLOSE_WAIT :
//				 memset (buf,0,DATA_BUF_SIZE_LISTEN);
//         if((ret=disconnect(sn,SOCKETIMEOUT)) != SOCK_OK) {
//					 clear_fifo8( &ETH.Eth_PortListen_Pc_rx_fifo);
//					 *errcode = ret;
//					 return false;
//				 }				
//				break;
//			
//      case SOCK_INIT :
//				 STXDONE = false;				
//			   memset (buf,0,DATA_BUF_SIZE_LISTEN);
//         if( (ret = listen(sn,SOCKETIMEOUT)) != SOCK_OK){
//					 *errcode = ret;
//					 return false;
//				 }
//         break;
//			
//      case SOCK_CLOSED:
//				 STXDONE = false;
//			   memset (buf,0,DATA_BUF_SIZE_LISTEN);
//			   clear_fifo8( &ETH.Eth_PortListen_Pc_rx_fifo);
//         if((ret=socket(sn,Sn_MR_TCP,portno,0x00,SOCKETIMEOUT)) != sn) {
//					 *errcode = ret;
//					 return false;
//				 }

//         break;
//				 
//			case SOCK_LISTEN:
//				__nop();
//				break;


//			   default:
//         break;
//		}
//	
//	}
//	 
//	return false;

//}


///*********************************************************************//**
// * @brief 	Ethernet mod�l Driver Init	
// * @param		None
// * @return		None
// ***********************************************************************/
//bool  ETH_CheckIncomingDataOnListenPortController(uint8_t sn, uint16_t portno, int8_t *errcode){
//int32_t ret,i;//,k,m;
//uint16_t size = 0;// sentsize=0;
//#define DATA_BUF_SIZE_LISTEN 1500
//#define STRLEN	10
//static uint8_t buf[DATA_BUF_SIZE_LISTEN];
////static bool soketestablished = 0;
////static bool comm_started=true;
//bool STXDONE = false;
////bool packageOK = false;
////bool ETXDONE = false;			

//	 switch(getSn_SR(sn))
//   {
//      case SOCK_ESTABLISHED :		
//         if(getSn_IR(sn) & Sn_IR_CON)
//         {
//            setSn_IR(sn,Sn_IR_CON);
//         }								 
//				 if((size = getSn_RX_RSR(sn)) > 0)
//         {  
//					 memset (buf,0,DATA_BUF_SIZE_LISTEN);
//						if(size > DATA_BUF_SIZE_LISTEN) size = DATA_BUF_SIZE_LISTEN;
//							ret = recv(sn,buf,size,SOCKETIMEOUT);
//						if(ret <= 0) {
//							*errcode = ret;
//							return false;
//						}
//						
//						for (i=0;i<size;i++) {
//							if (STXDONE == false ) {
//								if (buf[i]==STARTOFTEXT) {
//									STXDONE = true;
//									//clear_fifo8( &ETH.Eth_PortListen_Pc_rx_fifo);
//									
//									addElement_fifo8 (&(ETH.Eth_PortListen_Controller_rx_fifo),buf[i]);
//								}
//							}else {
//								if (buf[i]==ENDOFTEXT) {
//									
//									addElement_fifo8 (&(ETH.Eth_PortListen_Controller_rx_fifo),buf[i]);
//									
//									STXDONE = false;
//								}else if (buf[i]==STARTOFTEXT) {
//									STXDONE = true;
//									//clear_fifo8( &ETH.Eth_PortListen_Pc_rx_fifo);
//									addElement_fifo8 (&(ETH.Eth_PortListen_Controller_rx_fifo),buf[i]);		
//								}else {
//									addElement_fifo8 (&(ETH.Eth_PortListen_Controller_rx_fifo),buf[i]);								
//								}
//							
//							}

////
//																			
//						}
//				break;
//			
//      case SOCK_CLOSE_WAIT :
////				 comm_started = false;
////				 soketestablished = false;
//				 memset (buf,0,DATA_BUF_SIZE_LISTEN);
//         if((ret=disconnect(sn,SOCKETIMEOUT)) != SOCK_OK) {
//					 clear_fifo8( &ETH.Eth_PortListen_Controller_rx_fifo);
//					 *errcode = ret;
//					 return false;
//				 }				
//				break;
//			
//      case SOCK_INIT :
////				 soketestablished = false;
////				 comm_started = false;
//				 STXDONE = false;
//				// ETXDONE = false;				
//			   memset (buf,0,DATA_BUF_SIZE_LISTEN);
//         if( (ret = listen(sn,SOCKETIMEOUT)) != SOCK_OK){
//					 clear_fifo8( &ETH.Eth_PortListen_Controller_rx_fifo);
//					 *errcode = ret;
//					 return false;
//				 }
//         break;
//			
//      case SOCK_CLOSED:
////				 soketestablished = false;
////				 comm_started = false;
//				 STXDONE = false;
//				// ETXDONE = false;		
//			   memset (buf,0,DATA_BUF_SIZE_LISTEN);
//			   clear_fifo8( &ETH.Eth_PortListen_Controller_rx_fifo);
//         if((ret=socket(sn,Sn_MR_TCP,portno,0x00,SOCKETIMEOUT)) != sn) {
//					 *errcode = ret;
//					 return false;
//				 }

//         break;
//				 
//			case SOCK_LISTEN:
//				__nop();
//				break;


//			   default:
//         break;
//		}
//	
//	}
//	 
//	return false;

//}


//	
///*********************************************************************//**
// * @brief 	Ethernet mod�l Driver Init	
// * @param		None
// * @return		None
// ***********************************************************************/
//void  ETH_CheckDataOnListenPort(uint8_t sn, uint16_t portno){
//int32_t ret,i;//,k,m;
//uint16_t size = 0;// sentsize=0;
//#define DATA_BUF_SIZE_LISTEN 1500
//#define STRLEN	10
//static uint8_t buf[DATA_BUF_SIZE_LISTEN];
////static bool soketestablished = 0;
////static bool comm_started=true;
////bool STXDONE = false;
////bool ETXDONE = false;			

//	 switch(getSn_SR(sn))
//   {
//      case SOCK_ESTABLISHED :		
//         if(getSn_IR(sn) & Sn_IR_CON)
//         {
//            setSn_IR(sn,Sn_IR_CON);
//         }								 
//				 if((size = getSn_RX_RSR(sn)) > 0)
//         {
//					 memset (buf,0,DATA_BUF_SIZE_LISTEN);
//						if(size > DATA_BUF_SIZE_LISTEN) size = DATA_BUF_SIZE_LISTEN;
//							ret = recv(sn,buf,size,SOCKETIMEOUT);
//						if(ret <= 0) {
//							//*errcode = ret;
//							return;// false;
//						}
//						
//						for (i=0;i<size;i++) {
//							addElement_fifo8 (&(ETH.Eth_PortListen_Pc_rx_fifo),buf[i]);
//						}
//							
//							
////							{
////							if (STXDONE == false ) {
////								if (buf[i]==STARTOFTEXT) {
////									STXDONE = true;
////									clear_fifo8( &ETH.Eth_PortListen_Pc_rx_fifo);
////									//ETXDONE = false;								
////									addElement_fifo8 (&(ETH.Eth_PortListen_Pc_rx_fifo),buf[i]);
////								}
////							}else {
////								if (buf[i]==ENDOFTEXT) {
////									//ETXDONE = true;
////									addElement_fifo8 (&(ETH.Eth_PortListen_Pc_rx_fifo),buf[i]);
////									return true;
////								}else if (buf[i]==STARTOFTEXT) {
////									STXDONE = true;
////									clear_fifo8( &ETH.Eth_PortListen_Pc_rx_fifo);
////									addElement_fifo8 (&(ETH.Eth_PortListen_Pc_rx_fifo),buf[i]);		
////								}else {
////									addElement_fifo8 (&(ETH.Eth_PortListen_Pc_rx_fifo),buf[i]);								
////							}
////							
////						}
//////						comm_started = true;
//																			
//				 }
//				break;
//			
//      case SOCK_CLOSE_WAIT :
////				 comm_started = false;
////				 soketestablished = false;
//				 memset (buf,0,DATA_BUF_SIZE_LISTEN);
//         if((ret=disconnect(sn,SOCKETIMEOUT)) != SOCK_OK) {
////					 clear_fifo8( &ETH.Eth_PortListen_Pc_rx_fifo);
//					 //*errcode = ret;
//					 return ;//false;
//				 }				
//				break;
//			
//      case SOCK_INIT :
////				 soketestablished = false;
////				 comm_started = false;
////				 STXDONE = false;
//				// ETXDONE = false;				
//			   memset (buf,0,DATA_BUF_SIZE_LISTEN);
//         if( (ret = listen(sn,SOCKETIMEOUT)) != SOCK_OK){
//					 //*errcode = ret;
//					 return; // false;
//				 }
//         break;
//			
//      case SOCK_CLOSED:
////				 soketestablished = false;
////				 comm_started = false;
////				 STXDONE = false;
//				// ETXDONE = false;		
//			   memset (buf,0,DATA_BUF_SIZE_LISTEN);
////			   clear_fifo8( &ETH.Eth_PortListen_Pc_rx_fifo);
//         if((ret=socket(sn,Sn_MR_TCP,portno,0x00,SOCKETIMEOUT)) != sn) {
//					 //*errcode = ret;
//					 return; // false;
//				 }

//         break;
//				 
//			case SOCK_LISTEN:
//				__nop();
//				break;


//			   default:
//         break;
//		}
//	
//	}
//	 


///*********************************************************************//**
// * @brief 	Ethernet ETH_SendData	
// * @param		None
// * @return		None
// ***********************************************************************/
//int32_t  ETH_SendData (uint8_t sn, uint8_t* buf, uint8_t* destip, uint16_t destport) {
//   int32_t ret; // return value for SOCK_ERRORs
////   uint16_t size = 0, sentsize=0;
////#define DATA_BUF_SIZE 200

//   // Port number for TCP client (will be increased)
//   uint16_t any_port = 	50000;

//   // Socket Status Transitions
//   // Check the W5500 Socket n status register (Sn_SR, The 'Sn_SR' controlled by Sn_CR command or Packet send/recv status)
//   switch(getSn_SR(SOCK_TCPC_OUTGOING))
//   {
//      case SOCK_ESTABLISHED :
//         if(getSn_IR(SOCK_TCPC_OUTGOING) & Sn_IR_CON)	// Socket n interrupt register mask; TCP CON interrupt = connection with peer is successful
//         {
//			setSn_IR(SOCK_TCPC_OUTGOING, Sn_IR_CON);  // this interrupt should be write the bit cleared to '1'
//         }
//				ret = sendDataAndRetyIfBusy(SOCK_TCPC_OUTGOING, (uint8_t *)"erdem", 5 ,SOCKETIMEOUT,SOCKETBUSYTIMEOUT); // Data send process (User's buffer -> Destination through H/W Tx socket buffer)
//				//ret = send(SOCK_TCPC_OUTGOING, (uint8_t *)"erdem", 5 ,SOCKETIMEOUT); // Data send process (User's buffer -> Destination through H/W Tx socket buffer)
//				if(ret < 0) // Send Error occurred (sent data length < 0)
//				{
//					close(SOCK_TCPC_OUTGOING ,SOCKETIMEOUT); // socket close
//					return ret;
//				}				 
//         break;

//      case SOCK_CLOSE_WAIT :
//         if((ret=disconnect(SOCK_TCPC_OUTGOING,SOCKETIMEOUT)) != SOCK_OK) return ret;
//         break;

//      case SOCK_INIT :
//    	 if( (ret = connect(SOCK_TCPC_OUTGOING, destip, destport,SOCKETIMEOUT)) != SOCK_OK) return ret;	//	Try to TCP connect to the TCP server (destination)
//         break;

//      case SOCK_CLOSED:
//    	  close(sn,SOCKETIMEOUT);
//    	  if((ret=socket(SOCK_TCPC_OUTGOING, Sn_MR_TCP, any_port++, 0x00,SOCKETIMEOUT)) != SOCK_TCPC_OUTGOING) return ret; // TCP socket open with 'any_port' port number
//         break;
//      default:
//         break;
//   }
//   return 1;
//}

///*********************************************************************//**
// * @brief 	Ethernet ETH_SendData	
// * @param		None
// * @return		None
// ***********************************************************************/
//bool  ETH_SendDataConnectPort (uint8_t* destip, uint16_t destport, uint8_t *senddata, uint8_t size ) {
////#define DATA_BUF_SIZE 200
//int32_t ret; // return value for SOCK_ERRORs
////uint16_t  sentsize=0;

//   uint16_t any_port = 	50000;
//   // Socket Status Transitions
//   // Check the W5500 Socket n status register (Sn_SR, The 'Sn_SR' controlled by Sn_CR command or Packet send/recv status)
//   switch(getSn_SR(SOCK_TCPC_OUTGOING))
//   {
//      case SOCK_ESTABLISHED :
//					if(getSn_IR(SOCK_TCPC_OUTGOING) & Sn_IR_CON)	// Socket n interrupt register mask; TCP CON interrupt = connection with peer is successful
//					{
//						setSn_IR(SOCK_TCPC_OUTGOING, Sn_IR_CON);  // this interrupt should be write the bit cleared to '1'
//					}
//					ret = sendDataAndRetyIfBusy(SOCK_TCPC_OUTGOING, senddata, size ,SOCKETIMEOUT,SOCKETBUSYTIMEOUT); // Data send process (User's buffer -> Destination through H/W Tx socket buffer)
//	//				ret = send(SOCK_TCPC_OUTGOING, senddata, size ,SOCKETIMEOUT); // Data send process (User's buffer -> Destination through H/W Tx socket buffer)
//					if(ret < 0) // Send Error occurred (sent data length < 0)
//					{					
//						return false;
//					}else {
//						return true;
//					}
////         break;

//      case SOCK_CLOSE_WAIT :
//         if((ret=disconnect(SOCK_TCPC_OUTGOING,SOCKETIMEOUT)) != SOCK_OK) return false;
//         break;

//      case SOCK_INIT :
//    	 if( (ret = connect(SOCK_TCPC_OUTGOING, destip, destport,SOCKETIMEOUT)) != SOCK_OK) return false;	//	Try to TCP connect to the TCP server (destination)
//         break;						

//      case SOCK_CLOSED:
//    	  close(SOCK_TCPC_OUTGOING,SOCKETIMEOUT);
//    	  if((ret=socket(SOCK_TCPC_OUTGOING, Sn_MR_TCP, any_port++, 0x00,SOCKETIMEOUT)) != SOCK_TCPC_OUTGOING) return false; // TCP socket open with 'any_port' port number
//         break;			
//			
//      default:
//         break;
//			
//   }

//   return false;
//}


///*********************************************************************//**
// * @brief 	ETH_CheckConnectPortClosed	
// * @param		None
// * @return		None
// ***********************************************************************/
//bool  ETH_CheckConnectPortClosed(uint16_t timeout){

//	 timerETH_connectPortTimeOut = timeout;
//	
//	 while ( timerETH_connectPortTimeOut != 0 ) {
//	 
//		 switch(getSn_SR(SOCK_TCPC_OUTGOING))
//		 {
//				
//				case SOCK_CLOSED:
//					 return true;
////					 break;
//					 
//					 
//				default:
//					 break;
//			}
//		}
//	 
//		return false;
//}
///***********************************************************************
// * @brief 	ETH_SendSensGrupRegs 
// *					

// * @param		-
// *				
// * @return		-
// ***********************************************************************/
//void ETH_SendSensGrupRegs (uint32_t portNo) {	
//uint8_t i,k;
//uint8_t *buffPtr;
//sensGrupReg *sensorGrupPtr;
//uint8_t sensGrupRegAdet = 0;
//char Data [50];	
//	
//	ETH_SendSTX(portNo);
//	
//	 if ( portNo == PORT_LISTEN_PC ) {
//			 memset(ETH.PC_txBuff, 0, ETHERNET_PORTLISTEN_PC_BUF_SIZE);
//			 buffPtr = &ETH.PC_txBuff[0];		 
//	 }else if ( portNo == PORT_LISTEN_CONTROLLER ) {
//			 memset(ETH.Controller_txBuff, 0, ETHERNET_PORTLISTEN_CONTROLLER_BUF_SIZE);
//			 buffPtr = &ETH.Controller_txBuff[0];		 
//	 }else {
//		 return;
//	 }
//	 
////	 memset(ETH.txBuff, 0, ETHERNET_PORTLISTEN_PC_BUF_SIZE);
////	 buffPtr = &ETH.txBuff[0];
//	
//	strcat((char *)buffPtr,ANSWER_SENSORGRUPREG);
//	buffPtr += (sizeof(ANSWER_SENSORGRUPREG)-1);
//	

//	 for(k=1; k<=4; k++){
//		 
//		 if(k == 1 ){
//				sensorGrupPtr=sensorGrupRegPort1;	
//				sensGrupRegAdet = SENSORGRUPSAYISI;
//				strcat((char *)buffPtr,ANSWER_PORT1);
//				buffPtr += (sizeof(ANSWER_PORT1)-1);
//		 }else if(k == 2 ){
//				sensorGrupPtr=sensorGrupRegPort2;	
//				sensGrupRegAdet = SENSORGRUPSAYISI;
//				strcat((char *)buffPtr,ANSWER_PORT2);
//				buffPtr += (sizeof(ANSWER_PORT2)-1);
//		 }else if(k == 3 ){
//				sensorGrupPtr=sensorGrupRegPort3;	
//				sensGrupRegAdet = SENSORGRUPSAYISI;
//				strcat((char *)buffPtr,ANSWER_PORT3);
//				buffPtr += (sizeof(ANSWER_PORT3)-1);
//		 }else if(k == 4 ){
//				sensorGrupPtr=sensorGrupRegPort4;	
//				sensGrupRegAdet = SENSORGRUPSAYISI;
//				strcat((char *)buffPtr,ANSWER_PORT4);
//				buffPtr += (sizeof(ANSWER_PORT4)-1);	
//		 }
//		 
//		 for(i=0; i<sensGrupRegAdet; i++){
//			 	sprintf(Data, "[%d,%d]",
//						sensorGrupPtr[i].ToplamSensor,
//						sensorGrupPtr[i].ToplamBosYer
// 					);
////			 	sprintf(Data, "[%d,%d]",
////						sensorGrupPtr->ToplamSensor,
////						sensorGrupPtr->ToplamBosYer
//// 					);
//				 memcpy( buffPtr,Data,strlen(Data));
//				 buffPtr += strlen(Data);
//			 
//		 }
//	 }
//	 
//	if ( portNo == PORT_LISTEN_PC ) {
//			if (ETH_SendDataFromListenPortPC( (uint8_t*)ETH.PC_txBuff, strlen((char *)ETH.PC_txBuff)) == true ) {__nop();}
//	}
//	if ( portNo == PORT_LISTEN_CONTROLLER ) {
//		if (ETH_SendDataFromListenPortController( (uint8_t*)ETH.Controller_txBuff, strlen((char *)ETH.Controller_txBuff)) == true ) {__nop();}
//		
//	}
//	
//	ETH_SendETX(portNo);

//}
///***********************************************************************
// * @brief 	ETH_SendDeviceParameters 
// *					

// * @param		-
// *				
// * @return		-
// ***********************************************************************/
//void ETH_SendDeviceParameters (uint32_t portNo, uint8_t param) {	
//uint8_t i,k;
// uint8_t devCnt;
// deviceType *portDev;
//// uint8_t sensor_Dolu=0;
// sensorType *sensor;
// displayType *display;
//// fifo8_t *tx_fifo_ptr;
// uint8_t *buffPtr;
// char StrPtr[30];
// char Data [100];
// uint8_t sendOK=0;
//	
//	ETH_SendSTX(portNo);
//	
//	 if ( portNo == PORT_LISTEN_PC ) {
//			 memset(ETH.PC_txBuff, 0, ETHERNET_PORTLISTEN_PC_BUF_SIZE);
//			 buffPtr = &ETH.PC_txBuff[0];		
//	 }else if ( portNo == PORT_LISTEN_CONTROLLER ) {
//			 memset(ETH.Controller_txBuff, 0, ETHERNET_PORTLISTEN_CONTROLLER_BUF_SIZE);
//			 buffPtr = &ETH.Controller_txBuff[0];	
//	 }else {
//		 return;
//	 }
//	 
//	 
//	
//	 for(k=1; k<=4; k++){
//		 
//	 if ( portNo == PORT_LISTEN_PC ) {
//			 memset(ETH.PC_txBuff, 0, ETHERNET_PORTLISTEN_PC_BUF_SIZE);
//			 buffPtr = &ETH.PC_txBuff[0];		
//	 }else if ( portNo == PORT_LISTEN_CONTROLLER ) {
//			 memset(ETH.Controller_txBuff, 0, ETHERNET_PORTLISTEN_CONTROLLER_BUF_SIZE);
//			 buffPtr = &ETH.Controller_txBuff[0];	
//	 }else {
//		 return;
//	 }
//	 
//		 sendOK=0;
//		 devCnt = 0;
//		 if(k == 1 &&  (param == DEVICERS485PORT1 || param == DEVICEALL)){
//			 portDev=port1Devices;
//			 devCnt=port1DeviceCount;
//			 strcat((char *)buffPtr,ANSWER_DEVICEREGSPORT1);
//		   buffPtr += (sizeof(ANSWER_DEVICEREGSPORT1)-1);
//			 sendOK=1;
//			 
//		 }else if(k == 2  && (param == DEVICERS485PORT2 || param == DEVICEALL)){
//			 portDev=port2Devices;
//			 devCnt=port2DeviceCount;
//			 strcat((char *)buffPtr,ANSWER_DEVICEREGSPORT2);
//		   buffPtr += (sizeof(ANSWER_DEVICEREGSPORT2)-1);
//			 sendOK=1;
//		 }else if(k == 3  && (param == DEVICERS485PORT3 || param == DEVICEALL)){
//			 portDev=port3Devices;
//			 devCnt=port3DeviceCount;
//			 strcat((char *)buffPtr,ANSWER_DEVICEREGSPORT3);
//		   buffPtr += (sizeof(ANSWER_DEVICEREGSPORT3)-1);
//			 sendOK=1;		 
//		 }else if(k == 4  && (param == DEVICERS485PORT4 || param == DEVICEALL)){
//			 portDev=port4Devices;
//			 devCnt=port4DeviceCount;
//			 strcat((char *)buffPtr,ANSWER_DEVICEREGSPORT4);
//		   buffPtr += (sizeof(ANSWER_DEVICEREGSPORT4)-1);	
//			 sendOK=1;
//		 }
//		 
//		 for(i=0; i<devCnt; i++){
//			 if(portDev[i].device == devSENSOR){
//				 sensor = (sensorType *)(&portDev[i].device);
//				 sprintf(Data, "[%d,%d,%d,%d,%d,%d,%d,%d]",
//						portDev[i].deviceAddress,			//sensor->deviceAddress,
//						portDev[i].device,     //sensor->device,
//						sensor->GrupNo,
//						sensor->LEDGrubu,
//						sensor->LEDbagli,
//						sensor->workingMode,
//						sensor->LimitVal,
//						sensor->Dolu
// 					);
//				 memcpy( buffPtr,Data,strlen(Data));
//				 buffPtr += strlen(Data);

//			 }else if(portDev[i].device == devDISPLAY) {
//				display = (displayType *)(&portDev[i].device);					 
//				sprintf(Data, "[%d,%d,%d,%d,%d,%d,%d,%d]",
//						portDev[i].deviceAddress,			//display->deviceAddress,
//					  portDev[i].device,			//display->device,	
//				    display->firstChar,
//						display->inverted,
//						display->HaberlesmeGecikmesi,
//						display->Parlaklik,
//						display->OkYonu,
//						display->val
// 				 );
//				 memcpy( buffPtr,Data,strlen(Data));
//				 buffPtr += strlen(Data);				 
//	
//			 }
//		 }

//		 
//		 memset(StrPtr, 0, 30);
//		 if(k == 1 ) {
//			 strcat(StrPtr,ANSWER_DEVICEREGSPORT1EMPTY);
//		 }else if(k == 2 ) {
//			 strcat(StrPtr,ANSWER_DEVICEREGSPORT2EMPTY);
//		 }else if(k == 3 ) {
//			 strcat(StrPtr,ANSWER_DEVICEREGSPORT3EMPTY);	
//		 }else if(k == 4 ) {
//			 strcat(StrPtr,ANSWER_DEVICEREGSPORT4EMPTY);
//		 }
//		 
//			if ( sendOK ) {
//			if ( devCnt>0 ) {

//				if ( portNo == PORT_LISTEN_PC ) {
//					if (ETH_SendDataFromListenPortPC( (uint8_t*)ETH.PC_txBuff, strlen((char *)ETH.PC_txBuff)) == true ) {__nop();}
//				}
//				if ( portNo == PORT_LISTEN_CONTROLLER ) {
//					if (ETH_SendDataFromListenPortController( (uint8_t*)ETH.Controller_txBuff, strlen((char *)ETH.Controller_txBuff)) == true ) {__nop();}
//				}

//			}else {
//				if ( portNo == PORT_LISTEN_PC ) {
//					if (ETH_SendDataFromListenPortPC( (uint8_t*)StrPtr, strlen(StrPtr)) == true ) {__nop();}
//				}
//				if ( portNo == PORT_LISTEN_CONTROLLER ) {
//					if (ETH_SendDataFromListenPortController( (uint8_t*)StrPtr, strlen(StrPtr)) == true ) {__nop();}
//				}

//			}
//		}
//	
//	 }
//	 ETH_SendETX(portNo);

// }


///***********************************************************************
// * @brief 	ETH_SendProgDeviceStatus 
// *					

// * @param		-
// *				
// * @return		-
// ***********************************************************************/
//void ETH_SendProgDeviceStatus ( uint32_t portNo )  {
//uint16_t i,k;
//uint8_t devCnt;
//deviceType *portDev;
//sensorType *sensor;
//displayType *display;
////displayZeroChar_Type  readfirstChar;
////dispayOrientation_Type	readinverted;
////uint16_t readHaberlesmeGecikmesi;
////uint16_t	readParlaklik;
////uint16_t readOkYonu;					
////char Data[100];
//char sendData[50];
//uint8_t StrBuff[20];
//uint8_t size;
////uint8_t temp1;
////uint8_t temp2;
////uint8_t ParameterChanged = 0;
////osEvent evt;	
////LPC_USART_T *port;


//	ETH_SendSTX(portNo);
//	
//	for(k=1; k<=4; k++){
//		
//	 memset(StrBuff,0,20);
//		
//	 if(k == 1){		
//		 
//		 portDev=port1Devices;
//		 devCnt=port1DeviceCount;
//		 strcat((char *)StrBuff,ANSWER_PORT1);
//		 size = sizeof(ANSWER_PORT1)-1;
//		 //port = RS485_Port_1;		
//		 
//	 }else if(k == 2){
//		 
//		 portDev=port2Devices;
//		 devCnt=port2DeviceCount;
//		 strcat((char *)StrBuff,ANSWER_PORT2);
//		 //port = RS485_Port_2;
//		 size = sizeof(ANSWER_PORT2)-1;
//		 
//	 }else if(k == 3){
//		 
//		 portDev=port3Devices;
//		 devCnt=port3DeviceCount;
//		 strcat((char *)StrBuff,ANSWER_PORT3);
//		 //port = RS485_Port_3;
//		 size = sizeof(ANSWER_PORT3)-1;
//		 
//	 }else if(k == 4){
//		 
//		 portDev=port4Devices;
//		 devCnt=port4DeviceCount;
//		 strcat((char *)StrBuff,ANSWER_PORT4);
//		 //port = RS485_Port_4;
//		 size = sizeof(ANSWER_PORT4)-1;
//		 
//	 }

//		if ( portNo == PORT_LISTEN_PC ) {
//			if (ETH_SendDataFromListenPortPC( (uint8_t*)StrBuff, size) == true ) {__nop();}
//		}
//		if ( portNo == PORT_LISTEN_CONTROLLER ) {
//			if (ETH_SendDataFromListenPortController( (uint8_t*)StrBuff, size) == true ) {__nop();}
//		}	 
//	 
//	 for(i=0; i<devCnt; i++){
//		 if(portDev[i].device == devSENSOR){			  
//			 
//				sensor = (sensorType *)(&portDev[i].device);	

//				if (sensor->comError != 0 ) {
//					sprintf(sendData, "[%d,%d,NOK]", sensor->deviceAddress, sensor->device );     
//					ETH_SendString ( portNo, (uint8_t *)sendData, strlen(sendData));	
//				}	 

//				
//		 }else if(portDev[i].device == devDISPLAY){
//				if (display->comError != 0 ) {
//					sprintf(sendData, "[%d,%d,NOK]", display->deviceAddress, display->device );     
//					ETH_SendString ( portNo, (uint8_t *)sendData, strlen(sendData));	
//				}	 
//		 }
//		 
//	 }
//	}
//	ETH_SendETX(portNo);




//	
//}
// /***********************************************************************
// * @brief 	ETH_UpdateDeviceParameters 
// *					

// * @param		-
// *				
// * @return		-
// ***********************************************************************/
//void ETH_UpdateDeviceParameters (uint8_t * databuff, uint32_t datasize, uint8_t param, uint32_t portNo )  {
//uint8_t i,k;
//uint8_t *devCnt;
//deviceType *portDev;
////sensorType tempsensor;
////displayType tempdisplay;
////displayZeroChar_Type  readfirstChar;
////dispayOrientation_Type	readinverted;
////uint16_t readHaberlesmeGecikmesi;
////uint16_t	readParlaklik;
////uint16_t readOkYonu;					
//char Data[100];
//char sendData[50];
//uint8_t StrBuff[20];
//uint8_t size;
//uint8_t temp1;
//uint8_t temp2;
//uint8_t ParameterChanged = 0;
//osEvent evt;	
//LPC_USART_T *port;
//uint16_t farksize=0;
//	
//	ETH_SendSTX(portNo);
//	
////	clearDeviceRegs();	
//		
//	
//	mptrSensor = osPoolAlloc(mpoolSensor);                     // Allocate memory for the message
//	mptrDisplay = osPoolAlloc(mpoolDisplay);                     // Allocate memory for the message
//	
//	for(k=1; k<=4; k++){
//		
//		memset(StrBuff,0,20);
//		
//		if (k==1) {
//		
//			 portDev=port1Devices;
//			 devCnt=&port1DeviceCount;
//			 strcat((char *)StrBuff,ANSWER_PORT1);
//			 size = sizeof(ANSWER_PORT1)-1;
//		   port = RS485_Port_1;
//			 
//			
//		}else if (k==2) {

//			 portDev=port2Devices;
//			 devCnt=&port2DeviceCount;
//			 strcat((char *)StrBuff,ANSWER_PORT2);
//			 port = RS485_Port_2;
//			 size = sizeof(ANSWER_PORT2)-1;
//		}else if (k==3) {
//			
//			 portDev=port3Devices;
//			 devCnt=&port3DeviceCount;
//			 strcat((char *)StrBuff,ANSWER_PORT3);
//			 port = RS485_Port_3;
//			 size = sizeof(ANSWER_PORT3)-1;
//		}else if (k==4) {

//			 portDev=port4Devices;
//			 devCnt=&port4DeviceCount;
//			 strcat((char *)StrBuff,ANSWER_PORT4);
//			 port = RS485_Port_4;
//			 size = sizeof(ANSWER_PORT4)-1;
//		}			
//		


//		if ((strncmp((const char*)databuff,(const char *)StrBuff,size) == 0) ) {
//			
//			if ( portNo == PORT_LISTEN_PC ) {
//				if (ETH_SendDataFromListenPortPC( (uint8_t*)StrBuff, size) == true ) {__nop();}
//			}
//			if ( portNo == PORT_LISTEN_CONTROLLER ) {
//				if (ETH_SendDataFromListenPortController( (uint8_t*)StrBuff, size) == true ) {__nop();}
//			}
//		
//			databuff += size;
//			memset(Data, 0, 50);
//			for (i=0;i<MAX_PORT_DEVICE_CNT;i++){
//				

//				
//				if ( extract_between ((const char *) databuff, "[","]",Data,&farksize) == true ) {
//					
//					sscanf(Data,"%d,%d",(int *)&temp1,(int *)&temp2);
//																	
//					if (temp2 == devSENSOR ) {		
//						
//									sscanf(Data,"%d,%d,%d,%d,%d,%d,%d,%d",
//												(int *)&mptrSensor->sensorData.deviceAddress,
//												&mptrSensor->sensorData.device,
//												(int *)&mptrSensor->sensorData.GrupNo,
//												(int *)&mptrSensor->sensorData.LEDGrubu,
//												(int *)&mptrSensor->sensorData.LEDbagli,
//												(int *)&mptrSensor->sensorData.workingMode,
//												(int *)&mptrSensor->sensorData.LimitVal,
//												(int *)&mptrSensor->sensorData.Dolu
//												);
//						if (CheckSensorParametersSuitable ( &mptrSensor->sensorData ) == true ) {
////							if (CheckSensorParametersChanged(&mptrSensor->sensorData,portDev,devCnt)) {
//								
//								ParameterChanged=1;
//								mptrSensor->OK=0;
//								mptrSensor->port = k;
//								osMessagePut(MsgBoxSensor, (uint32_t)mptrSensor, osWaitForever);  // Send Message
//								evt = osSignalWait(ETHERNET_SIGNAL_DEVICEUPDATE, 10000);
//								if (evt.status == osEventSignal)  {
//									if(evt.value.signals&ETHERNET_SIGNAL_DEVICEUPDATE){
//											if (mptrSensor->OK ) {
//												sprintf(sendData, "[%d,%d,OK]", mptrSensor->sensorData.deviceAddress, mptrSensor->sensorData.device );     
//												ETH_SendString ( portNo, (uint8_t *)sendData, strlen(sendData));	
//											}else {
//												sprintf(sendData, "[%d,%d,NOK]", mptrSensor->sensorData.deviceAddress, mptrSensor->sensorData.device );     
//												ETH_SendString ( portNo, (uint8_t *)sendData, strlen(sendData)); 	
//											}
//									}

//								}else  {
//									sprintf(sendData, "[%d,%d,NOK]", mptrSensor->sensorData.deviceAddress, mptrSensor->sensorData.device );     
//									ETH_SendString ( portNo, (uint8_t *)sendData, strlen(sendData)); 	
//								}
//									
//								
////							}else {
////								sprintf(sendData, "[%d,%d,OK]", mptrSensor->sensorData.deviceAddress, mptrSensor->sensorData.device );     
////								ETH_SendString ( portNo, (uint8_t *)sendData, strlen(sendData));
////							}
//						}else {
//							sprintf(sendData, "[%d,%d,NOK]", mptrSensor->sensorData.deviceAddress, mptrSensor->sensorData.device );     
//							ETH_SendString ( portNo, (uint8_t *)sendData, strlen(sendData));
// 					
//						}							
//											
//					}else if ( temp2 == devDISPLAY ){
//																	
//									sscanf(Data,"%d,%d,%d,%d,%d,%d,%d,%d",
//												(int *)&mptrDisplay->displayData.deviceAddress,
//												&mptrDisplay->displayData.device,
//												(int *)&mptrDisplay->displayData.firstChar,
//												(int *)&mptrDisplay->displayData.inverted,
//												(int *)&mptrDisplay->displayData.HaberlesmeGecikmesi,
//												(int *)&mptrDisplay->displayData.Parlaklik,
//												(int *)&mptrDisplay->displayData.OkYonu,
//												(int *)&mptrDisplay->displayData.val
//												);
////												
//						if (CheckDisplayParametersSuitable ( &mptrDisplay->displayData ) == true ) {
////							if (CheckDisplayParametersChanged(&mptrDisplay->displayData,portDev,devCnt)) {
//								
//								ParameterChanged=1;
//								mptrDisplay->OK=0;
//								mptrDisplay->port = k;
//								osMessagePut(MsgBoxDisplay, (uint32_t)mptrSensor, osWaitForever);  // Send Message
//								evt = osSignalWait(ETHERNET_SIGNAL_DEVICEUPDATE, 10000);
//								if (evt.status == osEventSignal)  {
//									if(evt.value.signals&ETHERNET_SIGNAL_DEVICEUPDATE){
//											if (mptrDisplay->OK ) {
//												sprintf(sendData, "[%d,%d,OK]", mptrDisplay->displayData.deviceAddress, mptrDisplay->displayData.device );     
//												ETH_SendString ( portNo, (uint8_t *)sendData, strlen(sendData));	
//											}else {
//												sprintf(sendData, "[%d,%d,NOK]", mptrDisplay->displayData.deviceAddress, mptrDisplay->displayData.device );     
//												ETH_SendString ( portNo, (uint8_t *)sendData, strlen(sendData)); 	
//											}
//									}

//								}else  {
//									sprintf(sendData, "[%d,%d,NOK]", mptrSensor->sensorData.deviceAddress, mptrSensor->sensorData.device );     
//									ETH_SendString ( portNo, (uint8_t *)sendData, strlen(sendData)); 	
//								}								
//								
//						
////							}else {
////								sprintf(sendData, "[%d,%d,OK]", mptrDisplay->displayData.deviceAddress, mptrDisplay->displayData.device );     
////								ETH_SendString ( portNo, (uint8_t *)sendData, strlen(sendData));
////							}								
//						}else {
//							sprintf(sendData, "[%d,%d,NOK]", mptrDisplay->displayData.deviceAddress, mptrDisplay->displayData.device );     
//							ETH_SendString ( portNo, (uint8_t *)sendData, strlen(sendData));
// 					
//						}	
//						
//					}
//					
//			  databuff += farksize;
//				databuff += strlen(Data);	 					
//				databuff += 2  	;		// [ ve ] karakterleri i�in.	
//				if  ( *databuff == NULL ) {
//					break;
//				}
//				if  ( *databuff == '<' ) {
//					break;
//				}
//				
//			}

//		}
//			

//			
//			
//	}
//	

//	

//}
//	osPoolFree(mpoolSensor, mptrSensor);   
//	osPoolFree(mpoolDisplay, mptrDisplay);  

//	if ( ParameterChanged ) {
////		SaveAllParameterToFlash();
////		getDataFromFlash();
//		ETH_SendString ( portNo, (uint8_t *)ANSWER_DEVICEPARAMETERSETINGOK, strlen(ANSWER_DEVICEPARAMETERSETINGOK));
//	}else {
//		ETH_SendString ( portNo, (uint8_t *)ANSWER_DEVICEPARAMETERNOCHANGE, strlen(ANSWER_DEVICEPARAMETERNOCHANGE));
//	}
//	
//	ETH_SendETX(portNo);
//}

///***********************************************************************
// * @brief 	ETH_UpdateDeviceParameters 
// *					

// * @param		-
// *				
// * @return		-
// ***********************************************************************/
//void ETH_ReadSpecificDevice (uint8_t * databuff, uint32_t datasize,uint32_t portNo )  {
//uint8_t i,k;
////uint8_t *devCnt;
////deviceType *portDev;				
//char Data[100];
//char sendData[50];
//uint8_t StrBuff[20];
//uint8_t size;
//uint8_t temp1;
//uint8_t temp2;
//uint8_t ParameterChanged = 0;
//osEvent evt;	
//LPC_USART_T *port;
//uint16_t farksize=0;
//	
//	ETH_SendSTX(portNo);
//	
////	clearDeviceRegs();	
//		
//	
//	mptrReadSensor = osPoolAlloc(mpoolReadSensor);                     // Allocate memory for the message
//	mptrReadDisplay = osPoolAlloc(mpoolReadDisplay);                     // Allocate memory for the message
//	
//	for(k=1; k<=4; k++){
//		
//		memset(StrBuff,0,20);
//		
//		if (k==1) {
//		
////			 portDev=port1Devices;
////			 devCnt=&port1DeviceCount;
//			 strcat((char *)StrBuff,ANSWER_PORT1);
//			 size = sizeof(ANSWER_PORT1)-1;
//		   port = RS485_Port_1;			 		
//		}else if (k==2) {

////			 portDev=port2Devices;
////			 devCnt=&port2DeviceCount;
//			 strcat((char *)StrBuff,ANSWER_PORT2);
//			 port = RS485_Port_2;
//			 size = sizeof(ANSWER_PORT2)-1;
//		}else if (k==3) {
//			
////			 portDev=port3Devices;
////			 devCnt=&port3DeviceCount;
//			 strcat((char *)StrBuff,ANSWER_PORT3);
//			 port = RS485_Port_3;
//			 size = sizeof(ANSWER_PORT3)-1;
//		}else if (k==4) {

////			 portDev=port4Devices;
////			 devCnt=&port4DeviceCount;
//			 strcat((char *)StrBuff,ANSWER_PORT4);
//			 port = RS485_Port_4;
//			 size = sizeof(ANSWER_PORT4)-1;
//		}			
//		
//		
//		if ((strncmp((const char*)databuff,(const char *)StrBuff,size) == 0) ) {
//			
//			if ( portNo == PORT_LISTEN_PC ) {
//				if (ETH_SendDataFromListenPortPC( (uint8_t*)StrBuff, size) == true ) {__nop();}
//			}
//			if ( portNo == PORT_LISTEN_CONTROLLER ) {
//				if (ETH_SendDataFromListenPortController( (uint8_t*)StrBuff, size) == true ) {__nop();}
//			}			
//			databuff += size;
//			memset(Data, 0, 50);
//			for (i=0;i<MAX_PORT_DEVICE_CNT;i++){
//						
//				if ( extract_between ((const char *) databuff, "[","]",Data,&farksize) == true ) {
//					
//					sscanf(Data,"%d,%d",(int *)&temp1,(int *)&temp2);
//																	
//					if (temp2 == devSENSOR ) {
//						
//						mptrReadSensor->sensorData.deviceAddress = temp1;	
//						mptrReadSensor->sensorData.device = devSENSOR;	
//						mptrReadSensor->OK=0;
//						mptrReadSensor->port = k;
//						mptrReadSensor->sensorData.workingMode = 0;
//						mptrReadSensor->sensorData.LimitVal = 0;
//						mptrReadSensor->sensorData.Dolu = 0;
//						osMessagePut(MsgBoxReadSensor, (uint32_t)mptrReadSensor, osWaitForever);  // Send Message
//						evt = osSignalWait(ETHERNET_SIGNAL_DEVICEREAD, 10000);
//						if (evt.status == osEventSignal)  {
//							if(evt.value.signals&ETHERNET_SIGNAL_DEVICEREAD){
//									if (mptrReadSensor->OK ) {
//										sprintf(sendData, "[%d,%d,%d,%d,%d,%d,%d,%d]",
//											temp1,			//sensor->deviceAddress,
//											temp2,     //sensor->device,
//											mptrReadSensor->sensorData.GrupNo,
//											mptrReadSensor->sensorData.LEDGrubu,
//										  mptrReadSensor->sensorData.LEDbagli,
//											mptrReadSensor->sensorData.workingMode,
//											mptrReadSensor->sensorData.LimitVal,
//											mptrReadSensor->sensorData.Dolu
//										);

//										ETH_SendString ( portNo, (uint8_t *)sendData, strlen(sendData));	
//										
//									}else {
//										sprintf(sendData, "[%d,%d,NOK]", temp1, temp2 );     
//										ETH_SendString ( portNo, (uint8_t *)sendData, strlen(sendData)); 	
//									}
//							}

//						}else  {
//							sprintf(sendData, "[%d,%d,NOK]", temp1, temp2 );     
//							ETH_SendString ( portNo, (uint8_t *)sendData, strlen(sendData)); 	
//						}						

//					
//											
//					}else if ( temp2 == devDISPLAY ){
//						
//						mptrReadDisplay->displayData.deviceAddress = temp1;	
//						mptrReadDisplay->displayData.device = devDISPLAY;	
//						mptrReadDisplay->OK=0;
//						mptrReadDisplay->port = k;
//						mptrReadDisplay->displayData.firstChar = 0;
//						mptrReadDisplay->displayData.inverted = 0;
//						mptrReadDisplay->displayData.HaberlesmeGecikmesi = 0;
//						mptrReadDisplay->displayData.Parlaklik =0;
//						mptrReadDisplay->displayData.OkYonu = 0;
//						mptrReadDisplay->displayData.val = 0;
//						
//						osMessagePut(MsgBoxReadDisplay, (uint32_t)mptrReadDisplay, osWaitForever);  // Send Message
//						evt = osSignalWait(ETHERNET_SIGNAL_DEVICEREAD, 10000);						

//						if (evt.status == osEventSignal)  {
//							if(evt.value.signals&ETHERNET_SIGNAL_DEVICEREAD){
//									if (mptrReadDisplay->OK ) {
//										sprintf(sendData, "[%d,%d,%d,%d,%d,%d,%d,%d]",
//											temp1,			//sensor->deviceAddress,
//											temp2,     //sensor->device,
//											mptrReadDisplay->displayData.firstChar,
//											mptrReadDisplay->displayData.inverted,
//											mptrReadDisplay->displayData.HaberlesmeGecikmesi,
//											mptrReadDisplay->displayData.Parlaklik,
//											mptrReadDisplay->displayData.OkYonu,
//											mptrReadDisplay->displayData.val								
//										);
// 
//										ETH_SendString ( portNo, (uint8_t *)sendData, strlen(sendData));	
//										
//									}else {
//										sprintf(sendData, "[%d,%d,NOK]", temp1, temp2 );     
//										ETH_SendString ( portNo, (uint8_t *)sendData, strlen(sendData)); 	
//									}
//							}

//						}else  {
//							sprintf(sendData, "[%d,%d,NOK]", temp1, temp2 );     
//							ETH_SendString ( portNo, (uint8_t *)sendData, strlen(sendData)); 	
//						}								
//								
//								
//							
//							}else {
//								sprintf(sendData, "[%d,%d,OK]", mptrDisplay->displayData.deviceAddress, mptrDisplay->displayData.device );     
//								ETH_SendString ( portNo, (uint8_t *)sendData, strlen(sendData));
//							}								
//						}else {
//							sprintf(sendData, "[%d,%d,NOK]", mptrDisplay->displayData.deviceAddress, mptrDisplay->displayData.device );     
//							ETH_SendString ( portNo, (uint8_t *)sendData, strlen(sendData));
// 					
//						}	
//						
//				
//					
//			  databuff += farksize;
//				databuff += strlen(Data);	 					
//				databuff += 2  	;		// [ ve ] karakterleri i�in.	
//				if  ( *databuff == NULL ) {
//					break;
//				}
//				if  ( *databuff == '<' ) {
//					break;
//				}
//			}
//				
//			}

//		}
//			

//	

//	

//	osPoolFree(mpoolReadSensor, mptrReadSensor);   
//	osPoolFree(mpoolReadDisplay, mptrReadDisplay);  


//	
//	ETH_SendETX(portNo);
//}

// /***********************************************************************
// * @brief 	ETH_GetNewNetworkParameters 
// *					

// * @param		-
// *				
// * @return		-
// ***********************************************************************/
//void ETH_UpdateDisplayCommandRegs (uint8_t * databuff, uint32_t datasize, uint32_t portNo ) {
//uint8_t i,k;//,portNo;
//char Data[50];
////uint8_t *buffPtr;
//uint16_t farksize=0;	
//	
////	ETH_SendSTX(portNo);
////	portNo = 0;
//	
//	for(k=1; k<=4; k++){
//		
//		if ((strncmp((char const*)databuff,ANSWER_PORT1,sizeof(ANSWER_PORT1)-1) == 0) ) {
//			
//			databuff += sizeof(ANSWER_PORT1)-1;
//			memset(Data, 0, 50);
//			for (i=0;i<DISPLAYCOMMANDREGSAYISI;i++){
//				
//								
//				if ( extract_between ((const char *) databuff, "[","]",Data,&farksize) == true ) {
//					sscanf(Data,"%d,%d",(int *)&displayCommandRegPort1[i].adres,(int *)&displayCommandRegPort1[i].val);
//				}
//				databuff += farksize;
//				databuff += strlen(Data);	 	
//				databuff += 2  	;		// [ ve ] karakterleri i�in.	
//				if  ( *databuff == NULL ) {
//					break;
//				}
//				if  ( *databuff == '<' ) {
//					break;
//				}
//			}
//			
//		}if ((strncmp((char const*)databuff,ANSWER_PORT2,sizeof(ANSWER_PORT2)-1) == 0) ) { 
//			
//			databuff += sizeof(ANSWER_PORT2)-1;			
//			memset(Data, 0, 50);
//			for (i=0;i<DISPLAYCOMMANDREGSAYISI;i++){
//				if ( extract_between ((const char *) databuff, "[","]",Data,&farksize) == true ) {
//					sscanf(Data,"%d,%d",(int *)&displayCommandRegPort2[i].adres,(int *)&displayCommandRegPort2[i].val);
//				}
//				databuff += farksize;
//				databuff += strlen(Data);	 	
//				databuff += 2  ;
//				// [ ve ] karakterleri i�in.	
//				if  ( *databuff == NULL ) {
//					break;
//				}
//				if  ( *databuff == '<' ) {
//					break;
//				}				
//			}
//			
//		}if ((strncmp((char const*)databuff,ANSWER_PORT3,sizeof(ANSWER_PORT3)-1) == 0) ) { 

//			databuff += sizeof(ANSWER_PORT3)-1;			
//			memset(Data, 0, 50);
//			for (i=0;i<DISPLAYCOMMANDREGSAYISI;i++){
//				if ( extract_between ((const char *) databuff, "[","]",Data,&farksize) == true ) {
//					sscanf(Data,"%d,%d",(int *)&displayCommandRegPort3[i].adres,(int *)&displayCommandRegPort3[i].val);
//				}
//				databuff += farksize;
//				databuff += strlen(Data);	 	
//				databuff += 2  ;			// [ ve ] karakterleri i�in.	
//				if  ( *databuff == NULL ) {
//					break;
//				}
//				if  ( *databuff == '<' ) {
//					break;
//				}				
//			}
//			
//		}if ((strncmp((char const*)databuff,ANSWER_PORT4,sizeof(ANSWER_PORT4)-1) == 0) ) { 
//			
//			databuff += sizeof(ANSWER_PORT4)-1;			
//			memset(Data, 0, 50);
//			for (i=0;i<DISPLAYCOMMANDREGSAYISI;i++){
//				if ( extract_between ((const char *) databuff, "[","]",Data,&farksize) == true ) {
//					sscanf(Data,"%d,%d",(int *)&displayCommandRegPort4[i].adres,(int *)&displayCommandRegPort4[i].val);
//				}
//				databuff += farksize;
//				databuff += strlen(Data);	 	
//				databuff += 2  ;			// [ ve ] karakterleri i�in.		
//				if  ( *databuff == NULL ) {
//					break;
//				}
//				if  ( *databuff == '<' ) {
//					break;
//				}				
//			}
//			
//		}
//		
//		
//	}

//	ETH_SendETX(portNo);
	
//}

// /***********************************************************************
// * @brief 	ETH_ProgramMaster 
// *					

// * @param		-
// *				
// * @return		-
// ***********************************************************************/
//void ETH_ProgramMaster (uint8_t * databuff, uint32_t datasize, uint32_t portNo )  {
//uint8_t i,k;
//uint8_t *devCnt;
//deviceType *portDev;
//sensorType tempsensor;
//displayType tempdisplay;
////displayZeroChar_Type  readfirstChar;
////dispayOrientation_Type	readinverted;
////uint16_t readHaberlesmeGecikmesi;
////uint16_t	readParlaklik;
////uint16_t readOkYonu;					
//char Data[100];
//char sendData[50];
//uint8_t StrBuff[20];
//uint8_t size;
//uint8_t temp1;
//uint8_t temp2;
//uint8_t ParameterChanged = 0;
////osEvent evt;	
////LPC_USART_T *port;
//uint16_t farksize=0;
//	
//	ETH_SendSTX(portNo);
//	
//	clearDeviceRegs();	
//	
//	for(k=1; k<=4; k++){
//		
//		memset(StrBuff,0,20);
//		
//		if (k==1) {
//		
//			 portDev=port1Devices;
//			 devCnt=&port1DeviceCount;
//			 strcat((char *)StrBuff,ANSWER_PORT1);
//			 size = sizeof(ANSWER_PORT1)-1;
////		   port = RS485_Port_1;
//			 
//			
//		}else if (k==2) {

//			 portDev=port2Devices;
//			 devCnt=&port2DeviceCount;
//			 strcat((char *)StrBuff,ANSWER_PORT2);
//			 //port = RS485_Port_2;
//			 size = sizeof(ANSWER_PORT2)-1;
//		}else if (k==3) {
//			
//			 portDev=port3Devices;
//			 devCnt=&port3DeviceCount;
//			 strcat((char *)StrBuff,ANSWER_PORT3);
//			 //port = RS485_Port_3;
//			 size = sizeof(ANSWER_PORT3)-1;
//		}else if (k==4) {

//			 portDev=port4Devices;
//			 devCnt=&port4DeviceCount;
//			 strcat((char *)StrBuff,ANSWER_PORT4);
//			 //port = RS485_Port_4;
//			 size = sizeof(ANSWER_PORT4)-1;
//		}			
//		


//		if ((strncmp((const char*)databuff,(const char *)StrBuff,size) == 0) ) {
//			
//			if ( portNo == PORT_LISTEN_PC ) {
//				if (ETH_SendDataFromListenPortPC( (uint8_t*)StrBuff, size) == true ) {__nop();}
//			}
//			if ( portNo == PORT_LISTEN_CONTROLLER ) {
//				if (ETH_SendDataFromListenPortController( (uint8_t*)StrBuff, size) == true ) {__nop();}
//			}
//		
//			databuff += size;
//			memset(Data, 0, 50);
//			for (i=0;i<MAX_PORT_DEVICE_CNT;i++){
//				
//				
//				if ( extract_between ((const char *) databuff, "[","]",Data,&farksize) == true ) {
//					
//					sscanf(Data,"%d,%d",(int *)&temp1,(int *)&temp2);
//																	
//					if (temp2 == devSENSOR ) {
//																	
//									sscanf(Data,"%d,%d,%d,%d,%d,%d,%d,%d",
//												(int *)&tempsensor.deviceAddress,
//												&tempsensor.device,
//												(int *)&tempsensor.GrupNo,
//												(int *)&tempsensor.LEDGrubu,
//												(int *)&tempsensor.LEDbagli,
//												(int *)&tempsensor.workingMode,
//												(int *)&tempsensor.LimitVal,
//												(int *)&tempsensor.Dolu
//												);
//						if (CheckSensorParametersSuitable ( &tempsensor) == true ) {
//							if (CheckSensorParametersChanged(&tempsensor,portDev,devCnt)) {
//								
//								ParameterChanged=1;
//															
//							}
//						}else {
//							sprintf(sendData, "[%d,%d,NOK]", tempsensor.deviceAddress, tempsensor.device );     
//							ETH_SendString ( portNo, (uint8_t *)sendData, strlen(sendData));
// 					
//						}							
//											
//					}else if ( temp2 == devDISPLAY ){
//									sscanf(Data,"%d,%d,%d,%d,%d,%d,%d,%d",
//												(int *)&tempdisplay.deviceAddress,
//												&tempdisplay.device,
//												(int *)&tempdisplay.firstChar,
//												(int *)&tempdisplay.inverted,
//												(int *)&tempdisplay.HaberlesmeGecikmesi,
//												(int *)&tempdisplay.Parlaklik,
//												(int *)&tempdisplay.OkYonu,
//												(int *)&tempdisplay.val
//												);
//												
//						if (CheckDisplayParametersSuitable ( &tempdisplay ) == true ) {
//							if (CheckDisplayParametersChanged(&tempdisplay,portDev,devCnt)) {

//								ParameterChanged=1;
////			200 no lu displayi de set et edim ama sildi. device search te display bulunmustu			
//								
//							}								
//						}	else {
//							sprintf(sendData, "[%d,%d,NOK]", tempdisplay.deviceAddress, tempdisplay.device );     
//							ETH_SendString ( portNo, (uint8_t *)sendData, strlen(sendData));
// 					
//						}
//						
//					}
//					
//			  databuff += farksize;
//				databuff += strlen(Data);	 					
//				databuff += 2  	;		// [ ve ] karakterleri i�in.	
//				if  ( *databuff == NULL ) {
//					break;
//				}
//				if  ( *databuff == '<' ) {
//					break;
//				}
//				
//			}

//		}
//			
//	}
//	

//}	
//	
//	if ( ParameterChanged ) {
//		SaveAllParameterToFlash();
//		getDataFromFlash();
//		ETH_SendString ( portNo, (uint8_t *)ANSWER_DEVICEPARAMETERSETINGOK, strlen(ANSWER_DEVICEPARAMETERSETINGOK));
//	}else {
//		ETH_SendString ( portNo, (uint8_t *)ANSWER_DEVICEPARAMETERNOCHANGE, strlen(ANSWER_DEVICEPARAMETERNOCHANGE));
//	}
//	ETH_SendETX(portNo);
//	
//}


///*********************************************************************//**
// * @brief 	Ethernet send Listen Data	
// * @param		None
// * @return		None
// ***********************************************************************/
//bool ETH_SendDataFromListenPortController (uint8_t *data, uint16_t size ) {	
////	
////int32_t ret; // return value for SOCK_ERRORs
//////uint8_t STXptr = STARTOFTEXT;
//////uint8_t ETXptr = ENDOFTEXT;


////   // Socket Status Transitions
////   // Check the W5500 Socket n status register (Sn_SR, The 'Sn_SR' controlled by Sn_CR command or Packet send/recv status)
////   switch(getSn_SR(SOCK_TCPS_INCOMING_CONTROLLER))
////   {
////      case SOCK_ESTABLISHED :
////         if(getSn_IR(SOCK_TCPS_INCOMING_CONTROLLER) & Sn_IR_CON)	// Socket n interrupt register mask; TCP CON interrupt = connection with peer is successful
////         {
////					setSn_IR(SOCK_TCPS_INCOMING_CONTROLLER, Sn_IR_CON);  // this interrupt should be write the bit cleared to '1'
////         }

////         //////////////////////////////////////////////////////////////////////////////////////////////
////         // Data Transaction Parts; Handle the [data receive and send] process
////         //////////////////////////////////////////////////////////////////////////////////////////////

////				// Data sentsize control
////				//  ret = sendDataAndRetyIfBusy(SOCK_TCPS_INCOMING_CONTROLLER, &STXptr, 1,SOCKETIMEOUT,SOCKETBUSYTIMEOUT); // Data send process (User's buffer -> Destination through H/W Tx socket buffer)
////					if(ret < 0) // Send Error occurred (sent data length < 0)
////					{
////						return false;
////					}
////				  
////					while (size > WIZCHIP_READ_WRITE_SIZE-3 ) {
////						//ret = send(SOCK_TCPS_INCOMMING_PC, data, WIZCHIP_READ_WRITE_SIZE-3,SOCKETIMEOUT); // Data send process (User's buffer -> Destination through H/W Tx socket buffer)
////						ret = sendDataAndRetyIfBusy(SOCK_TCPS_INCOMING_CONTROLLER, data, WIZCHIP_READ_WRITE_SIZE-3,SOCKETIMEOUT,SOCKETBUSYTIMEOUT); // Data send process (User's buffer -> Destination through H/W Tx socket buffer)
////						if(ret < 0) // Send Error occurred (sent data length < 0)
////						{
////							return false;
////						}
////						data += WIZCHIP_READ_WRITE_SIZE-3;
////						size -= WIZCHIP_READ_WRITE_SIZE-3;
////						
////						if ( size < WIZCHIP_READ_WRITE_SIZE-3 ) {
////							break;
////						}
////					}
////					if (size > 0 ) {
////						//ret = send(SOCK_TCPS_INCOMMING_PC, data, size,SOCKETIMEOUT); // Data send process (User's buffer -> Destination through H/W Tx socket buffer)
////						
////						ret = sendDataAndRetyIfBusy(SOCK_TCPS_INCOMING_CONTROLLER, data, size,SOCKETIMEOUT,SOCKETBUSYTIMEOUT); // Data send process (User's buffer -> Destination through H/W Tx socket buffer)
////						if(ret < 0) // Send Error occurred (sent data length < 0)
////						{
////						return false;
////						}
////					}
////				
////					//ret = sendDataAndRetyIfBusy(SOCK_TCPS_INCOMING_CONTROLLER, &ETXptr, 1,SOCKETIMEOUT,SOCKETBUSYTIMEOUT); // Data send process (User's buffer -> Destination through H/W Tx socket buffer)				 
////					
////					if(ret < 0) // Send Error occurred (sent data length < 0)
////					{
////						return false;
////					}
////					else {
////						return true;
////					}


//// //        break;

////      case SOCK_CLOSE_WAIT :

////         return false;
//////         break;

////      case SOCK_INIT :

////         return false;
//////         break;

////      case SOCK_CLOSED:
////         return false;
////       //  break;
////			
////      default:
////         break;
////   }
////	 
//   return false;	
//	
//}


///*********************************************************************//**
// * @brief 	Ethernet ETH_SendData	
// * @param		None
// * @return		None
// ***********************************************************************/
//int32_t  ETH_SendData_Orginal (uint8_t sn, uint8_t* buf, uint8_t* destip, uint16_t destport) {
//   int32_t ret; // return value for SOCK_ERRORs
//   uint16_t size = 0, sentsize=0;
//#define DATA_BUF_SIZE 500
//   // Destination (TCP Server) IP info (will be connected)
//   // >> loopback_tcpc() function parameter
//   // >> Ex)
//   //	uint8_t destip[4] = 	{192, 168, 0, 214};
//   //	uint16_t destport = 	5000;

//   // Port number for TCP client (will be increased)
//   uint16_t any_port = 	50000;

//   // Socket Status Transitions
//   // Check the W5500 Socket n status register (Sn_SR, The 'Sn_SR' controlled by Sn_CR command or Packet send/recv status)
//   switch(getSn_SR(sn))
//   {
//      case SOCK_ESTABLISHED :
//         if(getSn_IR(sn) & Sn_IR_CON)	// Socket n interrupt register mask; TCP CON interrupt = connection with peer is successful
//         {
//#ifdef _LOOPBACK_DEBUG_
//			printf("%d:Connected to - %d.%d.%d.%d : %d\r\n",sn, destip[0], destip[1], destip[2], destip[3], destport);
//#endif
//			setSn_IR(sn, Sn_IR_CON);  // this interrupt should be write the bit cleared to '1'
//         }

//         //////////////////////////////////////////////////////////////////////////////////////////////
//         // Data Transaction Parts; Handle the [data receive and send] process
//         //////////////////////////////////////////////////////////////////////////////////////////////
//		 if((size = getSn_RX_RSR(sn)) > 0) // Sn_RX_RSR: Socket n Received Size Register, Receiving data length
//         {
//			if(size > DATA_BUF_SIZE) size = DATA_BUF_SIZE; // DATA_BUF_SIZE means user defined buffer size (array)
//			ret = recv(sn, buf, size ,SOCKETIMEOUT); // Data Receive process (H/W Rx socket buffer -> User's buffer)

//			if(ret <= 0) return ret; // If the received data length <= 0, receive failed and process end
//			sentsize = 0;
//			ret = sendDataAndRetyIfBusy(SOCK_TCPS_INCOMMING_PC, buf, size,SOCKETIMEOUT,SOCKETBUSYTIMEOUT); // Data send process (User's buffer -> Destination through H/W Tx socket buffer)
////				ret = send(SOCK_TCPS_INCOMMING_PC, buf, size,SOCKETIMEOUT); // Data send process (User's buffer -> Destination through H/W Tx socket buffer)
//					
//			// Data sentsize control
//			while(size != sentsize)
//			{
//				ret = sendDataAndRetyIfBusy(sn, buf+sentsize, size-sentsize ,SOCKETIMEOUT,SOCKETBUSYTIMEOUT); // Data send process (User's buffer -> Destination through H/W Tx socket buffer)
//				//ret = send(sn, buf+sentsize, size-sentsize ,SOCKETIMEOUT); // Data send process (User's buffer -> Destination through H/W Tx socket buffer)
//				
//				if(ret < 0) // Send Error occurred (sent data length < 0)
//				{
//					close(sn ,SOCKETIMEOUT); // socket close
//					return ret;
//				}
//				sentsize += ret; // Don't care SOCKERR_BUSY, because it is zero.
//			}
//         }
//		 //////////////////////////////////////////////////////////////////////////////////////////////
//         break;

//      case SOCK_CLOSE_WAIT :
//#ifdef _LOOPBACK_DEBUG_
//         //printf("%d:CloseWait\r\n",sn);
//#endif
//         if((ret=disconnect(sn,SOCKETIMEOUT)) != SOCK_OK) return ret;
//#ifdef _LOOPBACK_DEBUG_
//         printf("%d:Socket Closed\r\n", sn);
//#endif
//         break;

//      case SOCK_INIT :
//#ifdef _LOOPBACK_DEBUG_
//    	 printf("%d:Try to connect to the %d.%d.%d.%d : %d\r\n", sn, destip[0], destip[1], destip[2], destip[3], destport);
//#endif
//    	 if( (ret = connect(sn, destip, destport,SOCKETIMEOUT)) != SOCK_OK) return ret;	//	Try to TCP connect to the TCP server (destination)
//         break;

//      case SOCK_CLOSED:
//    	  close(sn,SOCKETIMEOUT);
//    	  if((ret=socket(sn, Sn_MR_TCP, any_port++, 0x00,SOCKETIMEOUT)) != sn) return ret; // TCP socket open with 'any_port' port number
//#ifdef _LOOPBACK_DEBUG_
//    	 //printf("%d:TCP client loopback start\r\n",sn);
//         //printf("%d:Socket opened\r\n",sn);
//#endif
//         break;
//      default:
//         break;
//   }
//   return 1;
//}

