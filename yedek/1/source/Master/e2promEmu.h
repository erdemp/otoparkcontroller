

#ifndef E2PROMEMU_H_
#define E2PROMEMU_H_

#include "chip.h"
#include "cmsis_os.h"
#include "crc.h"

#ifdef __cplusplus
extern "C"
{
#endif
	

	
	#define E2PROM_START_ADDRESS	0x0000F000		//en sona eklendi. Dolayisiyla bir birlestirme islemine gerek yok
	#define E2PROM_START_SECTOR_NUM	15 //
	#define E2PROM_END_SECTOR_NUM	15
		
	#define E2PROM_PAGE_SIZE		(4*1024)
	#define E2PROM_USER_SIZE		(32)	// 32 byte user size 
	#define E2PROM_MIN_WRITE_BYTE	256

	void SaveE2PromRAM(uint8_t *data, uint32_t index, uint32_t length);
	Bool writeE2Prom(void);
	uint8_t *GetE2PromDataPtr(uint32_t index);
	Bool e2PromEmptyCheck(void);
	void getE2PromDataToRAM(void);

#ifdef __cplusplus
}
#endif

#endif /* DELAYS_H_ */


