
#ifndef FILTERS_H_
#define FILTERS_H_

//#include "lpc_types.h"
#include "chip.h"
#include "cmsis_os.h"

#ifdef __cplusplus
extern "C"
{
#endif

typedef struct {
	uint32_t *buffer;
	uint32_t index;
	uint32_t counter;
	uint32_t bufferLength;
}averageBuffer_32bit;

typedef struct {
	uint32_t *buffer;
	uint32_t index;
	uint32_t bufferLength;
	bool firstMedian;
}medianBuffer_32bit;

uint32_t medianFilter_32bit(medianBuffer_32bit* medianPtr, uint32_t newVal);
uint32_t averageFilter_32bit(averageBuffer_32bit* ortalamaPtr, uint32_t newVal);
double timeDomainStabilization(double newVal, uint16_t timeVal, bool equilibrium, bool initializeFilter);	

#ifdef __cplusplus
}
#endif

#endif /* FILTERS_H_ */

