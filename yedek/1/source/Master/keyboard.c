
#include "keyboard.h"


//#define Key_Row1_Port	0
//#define Key_Row1_Pin	17
//#define Key_Key_Row2_Port	0
//#define Key_Row2_Pin	19
//#define Key_Row3_Port	2
//#define Key_Row3_Pin	10
//#define Row4_Port	2
//#define Row4_Pin	10
// #define Row5_Port	1
// #define Row5_Pin	15
// #define Row6_Port	1
// #define Row6_Pin	10

//#define Key_Col1_Port	0
//#define Key_Key_Col1_Pin	18
//#define Key_Col2_Port	0
//#define Key_Col2_Pin	20
//#define Col3_Port	0
//#define Col3_Pin	22
//#define Col4_Port	0
//#define Col4_Pin	22
//#define Col5_Port	1
// #define Col5_Pin	16
// #define Col6_Port	1
// #define Col6_Pin	14

//#define Buzzer_Port 1
//#define Buzzer_Pin	25

#define TUS_ADEDI	6
#define COL_ADEDI	2

#define BOUNCE_TIME			50
#define LOW_LEVEL_TIME	30
#define HIGH_LEVEL_TIME	30
#define KEY_INT_TIME	5

typedef struct{
	uint64_t	basilmisOlabilirReg;
	uint64_t	basildiReg;
	uint64_t  birakilmisOlabilirReg;
	uint64_t  birakildi;
	uint64_t  islendi;
	uint32_t	basildiBounce[TUS_ADEDI];
	uint32_t	lowLevelTime[TUS_ADEDI];
	uint32_t	birakildiBounce[TUS_ADEDI];
	uint32_t	highLevelTime[TUS_ADEDI];
	uint32_t	tusSuresi[TUS_ADEDI];
	uint8_t 	colTempData[TUS_ADEDI / COL_ADEDI];
}keyboard_t;

keyboard_t keyStatus;

uint16_t buzzerTime=0;

bool turnOff=false;
uint8_t OnOffKey;

void keyboardTimer_CallBack(void const *arg);
osTimerDef(keyboardTimer, keyboardTimer_CallBack);
osTimerId timer_keyboard = NULL;

uint8_t checkCol(void);
void keyScan(void);
void buzzerInit(void);
void setBuzzer(uint16_t ms);
void clearBuzzer(void);


 osMutexDef (buzzerMutex);
 osMutexId buzzer_mutex_id = NULL; 

osThreadId keyboardTaskID = NULL;
/*******************************************************************************
* @brief				Bu fonksiyon keyboard icin hazirlanmis RTOS taskidir
* @param[in]    None
* @return				None
*******************************************************************************/
void keyboardTask(void const *argument){
	keyboardInit();
	while(1){
		osEvent evt;
		evt = osSignalWait(KEYBOARD_TASK_TERMINATE, 0);
		if (evt.status == osEventSignal)  {
			if(evt.value.signals&KEYBOARD_TASK_TERMINATE){
				keyboardTurnOff();
				return;
			}
		}
		keyboardMain();
		osThreadYield();
		//osDelay(10);
	}
}

/*******************************************************************************
* @brief				Bu fonksiyon ADC taskini ve onunla iliskili herseyi kapatir
* @param[in]    None
* @return				None
*******************************************************************************/
void keyboardTurnOff(void){
	keyboardTaskID = osThreadGetId();
	if(keyboardTaskID!=NULL){
		if(timer_keyboard!=NULL){
			while(osTimerDelete(timer_keyboard)!=osOK){
				osDelay(10);
			}
			timer_keyboard=NULL;
		}
		if(buzzer_mutex_id!=NULL){
			while(osMutexDelete(buzzer_mutex_id)!=osOK){
					osDelay(10);
			}
			buzzer_mutex_id = NULL;
		}
		while(osThreadTerminate(keyboardTaskID)!=osOK){
				osDelay(10);
		}
		keyboardTaskID=NULL;
	}
}


//-----------------------------------------------------------------------
// 	  keyboardInit
// keyboard islemi icin initialize fonksiyonu
//-----------------------------------------------------------------------
void keyboardInit(void){

	
	buzzerInit();
	//deviceTurnOn();

	Chip_IOCON_PinMuxSet(LPC_IOCON, Key_Col1_Port, Key_Col1_Pin, Key_Col1_IOCON );
	Chip_IOCON_PinMuxSet(LPC_IOCON, Key_Col2_Port, Key_Col2_Pin, Key_Col2_IOCON );
//	Chip_IOCON_PinMuxSet(LPC_IOCON, Col3_Port, Col3_Pin, IOCON_FUNC0 | IOCON_MODE_PULLUP );
	
//	Chip_IOCON_PinMuxSet(LPC_IOCON, Key_Col1_Port, Key_Col1_Pin, IOCON_MODE_PULLUP);
//	Chip_IOCON_PinMuxSet(LPC_IOCON, Key_Col2_Port, Key_Col2_Pin, IOCON_MODE_PULLUP);
//	Chip_IOCON_PinMuxSet(LPC_IOCON, Col3_Port, Col3_Pin, IOCON_MODE_PULLUP);
	
	Chip_GPIO_SetPinDIRInput(LPC_GPIO, Key_Col1_Port, Key_Col1_Pin);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO, Key_Col2_Port, Key_Col2_Pin);
//	Chip_GPIO_SetPinDIRInput(LPC_GPIO, Col3_Port, Col3_Pin);
	
//	PullUpDownIOPin(Key_Col1_Port, Key_Col1_Pin, 1);
//	PullUpDownIOPin(Key_Col2_Port, Key_Col2_Pin, 1);
//	PullUpDownIOPin(Col3_Port, Col3_Pin, 1);
//	PullUpDownIOPin(Col4_Port, Col4_Pin, 1);
//// 	PullUpDownIOPin(Col5_Port, Col5_Pin, 1);
//// 	PullUpDownIOPin(Col6_Port, Col6_Pin, 1);
	
	
	
	
	
	Chip_IOCON_PinMuxSet(LPC_IOCON, Key_Row1_Port, Key_Row1_Pin, Key_Row1_IOCON );
	Chip_IOCON_PinMuxSet(LPC_IOCON, Key_Row2_Port, Key_Row2_Pin, Key_Row2_IOCON );
	Chip_IOCON_PinMuxSet(LPC_IOCON, Key_Row3_Port, Key_Row3_Pin, Key_Row3_IOCON );
	
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, Key_Row1_Port, Key_Row1_Pin);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, Key_Row2_Port, Key_Row2_Pin);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, Key_Row3_Port, Key_Row3_Pin);
	
//	DirectionIOPin(Key_Row1_Port, Key_Row1_Pin, 1);
//	DirectionIOPin(Key_Key_Row2_Port, Key_Row2_Pin, 1);
//	DirectionIOPin(Key_Row3_Port, Key_Row3_Pin, 1);
//	DirectionIOPin(Row4_Port, Row4_Pin, 1);
//// 	DirectionIOPin(Row5_Port, Row5_Pin, 1);
//// 	DirectionIOPin(Row6_Port, Row6_Pin, 1);


	Chip_GPIO_SetPinState(LPC_GPIO, Key_Row1_Port, Key_Row1_Pin	, true);
	Chip_GPIO_SetPinState(LPC_GPIO, Key_Row2_Port, Key_Row2_Pin	, true);
	Chip_GPIO_SetPinState(LPC_GPIO, Key_Row3_Port, Key_Row3_Pin	, true);
	
//	SetIOPin(Key_Row1_Port, Key_Row1_Pin);
//	SetIOPin(Key_Key_Row2_Port, Key_Row2_Pin);
//	SetIOPin(Key_Row3_Port, Key_Row3_Pin);
//	SetIOPin(Row4_Port, Row4_Pin);
// 	SetIOPin(Row5_Port, Row5_Pin);
// 	SetIOPin(Row6_Port, Row6_Pin);
	
	clearAllKey();
	
	timer_keyboard = osTimerCreate(osTimer(keyboardTimer), osTimerPeriodic, NULL);
	osTimerStart(timer_keyboard, 5);
	
}

//-----------------------------------------------------------------------
// 	  keyboardMain
// keyboard ana dongu fonksiyonudur
//-----------------------------------------------------------------------
void keyboardMain(void){
	
}
//-----------------------------------------------------------------------
// 	  keyboardTimer_CallBack
// keyboardTimer icin callback fonksiyonudur
//-----------------------------------------------------------------------
void keyboardTimer_CallBack(void const *arg){
	int i,k,m;
	uint64_t temp;
	uint8_t colTemp;
	keyScan();
	
	temp=0x0000000000000001;
	colTemp=0x01;
	k=0;
	m=0;
	for(i=0;i<TUS_ADEDI;i++){
		if(keyStatus.basilmisOlabilirReg & temp){
				//daha once tusa basilmis olabilir denmis.
			  if(keyStatus.basildiReg & temp){
					//tusa daha once basilmis denmis
					if(keyStatus.birakilmisOlabilirReg & temp){
							// tus birakilmis olabilir denmis 
							if(keyStatus.birakildi & temp){
								//tus zaten birakildigi icin herhangi birsey yapilmiyor
							}else{
								if(keyStatus.birakildiBounce[i]>=KEY_INT_TIME){
									keyStatus.birakildiBounce[i]-=KEY_INT_TIME;
								}else{
									keyStatus.birakildiBounce[i]=0;
								}
								if(keyStatus.birakildiBounce[i]==0){
									//bounce zamani dolmus fakat emin olunmasi icin highLevel'da kalacagi sure boyunca kontrol ediliyor
									if((keyStatus.colTempData[m] & colTemp)==0){
										if(keyStatus.highLevelTime[i]>=KEY_INT_TIME){
											keyStatus.highLevelTime[i]-=KEY_INT_TIME;
										}else{
											keyStatus.highLevelTime[i]=0;
										}
										if(keyStatus.highLevelTime[i]==0){
											//high levelda kalma suresi dolmus dolayisiyla tus gercekten birakilmis
											keyStatus.birakildi|=temp;	// tus gercekten birakildi
										}
									}else{
										//tus birakilmamis
										keyStatus.birakilmisOlabilirReg &= ~temp;
									}
								}
							}
					}else{
						if(keyStatus.colTempData[m] & colTemp){
							//tus hala basiliyor ise sureyi guncelle
							if(keyStatus.tusSuresi[i]<30000){
								keyStatus.tusSuresi[i]+=KEY_INT_TIME;
							}
						}else{
							// tus birakilmis olabilir. Bounce'u ve high level'i kur
							keyStatus.birakilmisOlabilirReg|=temp;	//tus birakilmis olabilir
							keyStatus.birakildiBounce[i]=BOUNCE_TIME;	//bounce suresi kuruldu
							keyStatus.highLevelTime[i]=HIGH_LEVEL_TIME; // bounce bittikten sonraki high'da kalma suresi
						}
					}
				}else{
					//tusa basilmis olabilir denmis ve bounce zamani dolup dolmadigi kontrol ediliyor
					if(keyStatus.islendi & temp){
						//tus islenmis ise birakilana kadar bekle
						if(keyStatus.birakilmisOlabilirReg & temp){
								// tus birakilmis olabilir denmis 
								if(keyStatus.birakildi & temp){
									//tus birakilmis herseyi sifirla
									keyStatus.basilmisOlabilirReg&=~temp;
									keyStatus.basildiReg&=~temp;
									keyStatus.birakilmisOlabilirReg&=~temp;
									keyStatus.birakildi&=~temp;
									keyStatus.islendi&=~temp;
									keyStatus.tusSuresi[i]=0;
									keyStatus.birakildiBounce[i]=0;
									keyStatus.basildiBounce[i]=0;
									keyStatus.highLevelTime[i]=0;
									keyStatus.lowLevelTime[i]=0;
								}else{
									if(keyStatus.birakildiBounce[i]>=KEY_INT_TIME){
										keyStatus.birakildiBounce[i]-=KEY_INT_TIME;
									}else{
										keyStatus.birakildiBounce[i]=0;
									}
									if(keyStatus.birakildiBounce[i]==0){
										//bounce zamani dolmus fakat emin olunmasi icin highLevel'da kalacagi sure boyunca kontrol ediliyor
										if((keyStatus.colTempData[m] & colTemp)==0){
											if(keyStatus.highLevelTime[i]>=KEY_INT_TIME){
												keyStatus.highLevelTime[i]-=KEY_INT_TIME;
											}else{
												keyStatus.highLevelTime[i]=0;
											}
											if(keyStatus.highLevelTime[i]==0){
												//high levelda kalma suresi dolmus dolayisiyla tus gercekten birakilmis
												keyStatus.basilmisOlabilirReg&=~temp;
												keyStatus.basildiReg&=~temp;
												keyStatus.birakilmisOlabilirReg&=~temp;
												keyStatus.birakildi&=~temp;
												keyStatus.islendi&=~temp;
												keyStatus.tusSuresi[i]=0;
												keyStatus.birakildiBounce[i]=0;
												keyStatus.basildiBounce[i]=0;
												keyStatus.highLevelTime[i]=0;
												keyStatus.lowLevelTime[i]=0;
											}
										}else{
											//tus birakilmamis
											keyStatus.birakilmisOlabilirReg &= ~temp;
										}
									}
								}
						}else{
							if(keyStatus.colTempData[m] & colTemp){
								//tus hala basiliyor ise sureyi guncelle
								if(keyStatus.tusSuresi[i]<30000){
									keyStatus.tusSuresi[i]+=KEY_INT_TIME;
								}
							}else{
								// tus birakilmis olabilir. Bounce'u ve high level'i kur
								keyStatus.birakilmisOlabilirReg|=temp;	//tus birakilmis olabilir
								keyStatus.birakildiBounce[i]=BOUNCE_TIME;	//bounce suresi kuruldu
								keyStatus.highLevelTime[i]=HIGH_LEVEL_TIME; // bounce bittikten sonraki high'da kalma suresi
							}
						}
					}else{
						if(keyStatus.basildiBounce[i]>=KEY_INT_TIME){
							keyStatus.basildiBounce[i]-=KEY_INT_TIME;
						}else{
							keyStatus.basildiBounce[i]=0;
						}
						if(keyStatus.basildiBounce[i]==0){
							//bounce zamani dolmus fakat emin olunmasi icin lowLevel'da kalacagi sure boyunca kontrol ediliyor
							if(keyStatus.colTempData[m] & colTemp){
								if(keyStatus.lowLevelTime[i]>=KEY_INT_TIME){
									keyStatus.lowLevelTime[i]-=KEY_INT_TIME;
								}else{
									keyStatus.lowLevelTime[i]=0;
								}
								if(keyStatus.lowLevelTime[i]==0){
									//low levelda kalma suresi dolmus dolayisiyla tusa gercekten basilmis 
									keyStatus.basildiReg|=temp;	// tusa gercekten basildi
									keyStatus.tusSuresi[i]=KEY_INT_TIME; // tus suresi guncellenmeli
									if(turnOff==false){
										setBuzzer(KEY_BUZZER_TIME);
									}
								}
							}else{
								//tus algilamasi hatali
								keyStatus.basilmisOlabilirReg&=~temp;
							}
						}
					}
				}
		}else{
			if(keyStatus.colTempData[m] & colTemp){
				keyStatus.basilmisOlabilirReg|=temp;	//tusa basilmis olabilir
				keyStatus.basildiBounce[i]=BOUNCE_TIME;	//bounce suresi kuruldu
				keyStatus.lowLevelTime[i]=LOW_LEVEL_TIME; // bounce bittikten sonraki sifirda kalma suresi
			}
		}
		
		temp<<=1;
		colTemp<<=1;
		k++;
		if(k>=COL_ADEDI){
			k=0;
			colTemp=0x01;
			m++;
			if(m>=COL_ADEDI){
				m=0;
			}
		}
	}
	
	if(buzzerTime>=KEY_INT_TIME){
		buzzerTime-=KEY_INT_TIME;
	}else{
		buzzerTime=0;
	}
	if(buzzerTime==0){
		clearBuzzer();
	}
}

//-----------------------------------------------------------------------
// 	  keyScan
// tus taramasi yapar
//-----------------------------------------------------------------------
void keyScan(void){
	
	Chip_GPIO_SetPinState(LPC_GPIO, Key_Row1_Port, Key_Row1_Pin	, false);
//	ClearIOPin(Key_Row1_Port, Key_Row1_Pin);
	keyStatus.colTempData[0]=checkCol();
	Chip_GPIO_SetPinState(LPC_GPIO, Key_Row1_Port, Key_Row1_Pin	, true);
//	SetIOPin(Key_Row1_Port, Key_Row1_Pin);
	
	Chip_GPIO_SetPinState(LPC_GPIO, Key_Row2_Port, Key_Row2_Pin	, false);
//	ClearIOPin(Key_Key_Row2_Port, Key_Row2_Pin);
	keyStatus.colTempData[1]=checkCol();
	Chip_GPIO_SetPinState(LPC_GPIO, Key_Row2_Port, Key_Row2_Pin	, true);
//	SetIOPin(Key_Key_Row2_Port, Key_Row2_Pin);
	
	Chip_GPIO_SetPinState(LPC_GPIO, Key_Row3_Port, Key_Row3_Pin	, false);
//	ClearIOPin(Key_Row3_Port, Key_Row3_Pin);
	keyStatus.colTempData[2]=checkCol();
	Chip_GPIO_SetPinState(LPC_GPIO, Key_Row3_Port, Key_Row3_Pin	, true);
//	SetIOPin(Key_Row3_Port, Key_Row3_Pin);
	
//	ClearIOPin(Row4_Port, Row4_Pin);
//	keyStatus.colTempData[3]=checkCol();
//	SetIOPin(Row4_Port, Row4_Pin);
	
// 	ClearIOPin(Row5_Port, Row5_Pin);
// 	keyStatus.colTempData[4]=checkCol();
// 	SetIOPin(Row5_Port, Row5_Pin);
// 	
// 	ClearIOPin(Row6_Port, Row6_Pin);
// 	keyStatus.colTempData[5]=checkCol();
// 	SetIOPin(Row6_Port, Row6_Pin);

}

//-----------------------------------------------------------------------
// 	  checkCol
// satirdan sifir verildiginde cagrilir. Sutunlardan sifirda olan var mi
// diye bakilir
//-----------------------------------------------------------------------
uint8_t checkCol(void){
	int i;
	uint8_t result=0;
	for(i=0;i<200;i++);
	
	if(Chip_GPIO_GetPinState(LPC_GPIO, Key_Col1_Port, Key_Col1_Pin)==0){
		result|=0x01;
	}
//	if(GetIOPin(Key_Col1_Port, Key_Col1_Pin)==0){
//		result|=0x01;
//	}
	
	if(Chip_GPIO_GetPinState(LPC_GPIO, Key_Col2_Port, Key_Col2_Pin)==0){
		result|=0x02;
	}
//	if(GetIOPin(Key_Col2_Port, Key_Col2_Pin)==0){
//		result|=0x02;
//	}
	
//	if(Chip_GPIO_GetPinState(LPC_GPIO, Col3_Port, Col3_Pin)==0){
//		result|=0x04;
//	}
//	if(GetIOPin(Col3_Port, Col3_Pin)==0){
//		result|=0x04;
//	}
//	if(GetIOPin(Col4_Port, Col4_Pin)==0){
//		result|=0x08;
//	}
// 	if(GetIOPin(Col5_Port, Col5_Pin)==0){
// 		result|=0x10;
// 	}
// 	if(GetIOPin(Col6_Port, Col6_Pin)==0){
// 		result|=0x20;
// 	}
	return result;
}


//-----------------------------------------------------------------------
// 	  getKey
// siradaki basilan tus verisi ile doner
// 
//-----------------------------------------------------------------------
uint8_t getKey(void){
	uint64_t temp;
	uint8_t i;
	if(keyStatus.basildiReg){
		temp=0x0000000000000001;
		if(turnOff){
			for(i=1;i<=TUS_ADEDI;i++){
				if(keyStatus.basildiReg&temp){
					if(i==OnOffKey){
						return i;
					}
				}
				if(keyStatus.birakildi & temp){
					keyIslendi(i);
				}
				temp<<=1;
			}
		}else{
			for(i=1;i<=TUS_ADEDI;i++){
				if(keyStatus.basildiReg&temp){
					return i;
				}
				temp<<=1;
			}
		}
	}
// 	if(keyStatus.basildiReg){
// 		temp=0x0000000000000001;
// 		for(i=1;i<=TUS_ADEDI;i++){
// 			if(keyStatus.basildiReg&temp){
// 				return i;
// 			}
// 			temp<<=1;
// 		}
// 	}
	return 0;
}

//-----------------------------------------------------------------------
// 	  getKeyRelease
// tus nosu verilen tusun birakilip birakilip birakilmadigi ile doner
// 
//-----------------------------------------------------------------------
uint8_t getKeyRelease(uint8_t keyCode){
	uint64_t temp;
	temp=0x0000000000000001;
	temp<<=(keyCode-1);
	if(keyStatus.birakildi & temp){
		return 1;
	}
	return 0;
}

//-----------------------------------------------------------------------
// 	  getKeyTime
// tus nosu verilen tusun basilma suresi ile doner
// 
//-----------------------------------------------------------------------
uint16_t getKeyTime(uint8_t keyCode){
	uint64_t temp;
	temp=0x0000000000000001;
	temp<<=(keyCode-1);
	if(keyStatus.basildiReg & temp){
		return keyStatus.tusSuresi[keyCode-1];
	}else{
		return 0;
	}
	
}

//-----------------------------------------------------------------------
// 	  keyIslendi
// tus nosu verilen tusun islendigi bu fonksiyonla belirtilir
// 
//-----------------------------------------------------------------------
void keyIslendi(uint8_t keyCode){
	uint64_t temp;
	temp=0x0000000000000001;
	temp<<=(keyCode-1);
	keyStatus.islendi|=temp;
	keyStatus.basildiReg&=~temp;
}
//-----------------------------------------------------------------------
// 	  clearAllKey
// o zamana kadar tum basilma islemlerini sifirlar
// 
//-----------------------------------------------------------------------
void clearAllKey(void){
	uint8_t i;
	
	keyStatus.basilmisOlabilirReg=0;
	keyStatus.basildiReg=0;
	keyStatus.birakilmisOlabilirReg=0;
	keyStatus.birakildi=0;
	keyStatus.islendi=0;
	
	for(i=0; i<TUS_ADEDI; i++){
		keyStatus.basildiBounce[i]=0;
		keyStatus.birakildiBounce[i]=0;
		keyStatus.tusSuresi[i]=0;
		keyStatus.lowLevelTime[i]=0;
		keyStatus.highLevelTime[i]=0;
	}
}

//-----------------------------------------------------------------------
// 	  buzzerInit
// 
//-----------------------------------------------------------------------
void buzzerInit(void){
	Chip_IOCON_PinMuxSet(LPC_IOCON, Buzzer_Port, Buzzer_Pin, Buzzer_IOCON );
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, Buzzer_Port, Buzzer_Pin);
	Chip_GPIO_SetPinState(LPC_GPIO, Buzzer_Port, Buzzer_Pin	, false);
//	DirectionIOPin(Buzzer_Port, Buzzer_Pin, 1);
//	ClearIOPin(Buzzer_Port, Buzzer_Pin);
}

//-----------------------------------------------------------------------
// 	  setBuzzer
// 
//-----------------------------------------------------------------------
void setBuzzer(uint16_t ms){
	osMutexWait(buzzer_mutex_id, osWaitForever );
	Chip_GPIO_SetPinState(LPC_GPIO, Buzzer_Port, Buzzer_Pin	, true);
//	SetIOPin(Buzzer_Port, Buzzer_Pin);
	buzzerTime=ms;
	osMutexRelease(buzzer_mutex_id);
}

//-----------------------------------------------------------------------
// 	  setBuzzerSeccondFunction
// 
//-----------------------------------------------------------------------
void setBuzzerSecondFunction(void){
	setBuzzer(KEY_BUZZER_TIME);
	osDelay(100);
	setBuzzer(KEY_BUZZER_TIME);
	osDelay(100);
}
//-----------------------------------------------------------------------
// 	  setBuzzerError
// 
//-----------------------------------------------------------------------
void setBuzzerError(void){
	setBuzzer(1000);
}

//-----------------------------------------------------------------------
// 	  clearBuzzer
// 
//-----------------------------------------------------------------------
void clearBuzzer(void){
//	ClearIOPin(Buzzer_Port, Buzzer_Pin);
	Chip_GPIO_SetPinState(LPC_GPIO, Buzzer_Port, Buzzer_Pin	, false);
	buzzerTime=0;
}

//-----------------------------------------------------------------------
// 	  deviceTurnOff
// 
//-----------------------------------------------------------------------
void deviceTurnOff(uint8_t onOffKey){
	OnOffKey=onOffKey;
	turnOff=true;
}

//-----------------------------------------------------------------------
// 	  deviceTurnOn
// 
//-----------------------------------------------------------------------
void deviceTurnOn(void){
	turnOff=false;
}

//-----------------------------------------------------------------------
// 	  deviceTurnOn
// 
//-----------------------------------------------------------------------
void keyboardPinSleep(void){
	Chip_IOCON_PinMuxSet(LPC_IOCON, Key_Row1_Port, Key_Row1_Pin, Key_Row1_Sleep_IOCON );
	Chip_IOCON_PinMuxSet(LPC_IOCON, Key_Row2_Port, Key_Row2_Pin, Key_Row2_Sleep_IOCON );
	Chip_IOCON_PinMuxSet(LPC_IOCON, Key_Row3_Port, Key_Row3_Pin, Key_Row3_Sleep_IOCON );
	
	Chip_IOCON_PinMuxSet(LPC_IOCON, Key_Col1_Port, Key_Col1_Pin, Key_Col1_Sleep_IOCON);
	Chip_IOCON_PinMuxSet(LPC_IOCON, Key_Col2_Port, Key_Col2_Pin, Key_Col2_Sleep_IOCON);
//	Chip_IOCON_PinMuxSet(LPC_IOCON, Col3_Port, Col3_Pin, IOCON_FUNC0 | IOCON_MODE_INACT );
	
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, Key_Col1_Port, Key_Col1_Pin);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, Key_Col2_Port, Key_Col2_Pin);
//	Chip_GPIO_SetPinDIROutput(LPC_GPIO, Col3_Port, Col3_Pin);
	
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, Key_Row1_Port, Key_Row1_Pin);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, Key_Row2_Port, Key_Row2_Pin);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, Key_Row3_Port, Key_Row3_Pin);
	
	Chip_GPIO_SetPinState(LPC_GPIO, Key_Row1_Port, Key_Row1_Pin	, false);
	Chip_GPIO_SetPinState(LPC_GPIO, Key_Row2_Port, Key_Row2_Pin	, false);
	Chip_GPIO_SetPinState(LPC_GPIO, Key_Row3_Port, Key_Row3_Pin	, false);
	
	Chip_GPIO_SetPinState(LPC_GPIO, Key_Col1_Port, Key_Col1_Pin	, false);
	Chip_GPIO_SetPinState(LPC_GPIO, Key_Col2_Port, Key_Col2_Pin	, false);
//	Chip_GPIO_SetPinState(LPC_GPIO, Col3_Port, Col3_Pin	, false);
	
	Chip_IOCON_PinMuxSet(LPC_IOCON, Buzzer_Port, Buzzer_Pin, Buzzer_Sleep_IOCON );
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, Buzzer_Port, Buzzer_Pin);
	Chip_GPIO_SetPinState(LPC_GPIO, Buzzer_Port, Buzzer_Pin	, false);
	
	
}

