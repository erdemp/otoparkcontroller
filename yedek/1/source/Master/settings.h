#ifndef SETTINGS_H_
#define SETTINGS_H_

//#include "lpc_types.h"
//#include "lpc17xx_timer.h"
#include "internalFlash.h"
#include <stdlib.h>
#include "cmsis_os.h"
#include "chip.h"
#include <string.h>
#include "device.h"
#include "sensorDriver.h"
#include "bsp.h"
#include "Ethernet.h"

#ifdef __cplusplus
extern "C"
{
#endif

//typedef struct{
//	uint8_t deviceAddress;	//sensor'in adresi
//	devType device;					//cihaz turu: sensor 
//	uint8_t GrupNo;
//	uint8_t LEDGrubu;
//	bool LEDbagli;
//	uint8_t	workingMode;		// sensorun calisma modu: sensor mod ise 0, merkez mod ise 1
//	uint16_t	LimitVal;
//}settingDeviceSensor_Type;


	
//*******************************************************************
//Cihaz versiyonu
#define REVISION		"1.0.0"
//*******************************************************************
	
#define 	ONEDEVICEDATARECORDSIZE		12
#define 	GATEWAYIPRECORDSIZE				4
#define 	LOCALIPRECORDSIZE					4
#define		SUBNETMASKRECORDSIZE			4
#define 	PORTLISTENRECORDSIZE			2


#define FLASH_DEFAULT_READY_DATA	0x01020304

#define FLASH_DATA_READY	0				//flash mass storage alanin basi
#define FLASH_DATA_READY_SIZE	4

#define ETHERNETPARAMETERS										(FLASH_DATA_READY + FLASH_DATA_READY_SIZE)
#define ETHERNETPARAMETERS_SIZE								 512

#define CONTROLLERCOMMANDS										(ETHERNETPARAMETERS + ETHERNETPARAMETERS_SIZE)				
#define CONTROLLERCOMMANDS_SIZE								6144
//#define SENSORGRUPREGPARAMETERS										(ETHERNETPARAMETERS + ETHERNETPARAMETERS_SIZE)
//#define SENSORGRUPREGPARAMETERS_SIZE								 512

//#define DISPLAYKOMUTREGPARAMETERS										(SENSORGRUPREGPARAMETERS + SENSORGRUPREGPARAMETERS_SIZE)
//#define DISPLAYKOMUTREGPARAMETERS_SIZE								 512

//#define	DEVICEREGPARAMETERS											(DISPLAYKOMUTREGPARAMETERS + DISPLAYKOMUTREGPARAMETERS_SIZE)
//#define DEVICEREGPARAMETERS_SIZE								6144

#define ETHERNETRECORDSIZE											30
/*
uint8_t deviceAddress;	//sensor'in adresi
		devType device;					//cihaz turu: sensor 
		uint8_t GrupNo;
		uint8_t LEDGrubu;
		bool LEDbagli;
		uint8_t	workingMode;		// sensorun calisma modu: sensor mod ise 0, merkez mod ise 1
		uint16_t	LimitVal;
*/
	
/*		uint8_t deviceAddress;	//display'in adresi
		devType device;					//cihaz turu: display		
		uint8_t firstChar;
		bool	inverted;
		uint32_t HaberlesmeGecikmesi;
		uint8_t	Parlaklik;
*/		
	
bool SaveDeviceRegsToRAM (void);
void SaveDataReadyDataToRAM (void);
void SaveSensorGrupRegsToRAM (void) ;
void SaveDisplayCommandRegsToRAM (void);
void SaveEthernetDataToRAM (void);
void SaveAllParameterToFlash (void);
void flashDiagnostic(void);
void loadDefaultValues (void);
void getDataFromFlash(void);
void getDeviceRegsFromFlash (void);
void getLastRecordFromFlashToRAM (void);
bool addControllerCommandsToRAM (uint8_t *cmdPtr,  uint32_t size);
uint8_t * getCmdBuffStartAdress (void);
uint8_t * getCmdBuffEndAdress (void);
#ifdef __cplusplus
}
#endif

#endif /* SETTINGS_H_ */
