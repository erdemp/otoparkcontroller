
#include "settings.h"


/*******************************************************************************
* @brief				Device REgisterlarin icerisinden 	ONEDEVICEDATARECORDSIZE  kadarini RAM buff a yazar
*								
* @param[in]    None
* @return				None
*******************************************************************************/
//bool SaveDeviceRegsToRAM (void) {
//uint16_t i,k;
//uint32_t adress;
//uint8_t *ptr;
////sensorType *sensor;
////displayType *display;	
//deviceType *portDev;;
////uint8_t tempArray[ONEDEVICEDATARECORDSIZE];
//	
//	adress = DEVICEREGPARAMETERS;

//	for (k=0;k<4;k++) {
//		if (k==0 ) {
//			portDev=port1Devices;
//		}else if (k==1) {
//			portDev=port2Devices;
//		}else if (k==2) {
//			portDev=port3Devices;
//		}else {
//			portDev=port4Devices;
//		}
//		
//		for (i=0;i<MAX_PORT_DEVICE_CNT;i++) {
//		
//			if ( portDev[i].device == devDISPLAY || portDev[i].device == devSENSOR ) {
////				portDev[i].idiotBuff[0] = portDev[i].device;
////				portDev[i].idiotBuff[1] = portDev[i].deviceAddress;
//				ptr = (uint8_t *)&(portDev[i]);
//				//ptr += 2;
//				ptr++;   // device adresten itibaren kayit yap. maximum data record size kadar. Normalde pc bu kadar data gondermiyor ama sonrasi i�in bos birakmistik.
//		//		device tipini de flash a kaydetmeliyiz yoksa anlamiyor. her yeri kontrol et.
//			//	device tipini yazmiyor. sonrada a�ildiktan sonra okuyamiyor.
//				if ( SaveToRAMbuff(ptr,adress,ONEDEVICEDATARECORDSIZE) == false ) {
//							 __nop();
//				}
//			}
//			adress += ONEDEVICEDATARECORDSIZE;
//			
//		}
//	}
//	return true;
//	
//}

/*******************************************************************************
* @brief			Flash in basina konulan (flash ta data var anlamindaki) data ready bilgisini	
*								flash in RAM deki karsiligi i�ine koyar.
* @param[in]    None
* @return				None
*******************************************************************************/
void SaveDataReadyDataToRAM (void) {
uint32_t data;
//uint8_t *ptr;	
	
	data = FLASH_DEFAULT_READY_DATA;
	//ptr = (uint8_t *)data;
	if ( SaveToRAMbuff((uint8_t *)&data,FLASH_DATA_READY,FLASH_DATA_READY_SIZE) == false ) {
				 __nop();
	}
	
}

/*******************************************************************************
* @brief				Sensor Grup REgisterlarin icerisinden 	ONEDEVICEDATARECORDSIZE  kadarini RAM buff a yazar
*								
* @param[in]    None
* @return				None
*******************************************************************************/
//void SaveSensorGrupRegsToRAM (void) {
//uint16_t i,k;
//uint32_t adress;
//uint8_t *ptr;
////sensorType *sensor;
////displayType *display;	
////deviceType *portDev;
//sensGrupReg *sensorGrupPort;

//	
//	adress = SENSORGRUPREGPARAMETERS;

//	for (k=0;k<4;k++) {
//		if (k==0 ) {
//			sensorGrupPort = sensorGrupRegPort1;
//		}else if (k==1) {
//			sensorGrupPort = sensorGrupRegPort2;
//		}else if (k==2) {
//			sensorGrupPort = sensorGrupRegPort3;
//		}else {
//			sensorGrupPort = sensorGrupRegPort4;
//		}
//		
//		for (i=0;i<SENSORGRUPSAYISI;i++) {
//			
//			ptr = (uint8_t *)&(sensorGrupPort[i]);
//			if ( SaveToRAMbuff(ptr,adress,sizeof(sensorGrupPort[i])) == false ) {
//						 __nop();
//			}
//			adress += sizeof(sensorGrupPort[i]);
//			
//		}
//	}
//	
//}



/*******************************************************************************
* @brief				Sensor Grup REgisterlarin icerisinden 	ONEDEVICEDATARECORDSIZE  kadarini RAM buff a yazar
*								
* @param[in]    None
* @return				None
*******************************************************************************/
//void SaveDisplayCommandRegsToRAM (void) {
//uint16_t i,k;
//uint32_t adress;
//uint8_t *ptr;
////sensorType *sensor;
////displayType *display;	
////deviceType *portDev;
//dispCommandReg *dispCommandRegPort;


//	adress = DISPLAYKOMUTREGPARAMETERS;

//	for (k=0;k<4;k++) {
//		if (k==0 ) {
//			dispCommandRegPort = displayCommandRegPort1;
//		}else if (k==1) {
//			dispCommandRegPort = displayCommandRegPort2;
//		}else if (k==2) {
//			dispCommandRegPort = displayCommandRegPort3;
//		}else {
//			dispCommandRegPort = displayCommandRegPort4;
//		}
//		
//		for (i=0;i<DISPLAYCOMMANDREGSAYISI;i++) {
//			
//			ptr = (uint8_t *)&(dispCommandRegPort[i]);
//			if ( SaveToRAMbuff(ptr,adress,sizeof(dispCommandRegPort[i].adres)) == false ) {
//						 __nop();
//			}
//			adress += sizeof(dispCommandRegPort[i]);
//			
//		}
//	}
//	
//}

/*******************************************************************************
* @brief				PC den kontroller komutlari geldigi zaman bunlari FLASH a yazmak �zere RAM area ya yazar.  
*								
* @param[in]    None
* @return				None
*******************************************************************************/
bool addControllerCommandsToRAM (uint8_t *cmdPtr,  uint32_t size) {
uint32_t emptyAdress, emptySize;

	
	emptyAdress=0;
	if ( getEmptyAdressForCommandFromRAMbuff (&emptyAdress,&emptySize,CONTROLLERCOMMANDS,0) == false ) {
		return false;
	}
	
	if ( size > emptySize ) {
		return false;
	}

	if ( SaveToRAMbuff(cmdPtr,emptyAdress,size) == false ) {
				 __nop();
		return false;
	}	
	
	return true;
	
}

/*******************************************************************************
* @brief				parametrlerin tutuldugu RAM alaninin baslangic adresini d�nd�r�r.  
*								
* @param[in]    None
* @return				None
*******************************************************************************/
uint8_t * getCmdBuffStartAdress (void) {
	

 return 	getparametersRAMStartAdress()+ CONTROLLERCOMMANDS;

}
/*******************************************************************************
* @brief				PC den kontroller komutlari geldigi zaman bunlari FLASH a yazmak �zere RAM area ya yazar.  
*								
* @param[in]    None
* @return				None
*******************************************************************************/
uint8_t * getCmdBuffEndAdress (void) {
	

 return 	getparametersRAMStartAdress()+ CONTROLLERCOMMANDS + CONTROLLERCOMMANDS_SIZE;

}

///*******************************************************************************
//* @brief				PC den kontroller komutlari geldigi zaman bunlari FLASH a yazmak �zere RAM area ya yazar.  
//*								
//* @param[in]    None
//* @return				None
//*******************************************************************************/
//bool clearCommands (void) {
//uint16_t i,k;
//uint32_t emptyAdress, emptySize;
//uint8_t *ptr;
//uint32_t lenght;
//uint8_t packageOK=0;
////sensorType *sensor;
////displayType *display;	
////deviceType *portDev;
//dispCommandReg *dispCommandRegPort;

////	if (cmdPtr[0] != STX ) {
////		return false;
////	}
////	
////	for (i=0;i<maxLength; i++) {
////		if (i==ETX) {
////			packageOK=1;
////			break;
////		}
////	}
////	
////	if (packageOK == 0 ) {
////		return false;
////	}


//	 for (i=0;i<CONTROLLERCOMMANDS_SIZE;i++) {
//		 eraseCommandsRamArea(CONTROLLERCOMMANDS+i,0);
//	 }

//	
//	
//	return true;
//	
//}

/*******************************************************************************
* @brief				Ethernet parametrelerini RAM buff a yazar. (RAM buff sonra flash a yazilir)
*								
* @param[in]    None
* @return				None
*******************************************************************************/
void SaveEthernetDataToRAM (void) {
uint32_t adres;	
	

	adres = ETHERNETPARAMETERS;	
	if ( SaveToRAMbuff((uint8_t *)ETH.LocalIP,adres,LOCALIPRECORDSIZE) == false ) {
				 __nop();
	}
	
	adres += LOCALIPRECORDSIZE;
	if ( SaveToRAMbuff((uint8_t *)ETH.GatewayIP,adres,GATEWAYIPRECORDSIZE) == false ) {
				 __nop();
	}	
	
	adres += GATEWAYIPRECORDSIZE;
	if ( SaveToRAMbuff((uint8_t *)ETH.SubnetMaskAdress,adres,SUBNETMASKRECORDSIZE) == false ) {
				 __nop();
	}
	
	adres += SUBNETMASKRECORDSIZE;
	if ( SaveToRAMbuff((uint8_t *)&ETH.Port_PCPortNo,adres,PORTLISTENRECORDSIZE) == false ) {
				 __nop();
	}	
	
	adres += PORTLISTENRECORDSIZE;
	if ( SaveToRAMbuff((uint8_t *)&ETH.Port_SistemPortNo,adres,PORTLISTENRECORDSIZE) == false ) {
				 __nop();
	}		
	
	
}

/*******************************************************************************
* @brief				Ethernet parametrelerini RAM buff a yazar. (RAM buff sonra flash a yazilir)
*								
* @param[in]    None
* @return				None
*******************************************************************************/
void SaveAllParameterToFlash (void) {
	
	SaveDataReadyDataToRAM ();
	SaveEthernetDataToRAM();
	
//	SaveDeviceRegsToRAM ();
//  SaveSensorGrupRegsToRAM () ;
//  SaveDisplayCommandRegsToRAM ();
	
	__disable_irq();	
	while( flashWrite()==0);
	__enable_irq();
	
	
}



/*******************************************************************************
* @brief			flashDiagnostic  - A�ilista parametrelerin tutuldugu flash i kontrol eder. 	
*								Eger crc si tutan anlamli bir data yoksa. backup taki (tabi backup taki datalar anlamli ve crc si tutuyor ise)
*								datalari parametre bolgesine yazar. 
* @param[in]    None
* @return				None
*******************************************************************************/
void flashDiagnostic(void){
	
	bool SettingFlashOK=true;
//	bool MixerFeederFlashOK=true;
	
	__disable_irq();
	//flashInit(FlashSettings);

	getLastRecordFromFlashToRAM();
	selectFlashArea(FLASH_PARAMETER_AREA);
	
	if((compareFlash(FLASH_DATA_READY, FLASH_DEFAULT_READY_DATA, FLASH_DATA_READY_SIZE) == false) || (checkFlashCRC() == false)){
		
		getDataFromFlashToRAM(FLASH2_START_ADDRESS);
		selectFlashArea(FLASH_BACKUP_AREA);
		//flashInit(FlashBackup);
		if(compareFlash(FLASH_DATA_READY, FLASH_DEFAULT_READY_DATA, FLASH_DATA_READY_SIZE) && checkFlashCRC()){
			
			while(1){
				selectFlashArea(FLASH_PARAMETER_AREA);
				flashInit(FlashSettings);
				selectFlashArea(FLASH_PARAMETER_AREA);
				if(restoreFlashBackup()){
					if(compareFlash(FLASH_DATA_READY, FLASH_DEFAULT_READY_DATA, FLASH_DATA_READY_SIZE) && checkFlashCRC()){
						break;
					}
				}
			}
			selectFlashArea(FLASH_PARAMETER_AREA);	
			getLastRecordFromFlashToRAM();
			//flashInit(FlashSettings);
			SettingFlashOK=true;
		}else{
			SettingFlashOK=false;
		}
	}
	

	
	if(!SettingFlashOK){
		eraseFlashRamArea();
		selectFlashArea(FLASH_PARAMETER_AREA);	
		flashInit(FlashSettings);
		loadDefaultValues(); //degisebilir. Kullanici bilsin diye ona sorulacak
		flashInit(FlashSettings);
	}
	

	__enable_irq();
	
}

/*******************************************************************************
* @brief		getLastRecordToRAM
*								
*								
* @param[in]    None
* @return				None
*******************************************************************************/
void getLastRecordFromFlashToRAM (void) {
uint32_t freeflashadress;	

	
 freeflashadress = getLastRecordStartAdress();

	getDataFromFlashToRAM(freeflashadress);
			

		 //ptr=(uint8_t *)FLASH_START_ADDRESS;
		 



 
 
	
}
/*******************************************************************************
* @brief		loadDefaultValues		
*								
*								
* @param[in]    None
* @return				None
*******************************************************************************/
void loadDefaultValues (void) {
	
	eraseFlashRamArea();
	ETH_GetDefaultParameters(&ETH);
//	ClearSensorGrupRegs();
//	ClearDisplayCommandRegs();
//	clearDeviceRegs();
	
	SaveAllParameterToFlash();
	
		
}


	
//-----------------------------------------------------------------------
// 	  getFlashData
// flashtan yeri belirtilen yerdeki veriyi alir
//-----------------------------------------------------------------------
void getFlashData(uint8_t *ptr, uint32_t flashAddress, uint16_t size){
	uint16_t i;
	for(i=0; i<size; i++){
		ptr[i] = GetData(flashAddress+i);
	}
}

/*******************************************************************************
* @brief				Ethernet parametrelerini RAM buffer dan okur.
*								
* @param[in]    None
* @return				None
*******************************************************************************/
void getEthernetDataFromFlash (void) {
uint32_t adres;	
	
//	burada flash tan oku..
//	getFlashData((uint8_t *)&ETH.LocalIP, adres, ETHERNETRECORDSIZE);
	
	
	adres = ETHERNETPARAMETERS;	
	getFlashData((uint8_t *)&ETH.LocalIP, adres, LOCALIPRECORDSIZE);
	
	adres += LOCALIPRECORDSIZE;	
	getFlashData((uint8_t *)&ETH.GatewayIP, adres, GATEWAYIPRECORDSIZE);

	
	adres += GATEWAYIPRECORDSIZE;
	getFlashData((uint8_t *)&ETH.SubnetMaskAdress, adres, SUBNETMASKRECORDSIZE);

	
	adres += SUBNETMASKRECORDSIZE;
	getFlashData((uint8_t *)&ETH.Port_PCPortNo, adres, PORTLISTENRECORDSIZE);
	
	adres += PORTLISTENRECORDSIZE;
	getFlashData((uint8_t *)&ETH.Port_SistemPortNo, adres, PORTLISTENRECORDSIZE);	
	
	
}


///*******************************************************************************
//* @brief				Device Reg parametrelerini RAM buffer dan okur
//*								
//* @param[in]    None
//* @return				None
//*******************************************************************************/
//void getDeviceRegsFromFlash (void) {
//uint16_t i,k,m;
//uint32_t adress;
//uint8_t *ptr;
////displayType *display;	
//deviceType *portDev;
////sensorType *sensor;
//uint8_t *devCnt;	
//uint8_t buff[ONEDEVICEDATARECORDSIZE+2];
////		getFlashData((uint8_t *)&ETH.GatewayIP, ETHERNETPARAMETERS, ETHERNETRECORDSIZE);

//	
//	memset (buff,0,ONEDEVICEDATARECORDSIZE+2);
//	
//	adress = DEVICEREGPARAMETERS;

//	for (k=0;k<4;k++) {
//		if (k==0 ) {
//			portDev=port1Devices;
//			devCnt=&port1DeviceCount;
//		}else if (k==1) {
//			portDev=port2Devices;
//			devCnt=&port2DeviceCount;
//		}else if (k==2) {
//			portDev=port3Devices;
//			devCnt=&port3DeviceCount;
//		}else {
//			portDev=port4Devices;
//			devCnt=&port4DeviceCount;
//		}
//		
//		for (i=0;i<MAX_PORT_DEVICE_CNT;i++) {
//			
//			memset (buff,0,ONEDEVICEDATARECORDSIZE+2);
//			getFlashData(buff, adress, ONEDEVICEDATARECORDSIZE);
//			adress += ONEDEVICEDATARECORDSIZE;
//			
//			if ( buff[0] == devDISPLAY || buff[0] == devSENSOR) {
//				  portDev[i].deviceAddress=buff[1];
//				  portDev[i].device = buff[0];
//				  ptr = (uint8_t *)&(portDev[i].idiotBuff);
//				  //ptr++;	// ilk data bos. deviceadress ten basla
//				  for (m=0;m<ONEDEVICEDATARECORDSIZE-1;m++) {
//						ptr[m]=buff[m+1];
//					}

//					if ( portDev[i].device ==  devSENSOR || portDev[i].device == devDISPLAY ) {
//						(*devCnt)++;
//					}
//		  }
//			
//		}
//	}
//}


///*******************************************************************************
//* @brief				getDisplayCommandRegsFromFlash
//*								
//* @param[in]    None
//* @return				None
//*******************************************************************************/
//void getDisplayCommandRegsFromFlash (void) {
//uint16_t i,k;
//uint32_t adress;
//uint8_t *ptr;
////sensorType *sensor;
////displayType *display;	
////deviceType *portDev;
//dispCommandReg *dispCommandRegPort;


//	adress = DISPLAYKOMUTREGPARAMETERS;

//	for (k=0;k<4;k++) {
//		if (k==0 ) {
//			dispCommandRegPort = displayCommandRegPort1;
//		}else if (k==1) {
//			dispCommandRegPort = displayCommandRegPort2;
//		}else if (k==2) {
//			dispCommandRegPort = displayCommandRegPort3;
//		}else {
//			dispCommandRegPort = displayCommandRegPort4;
//		}
//		
//		for (i=0;i<DISPLAYCOMMANDREGSAYISI;i++) {
//			
//			ptr = (uint8_t *)&(dispCommandRegPort[i]);
//			getFlashData(ptr, adress, sizeof(dispCommandRegPort[i].adres));
////			if ( SaveToRAMbuff(ptr,adress,sizeof(dispCommandRegPort[i])) == false ) {
////						 __nop();
////			}
//			adress += sizeof(dispCommandRegPort[i]);
//			
//		}
//	}
//	
//}

///*******************************************************************************
//* @brief				getSensorGrupRegsFromFlash
//*								
//* @param[in]    None
//* @return				None
//*******************************************************************************/
//void getSensorGrupRegsFromFlash (void) {
//uint16_t i,k;
//uint32_t adress;
//uint8_t *ptr;
////sensorType *sensor;
////displayType *display;	
////deviceType *portDev;
//sensGrupReg *sensorGrupPort;

//	
//	adress = SENSORGRUPREGPARAMETERS;

//	for (k=0;k<4;k++) {
//		if (k==0 ) {
//			sensorGrupPort = sensorGrupRegPort1;
//		}else if (k==1) {
//			sensorGrupPort = sensorGrupRegPort2;
//		}else if (k==2) {
//			sensorGrupPort = sensorGrupRegPort3;
//		}else {
//			sensorGrupPort = sensorGrupRegPort4;
//		}
//		
//		for (i=0;i<SENSORGRUPSAYISI;i++) {
//			
//			ptr = (uint8_t *)&(sensorGrupPort[i]);
//			getFlashData(ptr, adress, sizeof(sensorGrupPort[i]));
////			if ( SaveToRAMbuff(ptr,adress,sizeof(sensorGrupPort[i])) == false ) {
////						 __nop();
////			}
//			adress += sizeof(sensorGrupPort[i]);
//			
//		}
//	}
//	
//}

/*******************************************************************************
* @brief				Bu fonksiyon flashtan ayarlari alir ram'deki karsiliklarina atar
*								ayni zamanda ramdeki karsiliklarinin ozelliklerini de tanimlar
* @param[in]    None
* @return				None
*******************************************************************************/
void getDataFromFlash(void){
	
//	uint32_t boolCnt=0;
//	uint32_t listCnt=0;
//	uint32_t numberCnt=0;
//	uint32_t menuNumber=0;
//	uint32_t clockCnt=0;
	//uint32_t StrCnt=0;


	__disable_irq();
	//flashInit(FlashSettings);
		getLastRecordFromFlashToRAM();
	__enable_irq();
	
	getEthernetDataFromFlash();
//	getDeviceRegsFromFlash();
//	getSensorGrupRegsFromFlash();
//	getDisplayCommandRegsFromFlash();
	
	
}







