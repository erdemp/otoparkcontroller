
#ifndef SENSORDRIVER_H_
#define SENSORDRIVER_H_


#include "cmsis_os.h"
#include "chip.h"
#include "modbus.h"

#ifdef __cplusplus
extern "C"
{
#endif

	typedef enum {
		sensorMod_Sensor = 0,
		sensorMod_Merkez = 1
	}sensorModType;

	
	

	bool sensorLimitDegerDegistir(LPC_USART_T *port, uint8_t adres, uint16_t limitDeger);
	bool sensorLed_R_Yak(LPC_USART_T *port, uint8_t adres, uint8_t dataR);
	bool sensorLed_G_Yak(LPC_USART_T *port, uint8_t adres, uint8_t dataG);
	bool sensorLed_B_Yak(LPC_USART_T *port, uint8_t adres, uint8_t dataB);
	bool sensorFiltreDegeriDegistir(LPC_USART_T *port, uint8_t adres, uint8_t dataFiltre);
	bool sensorDurum(LPC_USART_T *port, uint8_t adres, uint8_t *durum);
	bool sensorReset(LPC_USART_T *port, uint8_t adres);
	bool sensorMesafeOku(LPC_USART_T *port, uint8_t adres, uint16_t *mesafe);
	bool sensorKilitAc(LPC_USART_T *port, uint8_t adres, bool enable);
	bool sensorUretimSayacYaz(LPC_USART_T *port, uint8_t adres, uint16_t dataSayac);
	bool sensorUretimSayacOku(LPC_USART_T *port, uint8_t adres, uint16_t *dataSayac);
	bool sensorFabrikaAyarlarinaDon(LPC_USART_T *port, uint8_t adres);
	bool sensorAdresDegistir(LPC_USART_T *port, uint8_t adres, uint8_t yeniAdres);
	bool sensorHabAyarDegistir(LPC_USART_T *port, uint8_t adres, haberlesmeAyarType data);
	bool sensorBaudrateDegistir(LPC_USART_T *port, uint8_t adres, uint32_t baudrate);
	bool sensorEsikDegerDegistir(LPC_USART_T *port, uint8_t adres, uint16_t esik1, uint16_t esik2, uint16_t esik3, uint16_t esik4, uint16_t esik5, uint16_t esik6);
	bool sensorSinyalAdet(LPC_USART_T *port, uint8_t adres, uint8_t data);
	bool sensorIkiClockModu(LPC_USART_T *port, uint8_t adres, bool enable);
	bool sensorIkiClockDeltaV_Yaz(LPC_USART_T *port, uint8_t adres, uint16_t deltaV);
	bool sensorModuOku(LPC_USART_T *port, uint8_t adres, sensorModType *sensorMod);
	bool sensorLimitDegerOku(LPC_USART_T *port, uint8_t adres, uint16_t *limitDeger);
	bool sensorModuDegistir(LPC_USART_T *port, uint8_t adres, uint8_t sensorMod);

#ifdef __cplusplus
}
#endif

#endif /* SENSORDRIVER_H_ */
