
#include "delays.h"

// micro saniye delayler icin timer
//#define USEC_DELAY_TIM	LPC_TIM0


 osMutexDef (delayMutex);
 osMutexId delay_mutex_id = NULL; 

/*******************************************************************************
* @brief				Bu fonksiyon delayler icin kosullama fonksiyonudur
* @param[in]    None
* @return				None
*******************************************************************************/
void initDelays(void){
   delay_mutex_id = osMutexCreate  (osMutex (delayMutex));
}

/*******************************************************************************
* @brief				Bu fonksiyon micro saniye bazinda delay olusturur
* @param[in]    None
* @return				None
*******************************************************************************/
void delayUs(uint32_t usecDelay){
	uint32_t tick;
 	osMutexWait(delay_mutex_id, osWaitForever );
// 	TIM_DelayUsec(USEC_DELAY_TIM, usecDelay);
	tick = osKernelSysTick();
	while((osKernelSysTick()-tick)<osKernelSysTickMicroSec(usecDelay));
	osMutexRelease(delay_mutex_id);
	
}

/*******************************************************************************
* @brief				Bu fonksiyon 
* @param[in]    None
* @return				None
*******************************************************************************/
void delayUs_TurnOff(void){
	if(delay_mutex_id!=NULL){
		while(osMutexDelete(delay_mutex_id)!=osOK){
				osDelay(10);
		}
		delay_mutex_id = NULL;
	}
}




