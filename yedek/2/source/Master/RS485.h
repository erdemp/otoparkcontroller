

#ifndef RS485_H_
#define RS485_H_

#include "chip.h"
#include "bsp.h"
//#include "mainRTX.h"
#include "cmsis_os.h"
#include "ring_buffer.h"

#ifdef __cplusplus
extern "C"
{
#endif

	//----------------DEFINITIONS-----------------------------------------------
#define		TXFIFO_SIZE		128
#define 	RXFIFO_SIZE		128
//----------------FUNCTION PROTOTYPES---------------------------------------	
void rs485Init(LPC_USART_T *pUART, uint32_t baudrate);
void rs485TransmitEnable (LPC_USART_T *pUART );
void rs485ReceiveEnable (LPC_USART_T *pUART ) ;
bool rs485SendData (LPC_USART_T *pUART , uint8_t *sendbuff, uint32_t sendSize, uint8_t *returndata, uint8_t returnSize, uint32_t timeout);
bool rs485_checkBufferDataSend (LPC_USART_T *pUART, RINGBUFF_T *RingBuff, uint16_t timeout );
bool rs485_WaitMsOrByteCount (RINGBUFF_T *RingBuff, uint16_t ms, uint16_t bytecount);	
void rs485_UartTurnOff( LPC_USART_T *pUART);
void ringBuffInit (void) ;
//---------------VARIABLES--------------------------------------------------	
typedef struct
{
  uint8_t     uartPort;
	uint32_t		baudrate;
	uint8_t 		TxPort;
	uint8_t			TxPin;
	uint8_t			TxIOCON;
	uint8_t			RxPort;
	uint8_t			RxPin;
	uint8_t			RxIOCON;
	uint8_t			DDPort;
	uint8_t			DDpin;
	uint8_t			DDIOCON;
	uint8_t			TxBuff[TXFIFO_SIZE];
	uint8_t			RxBuff[RXFIFO_SIZE];
	RINGBUFF_T  rbRx;
	RINGBUFF_T  rbTx;
	
}RS485_HandleTypeDef;

#ifdef __cplusplus
}
#endif

#endif /* RS485_H_ */




