#ifndef MAINRTX_H_
#define MAINRTX_H_


#include "cmsis_os.h"
#include "delays.h"
#include "master.h"
#include "settings.h"

#ifdef __cplusplus
extern "C"
{
#endif
	
void startRTX(void);
void DMA_IRQHandler(void);


extern osThreadDef_t keyboardTaskDef;
extern osThreadDef_t lcdTaskDef;

	#ifdef __cplusplus
}
#endif

#endif /* SENSORDRIVER_H_ */



