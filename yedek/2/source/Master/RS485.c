
#include "RS485.h"




#define TIMER_RS485_CALLBACK_PERIOD	10
#define RS485_DATASEND_TIMEOUT			100

uint16_t	timerRS485_TimeCount = 0;

void timerRS485_CallBack(void const *arg);
bool rs485_WaitMsOrByteCount (RINGBUFF_T *RingBuff, uint16_t ms, uint16_t bytecount) ;
bool rs485_checkAllDataSend (LPC_USART_T *pUART, RINGBUFF_T *RingBuff, uint16_t timeout );

osTimerDef(timerRS485, timerRS485_CallBack);
osTimerId timer_RS485ID = NULL;



static RS485_HandleTypeDef	rs485_1;
static RS485_HandleTypeDef	rs485_2;
static RS485_HandleTypeDef	rs485_3;
static RS485_HandleTypeDef	rs485_4;
/*******************************************************************************
* @brief				uart'i initialize eder
* @param[in]    none
* @return				none
*******************************************************************************/
void rs485Init(LPC_USART_T *pUART, uint32_t baudrate){

	
	if ( pUART == RS485_Port_1 ) {
		
			Chip_IOCON_PinMuxSet(LPC_IOCON, RS485_Port_1_Rxd_Port, RS485_Port_1_Rxd_Pin, RS485_Port_1_Rxd_IOCON );
			Chip_IOCON_PinMuxSet(LPC_IOCON, RS485_Port_1_Txd_Port, RS485_Port_1_Txd_Pin, RS485_Port_1_Txd_IOCON );
			Chip_IOCON_PinMuxSet(LPC_IOCON, RS485_Port_1_DD_Port, RS485_Port_1_DD_Pin, RS485_Port_1_DD_IOCON );
		
   		rs485ReceiveEnable(RS485_Port_1);
			Chip_GPIO_SetPinDIROutput(LPC_GPIO, RS485_Port_1_DD_Port, RS485_Port_1_DD_Pin);		
			rs485ReceiveEnable(RS485_Port_1);
		
			Chip_UART_Init(RS485_Port_1);
			Chip_UART_SetBaud(RS485_Port_1, baudrate);
			Chip_UART_ConfigData(RS485_Port_1, (UART_LCR_WLEN8 | UART_LCR_SBS_2BIT | UART_LCR_PARITY_EN | UART_LCR_PARITY_EVEN));	/* Default 8-E-2 */

			/* Enable UART Transmit */
			Chip_UART_TXEnable(RS485_Port_1);
			Chip_UART_SetupFIFOS(RS485_Port_1, (UART_FCR_FIFO_EN | UART_FCR_RX_RS | UART_FCR_TX_RS));


			RingBuffer_Init(&(rs485_1.rbTx),rs485_1.TxBuff,1,TXFIFO_SIZE);
			RingBuffer_Init(&(rs485_1.rbRx),rs485_1.RxBuff,1,RXFIFO_SIZE);
		
			Chip_UART_IntEnable(RS485_Port_1, UART_IER_RBRINT | UART_IER_RLSINT);

			/* Enable Interrupt for UART channel */
			/* Priority = 1 */
			NVIC_SetPriority(RS485_Port1_IRQn, 1);
			/* Enable Interrupt for UART channel */
			NVIC_EnableIRQ(RS485_Port1_IRQn);
		
		
	}	else if (pUART == RS485_Port_2 ) {
		
			Chip_IOCON_PinMuxSet(LPC_IOCON, RS485_Port_2_Rxd_Port, RS485_Port_2_Rxd_Pin, RS485_Port_2_Rxd_IOCON );
			Chip_IOCON_PinMuxSet(LPC_IOCON, RS485_Port_2_Txd_Port, RS485_Port_2_Txd_Pin, RS485_Port_2_Txd_IOCON );
			Chip_IOCON_PinMuxSet(LPC_IOCON, RS485_Port_2_DD_Port, RS485_Port_2_DD_Pin, RS485_Port_2_DD_IOCON );
		
   		rs485ReceiveEnable(RS485_Port_2);
			Chip_GPIO_SetPinDIROutput(LPC_GPIO, RS485_Port_2_DD_Port, RS485_Port_2_DD_Pin);		
			rs485ReceiveEnable(RS485_Port_2);
		
			Chip_UART_Init(RS485_Port_2);
			Chip_UART_SetBaud(RS485_Port_2, baudrate);
			Chip_UART_ConfigData(RS485_Port_2, (UART_LCR_WLEN8 | UART_LCR_SBS_2BIT | UART_LCR_PARITY_EN | UART_LCR_PARITY_EVEN));	/* Default 8-E-2 */

			/* Enable UART Transmit */
			Chip_UART_TXEnable(RS485_Port_2);
			Chip_UART_SetupFIFOS(RS485_Port_2, (UART_FCR_FIFO_EN | UART_FCR_RX_RS | UART_FCR_TX_RS));


			RingBuffer_Init(&(rs485_2.rbTx),rs485_2.TxBuff,1,TXFIFO_SIZE);
			RingBuffer_Init(&(rs485_2.rbRx),rs485_2.RxBuff,1,RXFIFO_SIZE);

			Chip_UART_IntEnable(RS485_Port_2, UART_IER_RBRINT | UART_IER_RLSINT);

			/* Enable Interrupt for UART channel */
			/* Priority = 1 */
			NVIC_SetPriority(RS485_Port2_IRQn, 1);
			/* Enable Interrupt for UART channel */
			NVIC_EnableIRQ(RS485_Port2_IRQn);
	
	
	}	else if (pUART == RS485_Port_3 ) {
		
			Chip_IOCON_PinMuxSet(LPC_IOCON, RS485_Port_3_Rxd_Port, RS485_Port_3_Rxd_Pin, RS485_Port_3_Rxd_IOCON );
			Chip_IOCON_PinMuxSet(LPC_IOCON, RS485_Port_3_Txd_Port, RS485_Port_3_Txd_Pin, RS485_Port_3_Txd_IOCON );
			Chip_IOCON_PinMuxSet(LPC_IOCON, RS485_Port_3_DD_Port, RS485_Port_3_DD_Pin, RS485_Port_3_DD_IOCON );
		
   		rs485ReceiveEnable(RS485_Port_3);
			Chip_GPIO_SetPinDIROutput(LPC_GPIO, RS485_Port_3_DD_Port, RS485_Port_3_DD_Pin);		
			rs485ReceiveEnable(RS485_Port_3);
		
			Chip_UART_Init(RS485_Port_3);
			Chip_UART_SetBaud(RS485_Port_3, baudrate);
			Chip_UART_ConfigData(RS485_Port_3, (UART_LCR_WLEN8 | UART_LCR_SBS_2BIT | UART_LCR_PARITY_EN | UART_LCR_PARITY_EVEN));	/* Default 8-E-2 */

			/* Enable UART Transmit */
			Chip_UART_TXEnable(RS485_Port_3);
			Chip_UART_SetupFIFOS(RS485_Port_3, (UART_FCR_FIFO_EN | UART_FCR_RX_RS | UART_FCR_TX_RS));


			RingBuffer_Init(&(rs485_3.rbTx),rs485_3.TxBuff,1,TXFIFO_SIZE);
			RingBuffer_Init(&(rs485_3.rbRx),rs485_3.RxBuff,1,RXFIFO_SIZE);

			Chip_UART_IntEnable(RS485_Port_3, UART_IER_RBRINT | UART_IER_RLSINT);

			/* Enable Interrupt for UART channel */
			/* Priority = 1 */
			NVIC_SetPriority(RS485_Port3_IRQn, 1);
			/* Enable Interrupt for UART channel */
			NVIC_EnableIRQ(RS485_Port3_IRQn);
	
	}	else if (pUART == RS485_Port_4) {
		
			Chip_IOCON_PinMuxSet(LPC_IOCON, RS485_Port_4_Rxd_Port, RS485_Port_4_Rxd_Pin, RS485_Port_4_Rxd_IOCON );
			Chip_IOCON_PinMuxSet(LPC_IOCON, RS485_Port_4_Txd_Port, RS485_Port_4_Txd_Pin, RS485_Port_4_Txd_IOCON );
			Chip_IOCON_PinMuxSet(LPC_IOCON, RS485_Port_4_DD_Port, RS485_Port_4_DD_Pin, RS485_Port_4_DD_IOCON );
		
   		rs485ReceiveEnable(RS485_Port_4);
			Chip_GPIO_SetPinDIROutput(LPC_GPIO, RS485_Port_4_DD_Port, RS485_Port_4_DD_Pin);		
			rs485ReceiveEnable(RS485_Port_4);
		
			Chip_UART_Init(RS485_Port_4);
			Chip_UART_SetBaud(RS485_Port_4, baudrate);
			Chip_UART_ConfigData(RS485_Port_4, (UART_LCR_WLEN8 | UART_LCR_SBS_2BIT | UART_LCR_PARITY_EN | UART_LCR_PARITY_EVEN));	/* Default 8-E-2 */

			/* Enable UART Transmit */
			Chip_UART_TXEnable(RS485_Port_4);
			Chip_UART_SetupFIFOS(RS485_Port_4, (UART_FCR_FIFO_EN | UART_FCR_RX_RS | UART_FCR_TX_RS));


			RingBuffer_Init(&(rs485_4.rbTx),rs485_4.TxBuff,1,TXFIFO_SIZE);
			RingBuffer_Init(&(rs485_4.rbRx),rs485_4.RxBuff,1,RXFIFO_SIZE);

			Chip_UART_IntEnable(RS485_Port_4, UART_IER_RBRINT | UART_IER_RLSINT);

			/* Enable Interrupt for UART channel */
			/* Priority = 1 */
			NVIC_SetPriority(RS485_Port4_IRQn, 1);
			/* Enable Interrupt for UART channel */
			NVIC_EnableIRQ(RS485_Port4_IRQn);
	}
	
	if(timer_RS485ID==NULL){
			timer_RS485ID= osTimerCreate(osTimer(timerRS485), osTimerPeriodic, NULL);
			osTimerStart(timer_RS485ID, TIMER_RS485_CALLBACK_PERIOD);
	}
	

}

/*******************************************************************************
* @brief		 Girilen  rs485 i �ikis kosullar.	
* @param[in] pin no 
* @return		 Nothing
 *******************************************************************************/
void rs485TransmitEnable (LPC_USART_T *pUART ) {
uint32_t i=0;
	
	if (pUART == RS485_Port_1 ) {
		Chip_GPIO_SetPinState(LPC_GPIO, RS485_Port_1_DD_Port , RS485_Port_1_DD_Pin	, true);
	}else if (pUART == RS485_Port_2) {
		Chip_GPIO_SetPinState(LPC_GPIO, RS485_Port_2_DD_Port , RS485_Port_2_DD_Pin	, true);
	}else if (pUART == RS485_Port_3) {
		Chip_GPIO_SetPinState(LPC_GPIO, RS485_Port_3_DD_Port , RS485_Port_3_DD_Pin	, true);
	}else if (pUART == RS485_Port_4) {
		Chip_GPIO_SetPinState(LPC_GPIO, RS485_Port_4_DD_Port , RS485_Port_4_DD_Pin	, true);
	}	
	for (i=0;i<1000;i++) {
		__nop();
		__nop();
		__nop();
		__nop();
		__nop();
		__nop();
		__nop();
		__nop();
		__nop();
		__nop();
	}
		
}

/*******************************************************************************
* @brief		 Girilen s485 i giris kosullar	
* @param[in] pin no 
* @return		 Nothing
 *******************************************************************************/
void rs485ReceiveEnable (LPC_USART_T *pUART ) {
//uint32_t i=0;	
	
	if (pUART == RS485_Port_1 ) {
		Chip_GPIO_SetPinState(LPC_GPIO, RS485_Port_1_DD_Port , RS485_Port_1_DD_Pin	, false);
	}else if (pUART == RS485_Port_2) {
		Chip_GPIO_SetPinState(LPC_GPIO, RS485_Port_2_DD_Port , RS485_Port_2_DD_Pin	, false);
	}else if (pUART == RS485_Port_3) {
		Chip_GPIO_SetPinState(LPC_GPIO, RS485_Port_3_DD_Port , RS485_Port_3_DD_Pin	, false);
	}else if (pUART == RS485_Port_4) {
		Chip_GPIO_SetPinState(LPC_GPIO, RS485_Port_4_DD_Port , RS485_Port_4_DD_Pin	, false);
	}		
	
//	for (i=0;i<1000;i++) {
//		__nop();
//		__nop();
//		__nop();
//		__nop();
//		__nop();
//		__nop();
//		__nop();
//		__nop();
//		__nop();
//		__nop();
//	}
		
}
/*******************************************************************************
* @brief	Girilen datalari RS485 �zerinden g�nderir ve cevap paketinin gelmesini bekler.
* @param[in]  rs485Port   : Kullanilan uart portu. Fonksiyona verilen port numarasinin hangi uart portuna ait  oldugu bilgisi bsp den alinir.
							*sendbuff	  : Gonderilecek datalarin tutuldugu buffer in pointeri
							sendSize	  : Gonderilecek data adeti
						  *returndata : Gelen datalarin konuldugu yer
							returnSize  : Gelecek datalarin adeti
							timeout			: ms bazinda timeout
* @return			true,false
							timeout olursa false, otherwise true
 *******************************************************************************/
bool rs485SendData (LPC_USART_T *pUART , uint8_t *sendbuff, uint32_t sendSize, uint8_t *returndata, uint8_t returnSize, uint32_t timeout) {
uint8_t voidData = 0;
//uint8_t devam = 0;
//uint32_t i;
	
	//---------------------------------------------------------------------------------------------------------------
	if (pUART == RS485_Port_1 ) {			
		// ------------------------------------------------------------------------------------------------------------
		timerRS485_TimeCount = 100; 		// ring buffer kontrol timeout u .  bir sekilde ringbuff rutinleri kitlenirse diye.
		while ( RingBuffer_Pop (&(rs485_1.rbRx),&voidData) == 1 ) {		// anlamsiz data varsa bunlari �ek
			__nop();
			if (timerRS485_TimeCount == 0 ) {
				return false;
			}
		}
		
		rs485TransmitEnable(RS485_Port_1);
		Chip_UART_SendRB(RS485_Port_1,&(rs485_1.rbTx),sendbuff,sendSize);  // datalari gonder.
		if ( rs485_checkBufferDataSend (RS485_Port_1,&(rs485_1.rbTx),RS485_DATASEND_TIMEOUT) == false ) { // datalar gidene kadar bekle
			rs485ReceiveEnable(RS485_Port_1);																																	
			return false;
		}
		
		rs485ReceiveEnable(RS485_Port_1);
		if ( rs485_WaitMsOrByteCount (&(rs485_1.rbRx), timeout, returnSize) == false ) {					
			return false;
		}
		RingBuffer_PopMult(&(rs485_1.rbRx), (uint8_t *) returndata, returnSize);		// gelen datalari user buffer a at
		return true;
		//---------------------------------------------------------------------------------------------------------
	}else if (pUART == RS485_Port_2 ) {
		//---------------------------------------------------------------------------------------------------------
		timerRS485_TimeCount = 100; 		// ring buffer kontrol timeout u .  bir sekilde ringbuff rutinleri kitlenirse diye.
		while ( RingBuffer_Pop (&(rs485_2.rbRx),&voidData) == 1 ) {		// anlamsiz data varsa bunlari �ek
			if (timerRS485_TimeCount == 0 ) {
				return false;
			}
		}
		
		rs485TransmitEnable(RS485_Port_2);
		Chip_UART_SendRB(RS485_Port_2,&(rs485_2.rbTx),sendbuff,sendSize);  // datalari gonder.
		if ( rs485_checkBufferDataSend (RS485_Port_2,&(rs485_2.rbTx),RS485_DATASEND_TIMEOUT) == false ) { // datalar gidene kadar bekle
			rs485ReceiveEnable(RS485_Port_2);
			return false;
		}
		rs485ReceiveEnable(RS485_Port_2);
		if ( rs485_WaitMsOrByteCount (&(rs485_2.rbRx), timeout, returnSize) == false ) {
			return false;
		}
		RingBuffer_PopMult(&(rs485_2.rbRx), (uint8_t *) returndata, returnSize);			// gelen datalari user buffer a at
		return true;
		//----------------------------------------------------------------------------------------------------------------
	}else if (pUART == RS485_Port_3 ) {
		//-----------------------------------------------------------------------------------------------------------------
		timerRS485_TimeCount = 100; 		// ring buffer kontrol timeout u .  bir sekilde ringbuff rutinleri kitlenirse diye.
		while ( RingBuffer_Pop (&(rs485_3.rbRx),&voidData) == 1 ) {		// anlamsiz data varsa bunlari �ek
			if (timerRS485_TimeCount == 0 ) {
				return false;
			}
		}
		
		rs485TransmitEnable(RS485_Port_3);
		Chip_UART_SendRB(RS485_Port_3,&(rs485_3.rbTx),sendbuff,sendSize);  // datalari gonder.
		if ( rs485_checkBufferDataSend (RS485_Port_3,&(rs485_3.rbTx),RS485_DATASEND_TIMEOUT) == false ) { // datalar gidene kadar bekle
			rs485ReceiveEnable(RS485_Port_3);
			return false;
		}
		rs485ReceiveEnable(RS485_Port_3);
		if ( rs485_WaitMsOrByteCount (&(rs485_3.rbRx), timeout, returnSize) == false ) {
			return false;
		}
		RingBuffer_PopMult(&(rs485_3.rbRx), (uint8_t *) returndata, returnSize);			// gelen datalari user buffer a at
		return true;
		//---------------------------------------------------------------------------------------------------------------
	}else if (pUART == RS485_Port_4 ) {
		//---------------------------------------------------------------------------------------------------------------
		timerRS485_TimeCount = 100; 		// ring buffer kontrol timeout u .  bir sekilde ringbuff rutinleri kitlenirse diye.
		while ( RingBuffer_Pop (&(rs485_4.rbRx),&voidData) == 1 ) {		// anlamsiz data varsa bunlari �ek
			if (timerRS485_TimeCount == 0 ) {
				return false;
			}
		}
		
		rs485TransmitEnable(RS485_Port_4);
		Chip_UART_SendRB(RS485_Port_4,&(rs485_4.rbTx),sendbuff,sendSize);  // datalari gonder.
		if ( rs485_checkBufferDataSend (RS485_Port_4,&(rs485_4.rbTx),RS485_DATASEND_TIMEOUT) == false ) { // datalar gidene kadar bekle
			rs485ReceiveEnable(RS485_Port_4);
			return false;
		}
		rs485ReceiveEnable(RS485_Port_4);
		if ( rs485_WaitMsOrByteCount (&(rs485_4.rbRx), timeout, returnSize) == false ) {
			return false;
		}
		RingBuffer_PopMult(&(rs485_4.rbRx), (uint8_t *) returndata, returnSize);			// gelen datalari user buffer a at
		return true;
	}
	
	return false;
	

}	

/*******************************************************************************
* @brief	Bufferdaki b�t�n datalarin gitmesini bekler.
* @param[in]  *pUART 		: UART 
							*RingBuff : gonderilen datalarin tutuldugu ring buff
							timeout		:
 * @return	true,false
 *******************************************************************************/
bool rs485_checkBufferDataSend (LPC_USART_T *pUART, RINGBUFF_T *RingBuff, uint16_t timeout ) {
	
		timerRS485_TimeCount = timeout;
		while (timerRS485_TimeCount > 0 ) {
			if (RingBuffer_IsEmpty(RingBuff) ) {
				if ( (Chip_UART_ReadLineStatus(pUART) & UART_LSR_TEMT)   ) {
					return true;
				}
			}
		}
		
		return false;
	
}
/*******************************************************************************
* @brief	Belirli bir s�re boyunca istenen sayida datanin gelmesini bekler.
* @param[in]  ms 				: timeout s�resi
							bytecount : beklenen data sayisi
 * @return	true,false
 *******************************************************************************/
bool rs485_WaitMsOrByteCount (RINGBUFF_T *RingBuff, uint16_t ms, uint16_t bytecount) {
uint8_t voidData;
	
	timerRS485_TimeCount = ms;
	
	while ( timerRS485_TimeCount != 0 ) {
		if (RingBuffer_GetCount(RingBuff) >= bytecount ) {
			return true;
		}
	}

	timerRS485_TimeCount = 100; 		// ring buffer kontrol timeout u .  bir sekilde ringbuff rutinleri kitlenirse diye.
	while ( RingBuffer_Pop (RingBuff,&voidData) == 1 ) {		// anlamsiz data varsa bunlari �ek
		__nop();
		if (timerRS485_TimeCount == 0 ) {
			return false;
		}
	}
	return false;
	
}
/*******************************************************************************
* @brief	IRQHANDLER 
 * @param[in]  
 * @return	Nothing
 *******************************************************************************/
void RS485_Port1_IRQHandler(void)
{
	Chip_UART_IRQRBHandler(RS485_Port_1, &(rs485_1.rbRx), &(rs485_1.rbTx));
}

/*******************************************************************************
* @brief	IRQHANDLER 
 * @param[in]  
 * @return	Nothing
 *******************************************************************************/
void RS485_Port2_IRQHandler(void)
{
	Chip_UART_IRQRBHandler(RS485_Port_2, &(rs485_2.rbRx), &(rs485_2.rbTx));
}
/*******************************************************************************
* @brief	IRQHANDLER 
 * @param[in]  
 * @return	Nothing
 *******************************************************************************/
void RS485_Port3_IRQHandler(void)
{
	Chip_UART_IRQRBHandler(RS485_Port_3, &(rs485_3.rbRx), &(rs485_3.rbTx));
}
/*******************************************************************************
* @brief	IRQHANDLER 
 * @param[in]  
 * @return	Nothing
 *******************************************************************************/
void RS485_Port4_IRQHandler(void)
{
	Chip_UART_IRQRBHandler(RS485_Port_4, &(rs485_4.rbRx), &(rs485_4.rbTx));
}

/*******************************************************************************
* @brief				uart'i Deinitialize eder
* @param[in]    none
* @return				none
*******************************************************************************/
void rs485_UartTurnOff( LPC_USART_T *pUART){
	
	if (pUART == RS485_Port_1 ) {
		Chip_UART_DeInit(RS485_Port_1);
		Chip_IOCON_PinMuxSet(LPC_IOCON, RS485_Port_1_Rxd_Port, RS485_Port_1_Rxd_Pin, RS485_Port_1_Rxd_IOCON );
		Chip_IOCON_PinMuxSet(LPC_IOCON, RS485_Port_1_Txd_Port, RS485_Port_1_Txd_Pin, RS485_Port_1_Txd_IOCON );
		Chip_IOCON_PinMuxSet(LPC_IOCON, RS485_Port_1_DD_Port, RS485_Port_1_DD_Pin, RS485_Port_1_DD_IOCON );
		
		rs485ReceiveEnable(RS485_Port_1);
		Chip_GPIO_SetPinDIROutput(LPC_GPIO, RS485_Port_1_DD_Port, RS485_Port_1_DD_Pin);		
		rs485ReceiveEnable(RS485_Port_1);
		
	}else if (pUART == RS485_Port_2) {
		
		Chip_UART_DeInit(RS485_Port_2);
		Chip_IOCON_PinMuxSet(LPC_IOCON, RS485_Port_2_Rxd_Port, RS485_Port_2_Rxd_Pin, RS485_Port_2_Rxd_IOCON );
		Chip_IOCON_PinMuxSet(LPC_IOCON, RS485_Port_2_Txd_Port, RS485_Port_2_Txd_Pin, RS485_Port_2_Txd_IOCON );
		Chip_IOCON_PinMuxSet(LPC_IOCON, RS485_Port_2_DD_Port, RS485_Port_2_DD_Pin, RS485_Port_2_DD_IOCON );
		
		rs485ReceiveEnable(RS485_Port_2);
		Chip_GPIO_SetPinDIROutput(LPC_GPIO, RS485_Port_2_DD_Port, RS485_Port_2_DD_Pin);		
		rs485ReceiveEnable(RS485_Port_2);
		
	}else if (pUART == RS485_Port_3) {
		
		Chip_UART_DeInit(RS485_Port_3);
		Chip_IOCON_PinMuxSet(LPC_IOCON, RS485_Port_3_Rxd_Port, RS485_Port_3_Rxd_Pin, RS485_Port_3_Rxd_IOCON );
		Chip_IOCON_PinMuxSet(LPC_IOCON, RS485_Port_3_Txd_Port, RS485_Port_3_Txd_Pin, RS485_Port_3_Txd_IOCON );
		Chip_IOCON_PinMuxSet(LPC_IOCON, RS485_Port_3_DD_Port, RS485_Port_3_DD_Pin, RS485_Port_3_DD_IOCON );
		
		rs485ReceiveEnable(RS485_Port_3);
		Chip_GPIO_SetPinDIROutput(LPC_GPIO, RS485_Port_3_DD_Port, RS485_Port_3_DD_Pin);		
		rs485ReceiveEnable(RS485_Port_3);
		
		
	}else if (pUART == RS485_Port_4) {
	
		Chip_UART_DeInit(RS485_Port_4);
		Chip_IOCON_PinMuxSet(LPC_IOCON, RS485_Port_4_Rxd_Port, RS485_Port_4_Rxd_Pin, RS485_Port_4_Rxd_IOCON );
		Chip_IOCON_PinMuxSet(LPC_IOCON, RS485_Port_4_Txd_Port, RS485_Port_4_Txd_Pin, RS485_Port_4_Txd_IOCON );
		Chip_IOCON_PinMuxSet(LPC_IOCON, RS485_Port_4_DD_Port, RS485_Port_4_DD_Pin, RS485_Port_4_DD_IOCON );
		
		rs485ReceiveEnable(RS485_Port_4);
		Chip_GPIO_SetPinDIROutput(LPC_GPIO, RS485_Port_4_DD_Port, RS485_Port_4_DD_Pin);		
		rs485ReceiveEnable(RS485_Port_4);
		
	}		
	

}
/*******************************************************************************
* @brief				Bu fonksiyon RS485 driver isleri icin timer callback fonksiyonudur
* @param[in]    arg
* @return				None
*******************************************************************************/
void ringBuffInit (void) {

	RingBuffer_Init(&(rs485_1.rbTx),rs485_1.TxBuff,1,TXFIFO_SIZE);
	RingBuffer_Init(&(rs485_1.rbRx),rs485_1.RxBuff,1,RXFIFO_SIZE);

}
/*******************************************************************************
* @brief				Bu fonksiyon RS485 driver isleri icin timer callback fonksiyonudur
* @param[in]    arg
* @return				None
*******************************************************************************/
void timerRS485_CallBack(void const *arg){
	
	if(timerRS485_TimeCount>=TIMER_RS485_CALLBACK_PERIOD){
		timerRS485_TimeCount-=TIMER_RS485_CALLBACK_PERIOD;
	}else{
		timerRS485_TimeCount=0;
	}
	
}



