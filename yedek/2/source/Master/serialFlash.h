
#ifndef SERIALFLASH_H_
#define SERIALFLASH_H_

#include "chip.h"
#include "cmsis_os.h"
#include <stdio.h>
#include <string.h>
#include "crc.h"

#ifdef __cplusplus
extern "C"
{
#endif

	//#define sFLASH_MAX_PAGE_NUMBER		2048
	#define sFLASH_PAGE_SIZE					256
	//#define sFLASH_MAX_SECTOR_NUMBER	128
	#define sFLASH_SECTOR_SIZE				(4*1024)
	//#define sFLASH_MAX_BLOCK_NUMBER	8
	#define sFLASH_BLOCK_SIZE				(64*1024)
	
	
	typedef struct{
		uint32_t receiptNumber;
		uint32_t date;
		uint16_t hour;
		uint8_t net;
		uint8_t volOnTop;
		int32_t weight;
		uint8_t weightDotPos;
		uint8_t weightUnit;
		int32_t volume;
		uint8_t volumeDotPos;
		uint8_t volumeUnit;
		uint32_t volumeCoef;
		uint8_t volumeCoefDotPos;
		uint16_t recordCRC;
	}sflashRecordType;
	
	
	bool sFLASH_Init(void);
	bool sFLASH_ChipErase(void);
	bool sFLASH_SectorErase(uint16_t sectorNumber);
	bool sFLASH_WriteData(uint8_t *data, uint16_t dataLength, uint32_t address);
	bool sFLASH_ReadData(uint8_t *data, uint16_t dataLength, uint32_t address);
	bool sFlash_WriteRecord(sflashRecordType *record);
	//bool sFlash_ReadRecord(sflashRecordType *record, uint32_t recordNumber);
	bool sFlash_ReadRecord(sflashRecordType *record, uint32_t recordAddress);
	bool sFLASH_LoadDefaultVal(void);
	uint32_t sFLASH_GetTotalRecordNumber(void);
	bool sFLASH_GetLastRecord(sflashRecordType *record, uint32_t ofset);
	void sFlash_PinSleep(void);
	
	typedef struct{
		uint32_t flashInPointer;
		uint32_t flashRecordNumber;
		uint32_t maxRecordNumber;
		uint32_t receiptNumber;
		uint32_t lastFlashAddress;
	}sflashGenDataType;
	
	
	


	#define SFLASH_IN_POINTER						0x00000000
	#define SFLASH_IN_POINTER_SIZE			4
	
	#define SFLASH_RECORD_NUMBER				(SFLASH_IN_POINTER + SFLASH_IN_POINTER_SIZE)
	#define SFLASH_RECORD_NUMBER_SIZE		4
	
	#define SFLASH_RECEIPT_NUMBER				(SFLASH_RECORD_NUMBER + SFLASH_RECORD_NUMBER_SIZE)
	#define SFLASH_RECEIPT_NUMBER_SIZE	4

	
	
	
	
	#define SFLASH_RECEIPT_NO							0
	#define SFLASH_RECEIPT_NO_SIZE				4
	
	#define SFLASH_DATE										(SFLASH_RECEIPT_NO + SFLASH_RECEIPT_NO_SIZE)
	#define SFLASH_DATE_SIZE							4
	
	#define SFLASH_HOUR										(SFLASH_DATE + SFLASH_DATE_SIZE)
	#define SFLASH_HOUR_SIZE							2
	
	#define SFLASH_GROSS_NET							(SFLASH_HOUR + SFLASH_HOUR_SIZE)
	#define SFLASH_GROSS_NET_SIZE					1
	
	#define SFLASH_VOL_ON_TOP							(SFLASH_GROSS_NET + SFLASH_GROSS_NET_SIZE)
	#define SFLASH_VOL_ON_TOP_SIZE				1
	
	#define SFLASH_WEIGHT									(SFLASH_VOL_ON_TOP + SFLASH_VOL_ON_TOP_SIZE)
	#define SFLASH_WEIGHT_SIZE						4
	
	#define SFLASH_WEIGHT_DOT_POS					(SFLASH_WEIGHT + SFLASH_WEIGHT_SIZE)
	#define SFLASH_WEIGHT_DOT_POS_SIZE		1
	
	#define SFLASH_WEIGHT_UNIT						(SFLASH_WEIGHT_DOT_POS + SFLASH_WEIGHT_DOT_POS_SIZE)
	#define SFLASH_WEIGHT_UNIT_SIZE				1
	
	#define SFLASH_VOLUME									(SFLASH_WEIGHT_UNIT + SFLASH_WEIGHT_UNIT_SIZE)
	#define SFLASH_VOLUME_SIZE						4
	
	#define SFLASH_VOLUME_DOT_POS					(SFLASH_VOLUME + SFLASH_VOLUME_SIZE)
	#define SFLASH_VOLUME_DOT_POS_SIZE		1
	
	#define SFLASH_VOLUME_UNIT						(SFLASH_VOLUME_DOT_POS + SFLASH_VOLUME_DOT_POS_SIZE)
	#define SFLASH_VOLUME_UNIT_SIZE				1
	
	#define SFLASH_VOL_COEF								(SFLASH_VOLUME_UNIT + SFLASH_VOLUME_UNIT_SIZE)
	#define SFLASH_VOL_COEF_SIZE					4
	
	#define SFLASH_VOL_COEF_DOT_POS				(SFLASH_VOL_COEF + SFLASH_VOL_COEF_SIZE)
	#define SFLASH_VOL_COEF_DOT_POS_SIZE	1

	#define SFLASH_RECORD_CRC							(SFLASH_VOL_COEF_DOT_POS + SFLASH_VOL_COEF_DOT_POS_SIZE)
	#define SFLASH_RECORD_CRC_SIZE				2
	
	#define SFLASH_RECEIPT_START_ADDRESS	0x0000000800
	#define SFLASH_RECEIPT_SIZE						(SFLASH_RECORD_CRC + SFLASH_RECORD_CRC_SIZE)
	

#ifdef __cplusplus
}
#endif

#endif /* SERIALFLASH_H_ */
