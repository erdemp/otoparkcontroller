

#ifndef KEYBOARD_H_
#define KEYBOARD_H_

//#include <stdint.h>
#include "cmsis_os.h"
//#include "lpc17xx_gpio.h"
#include "chip.h"
#include "bsp.h"


#ifdef __cplusplus
extern "C"
{
#endif

#define KEYBOARD_TASK_TERMINATE	0x0001
	
#define KEY_BUZZER_TIME	50

extern osThreadId keyboardTaskID;
void keyboardTask(void const *argument);
	
void keyboardInit(void);
void keyboardMain(void);
uint8_t getKey(void);
uint8_t getKeyRelease(uint8_t keyCode);
uint16_t getKeyTime(uint8_t keyCode);
void keyIslendi(uint8_t keyCode);
void clearAllKey(void);

void setBuzzer(uint16_t ms);
	
void deviceTurnOff(uint8_t onOffKey);
void deviceTurnOn(void);
void setBuzzerSecondFunction(void);
void setBuzzerError(void);
void keyboardPinSleep(void);

void keyboardTurnOff(void);

#ifdef __cplusplus
}
#endif

#endif /* KEYBOARD_H_ */







