
#ifndef FIFO8_H_
#define FIFO8_H_


#include "chip.h"


//#include "cmsis_os.h"
//#include "types.h"

#ifdef __cplusplus
extern "C"
{
#endif

	
	typedef struct{
		uint8_t *buf_ptr;
		uint16_t bufferSize;
		uint16_t next;
		uint16_t first;
		uint16_t cnt;
	}fifo8_t;

	void clear_fifo8(fifo8_t *fifo);
	void init_fifo8(fifo8_t *fifo, uint16_t bufferSize);
	bool isFull_fifo8(fifo8_t *fifo);
	bool isEmpty_fifo8(fifo8_t *fifo);
	bool addElement_fifo8(fifo8_t *fifo, uint8_t data);
	uint8_t getElement_fifo8(fifo8_t *fifo);
	bool getElements_fifo8(fifo8_t *fifo, uint8_t *buf, uint16_t elementCount, uint16_t *usableElementCount);
	uint16_t getElementsNumber_fifo8(fifo8_t *fifo);

#ifdef __cplusplus
}
#endif

#endif /* FIFO8_H_ */

