
#include "fifo8.h"


/**
  * @brief  bu fonksiyon fifo buffer'i olusturur
  * @param  fifo: fifo8_t turunden degiskendir. fifo parametreleri burada tutulur
  *                  
  * @param  bufferSize: fifo icin ayrilan buffer buyuklugudur
  * @retval None
  */
	void init_fifo8(fifo8_t *fifo, uint16_t bufferSize){
		fifo->bufferSize = bufferSize;
		fifo->first = 0;
		fifo->next = 0;
		fifo->cnt = 0;
	}
	
/**
  * @brief  bu fonksiyon fifoya eleman eklemek icin kullanilir
  * @param  fifo: fifo8_t turunden degiskendir. fifo parametreleri burada tutulur
  *                  
  * @param  data: fifoya eklenecek yeni eleman
  * @retval fifoya dolulugu yuzunden eleman eklenemiyor ise FALSE ile donulur, eleman
	*					eklendiyse TRUE ile donulur
  */	
bool addElement_fifo8(fifo8_t *fifo, uint8_t data){    
        if (!isFull_fifo8(fifo)){
					 fifo->buf_ptr[fifo->next] = data;
					 fifo->next++;
					 if(fifo->next >= fifo->bufferSize){
						 fifo->next=0;
					 }
					 fifo->cnt++;
					 return true;
        }else{
					 return false;
        }      
}
/**
  * @brief  bu fonksiyon fifodan eleman cekmek icin kullanilir.
	*	@note		bu fonksiyon cagrilmadan once mutlaka fifoda eleman olup olmadigi
	*  				kontrol edilmelidir. Aksi halde yeni eleman 0 mis gibi alinir
  * @param  fifo: fifo8_t turunden degiskendir. fifo parametreleri burada tutulur
  *                  
  * @retval fifoda bulunan ilk eleman ile donulur
  */	
uint8_t getElement_fifo8(fifo8_t *fifo){
        uint8_t theElement = 0;
        if (! isEmpty_fifo8(fifo)){        
                theElement =  fifo->buf_ptr[fifo->first];  
								fifo->first++;
								if(fifo->first >= fifo->bufferSize){
									fifo->first=0;
								}
								fifo->cnt--;
        }

        return theElement;
}

/**
  * @brief  bu fonksiyon fifoda belirtilen say1daki eleman1 cekmek icin kullanilir.
  * @param  fifo: fifo8_t turunden degiskendir. fifo parametreleri burada tutulur
	* @param  buf: fifodan cekilen elemanlar bu buffera kaydedilir
	* @param	elemanCount: fifodan cekilmek istenen eleman sayisidir
	*
	* @retval usableElementCount: fifodan cekilebilen eleman sayisi bu degisken ile dondurulur
	*					fifo bos ise FALSE ile doner
  */	
bool getElements_fifo8(fifo8_t *fifo, uint8_t *buf, uint16_t elementCount, uint16_t *usableElementCount){
	
	*usableElementCount=0;
	
	if (isEmpty_fifo8(fifo)){  
		return false;
	}
	while(!isEmpty_fifo8(fifo)){
		buf[*usableElementCount]=getElement_fifo8(fifo);
		(*usableElementCount)++;
		if(*usableElementCount >= elementCount){
			return true;
		}
	}
	return true;
}


/**
  * @brief  bu fonksiyon fifo'yu bosaltir.
  * @param  fifo: fifo8_t turunden degiskendir. fifo parametreleri burada tutulur
  *                  
  * @retval 
  */	
void clear_fifo8(fifo8_t *fifo) {
uint16_t cnt = 0;
uint16_t i;
	
		cnt = getElementsNumber_fifo8(fifo);
		
	  for (i=0;i<cnt;i++) {
			(void) getElement_fifo8(fifo);			
		}
		
		
	
}
/**
  * @brief  bu fonksiyon fifo'nun dolu olup olmadigini sorgulamak icindir
  * @param  fifo: fifo8_t turunden degiskendir. fifo parametreleri burada tutulur
  *                  
  * @retval fifo dolu ise TRUE, henuz dolmamis FALSE ile donulur
  */	
bool isFull_fifo8(fifo8_t *fifo){
        if (getElementsNumber_fifo8(fifo) >= fifo->bufferSize){
                return true;
        }else{
                return false;
        }
}

/**
  * @brief  bu fonksiyon fifo'nun bos olup olmadigini sorgulamak icindir
  * @param  fifo: fifo8_t turunden degiskendir. fifo parametreleri burada tutulur
  *                  
  * @retval fifo bos ise TRUE, bos degilse FALSE ile donulur
  */	
bool isEmpty_fifo8(fifo8_t *fifo){
        if ( fifo->cnt == 0 ){
                return true;
        }else{
                return false;
        }      
}

/**
  * @brief  bu fonksiyon fifo bufferdaki eleman sayisini verir
  * @param  fifo: fifo8_t turunden degiskendir. fifo parametreleri burada tutulur
  *                  
  * @retval fifodaki eleman sayisi ile donulur
  */	
uint16_t getElementsNumber_fifo8(fifo8_t *fifo){   
        return fifo->cnt;   
}
