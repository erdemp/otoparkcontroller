#include "master.h"


//osThreadId Master_TaskID = NULL;

// osPoolDef(mpool_sensor, 1, sensorType);                    // Define memory pool
// osPoolId  mpool_sensor_id;
// sensorType *sensor_data;
// 
///*******************************************************************************
//* @brief				Bu fonksiyon indikator ana isleri icin hazirlanmis RTOS taskidir
//* @param[in]    None
//* @return				None
//*******************************************************************************/
//void Master_Task(void const *argument){
////uint16_t temp;	
//osEvent evt;
////uint16_t limitVal;
//	
//	
//	rs485Init(RS485_Port_1, 38400);
//	rs485Init(RS485_Port_2, 38400);
//	rs485Init(RS485_Port_3, 38400);
//	rs485Init(RS485_Port_4, 38400);

//	osDelay(1000);
//	
//	ringBuffInit();
//	
//	CheckDeviceParameters();

//	for(;;){

//		
//		sensorDurumlariniOku();
//		sensorLedGuncelle();
//		sensorGrupRegUpdate();

//    		
//		displayUpdate();

//		
//		evt = osSignalWait(0, 0);
//		if (evt.status == osEventSignal)  {
//			if(evt.value.signals&STARTDEVICESEARCH_SIGNAL){
//				cihazTaramasi();
//			}else if ( evt.value.signals&STARTDEVICEPROGRAMMING_SIGNAL){
//				ProgramDevices () ;
//			}
//		} 
//		
//		checkAnySignalFromEthernetThread();

//		osDelay(50);

//		
//	}
//	
//}
///*******************************************************************************
//* @brief				Bu fonksiyon master ana d�ng�de ethernetten bir signal gelip gelmedigini kontrol eder.
//* @param[in]    None
//* @return				None
//*******************************************************************************/
//void checkAnySignalFromEthernetThread (void) {
//mpoolSensorType *rptrSensor;
//mpoolDisplayType *rptrDisplay;
//mpoolSensorType *rptrReadSensor;
//mpoolDisplayType *rptrReadDisplay;	
//osEvent evt;
//LPC_USART_T *port;
//	
//		//burada bir d�ng� yapilabilir b��tn cihazlar bitene kadar. burada kalsin. yukariya gitmesine gerek yok.
//		// yavaslatiyor.
//		//---------sensor programla
//	  while (1) {
//			evt = osMessageGet(MsgBoxSensor, 0);  // wait for message
//			if (evt.status == osEventMessage) {
//				rptrSensor = evt.value.p;		
//				osDelay(20);	      
//				if ( SetSensorParameterAndCheck( rptrSensor->port, &rptrSensor->sensorData )) {
//						rptrSensor->OK = 1;
//				}else {
//						rptrSensor->OK = 0;
//				}			
//				osSignalSet(Ethernet_TaskID, ETHERNET_SIGNAL_DEVICEUPDATE);
//				osDelay(10);
//				//osPoolFree(mpoolSensor, rptrSensor);                  // free memory allocated for message
//			}else {
//				__nop();
//				break;
//			}
//		}
//		//--------display programla
//		while (1) {
//			evt = osMessageGet(MsgBoxDisplay, 0);  // wait for message
//			if (evt.status == osEventMessage) {
//				rptrDisplay = evt.value.p;			      
//				if ( SetDisplayParameterAndCheck( rptrDisplay->port, &rptrDisplay->displayData )) {
//						rptrDisplay->OK = 1;
//				}else {
//						rptrDisplay->OK = 0;
//				}			
//				osSignalSet(Ethernet_TaskID, ETHERNET_SIGNAL_DEVICEUPDATE);
//				osDelay(10);
//				//osPoolFree(mpoolSensor, rptrSensor);                  // free memory allocated for message
//			}	else {
//				break;
//			}			
//		}
//		//---------sensor oku
//		while (1) {
//			evt = osMessageGet(MsgBoxReadSensor, 0);  // wait for message
//			if (evt.status == osEventMessage) {
//				rptrReadSensor = evt.value.p;	 
//				osDelay(20);
//				if ( readSensorParameter( rptrReadSensor->port, rptrReadSensor->sensorData.deviceAddress, &rptrReadSensor->sensorData)) {
//						rptrReadSensor->OK = 1;
//				}else {
//						rptrReadSensor->OK = 0;
//				}			
//				osSignalSet(Ethernet_TaskID, ETHERNET_SIGNAL_DEVICEREAD);
//				osDelay(10);
//				//osPoolFree(mpoolSensor, rptrSensor);                  // free memory allocated for message
//			}else {
//				break;
//			}				
//		}
//		//--------display oku
//		while (1) { 
//			evt = osMessageGet(MsgBoxReadDisplay, 0);  // wait for message
//			if  (evt.status == osEventMessage) {
//				rptrReadDisplay = evt.value.p;	
//				if ( rptrReadDisplay->port == 1 ) {
//						port = RS485_Port_1;
//				}else if ( rptrReadDisplay->port == 2 ) {
//						port = RS485_Port_2;
//				}else if ( rptrReadDisplay->port == 3 ) {
//						port = RS485_Port_3;
//				}else if ( rptrReadDisplay->port == 4 ) {
//						port = RS485_Port_4;
//				}			
//				if ( getDisplayParameters( port, rptrReadDisplay->displayData.deviceAddress, &rptrReadDisplay->displayData )) {
//						rptrReadDisplay->OK = 1;
//				}else {
//						rptrReadDisplay->OK = 0;
//				}			
//				osSignalSet(Ethernet_TaskID, ETHERNET_SIGNAL_DEVICEREAD);
//				osDelay(10);
//				//osPoolFree(mpoolSensor, rptrSensor);                  // free memory allocated for message
//			}else {
//				break;
//			}
//		}
//	}
/*******************************************************************************
* @brief				Bu fonksiyon indikator ana isleri icin hazirlanmis RTOS taskidir
* @param[in]    None
* @return				None
*******************************************************************************/
bool extract_between (const char *str, const char *p1, const char *p2, char * returndata, uint16_t *fark)
{
bool retval= false;
 	
  const char *i1 = strstr(str, p1);
	
	*fark = (uint16_t) (i1 - str) ;
  if(i1 != NULL)
  {
    const size_t pl1 = strlen(p1);
    const char *i2 = strstr(i1 + pl1, p2);
    if(p2 != NULL)
    {
     /* Found both markers, extract text. */
     const size_t mlen = i2 - (i1 + pl1);
		{
			 char *ret = malloc(mlen + 1);
			 if(ret != NULL)
			 {
				 memcpy(returndata, i1 + pl1, mlen);
				 returndata[mlen] = '\0';
				 retval = true;
			 }
			 free(ret);
			}
    }
  }
	return retval;
}

