

#include "serialFlash.h"

#define sFLASH_CS_PORT_NUM	0
#define sFLASH_CS_PIN_NUM		6
#define sFLASH_CS_Clear			Chip_GPIO_SetPinState(LPC_GPIO, sFLASH_CS_PORT_NUM, sFLASH_CS_PIN_NUM	, false)
#define sFLASH_CS_Set				Chip_GPIO_SetPinState(LPC_GPIO, sFLASH_CS_PORT_NUM, sFLASH_CS_PIN_NUM	, true)

#define sFLASH_CLK_PORT_NUM	0
#define sFLASH_CLK_PIN_NUM	7

#define sFLASH_MISO_PORT_NUM	0
#define sFLASH_MISO_PIN_NUM	8

#define sFLASH_MOSI_PORT_NUM	0
#define sFLASH_MOSI_PIN_NUM	9

// #define sFLASH_HOLD_PORT_NUM 4
// #define sFLASH_HOLD_PIN_NUM 29


//status register
#define STATUS_WIP	0
#define STATUS_WEL	1
#define STATUS_BP0	2
#define STATUS_BP1	3
#define STATUS_BP2	4
#define STATUS_SRP	7

//flash commands
#define CMD_WRITE_EN								0x06
#define CMD_WRITE_DIS								0x04
#define CMD_READ_STATUS_REG					0x05
#define	CMD_WRITE_STATUS_REG				0x01
#define CMD_READ_DATA								0x03
#define CMD_FAST_READ								0x0B
#define CMD_PAGE_PROGRAM						0x02
#define CMD_PAGE_WRITE							0x0A
#define CMD_SECTOR_ERASE						0x20
#define CMD_BLOCK_ERASE							0xD8
#define CMD_CHIP_ERASE							0xC7
#define CMD_DEEP_POWER_DOWN					0xB9
#define CMD_RELEASE_DEEP_POWER_DOWN	0xAB
#define CMD_MANUFACTURER_DEVICE_ID	0x90
#define CMD_READ_IDENTIFICATION			0x9F
#define	CMD_ENTER_OTP_MODE					0x3A

#define SFLASH_SIZE_1MBIT						0x01
#define SFLASH_SIZE_2MBIT						0x02
#define SFLASH_SIZE_4MBIT						0x03
#define SFLASH_SIZE_8MBIT						0x04
#define SFLASH_SIZE_16MBIT					0x05

//timeoutlar
#define sFLASH_CHIP_ERASE_TIMEOUT		12000	//12sn
#define sFLASH_SECTOR_ERASE_TIMEOUT	5000	//5sn
#define sFLASH_PAGE_PROGRAM_TIMEOUT	10		//10ms
#define sFLASH_PAGE_WRITE_TIMEOUT		30		//30ms
#define sFLASH_PAGE_WRITE_STATUS		20		//20ms

#define LPC_SSP		LPC_SSP1

// SPI Configuration structure variable
//SPI_CFG_Type SPI_ConfigStruct;
//SPI_DATA_SETUP_Type xferConfig;

//static SSP_ConfigFormat ssp_format;
static Chip_SSP_DATA_SETUP_T	xferConfig;

uint32_t sFlashMemorySize;

sflashGenDataType sflashGenData;


uint8_t sFLASH_Read(uint8_t address);
void sFLASH_Write(uint8_t address, uint8_t data);
bool sFLASH_WriteEn(void);
bool sFLASH_WriteDis(void);
uint8_t sFLASH_ReadStatus(void);
bool sFLASH_WriteStatus(uint8_t data);
void sFLASH_ReadIdentification(uint8_t *result);



/*********************************************************************//**
 * @brief 		Initialize CS pin as GPIO function to drive /CS pin
 * 				due to definition of CS_PORT_NUM and CS_PORT_NUM
 * @param		None
 * @return		None
 ***********************************************************************/
//void sFLASH_CS_Init(void)
//{

//	
//	Chip_GPIO_SetPinDIROutput(LPC_GPIO, sFLASH_CS_PORT_NUM, sFLASH_CS_PIN_NUM);
//	sFLASH_CS_Set;
//}

/*********************************************************************//**
 * @brief 		Drive CS output pin to low/high level to select slave device
 * 				via /CS pin state
 * @param[in]	state State of CS output pin that will be driven:
 * 				- 0: Drive CS pin to low level
 * 				- 1: Drive CS pin to high level
 * @return		None
 ***********************************************************************/
//void sFLASH_CS_Force(int32_t state)
//{
//	if (state){
//		GPIO_SetValue(sFLASH_CS_PORT_NUM, (1<<sFLASH_CS_PIN_NUM));
//	}else{
//		GPIO_ClearValue(sFLASH_CS_PORT_NUM, (1<<sFLASH_CS_PIN_NUM));
//	}
//}

/*********************************************************************//**
 * @brief 		Initialize HOLD pin as GPIO function 
 * @param		None
 * @return		None
 ***********************************************************************/
// void sFLASH_HOLD_Init(void)
// {
// 	GPIO_SetDir(sFLASH_HOLD_PORT_NUM, (1<<sFLASH_HOLD_PIN_NUM), 1);
// 	GPIO_SetValue(sFLASH_HOLD_PORT_NUM, (1<<sFLASH_HOLD_PIN_NUM));
// }


/*********************************************************************//**
 * @brief 		Drive HOLD output pin to low/high level 
 * @param[in]	state State of HOLD output pin that will be driven:
 * 				- 0: Drive HOLD pin to low level
 * 				- 1: Drive HOLD pin to high level
 * @return		None
 ***********************************************************************/
// void sFLASH_HOLD_Force(int32_t state)
// {
// 	if (state){
// 		GPIO_SetValue(sFLASH_HOLD_PORT_NUM, (1<<sFLASH_HOLD_PIN_NUM));
// 	}else{
// 		GPIO_ClearValue(sFLASH_HOLD_PORT_NUM, (1<<sFLASH_HOLD_PIN_NUM));
// 	}
//}

/*********************************************************************//**
 * @brief 		25AA02E48'icin MCU pinlerini initialize eder
 * @param		None
 * @return		None
 ***********************************************************************/
bool sFLASH_Init(void){
//	uint8_t data[256];
	uint8_t identification[3];
//	uint16_t i;
//	uint32_t address;
//	sflashRecordType record;
	
	Chip_IOCON_PinMuxSet(LPC_IOCON, sFLASH_CS_PORT_NUM, sFLASH_CS_PIN_NUM, IOCON_FUNC2 );
	Chip_IOCON_PinMuxSet(LPC_IOCON, sFLASH_CLK_PORT_NUM, sFLASH_CLK_PIN_NUM, IOCON_FUNC2 );
	Chip_IOCON_PinMuxSet(LPC_IOCON, sFLASH_MISO_PORT_NUM, sFLASH_MISO_PIN_NUM, IOCON_FUNC2 );
	Chip_IOCON_PinMuxSet(LPC_IOCON, sFLASH_MOSI_PORT_NUM, sFLASH_MOSI_PIN_NUM, IOCON_FUNC2 );
	
	/* SSP initialization */
	Chip_SSP_Init(LPC_SSP);
	Chip_SSP_SetFormat(LPC_SSP, SSP_BITS_8, SSP_FRAMEFORMAT_SPI, SSP_CLOCK_CPHA1_CPOL1);
	//ssp_format.frameFormat = SSP_FRAMEFORMAT_SPI;
//	ssp_format.bits = SSP_BITS_8;
//	ssp_format.clockMode = SSP_CLOCK_MODE0;
//	Chip_SSP_SetFormat(LPC_SSP, &ssp_format);
	Chip_SSP_Enable(LPC_SSP);
	
	if(sFLASH_WriteStatus(0x00)==false){
		return false;
	}
	
	//sFLASH_ChipErase();
	
	sFLASH_ReadIdentification(identification);
	if((identification[2] & 0x0F) == SFLASH_SIZE_1MBIT){
		sFlashMemorySize = (1024*1/8)*1024; //byte
	}else if((identification[2] & 0x0F) == SFLASH_SIZE_2MBIT){
		sFlashMemorySize = (1024*2/8)*1024; //byte
	}else if((identification[2] & 0x0F) == SFLASH_SIZE_4MBIT){
		sFlashMemorySize = (1024*4/8)*1024; //byte
	}else if((identification[2] & 0x0F) == SFLASH_SIZE_8MBIT){
		sFlashMemorySize = (1024*8/8)*1024; //byte
	}else if((identification[2] & 0x0F) == SFLASH_SIZE_16MBIT){
		sFlashMemorySize = (1024*16/8)*1024; //byte
	}else{
		return false;
	}
	
	sflashGenData.lastFlashAddress= (((sFlashMemorySize - SFLASH_RECEIPT_START_ADDRESS) / SFLASH_RECEIPT_SIZE)*SFLASH_RECEIPT_SIZE) + SFLASH_RECEIPT_START_ADDRESS - 1;
	
	sflashGenData.maxRecordNumber = (sFlashMemorySize - SFLASH_RECEIPT_START_ADDRESS) / SFLASH_RECEIPT_SIZE;
	
	osDelay(1);
	
	if(sFLASH_WriteDis()==false){
		return false;
	}
	
	osDelay(1);
	
	if(sFLASH_ReadData((uint8_t*)&sflashGenData.flashInPointer,SFLASH_IN_POINTER_SIZE, SFLASH_IN_POINTER)){
		if((sflashGenData.flashInPointer>sflashGenData.lastFlashAddress)||(sflashGenData.flashInPointer<SFLASH_RECEIPT_START_ADDRESS)){
			//if(sFLASH_LoadDefaultVal()==false){
				return false;
			//}
		}
	}else{
		return false;
	}

	osDelay(1);

	if(sFLASH_ReadData((uint8_t*)&sflashGenData.flashRecordNumber,SFLASH_RECORD_NUMBER_SIZE, SFLASH_RECORD_NUMBER)){
		if(sflashGenData.flashRecordNumber>((sFlashMemorySize - SFLASH_RECEIPT_START_ADDRESS) / SFLASH_RECEIPT_SIZE)){
			//if(sFLASH_LoadDefaultVal()==false){
				return false;
			//}
		}
	}else{
		return false;
	}
	
	osDelay(1);
	
	if(sFLASH_ReadData((uint8_t*)&sflashGenData.receiptNumber,SFLASH_RECEIPT_NUMBER_SIZE, SFLASH_RECEIPT_NUMBER)==false){
		return false;
	}
	

//	for(i=1; i<=sflashGenData.flashRecordNumber;i++){
//		sFlash_ReadRecord(&record,i);
//	}
	
	return true;
}



/*********************************************************************//**
 * @brief 		Write enable yapar
 * @param		None
 * @return		None
 ***********************************************************************/
bool sFLASH_WriteEn(void){
	uint8_t status;
	uint8_t sendBuffer[1];
	uint8_t result[1];
	
	sendBuffer[0]=CMD_WRITE_EN;
	
	//sFLASH_CS_Force(0);
	xferConfig.tx_data = sendBuffer;
	xferConfig.rx_data = result;
	xferConfig.length = sizeof (sendBuffer);
	xferConfig.tx_cnt=0;
	xferConfig.rx_cnt=0;
	//SPI_ReadWrite(LPC_SPI, &xferConfig, SPI_TRANSFER_POLLING);
	Chip_SSP_RWFrames_Blocking(LPC_SSP, &xferConfig);
	//sFLASH_CS_Force(1);
	
	status=sFLASH_ReadStatus();
	if(status&(0x01<<STATUS_WEL)){
		return true;
	}
	return false;
}

/*********************************************************************//**
 * @brief 		Write disable yapar
 * @param		None
 * @return		None
 ***********************************************************************/
bool sFLASH_WriteDis(void){
	uint8_t status;
	uint8_t sendBuffer[1];
	uint8_t result[1];
	
	sendBuffer[0]=CMD_WRITE_DIS;
	
	//sFLASH_CS_Force(0);
	xferConfig.tx_data = sendBuffer;
	xferConfig.rx_data = result;
	xferConfig.length = sizeof (sendBuffer);
	xferConfig.tx_cnt=0;
	xferConfig.rx_cnt=0;
	//SPI_ReadWrite(LPC_SPI, &xferConfig, SPI_TRANSFER_POLLING);
	Chip_SSP_RWFrames_Blocking(LPC_SSP, &xferConfig);
	//sFLASH_CS_Force(1);
	
	status=sFLASH_ReadStatus();
	if(status&(0x01<<STATUS_WEL)){
		return false;
	}
	return true;
}

/*********************************************************************//**
 * @brief 		status'u okur
 * @param		None
 * @return		None
 ***********************************************************************/
uint8_t sFLASH_ReadStatus(void){
	uint8_t sendBuffer[2];
	uint8_t result[2];
	
	sendBuffer[0]=CMD_READ_STATUS_REG;
	
	//sFLASH_CS_Force(0);
//	xferConfig.tx_data = sendBuffer;
//	xferConfig.rx_data = result;
//	xferConfig.length = sizeof (sendBuffer);
//	SPI_ReadWrite(LPC_SPI, &xferConfig, SPI_TRANSFER_POLLING);
//	sFLASH_CS_Force(1);
	xferConfig.tx_data = sendBuffer;
	xferConfig.rx_data = result;
	xferConfig.length = sizeof(sendBuffer);
	xferConfig.tx_cnt=0;
	xferConfig.rx_cnt=0;
	Chip_SSP_RWFrames_Blocking(LPC_SSP, &xferConfig);
	return result[1];
}

/*********************************************************************//**
 * @brief 		
 * @param		None
 * @return		None
 ***********************************************************************/
void sFLASH_ReadIdentification(uint8_t *result){
	uint8_t sendBuffer[4];
	uint8_t res[5];
	
	sendBuffer[0]=CMD_READ_IDENTIFICATION;
	
	xferConfig.tx_data = sendBuffer;
	xferConfig.rx_data = res;
	xferConfig.length = sizeof(sendBuffer);
	xferConfig.tx_cnt=0;
	xferConfig.rx_cnt=0;
	Chip_SSP_RWFrames_Blocking(LPC_SSP, &xferConfig);
	memcpy(result, &res[1], 3);
	//return result[1];
}

/*********************************************************************//**
 * @brief 		status'u okur
 * @param		None
 * @return		None
 ***********************************************************************/
bool sFLASH_WriteStatus(uint8_t data){
	uint8_t sendBuffer[2];
	uint8_t result[2];
	uint16_t i;
	uint8_t status;
	
	if(sFLASH_WriteEn()==false){
		sFLASH_WriteDis();
		return false;
	}
	
//	for(i=0;i<3;i++){
//		sFLASH_WriteEn();
//		status=sFLASH_ReadStatus();
//		if(status&(0x01<<STATUS_WEL)){
//			break;
//		}
//	}
//	if((status&(0x01<<STATUS_WEL))==0){
//		sFLASH_WriteDis();
//		return FALSE;
//	}
	
	sendBuffer[0]=CMD_WRITE_STATUS_REG;
	sendBuffer[1]=data;
	
	//sFLASH_CS_Force(0);
	xferConfig.tx_data = sendBuffer;
	xferConfig.rx_data = result;
	xferConfig.length = sizeof (sendBuffer);
	xferConfig.tx_cnt=0;
	xferConfig.rx_cnt=0;
	//SPI_ReadWrite(LPC_SPI, &xferConfig, SPI_TRANSFER_POLLING);
	Chip_SSP_RWFrames_Blocking(LPC_SSP, &xferConfig);
	//sFLASH_CS_Force(1);
	
	i=sFLASH_PAGE_WRITE_STATUS;
	do{
		status=sFLASH_ReadStatus();
		osDelay(sFLASH_PAGE_WRITE_STATUS/10);
		i-=sFLASH_PAGE_WRITE_STATUS/10;
		if((i==0)&&(status&(0x01<<STATUS_WIP))){
			sFLASH_WriteDis();
			return false;
		}
	}while(status&(0x01<<STATUS_WIP));
	
	sFLASH_WriteDis();
	return true;
}







/*********************************************************************//**
 * @brief 		Tum flashi temizler
 * @param		None
 * @return		True false
 ***********************************************************************/
bool sFLASH_ChipErase(void){
	uint8_t sendBuffer[1];
	uint8_t result[1];
	uint8_t status;
	uint16_t i;
	
	if(sFLASH_WriteEn()==false){
		sFLASH_WriteDis();
		return false;
	}
//	for(i=0;i<3;i++){
//		sFLASH_WriteEn();
//		status=sFLASH_ReadStatus();
//		if(status&(0x01<<STATUS_WEL)){
//			break;
//		}
//	}
//	if((status&(0x01<<STATUS_WEL))==0){
//		sFLASH_WriteDis();
//		return FALSE;
//	}
	
	sendBuffer[0]=CMD_CHIP_ERASE;
	
	//sFLASH_CS_Force(0);
	xferConfig.tx_data = sendBuffer;
	xferConfig.rx_data = result;
	xferConfig.length = sizeof (sendBuffer);
	xferConfig.tx_cnt=0;
	xferConfig.rx_cnt=0;
	//SPI_ReadWrite(LPC_SPI, &xferConfig, SPI_TRANSFER_POLLING);
	Chip_SSP_RWFrames_Blocking(LPC_SSP, &xferConfig);
	//sFLASH_CS_Force(1);
	
	i=sFLASH_CHIP_ERASE_TIMEOUT;
	do{
		status=sFLASH_ReadStatus();
		osDelay(1000);
		i-=1000;
		if((i==0)&&(status&(0x01<<STATUS_WIP))){
			sFLASH_WriteDis();
			return FALSE;
		}
	}while(status&(0x01<<STATUS_WIP));
	
	sFLASH_WriteDis();
	return TRUE;
}


/*********************************************************************//**
 * @brief 		belirtilen sector'u temizler
 * @param		sector number	4Mbit icin 0-127 arasi her sector 4K boyutundadir
 * @return		True false
 ***********************************************************************/
bool sFLASH_SectorErase(uint16_t sectorNumber){
	uint8_t sendBuffer[4];
	uint8_t result[4];
	uint8_t status;
	uint16_t i;
	uint32_t sectorAddress;
	uint8_t data;
	
//	if(sectorNumber>=sFLASH_MAX_SECTOR_NUMBER){
//		return FALSE;
//	}
	
	sectorAddress=sectorNumber*sFLASH_SECTOR_SIZE;
	
	if(sFLASH_WriteEn()==false){
		sFLASH_WriteDis();
		return false;
	}
//	for(i=0;i<3;i++){
//		sFLASH_WriteEn();
//		status=sFLASH_ReadStatus();
//		if(status&(0x01<<STATUS_WEL)){
//			break;
//		}
//	}
//	if((status&(0x01<<STATUS_WEL))==0){
//		sFLASH_WriteDis();
//		return FALSE;
//	}
	
	
	
	sendBuffer[0]=CMD_SECTOR_ERASE;
	data=(uint8_t)(sectorAddress>>16);
	sendBuffer[1]=data;
	data=(uint8_t)(sectorAddress>>8);
	sendBuffer[2]=data;
	data=(uint8_t)(sectorAddress>>0);
	sendBuffer[3]=data;
	
	//sFLASH_CS_Force(0);
	xferConfig.tx_data = sendBuffer;
	xferConfig.rx_data = result;
	xferConfig.length = sizeof (sendBuffer);
	xferConfig.tx_cnt=0;
	xferConfig.rx_cnt=0;
	//SPI_ReadWrite(LPC_SPI, &xferConfig, SPI_TRANSFER_POLLING);
	Chip_SSP_RWFrames_Blocking(LPC_SSP, &xferConfig);
	//sFLASH_CS_Force(1);
	
	i=sFLASH_SECTOR_ERASE_TIMEOUT;
	do{
		status=sFLASH_ReadStatus();
		osDelay(1000);
		i-=1000;
		if((i==0)&&(status&(0x01<<STATUS_WIP))){
			sFLASH_WriteDis();
			return FALSE;
		}
	}while(status&(0x01<<STATUS_WIP));
	
	sFLASH_WriteDis();
	return TRUE;
}

/*********************************************************************//**
 * @brief 		belirtilen adresten itibaren belirtilen adette veriyi 
 *						programlar. Eger page asiliyorsa hic programlama yapmadan 
 *						hata ile doner
 * @param		
 * @return		True false
 ***********************************************************************/
bool sFLASH_WriteData(uint8_t *data, uint16_t dataLength, uint32_t address){
	uint8_t status;
	uint16_t i;
	uint16_t kalan;
	uint16_t maxDataLength;
	uint8_t sendBuffer[sFLASH_PAGE_SIZE + 4];
	uint8_t result[sFLASH_PAGE_SIZE + 4];
	uint8_t d;
	uint16_t dl;
	uint32_t adr;
	uint8_t *dataptr;
	
	if(dataLength>sFLASH_PAGE_SIZE){
		return false;
	}
	
	
	
	dl=dataLength;
	adr=address;
	dataptr=data;
	kalan = adr % sFLASH_PAGE_SIZE;
	maxDataLength=sFLASH_PAGE_SIZE-kalan;
	
	while(dl>0){
		if(sFLASH_WriteEn()==false){
			sFLASH_WriteDis();
			return false;
		}
		if(dl<=maxDataLength){
			sendBuffer[0]=CMD_PAGE_WRITE;
			d=(uint8_t)(adr>>16);
			sendBuffer[1]=d;
			d=(uint8_t)(adr>>8);
			sendBuffer[2]=d;
			d=(uint8_t)(adr>>0);
			sendBuffer[3]=d;
			
			for(i=0; i<dl; i++){
				sendBuffer[4+i]=*dataptr;
				dataptr++;
			}
			
			xferConfig.tx_data = sendBuffer;
			xferConfig.rx_data = result;
			xferConfig.length = 4+dl;
			xferConfig.tx_cnt=0;
			xferConfig.rx_cnt=0;
			Chip_SSP_RWFrames_Blocking(LPC_SSP, &xferConfig);
			dl=0;
		}else{
			sendBuffer[0]=CMD_PAGE_WRITE;
			d=(uint8_t)(adr>>16);
			sendBuffer[1]=d;
			d=(uint8_t)(adr>>8);
			sendBuffer[2]=d;
			d=(uint8_t)(adr>>0);
			sendBuffer[3]=d;
			
			for(i=0; i<maxDataLength; i++){
				sendBuffer[4+i]=*dataptr;
				dataptr++;
			}
			
			xferConfig.tx_data = sendBuffer;
			xferConfig.rx_data = result;
			xferConfig.length = 4+maxDataLength;
			xferConfig.tx_cnt=0;
			xferConfig.rx_cnt=0;
			Chip_SSP_RWFrames_Blocking(LPC_SSP, &xferConfig);
			dl-=maxDataLength;
			adr+=maxDataLength;
			kalan = adr % sFLASH_PAGE_SIZE;
			maxDataLength=sFLASH_PAGE_SIZE-kalan;
		}
		
		i=sFLASH_PAGE_WRITE_TIMEOUT;
		do{
			status=sFLASH_ReadStatus();
			osDelay(sFLASH_PAGE_WRITE_TIMEOUT/10);
			i-=(sFLASH_PAGE_WRITE_TIMEOUT/10);
			if((i==0)&&(status&(0x01<<STATUS_WIP))){
				sFLASH_WriteDis();
				return false;
			}
		}while(status&(0x01<<STATUS_WIP));
		
	}
	
	sFLASH_WriteDis();
	
	if(sFLASH_ReadData(sendBuffer, dataLength, address)==false){
		return false;
	}
	
	for(i=0; i<dataLength; i++){
		if(sendBuffer[i]!=data[i]){
			return false;
		}
	}
	
	return true;
	

	
}

/*********************************************************************//**
 * @brief 		belirtilen adresten itibaren belirtilen adette veriyi 
 *						okur
 * @param		
 * @return		True false
 ***********************************************************************/
bool sFLASH_ReadData(uint8_t *data, uint16_t dataLength, uint32_t address){
	uint8_t sendBuffer[sFLASH_PAGE_SIZE+4];
	uint8_t result[sFLASH_PAGE_SIZE+4];
	uint8_t d;
	uint16_t i;
	
	if(dataLength>sFLASH_PAGE_SIZE){
		return false;
	}

	sendBuffer[0]=CMD_READ_DATA;
	d=(uint8_t)(address>>16);
	sendBuffer[1]=d;
	d=(uint8_t)(address>>8);
	sendBuffer[2]=d;
	d=(uint8_t)(address>>0);
	sendBuffer[3]=d;
	
	
	
	//sFLASH_CS_Force(0);
	xferConfig.tx_data = sendBuffer;
	xferConfig.rx_data = result;
	xferConfig.length = dataLength+4;
	xferConfig.tx_cnt=0;
	xferConfig.rx_cnt=0;
	//SPI_ReadWrite(LPC_SPI, &xferConfig, SPI_TRANSFER_POLLING);
	Chip_SSP_RWFrames_Blocking(LPC_SSP, &xferConfig);
	
	for(i=0; i<dataLength; i++){
		data[i]=result[i+4];
	}

	return true;
}


/*********************************************************************//**
 * @brief 		flashta olmasi gereken default degerleri yazar
 * @param		
 * @return		True false
 ***********************************************************************/
bool sFLASH_LoadDefaultVal(void){
	sflashGenData.flashInPointer = SFLASH_RECEIPT_START_ADDRESS;
	sflashGenData.flashRecordNumber = 0;
	sflashGenData.receiptNumber = 0;
	
	if(sFLASH_WriteData((uint8_t *)&sflashGenData.flashInPointer, SFLASH_IN_POINTER_SIZE, SFLASH_IN_POINTER)==false){
		return false;
	}
	
	if(sFLASH_WriteData((uint8_t *)&sflashGenData.flashRecordNumber, SFLASH_RECORD_NUMBER_SIZE, SFLASH_RECORD_NUMBER)==false){
		return false;
	}
	
	if(sFLASH_WriteData((uint8_t *)&sflashGenData.receiptNumber, SFLASH_RECEIPT_NUMBER_SIZE, SFLASH_RECEIPT_NUMBER)==false){
		return false;
	}
	return true;
}

/*********************************************************************//**
 * @brief 		kayit yazar
 * @param		
 * @return		True false
 ***********************************************************************/
bool sFlash_WriteRecord(sflashRecordType *record){
	uint8_t sendBuffer[SFLASH_RECEIPT_SIZE];
	
	uint32_t address = sflashGenData.flashInPointer;
	uint32_t recordNumber = sflashGenData.flashRecordNumber;
	uint32_t receiptNumber = sflashGenData.receiptNumber;
	
	record->receiptNumber = sflashGenData.receiptNumber + 1;
	
	
	
	
	
	sendBuffer[0]=record->receiptNumber>>24;
	sendBuffer[1]=record->receiptNumber>>16;
	sendBuffer[2]=record->receiptNumber>>8;
	sendBuffer[3]=record->receiptNumber>>0;
	sendBuffer[4]=record->date>>24;
	sendBuffer[5]=record->date>>16;
	sendBuffer[6]=record->date>>8;
	sendBuffer[7]=record->date>>0;
	sendBuffer[8]=record->hour>>8;
	sendBuffer[9]=record->hour>>0;
	sendBuffer[10]=record->net;
	sendBuffer[11]=record->volOnTop;
	sendBuffer[12]=record->weight>>24;
	sendBuffer[13]=record->weight>>16;
	sendBuffer[14]=record->weight>>8;
	sendBuffer[15]=record->weight>>0;
	sendBuffer[16]=record->weightDotPos;
	sendBuffer[17]=record->weightUnit;
	sendBuffer[18]=record->volume>>24;
	sendBuffer[19]=record->volume>>16;
	sendBuffer[20]=record->volume>>8;
	sendBuffer[21]=record->volume>>0;
	sendBuffer[22]=record->volumeDotPos;
	sendBuffer[23]=record->volumeUnit;
	sendBuffer[24]=record->volumeCoef>>24;
	sendBuffer[25]=record->volumeCoef>>16;
	sendBuffer[26]=record->volumeCoef>>8;
	sendBuffer[27]=record->volumeCoef>>0;
	sendBuffer[28]=record->volumeCoefDotPos;
	
	record->recordCRC = crc16(sendBuffer,(SFLASH_RECEIPT_SIZE-SFLASH_RECORD_CRC_SIZE));
	
	sendBuffer[29]=record->recordCRC>>8;
	sendBuffer[30]=record->recordCRC>>0;
	
	if(sFLASH_WriteData(sendBuffer, SFLASH_RECEIPT_SIZE, sflashGenData.flashInPointer)==false){
		return false;
	}
	
	if((sflashGenData.flashInPointer + SFLASH_RECEIPT_SIZE) >= sflashGenData.lastFlashAddress){
		sflashGenData.flashInPointer = SFLASH_RECEIPT_START_ADDRESS;
	}else{
		sflashGenData.flashInPointer += SFLASH_RECEIPT_SIZE;
	}
	
	if(sflashGenData.flashRecordNumber<sflashGenData.maxRecordNumber){
		sflashGenData.flashRecordNumber++;
	}
	
	if(sFLASH_WriteData((uint8_t *)&sflashGenData.flashInPointer, SFLASH_IN_POINTER_SIZE, SFLASH_IN_POINTER)==false){
		sflashGenData.flashInPointer = address;
		sflashGenData.flashRecordNumber = recordNumber;
		sflashGenData.receiptNumber = receiptNumber;
		sFLASH_WriteData((uint8_t *)&sflashGenData.flashInPointer, SFLASH_IN_POINTER_SIZE, SFLASH_IN_POINTER);
		sFLASH_WriteData((uint8_t *)&sflashGenData.flashRecordNumber, SFLASH_RECORD_NUMBER_SIZE, SFLASH_RECORD_NUMBER);
		sFLASH_WriteData((uint8_t *)&sflashGenData.receiptNumber, SFLASH_RECEIPT_NUMBER_SIZE, SFLASH_RECEIPT_NUMBER);
		return false;
	}
	
	if(sFLASH_WriteData((uint8_t *)&sflashGenData.flashRecordNumber, SFLASH_RECORD_NUMBER_SIZE, SFLASH_RECORD_NUMBER)==false){
		sflashGenData.flashInPointer = address;
		sflashGenData.flashRecordNumber = recordNumber;
		sflashGenData.receiptNumber = receiptNumber;
		sFLASH_WriteData((uint8_t *)&sflashGenData.flashInPointer, SFLASH_IN_POINTER_SIZE, SFLASH_IN_POINTER);
		sFLASH_WriteData((uint8_t *)&sflashGenData.flashRecordNumber, SFLASH_RECORD_NUMBER_SIZE, SFLASH_RECORD_NUMBER);
		sFLASH_WriteData((uint8_t *)&sflashGenData.receiptNumber, SFLASH_RECEIPT_NUMBER_SIZE, SFLASH_RECEIPT_NUMBER);
		return false;
	}
	
	sflashGenData.receiptNumber++;
	
	if(sFLASH_WriteData((uint8_t *)&sflashGenData.receiptNumber, SFLASH_RECEIPT_NUMBER_SIZE, SFLASH_RECEIPT_NUMBER)==false){
		sflashGenData.flashInPointer = address;
		sflashGenData.flashRecordNumber = recordNumber;
		sflashGenData.receiptNumber = receiptNumber;
		sFLASH_WriteData((uint8_t *)&sflashGenData.flashInPointer, SFLASH_IN_POINTER_SIZE, SFLASH_IN_POINTER);
		sFLASH_WriteData((uint8_t *)&sflashGenData.flashRecordNumber, SFLASH_RECORD_NUMBER_SIZE, SFLASH_RECORD_NUMBER);
		sFLASH_WriteData((uint8_t *)&sflashGenData.receiptNumber, SFLASH_RECEIPT_NUMBER_SIZE, SFLASH_RECEIPT_NUMBER);
		return false;
	}
	
	return true;
}

/*********************************************************************//**
 * @brief 		kayit okur
 * @param		
 * @return		True false
 ***********************************************************************/
//bool sFlash_ReadRecord(sflashRecordType *record, uint32_t recordNumber){
//	uint32_t recordAddress;
//	uint32_t i;
//	uint8_t readData[SFLASH_RECEIPT_SIZE];
//	uint16_t crc;
//	
//	recordAddress = sflashGenData.flashInPointer;
//	
//	for(i=sflashGenData.flashRecordNumber; i>=recordNumber; i--){
//		if((recordAddress - SFLASH_RECEIPT_SIZE) < SFLASH_RECEIPT_START_ADDRESS){
//			recordAddress = sflashGenData.lastFlashAddress + 1 - SFLASH_RECEIPT_SIZE;
//		}else{
//			recordAddress -= SFLASH_RECEIPT_SIZE;
//		}
//	}
//	
//	if (sFLASH_ReadData(readData, SFLASH_RECEIPT_SIZE, recordAddress)==false){
//		return false;
//	}
//	
//	record->receiptNumber = (((uint32_t)readData[0])<<24) | (((uint32_t)readData[1])<<16) | (((uint32_t)readData[2])<<8) | ((uint32_t)readData[3]);
//	record->date = (((uint32_t)readData[4])<<24) | (((uint32_t)readData[5])<<16) | (((uint32_t)readData[6])<<8) | ((uint32_t)readData[7]);
//	record->hour = (((uint32_t)readData[8])<<8) | ((uint32_t)readData[9]);
//	record->net = readData[10];
//	record->volOnTop = readData[11];
//	record->weight = (((uint32_t)readData[12])<<24) | (((uint32_t)readData[13])<<16) | (((uint32_t)readData[14])<<8) | ((uint32_t)readData[15]);
//	record->weightDotPos = readData[16];
//	record->weightUnit = readData[17];
//	record->volume = (((uint32_t)readData[18])<<24) | (((uint32_t)readData[19])<<16) | (((uint32_t)readData[20])<<8) | ((uint32_t)readData[21]);
//	record->volumeDotPos = readData[22];
//	record->volumeUnit = readData[23];
//	record->volumeCoef = (((uint32_t)readData[24])<<24) | (((uint32_t)readData[25])<<16) | (((uint32_t)readData[26])<<8) | ((uint32_t)readData[27]);
//	record->volumeCoefDotPos = readData[28];
//	record->recordCRC = (((uint32_t)readData[29])<<8) | ((uint32_t)readData[30]);

//	crc = crc16((uint8_t *)record,(SFLASH_RECEIPT_SIZE-SFLASH_RECORD_CRC_SIZE));
//	
//	if(record->recordCRC != crc){
//		return false;
//	}
//	
//	return true;
//	
//}

/*********************************************************************//**
 * @brief 		toplam kayit sayisini verir
 * @param		
 * @return		True false
 ***********************************************************************/
uint32_t sFLASH_GetTotalRecordNumber(void){
	return sflashGenData.flashRecordNumber;
}

/*********************************************************************//**
 * @brief 		son kayittan ofset kadar onceki kayit bilgisi ile doner
 * @param		
 * @return		True false
 ***********************************************************************/
bool sFLASH_GetLastRecord(sflashRecordType *record, uint32_t ofset){
	uint32_t totalRecordNumber=sflashGenData.flashRecordNumber;
	uint32_t recordAddress;
	
	if(totalRecordNumber<=ofset){
		return false;
	}
	
	recordAddress = sflashGenData.flashInPointer;
	
	ofset++;
	do{
		recordAddress -= SFLASH_RECEIPT_SIZE;
		
		if(recordAddress < SFLASH_RECEIPT_START_ADDRESS){
			recordAddress = sflashGenData.lastFlashAddress - SFLASH_RECEIPT_SIZE;
		}
		ofset--;
	}while(ofset > 0);
	
	return sFlash_ReadRecord(record, recordAddress);
}

/*********************************************************************//**
 * @brief 		kayit okur
 * @param		
 * @return		True false
 ***********************************************************************/
bool sFlash_ReadRecord(sflashRecordType *record, uint32_t recordAddress){
//	uint32_t recordAddress;
//	uint32_t i;
	uint8_t readData[SFLASH_RECEIPT_SIZE];
	uint16_t crc;
	uint8_t errorCnt=0;
	
//	recordAddress = sflashGenData.flashInPointer;
//	
//	for(i=sflashGenData.flashRecordNumber; i>=recordNumber; i--){
//		if((recordAddress - SFLASH_RECEIPT_SIZE) < SFLASH_RECEIPT_START_ADDRESS){
//			recordAddress = sflashGenData.lastFlashAddress + 1 - SFLASH_RECEIPT_SIZE;
//		}else{
//			recordAddress -= SFLASH_RECEIPT_SIZE;
//		}
//	}
	
	while(true){
		
		if (sFLASH_ReadData(readData, SFLASH_RECEIPT_SIZE, recordAddress)==false){
			return false;
		}
		
		record->receiptNumber = (((uint32_t)readData[0])<<24) | (((uint32_t)readData[1])<<16) | (((uint32_t)readData[2])<<8) | ((uint32_t)readData[3]);
		record->date = (((uint32_t)readData[4])<<24) | (((uint32_t)readData[5])<<16) | (((uint32_t)readData[6])<<8) | ((uint32_t)readData[7]);
		record->hour = (((uint32_t)readData[8])<<8) | ((uint32_t)readData[9]);
		record->net = readData[10];
		record->volOnTop = readData[11];
		record->weight = (((uint32_t)readData[12])<<24) | (((uint32_t)readData[13])<<16) | (((uint32_t)readData[14])<<8) | ((uint32_t)readData[15]);
		record->weightDotPos = readData[16];
		record->weightUnit = readData[17];
		record->volume = (((uint32_t)readData[18])<<24) | (((uint32_t)readData[19])<<16) | (((uint32_t)readData[20])<<8) | ((uint32_t)readData[21]);
		record->volumeDotPos = readData[22];
		record->volumeUnit = readData[23];
		record->volumeCoef = (((uint32_t)readData[24])<<24) | (((uint32_t)readData[25])<<16) | (((uint32_t)readData[26])<<8) | ((uint32_t)readData[27]);
		record->volumeCoefDotPos = readData[28];
		record->recordCRC = (((uint32_t)readData[29])<<8) | ((uint32_t)readData[30]);

		crc = crc16(readData,(SFLASH_RECEIPT_SIZE-SFLASH_RECORD_CRC_SIZE));
		
		if(record->recordCRC != crc){
			errorCnt++;
			if(errorCnt==5){
				return false;
			}
		}else{
			return true;
		}
		osDelay(1);
	}
	
	
	
}

/*********************************************************************//**
 * @brief 		kayit okur
 * @param		
 * @return		True false
 ***********************************************************************/
void sFlash_PinSleep(void){
	Chip_IOCON_PinMuxSet(LPC_IOCON, sFLASH_CS_PORT_NUM, sFLASH_CS_PIN_NUM, IOCON_FUNC0 | IOCON_MODE_INACT );
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, sFLASH_CS_PORT_NUM, sFLASH_CS_PIN_NUM); 
	Chip_GPIO_SetPinState(LPC_GPIO, sFLASH_CS_PORT_NUM, sFLASH_CS_PIN_NUM	, true);
	
	Chip_IOCON_PinMuxSet(LPC_IOCON, sFLASH_CLK_PORT_NUM, sFLASH_CLK_PIN_NUM, IOCON_FUNC0 | IOCON_MODE_INACT );
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, sFLASH_CLK_PORT_NUM, sFLASH_CLK_PIN_NUM); 
	Chip_GPIO_SetPinState(LPC_GPIO, sFLASH_CLK_PORT_NUM, sFLASH_CLK_PIN_NUM	, false);
	
	Chip_IOCON_PinMuxSet(LPC_IOCON, sFLASH_MOSI_PORT_NUM, sFLASH_MOSI_PIN_NUM, IOCON_FUNC0 | IOCON_MODE_INACT );
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, sFLASH_MOSI_PORT_NUM, sFLASH_MOSI_PIN_NUM); 
	Chip_GPIO_SetPinState(LPC_GPIO, sFLASH_MOSI_PORT_NUM, sFLASH_MOSI_PIN_NUM	, false);
	
	Chip_IOCON_PinMuxSet(LPC_IOCON, sFLASH_MISO_PORT_NUM, sFLASH_MISO_PIN_NUM, IOCON_FUNC0 | IOCON_MODE_PULLUP );
	Chip_GPIO_SetPinDIRInput(LPC_GPIO, sFLASH_MISO_PORT_NUM, sFLASH_MISO_PIN_NUM); 
	
	
}

