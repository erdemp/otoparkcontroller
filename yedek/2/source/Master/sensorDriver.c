#include "sensorDriver.h"


/****************************************************************
* sensor holding register haritasi
***************************************************************/
#define SENSOR_MODBUS_ADRES_HR			4001
#define SENSOR_MODBUS_HAB_AYARI_HR	4002
#define SENSOR_MODBUS_BAUDRATE_HR		4003 
#define SENSOR_MODBUS_RESET_HR			4004
#define SENSOR_MODBUS_FIRMWARE_UPDATE_HR		4005
#define SENSOR_MODBUS_LOCK_HR				4006
#define SENSOR_MODBUS_DURUM_HR			4007
#define SENSOR_MODBUS_MESAFE_HR			4008
#define SENSOR_MODBUS_LIMIT_DEGER_HR		4009
#define SENSOR_MODBUS_FILTRE_DEGERI_HR		4010
#define SENSOR_MODBUS_MOD_HR				4011 
#define SENSOR_MODBUS_LED_R_HR		4012
#define SENSOR_MODBUS_LED_G_HR		4013
#define SENSOR_MODBUS_LED_B_HR		4014
#define SENSOR_MODBUS_DEFAULT_HR		4015
#define SENSOR_MODBUS_WRITE_ERROR_HR 4016
#define SENSOR_MODBUS_URETIM_SAYAC_HR		4017
#define SENSOR_MODBUS_SINYAL_ADET_HR		4018
#define SENSOR_MODBUS_ZONE1_DELTAV_HR		4019
#define SENSOR_MODBUS_ZONE2_DELTAV_HR		4020
#define SENSOR_MODBUS_ZONE3_DELTAV_HR		4021
#define SENSOR_MODBUS_ZONE4_DELTAV_HR		4022
#define SENSOR_MODBUS_ZONE5_DELTAV_HR		4023
#define SENSOR_MODBUS_ZONE6_DELTAV_HR		4024
#define SENSOR_MODBUS_IKI_CLK_DIS_HR		4025
#define SENSOR_MODBUS_IKI_CLK_DELTAV_HR		4026





/***********************************************************************
 * @brief 		Sensor ledinin kontrolunu degistirmek icin kullanilir
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek sensorun modbus adresi
 *					sensorMod: sensor veya merkez secimi
 * @return		true, false
 ***********************************************************************/
bool sensorModuDegistir(LPC_USART_T *port, uint8_t adres, uint8_t sensorMod){
	uint16_t data;
	
	if(sensorMod==sensorMod_Sensor){
		data=0;
	}else{
		data=1;
	}
	
	return modbusWriteHoldingReg(port, adres, SENSOR_MODBUS_MOD_HR, data);

}
/***********************************************************************
 * @brief 		Sensor calisma modunu okur
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek sensorun modbus adresi
 *					sensorMod: sensor veya merkez secimi
 * @return		true, false
 ***********************************************************************/
bool sensorModuOku(LPC_USART_T *port, uint8_t adres, sensorModType *sensorMod){
	uint16_t data;
	
	
	if ( modbusReadHoldingReg(port,adres,SENSOR_MODBUS_MOD_HR,&data,100) == false ) {
		return false;
	}else {
		if (data == 1 ) {
			*sensorMod = sensorMod_Merkez;
		}else {
			*sensorMod = sensorMod_Sensor;
		}
	}
	return true;

}
/***********************************************************************
 * @brief 		sensorde limit deger degistirmek icin kullanilir
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek sensorun modbus adresi
 *					limitDeger: degistirilecek limit deger
 * @return		true, false
 ***********************************************************************/
bool sensorLimitDegerDegistir(LPC_USART_T *port, uint8_t adres, uint16_t limitDeger){
	
	return modbusWriteHoldingReg(port, adres, SENSOR_MODBUS_LIMIT_DEGER_HR, limitDeger);
}

/***********************************************************************
 * @brief 		sensorde limit deger degistirmek icin kullanilir
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek sensorun modbus adresi
 *					limitDeger: degistirilecek limit deger
 * @return		true, false
 ***********************************************************************/
bool sensorLimitDegerOku(LPC_USART_T *port, uint8_t adres, uint16_t *limitDeger){
	
	return modbusReadHoldingReg(port, adres, SENSOR_MODBUS_LIMIT_DEGER_HR, limitDeger,100);
}

/***********************************************************************
 * @brief 		sensor merkez kontrollu modda ise R ledini merkezden acma kapamaya yarar
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek sensorun modbus adresi
 *					dataR: 0 - 250 arasi. 0 kapali
 * @return		true, false
 ***********************************************************************/
bool sensorLed_R_Yak(LPC_USART_T *port, uint8_t adres, uint8_t dataR){
	return modbusWriteHoldingReg(port, adres, SENSOR_MODBUS_LED_R_HR, (uint16_t)dataR);
}

/***********************************************************************
 * @brief 		sensor merkez kontrollu modda ise G ledini merkezden acma kapamaya yarar
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek sensorun modbus adresi
 *					dataG: 0 - 250 arasi. 0 kapali
 * @return		true, false
 ***********************************************************************/
bool sensorLed_G_Yak(LPC_USART_T *port, uint8_t adres, uint8_t dataG){
	
	return modbusWriteHoldingReg(port, adres, SENSOR_MODBUS_LED_G_HR, (uint16_t)dataG);
}

/***********************************************************************
 * @brief 		sensor merkez kontrollu modda ise B ledini merkezden acma kapamaya yarar
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek sensorun modbus adresi
 *					dataB: 0 - 250 arasi. 0 kapali
 * @return		true, false
 ***********************************************************************/
bool sensorLed_B_Yak(LPC_USART_T *port, uint8_t adres, uint8_t dataB){
	
	return modbusWriteHoldingReg(port, adres, SENSOR_MODBUS_LED_B_HR, (uint16_t)dataB);
}

/***********************************************************************
 * @brief 	dolu bos karar vermesi icin isletilen filtre degeri
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek sensorun modbus adresi
 *					dataFiltre: 0 - 10 arasi
 * @return		true, false
 ***********************************************************************/
bool sensorFiltreDegeriDegistir(LPC_USART_T *port, uint8_t adres, uint8_t dataFiltre){
	
	return modbusWriteHoldingReg(port, adres, SENSOR_MODBUS_FILTRE_DEGERI_HR, (uint16_t)dataFiltre);
}

/***********************************************************************
 * @brief 	sensor durum bilgisi alir. true dolu, false bos
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek sensorun modbus adresi
 *					durum: true ise dolu, false ise bos
 * @return		true, false (sensor ile haberlesme sikintisi var ise false
 ***********************************************************************/
bool sensorDurum(LPC_USART_T *port, uint8_t adres, uint8_t *durum){
	uint16_t i;
	if(modbusReadHoldingReg(port, adres, SENSOR_MODBUS_DURUM_HR, &i,100)){
		if(i){
			*durum=true;
		}else{
			*durum=false;
		}
		return true;
	}
	return false;
}

/***********************************************************************
 * @brief 	sensoru yeniden baslatir
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek sensorun modbus adresi
 * @return		true, false (sensor ile haberlesme sikintisi var ise false)
 ***********************************************************************/
bool sensorReset(LPC_USART_T *port, uint8_t adres){
	return modbusWriteHoldingReg(port, adres, SENSOR_MODBUS_RESET_HR, 1);
}

/***********************************************************************
 * @brief 	sensorun olctugu mesafeyi okur
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek sensorun modbus adresi
 *					mesafe: sensorun okudugu mesafe
 * @return		true, false (sensor ile haberlesme sikintisi var ise false)
 ***********************************************************************/
bool sensorMesafeOku(LPC_USART_T *port, uint8_t adres, uint16_t *mesafe){
	return modbusReadHoldingReg(port, adres, SENSOR_MODBUS_MESAFE_HR, mesafe, 100);
}

/***********************************************************************
 * @brief 	sensoru kilitli registerlara yazmak i�in kilit acar
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek sensorun modbus adresi
*						enable: true ise kilit acar false ise kapatir
 * @return		true, false (sensor ile haberlesme sikintisi var ise false)
 ***********************************************************************/
bool sensorKilitAc(LPC_USART_T *port, uint8_t adres, bool enable){
	if(enable){
		return modbusWriteHoldingReg(port, adres, SENSOR_MODBUS_LOCK_HR, 1071);
	}
	return modbusWriteHoldingReg(port, adres, SENSOR_MODBUS_LOCK_HR, 0);
}

/***********************************************************************
 * @brief 	sensorun uretim sayac registerina yazar
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek sensorun modbus adresi
 *					dataSayac: yazilmak istenen deger
 * @return		true, false (sensor ile haberlesme sikintisi var ise false)
 ***********************************************************************/
bool sensorUretimSayacYaz(LPC_USART_T *port, uint8_t adres, uint16_t dataSayac){
	
	return modbusWriteHoldingReg(port, adres, SENSOR_MODBUS_URETIM_SAYAC_HR, dataSayac);
}

/***********************************************************************
 * @brief 	sensorun uretim sayac registerina yazar
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek sensorun modbus adresi
 *					dataSayac: okunan deger buraya donulur
 * @return		true, false (sensor ile haberlesme sikintisi var ise false)
 ***********************************************************************/
bool sensorUretimSayacOku(LPC_USART_T *port, uint8_t adres, uint16_t *dataSayac){
	return modbusReadHoldingReg(port, adres, SENSOR_MODBUS_URETIM_SAYAC_HR, dataSayac, 100);
}

/***********************************************************************
 * @brief 	sensoru fabrika ayarlarina geri dondurur
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek sensorun modbus adresi
 * @return		true, false (sensor ile haberlesme sikintisi var ise false)
 ***********************************************************************/
bool sensorFabrikaAyarlarinaDon(LPC_USART_T *port, uint8_t adres){
	return modbusWriteHoldingReg(port, adres, SENSOR_MODBUS_DEFAULT_HR, 1001);
}

/***********************************************************************
 * @brief 	sensorun adresini degistirir.
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek sensorun modbus adresi
*					yeniAdres: yeni adres girilir
 * @return		true, false (sensor ile haberlesme sikintisi var ise false)
 ***********************************************************************/
bool sensorAdresDegistir(LPC_USART_T *port, uint8_t adres, uint8_t yeniAdres){
	
	return modbusWriteHoldingReg(port, adres, SENSOR_MODBUS_ADRES_HR, yeniAdres);
}

/***********************************************************************
 * @brief 	sensorun adresini degistirir.
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek sensorun modbus adresi
*					data: yeni haberlesme ayari
 * @return		true, false (sensor ile haberlesme sikintisi var ise false)
 ***********************************************************************/
bool sensorHabAyarDegistir(LPC_USART_T *port, uint8_t adres, haberlesmeAyarType data){
	uint16_t reg;
	
	if(data==haberlesmeAyar_8N2){
		reg=0;
	}else if(data==haberlesmeAyar_8E1){
		reg=1;
	}else{
		reg=2;
	}
	return modbusWriteHoldingReg(port, adres, SENSOR_MODBUS_HAB_AYARI_HR, reg);
}

/***********************************************************************
 * @brief 	sensorun haberlesme hizini degistirir
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek sensorun modbus adresi
*					baudrate: yeni baudrate
 * @return		true, false (sensor ile haberlesme sikintisi var ise false)
 ***********************************************************************/
bool sensorBaudrateDegistir(LPC_USART_T *port, uint8_t adres, uint32_t baudrate){
	uint16_t reg;
	if(baudrate==9600){
		reg=0;
	}else if(baudrate==19200){
		reg=1;
	}else if(baudrate==38400){
		reg=2;
	}else if(baudrate==57600){
		reg=3;
	}else{//115200
		reg=4;
	}
	return modbusWriteHoldingReg(port, adres, SENSOR_MODBUS_BAUDRATE_HR, reg);
}

/***********************************************************************
 * @brief 	sensorun esik degerlerini degistirir
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek sensorun modbus adresi
*					esik1,esik2,esik3,esik4,esik5,esik6: yeni esik degerler
 * @return		true, false (sensor ile haberlesme sikintisi var ise false)
 ***********************************************************************/
bool sensorEsikDegerDegistir(LPC_USART_T *port, uint8_t adres, uint16_t esik1, uint16_t esik2, uint16_t esik3, uint16_t esik4, uint16_t esik5, uint16_t esik6){
	
	if(modbusWriteHoldingReg(port, adres, SENSOR_MODBUS_ZONE1_DELTAV_HR, esik1)==false){
		return false;
	}
	if(modbusWriteHoldingReg(port, adres, SENSOR_MODBUS_ZONE2_DELTAV_HR, esik2)==false){
		return false;
	}
	if(modbusWriteHoldingReg(port, adres, SENSOR_MODBUS_ZONE3_DELTAV_HR, esik3)==false){
		return false;
	}
	if(modbusWriteHoldingReg(port, adres, SENSOR_MODBUS_ZONE4_DELTAV_HR, esik4)==false){
		return false;
	}
	if(modbusWriteHoldingReg(port, adres, SENSOR_MODBUS_ZONE5_DELTAV_HR, esik5)==false){
		return false;
	}
	if(modbusWriteHoldingReg(port, adres, SENSOR_MODBUS_ZONE6_DELTAV_HR, esik6)==false){
		return false;
	}
	
	return true;
}

/***********************************************************************
 * @brief 	sensorun gonderdigi pulse sayisi
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek sensorun modbus adresi
*					data: pulse adedi
 * @return		true, false (sensor ile haberlesme sikintisi var ise false)
 ***********************************************************************/
bool sensorSinyalAdet(LPC_USART_T *port, uint8_t adres, uint8_t data){
	return modbusWriteHoldingReg(port, adres, SENSOR_MODBUS_SINYAL_ADET_HR, (uint16_t)data);
}

/***********************************************************************
 * @brief 	sensoru iki clock moduna alir cikarir
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek sensorun modbus adresi
 *					enable: iki clock modunu acar kapatir
 * @return		true, false (sensor ile haberlesme sikintisi var ise false)
 ***********************************************************************/
bool sensorIkiClockModu(LPC_USART_T *port, uint8_t adres, bool enable){
	if(enable){
		return modbusWriteHoldingReg(port, adres, SENSOR_MODBUS_IKI_CLK_DIS_HR, 1);
	}else{
		return modbusWriteHoldingReg(port, adres, SENSOR_MODBUS_IKI_CLK_DIS_HR, 0);
	}
}

/***********************************************************************
 * @brief 	sensoru iki clock modundaki deltaV degeri
 * @param		portNo: haberlesilecek UART numarasi
 *					adres: haberlesilecek sensorun modbus adresi
 *					deltaV: deltaV degeri
 * @return		true, false (sensor ile haberlesme sikintisi var ise false)
 ***********************************************************************/
bool sensorIkiClockDeltaV_Yaz(LPC_USART_T *port, uint8_t adres, uint16_t deltaV){
	
	return modbusWriteHoldingReg(port, adres, SENSOR_MODBUS_IKI_CLK_DELTAV_HR, deltaV);
}

