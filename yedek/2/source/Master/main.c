#include "cmsis_os.h"      // CMSIS-RTOS API Functions
//#include "chip.h"
#include "mainRTX.h"
//#include "power.h"
//#include "appUart.h"


const uint32_t OscRateIn=12000000;
const uint32_t ExtRateIn=0;
const uint32_t RTCOscRateIn=32768;

/* Add Private Types */
/* <<add private type here >> */

/* Add Private Variables */
/* <<add private variables here >> */

/* Add Private Functions */
/* <<add private functions here >> */

/**
 * @brief Main program body
 */
int c_entry(void)
{
	/* <<add code here >> */
	SystemCoreClockUpdate();
 	startRTX();
	
	
	return 1;
}










/* Support required entry point for other toolchain */
int main (void)
 {


	 Chip_GPIO_Init(LPC_GPIO);
	/*Chip_IOCON_Init(LPC_IOCON);
	
	Chip_RTC_Init(LPC_RTC);
	Chip_RTC_Enable(LPC_RTC, ENABLE);
	*/


	 c_entry();
	 
	 for(;;) {
		 osThreadYield();
	 }
	 
	// return 1;
}


