#ifndef BSP_H_
#define BSP_H_

#include "chip.h"

#ifdef __cplusplus
extern "C"
{
#endif
	//-----------------------RS485 port definitions----------------------------------
	
	#define RS485_Port_1		LPC_UART0
	#define RS485_Port_2		LPC_UART3
	#define RS485_Port_3		LPC_UART2
	#define RS485_Port_4		LPC_UART1
	
	#define RS485_Port1_IRQn	UART0_IRQn
	#define RS485_Port2_IRQn	UART3_IRQn
	#define RS485_Port3_IRQn	UART2_IRQn
	#define RS485_Port4_IRQn	UART1_IRQn
	
	#define RS485_Port1_IRQHandler			UART0_IRQHandler
	#define RS485_Port2_IRQHandler			UART3_IRQHandler
	#define RS485_Port3_IRQHandler			UART2_IRQHandler
	#define RS485_Port4_IRQHandler			UART1_IRQHandler
	
	//-----------------------RS485 definitions----------------------------------
	#define RS485_Port_1_Txd_Port							0
	#define RS485_Port_1_Txd_Pin							2
	#define RS485_Port_1_Txd_IOCON						(IOCON_FUNC1 /*| IOCON_MODE_INACT*/)
	
	#define RS485_Port_1_Rxd_Port	0
	#define RS485_Port_1_Rxd_Pin		3
	#define RS485_Port_1_Rxd_IOCON							(IOCON_FUNC1 /*| IOCON_MODE_PULLUP*/)
	
	#define RS485_Port_1_DD_Port		1
	#define RS485_Port_1_DD_Pin		25
	#define RS485_Port_1_DD_IOCON							IOCON_FUNC0
	
	#define RS485_Port_2_Txd_Port	0
	#define RS485_Port_2_Txd_Pin		0
	#define RS485_Port_2_Txd_IOCON							(IOCON_FUNC2 | IOCON_MODE_INACT)
	
	#define RS485_Port_2_Rxd_Port	0
	#define RS485_Port_2_Rxd_Pin		1
	#define RS485_Port_2_Rxd_IOCON							(IOCON_FUNC2 | IOCON_MODE_PULLUP)
	
	#define RS485_Port_2_DD_Port		1
	#define RS485_Port_2_DD_Pin		29
	#define RS485_Port_2_DD_IOCON							IOCON_FUNC0
	
	#define RS485_Port_3_Txd_Port	0
	#define RS485_Port_3_Txd_Pin		10
	#define RS485_Port_3_Txd_IOCON							(IOCON_FUNC1 | IOCON_MODE_INACT)
	
	#define RS485_Port_3_Rxd_Port	0
	#define RS485_Port_3_Rxd_Pin		11
	#define RS485_Port_3_Rxd_IOCON							(IOCON_FUNC1 | IOCON_MODE_PULLUP)
	
	#define RS485_Port_3_DD_Port		2
	#define RS485_Port_3_DD_Pin		13
	#define RS485_Port_3_DD_IOCON							IOCON_FUNC0
	
	#define RS485_Port_4_Txd_Port	2
	#define RS485_Port_4_Txd_Pin		0
	#define RS485_Port_4_Txd_IOCON							(IOCON_FUNC2 | IOCON_MODE_INACT)
	
	#define RS485_Port_4_Rxd_Port	2
	#define RS485_Port_4_Rxd_Pin		1
	#define RS485_Port_4_Rxd_IOCON							(IOCON_FUNC2 | IOCON_MODE_PULLUP)
	
	#define RS485_Port_4_DD_Port		2
	#define RS485_Port_4_DD_Pin		2
	#define RS485_Port_4_DD_IOCON							IOCON_FUNC0
	//--------------------------------------------------------------------------
	
	//-----------------------Relay definitions----------------------------------
	#define Relay1_Port				2
	#define Relay1_Pin				5
	#define Relay1_IOCON			(IOCON_FUNC0 | IOCON_MODE_INACT)
	
	#define Relay2_Port				2
	#define Relay2_Pin				4
	#define Relay2_IOCON			(IOCON_FUNC0 | IOCON_MODE_INACT)
	//--------------------------------------------------------------------------
	
	//-----------------------Input definitions----------------------------------
	#define Input1_Port				0
	#define Input1_Pin				5
	#define Input1_IOCON			(IOCON_FUNC0 | IOCON_MODE_PULLUP)
	
	#define Input2_Port				0
	#define Input2_Pin				4
	#define Input2_IOCON			(IOCON_FUNC0 | IOCON_MODE_PULLUP)
	
	#define Ang_Input1_Port		0
	#define Ang_Input1_Pin		26
	#define Ang_Input1_IOCON	(IOCON_FUNC1 | IOCON_MODE_INACT)
	//--------------------------------------------------------------------------
	
	//-----------------------keyboard definitions----------------------------------
	#define Key_Row1_Port				2
	#define Key_Row1_Pin				8
	#define Key_Row1_IOCON			(IOCON_FUNC0 | IOCON_MODE_INACT)
	#define	Key_Row1_Sleep_IOCON (IOCON_FUNC0 | IOCON_MODE_INACT)
	
	#define Key_Row2_Port				0
	#define Key_Row2_Pin				16
	#define Key_Row2_IOCON			(IOCON_FUNC0 | IOCON_MODE_INACT)
	#define	Key_Row2_Sleep_IOCON (IOCON_FUNC0 | IOCON_MODE_INACT)
	
	#define Key_Row3_Port				0
	#define Key_Row3_Pin				15
	#define Key_Row3_IOCON			(IOCON_FUNC0 | IOCON_MODE_INACT)
	#define	Key_Row3_Sleep_IOCON (IOCON_FUNC0 | IOCON_MODE_INACT)
	
	#define Key_Col1_Port				2
	#define Key_Col1_Pin				7
	#define Key_Col1_IOCON			(IOCON_FUNC0 | IOCON_MODE_PULLUP)
	#define	Key_Col1_Sleep_IOCON (IOCON_FUNC0 | IOCON_MODE_INACT)
	
	#define Key_Col2_Port				2
	#define Key_Col2_Pin				9
	#define Key_Col2_IOCON			(IOCON_FUNC0 | IOCON_MODE_PULLUP)
	#define	Key_Col2_Sleep_IOCON (IOCON_FUNC0 | IOCON_MODE_INACT)
	//--------------------------------------------------------------------------
	
	//-----------------------LCD definitions----------------------------------
	#define LCD_Data_Port				0
	#define LCD_Data_Pin				17
	#define LCD_Data_IOCON			(IOCON_FUNC0 | IOCON_MODE_INACT)
	
	#define LCD_CLK_Port				0
	#define LCD_CLK_Pin					19
	#define LCD_CLK_IOCON				(IOCON_FUNC0 | IOCON_MODE_INACT)
	
	#define LCD_LE_Port					0
	#define LCD_LE_Pin					18
	#define LCD_LE_IOCON				(IOCON_FUNC0 | IOCON_MODE_INACT)
	//--------------------------------------------------------------------------
	
	//-----------------------LED definitions----------------------------------
	#define LED1_Port						0
	#define LED1_Pin						21
	#define LED1_IOCON					(IOCON_FUNC0 | IOCON_MODE_INACT)
	
	#define LED2_Port						0
	#define LED2_Pin						20
	#define LED2_IOCON					(IOCON_FUNC0 | IOCON_MODE_INACT)
	
	#define LED3_Port						2
	#define LED3_Pin						10
	#define LED3_IOCON					(IOCON_FUNC0 | IOCON_MODE_INACT)
	
	#define LED4_Port						0
	#define LED4_Pin						22
	#define LED4_IOCON					(IOCON_FUNC0 | IOCON_MODE_INACT)
	
	#define LED5_Port						2
	#define LED5_Pin						11
	#define LED5_IOCON					(IOCON_FUNC0 | IOCON_MODE_INACT)
	
	#define LED6_Port						2
	#define LED6_Pin						12
	#define LED6_IOCON					(IOCON_FUNC0 | IOCON_MODE_INACT)
	//--------------------------------------------------------------------------
	
	//-----------------------USB definitions----------------------------------
	#define USB_VBUS_Port				1
	#define USB_VBUS_Pin				30
	#define USB_DP_Port					0
	#define USB_DP_Pin					30
	#define USB_DN_Port					0
	#define USB_DN_Pin					29
	//--------------------------------------------------------------------------
	
	//-----------------------Serial Flash Memory definitions--------------------
	#define sFlash_CS_Port			0
	#define sFlash_CS_Pin				6
	#define sFlash_CS_IOCON			(IOCON_FUNC2 | IOCON_MODE_INACT)
	
	#define sFlash_SCK_Port			0
	#define sFlash_SCK_Pin			7
	#define sFlash_SCK_IOCON		(IOCON_FUNC2 | IOCON_MODE_INACT)
	
	#define sFlash_MISO_Port		0
	#define sFlash_MISO_Pin			8
	#define sFlash_MISO_IOCON		(IOCON_FUNC2 | IOCON_MODE_PULLUP)
	
	#define sFlash_MOSI_Port		0
	#define sFlash_MOSI_Pin			9
	#define sFlash_MOSI_IOCON		(IOCON_FUNC2 | IOCON_MODE_INACT)
	//--------------------------------------------------------------------------
	
	//-----------------------Ethernet Module definitions--------------------
	#define eNet_SSP						LPC_SSP0
	
	#define eNet_SSP_CLK				 SYSCTL_CLOCK_SSP0
	
	#define eNet_RDY_Port				1
	#define eNet_RDY_Pin				22
	#define eNet_RDY_IOCON		(IOCON_FUNC0 )
	
	#define eNet_RST_Port				1
	#define eNet_RST_Pin				19
	#define eNet_RST_IOCON		(IOCON_FUNC0 | IOCON_MODE_PULLUP)
	
	#define eNet_INT_Port				1
	#define eNet_INT_Pin				18
	#define eNet_INT_IOCON		(IOCON_FUNC0 | IOCON_MODE_PULLUP)
	
	#define eNet_MOSI_Port			1
	#define eNet_MOSI_Pin				24
	#define eNet_MOSI_IOCON		(IOCON_FUNC3 | IOCON_MODE_INACT)
	
	
	#define eNet_MISO_Port			1
	#define eNet_MISO_Pin				23
	#define eNet_MISO_IOCON		(IOCON_FUNC3 | IOCON_MODE_PULLUP)
	
	#define eNet_SCK_Port				1
	#define eNet_SCK_Pin				20
	#define eNet_SCK_IOCON		(IOCON_FUNC3 | IOCON_MODE_INACT)
	
	#define eNet_CS_Port				1
	#define eNet_CS_Pin					21
	#define eNet_CS_IOCON			(IOCON_FUNC3 | IOCON_MODE_PULLUP)
	//--------------------------------------------------------------------------
	
	//-----------------------DIP Swtich definitions--------------------
	#define dipSwitch1_Port			1
	#define dipSwitch1_Pin			14
	#define dipSwitch1_IOCON		(IOCON_FUNC0 | IOCON_MODE_PULLUP)
	
	#define dipSwitch2_Port			1
	#define dipSwitch2_Pin			15
	#define dipSwitch2_IOCON		(IOCON_FUNC0 | IOCON_MODE_PULLUP)
	
	#define dipSwitch3_Port			1
	#define dipSwitch3_Pin			16
	#define dipSwitch3_IOCON		(IOCON_FUNC0 | IOCON_MODE_PULLUP)
	
	#define dipSwitch4_Port			1
	#define dipSwitch4_Pin			17
	#define dipSwitch4_IOCON		(IOCON_FUNC0 | IOCON_MODE_PULLUP)
	//--------------------------------------------------------------------------
	
	//-----------------------buzzer definitions--------------------
	#define Buzzer_Port			2
	#define Buzzer_Pin			6
	#define Buzzer_IOCON		(IOCON_FUNC0 | IOCON_MODE_INACT)
	#define Buzzer_Sleep_IOCON		(IOCON_FUNC0 | IOCON_MODE_INACT)
	//--------------------------------------------------------------------------
	
	
	
	

#ifdef __cplusplus
}
#endif

#endif /* BSP_H_ */
